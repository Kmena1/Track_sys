app.service('busquedas',['$http','$cookies', function($http,$cookies){
    //funciones
    function consult(tolook,div,i,j,datos,id,k,callback){
        console.log(tolook,div,i,j,datos,id,k)
        //console.log(response.data);
        $http({
            method:'GET',
            url:'/divice/keys/'+div[j].id+'/date,time,'+String(tolook)+'/'+String(k)+'/'+String(id)
        }).
        then(function(response){
            //console.log(response.data)
            //var data=response.data.data
            datos=datos.concat(response.data.data)
            id=response.data.last;
            if(id!=null && i<4) consult(tolook,div,i+1,j,datos,id,k,callback)
            //solo actualiza el mapa si se pregunto por la latitud y longitud en el extra
            else{
                callback(datos,id)
                //$scope.dat=$scope.datos;
                //if(extras==',lat,lon' || extras==',lon,lat')$scope.mapPoints($scope.datos);
                //$scope.prom($scope.dat);
            }    
        });
    }

    function invent(empresa,callback){
        $http({
            method:'GET',
            url:'/get/inventario/'+empresa
        }).
        then(function(response){
            console.log(response.data)
            //$scope.collections = response.data.keys;
            //$scope.inventario= response.data.data;
            callback(response.data.keys,response.data.data)
        })
    }

    function catalog(empresa,callback){
        $http({
            method:'GET',
            url:'/get/catalogo/'+empresa
        }).
        then(function(response){
            console.log(response.data);
            //$scope.CatKey = response.data.keys;
            //$scope.catalogo= response.data.data;
            callback(response.data.keys,response.data.data)
        })
    }

    function reserv(id,callback){
        $http({
            method:'GET',
            url:'/get/reservas/'+id+'/100'
        }).
        then(function(response){
            console.log(response.data);
            //$scope.resvObj=response.data.data
            //$scope.NombreResrvas();
            callback(response.data.data)
        })
    }

    function GetReservas(array,callback){
        var data={
            array:array
        };
        $http({
            method:'POST',
            url:'/get/reserv/',
            data:data
        }).
        then(function(response){
            callback(response)
        })
    }

    function GetRecetas(emp,since,max,callback){
        $http({
            method:'GET',
            url:'/find/recetas/'+String(emp)+'/'+String(since)+'/'+String(max),
        }).
        then(function(response){
            console.log(response.data);
            callback(response)
        })
    }

    function client(empresa,since,i,callback){
         $http({
            method:'GET',
            url:'/clientes/get/'+empresa+'/'+since+'/'+String(i)+'/100'
        }).
        then(function(response){
            console.log(response.data);
            if (response.data.err == 0) {
                //llena el scope de respaldo
                callback(response.data)
                //$scope.clientes=$scope.clientes.concat(response.data.datos);
                //$scope.Tclients()
                //filtra
                //$scope.filter()
            } 
            else {
                
            }
        })
    }

    function allhist(emp,callback) {
        $http({
            method:'GET',
            url:'/all/historial/'+emp
        }).
        then(function(response){
            console.log(response.data);
            callback(response)
        })
    }

    function gethist(hst,keys,callback) {
        $http({
            method:'GET',
            url:'/get/historial/'+hst+'/'+keys
        }).
        then(function(response){
            console.log(response.data);
            callback(response)
        })
    }

    function AgentesClient(user,where,i,a,callback){
        $http({
            method:'GET',
            url:'/clientes/agentes/get/'+user+'/'+where+'/'+i*a+'/'+String(a)
        }).
        then(function(response){
            callback(response)
        })
    }

    function Archiv(Archiving,callback){
        var data={
            'ArchSetts':Archiving
        };
        $http({
            method:'POST',
            url:'/find/arch',
            data:data
        }).
        then(function(response){
            callback(response)
        })
    }

    function buscarUsuario(user,callback){
        $http({
            method:'GET',
            url:'/find/user/'+String(user)
        }).
        then(function(response){
            callback(response)
        })
    }

    function GetPedidos(empresa,Lastid,callback){
        $http({
            method:'GET',
            url:'/get/pedidos/'+empresa+'/'+Lastid+'/100'
        }).
        then(function(response){
            callback(response)
        })
    }

    function Mensaj(user,max,tipo,callback) {
        $http({
            method:'GET',
            url:'/get/mensaje/'+String(user)+'/'+String(max)+'/'+String(tipo)
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function Transp(emp,callback){
        $http({
            method:'GET',
            url:'/empresa/transportes/'+String(emp)
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function empresa(emp,callback){
        $http({
            method:'GET',
            url:'/find/company/'+String(emp)
        }).
        then(function(response){
            console.log(response.data);
            callback(response)
        })
    }

    function currency(callback) {
        $http({
            method:'GET',
            url:'http://free.currencyconverterapi.com/api/v5/convert?q=USD_CRC&compact=y'
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data.USD_CRC)
        })
    }
    //headers
    this.getCurrency=function(callback){
        currency(callback)
    }

    this.usuarios=function(user,callback){
        buscarUsuario(user,callback)
    }

    this.clientes=function(empresa,since,i,callback){
        client(empresa,since,i,callback);
    }

    this.AgentesClientes=function(user,where,i,a,callback){
        AgentesClient(user,where,i,a,callback)
    }

    this.reservas=function(id,callback){
        reserv(id,callback);
    }

    this.catalogo=function(empresa,callback){
        catalog(empresa,callback)
    }

    this.inventario=function(empresa,callback){
        invent(empresa,callback)
    }

    this.recetas=function(emp,since,max,callback){
        GetRecetas(emp,since,max,callback)
    }

    this.consultas=function(tolook,div,i,j,datos,id,callback) {
        consult(tolook,div,i,j,datos,id,500,callback)
    }

    this.consultaslast=function(tolook,div,i,j,datos,id,callback) {
        console.log(tolook,div,i,j,datos,id)
        consult(tolook,div,i,j,datos,id,2,callback)
    }

    this.divices=function(user,callback){
        if(user != undefined){
            $http({
                method:'GET',
                url:'/divice/of/'+user
            }).
            then(function(response){
                callback(response.data)
            })
        }
    }

    this.imei=function(imei,callback){ 
        $http({
            method:'GET',
            url:'/get/imei/'+imei
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    this.dispositivo=function(dispositivo,callback){
        $http({
            method:'GET',
            url:'/find/div/'+dispositivo
        }).
        then(function(response){
            callback(response.data)
        })
    }

    this.alertas=function(alerta,callback){
        $http({
            method:'GET',
            url:'/divice/alerts/'+alerta+'/100'
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data.data);
        }) 
    }

    this.Archiving = function(Archiving,callback){
        Archiv(Archiving,callback);
    }

    //buscar posiciones en el mapa
    this.searchPos=function(centpos,limit,callback){
        if(centpos != undefined && centpos != ''){
            $http({
                method:'GET',
                url:'http://nominatim.openstreetmap.org/search?format=json&limit='+String(limit)+'&q='+centpos
            }).
            then(function(response){
                callback(response);
            })
        }
    }

    this.mypos=function(callback){
        $http({
            method:'GET',
            url:'https://ipinfo.io/geo'
        }).
        then(function(response){
            callback(response.data);
        })
    }

    this.reverserGeoJson=function(lat,lng,z,callback){
        $http({
            method:'GET',
            url:'https://nominatim.openstreetmap.org/reverse?format=json&lat='+lat+'&lon='+lng+'&zoom='+z
        }).
        then(function(response){
            callback(response)
        })
    }

    this.estrcuturas = function(callback){
        $http({
            method:'GET',
            url:'/struc/all'
        }).
        then(function(response){
            callback(response)
        })
    }

    this.arrayReservas=function(array,callback){
        GetReservas(array,callback)
    }

    this.pedidos=function(empresa,Lastid,callback){
        GetPedidos(empresa,Lastid,callback)
    }

    //busca los mensajes nuevos, solo 10
    this.initmensajes=function(user,callback){
        Mensaj(user,10,0,callback)
    }

    //busca todos los mensajes
    this.mensajes=function(user,callback){
        var RetMensajes={
            nuevos:{},
            leidos:{},
            enviados:{}
        }
        Mensaj(user,100,0,function(nuevos){
            RetMensajes.nuevos=nuevos;
            Mensaj(user,100,1,function(leidos){
                RetMensajes.leidos=leidos;
                Mensaj(user,100,2,function(enviados){
                    RetMensajes.enviados=enviados;
                    callback(RetMensajes);
                })
            })
        })
    }

    this.transporte = function(emp,callback){
        Transp(emp,callback)   
    }

    this.company=function(emp,callback){
        empresa(emp,callback)
    }
    //dispositivo del que se quieren obtener el historial
    this.allhistorial=function(disp,callback){
        allhist(disp,callback)
    }
    this.gethistorial=function(hst,keys,callback){
        gethist(hst,keys,callback)
    }

    //busca datos de la empresa y empleados ademas de los 10 primeros mensajes no leidos
    this.init=function(callback){
            //obtener empleados
        $http({
            method:'GET',
            url:'/find/empresa/'+String($cookies.get('empresa'))
        }).
        then(function(response){
            console.log('empresa y usuarios',response.data);
            //$scope.users= response.data.datos;
            //$scope.empresa= response.data.extra;
            Mensaj($cookies.get('id'),10,0,function(mensajes){
                callback(response.data.datos,response.data.extra,mensajes.data)
            })
        });
    };

    this.ordenes=function(callback){
        $http({
            method:'GET',
            url:'/get/ordenes/'+$cookies.get('empresa')
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    this.clientesArray=function(clientes,callback){
        var data={
            clientes:clientes
        };
        $http({
            method:'POST',
            url:'/clientes/array/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    this.productosArray=function(productos,callback){
        var data={
            productos:productos
        };
        $http({
            method:'POST',
            url:'/productos/array/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }
}])