app.service('crear',['$http','$cookies', function($http,$cookies){
    //funciones
    function crearuser(nombre,email,cedula,telefono,nivel,user,password,empresa,callback){
        data={
            'nombre':nombre,
			'email':email,
			'cedula':cedula,
			'numero_tel':telefono,
			'tipo': nivel,
            'user'		: user,
            'password'	: password,
            'empresa'   : empresa
        }
        console.log(data);
        $http({
            method: 'POST',
            url: 'http://34.237.241.0:2500/crear/usuario/',
            data: data
        }).
        then(function(response){
            console.log(response.data)
            callback(response)
            
        })
    }

    function creardispositivo(unidad,name,gps,odo,gas,km,temp,ibt,panic,vs,dall,ROnf,Acc,Canb,voz,others,tipo,model,callback){
        var data={
            'unid':unidad,
            'name':name,
            'gps':gps,
            'odo':odo,
            'gas':gas,
            'km':km,
            'temp':temp,
            'ibt':ibt,
            'panic':panic,
            'vs':vs,
            'dall':dall,
            'ROnf':ROnf,
            'Acc':Acc,
            'Canb':Canb,
            'voz':voz,
            'others':others,
            'header':{
                'tipo':tipo,
                'model_ID':model
            },
        };
        $http({
            method:'POST',
            url:'/new/device',
            data:data
        }).
        then(function(response){
            callback(response)
        })
    }

    function crearcln(Cnombre,lat,lng,region,empresa,callback){
        var cliente={
            nombre:Cnombre,
            lat:lat,
            lon:lng,
            region:region
        }
        //data a enviar
        var data={
            id:empresa,
            datos:JSON.stringify(cliente)
        };
        console.log(data);
        $http({
            method:'POST',
            url:'/clientes/set',
            data:data
        }).
        then(function(response){
            callback(response)
        })
    }

    function Setarchiving(id,rule,propiedad,tolerancia,modulo,ref,operator,master,callback){
        if(id != undefined && operator!=undefined && tolerancia!=undefined  && propiedad!=undefined){
            if(ref==undefined)var reference=null
            else reference=ref
            if(rule=="radio"){
                var property="radius";
            }
            else{
                var property=propiedad
            }
            var data={
                deviceId:id,
                settings:{
                    rule		 :rule,
                    property	 :property,
                    tolerance	 :tolerancia,
                    module		 :modulo,
                    reference    :{
                        ref1:reference,
                    },
                    operator     :operator,
                    master       :master
                }
            };
            $http({
                method:'POST',
                url:'http://34.237.241.0:2500/divice/set/achieve',
                data:data
            }).
            then(function(response){
                callback(response)
            })
        }
    }

    function Nuevomensaje(sender,reciver,asunto,mensj,callback) {
        console.log(sender,reciver,asunto,mensj);
        var data={
            sender:sender,
            reciver:reciver,
            asunto:asunto,
            mensj:mensj
        };
        $http({
            method:'POST',
            url:'/nuevo/mensaje',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function NuevoTransporte(emp,transporte,callback){
        var data={
            emp:emp,
            transportes:transporte
        };
        $http({
            method:'POST',
            url:'/empresa/nuevoTransporte',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function NuevaCategoria(emp,cat,tipo,callback) {
        if(tipo==null)tipo='PT'
        var data={
            empresa:emp,
            cat:cat,
            tipo:tipo
        };
        $http({
            method:'POST',
            url:'/nuevo/categoria',
            data:data
        }).
        then(function(response){
            callback(response)
        })
    }

    function crearObjeto(emp,colleccion,nombre,precio,cantidad,codigo,exentos,descripcion,Incatalogo,unidad,Dolares,contenidoNeto,convertFactor,min,max,contenidoNeto,convertFactor,callback){
        var data={
            empresa:emp,
            cat:colleccion,
            Obj:nombre,
            precio:precio,
            cantidad:cantidad,
            codigo:codigo,
            exentos: exentos,
            descripcion : descripcion,
            Incatalogo: Incatalogo,
            unidad:unidad,
            Dolares:Dolares,
            contenidoNeto:contenidoNeto,
            convertFactor:convertFactor,
            min:min,
            max:max,
            contenidoNeto:contenidoNeto,
            convertFactor:convertFactor
        };
        $http({
            method:'POST',
            url:'/nuevo/objetos',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response)
        })
    }

    //objeto:objeto_id
    //descripcion:descripcion del ojeto
    //empresa:_id de la empresa
    function crearcatalog(empresa,description,objeto,catalogo,exento,formData,callback){
        if(catalogo==null){
            $http({
                method:'POST',
                url:'/empresa/imagenes/catalogo?username='+String(empresa)+'&descripcion='+description+'&name='+objeto+'&exento='+String(exento),
                data:formData,
                headers:{
                    "Content-type": undefined
                }, 
                transformRequest: angular.identity
            }).
            then(function(response){
                console.log(response.data);
                callback(response.data)
            })
        }
        else{
            $http({
                method:'POST',
                url:'/empresa/editar/catalogo?username='+String(empresa)+'&descripcion='+description+'&name='+objeto+'&catalogo='+catalogo+'&exento='+String(exento),
                data:formData,
                headers:{
                    "Content-type": undefined
                }, 
                transformRequest: angular.identity
            }).
            then(function(response){
                console.log(response.data);
                callback(response.data)
            })
        }
    }

    function uploadimage(empresa,objeto,formData,callback){
        
            $http({
                method:'POST',
                url:'/empresa/imagenes/inventario?username='+String(empresa)+'&name='+objeto,
                data:formData,
                headers:{
                    "Content-type": undefined
                }, 
                transformRequest: angular.identity
            }).
            then(function(response){
                console.log(response.data);
                callback(response.data)
            })
    }

    function uploadFile(empresa,nombre,formData,callback){
        
            $http({
                method:'POST',
                url:'/empresa/clientes/excel?username='+String(empresa)+'&name='+nombre,
                data:formData,
                headers:{
                    "Content-type": undefined
                }, 
                transformRequest: angular.identity
            }).
            then(function(response){
                console.log(response.data);
                callback(response.data)
            })
    }

    function uploadParseFile(empresa,nombre,formData,callback){
        $http({
            method:'POST',
            url:'/upload/parser/page?username='+String(empresa)+'&name='+nombre,
            data:formData,
            headers:{
                "Content-type": undefined
            }, 
            transformRequest: angular.identity
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function Makepedido(emp,agente,reservas,contado,abono,transporte,total,cliente,resposable,callback) {
        var data={
            empresa:emp,
            agente:agente,
            reservas:reservas,
            contado:contado,
            abono:abono,
            cliente:cliente,
            transporte:transporte,
            total:total,
            resposable:resposable
        };
        $http({
            method:'POST',
            url:'/make/pedido',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response);
        })
    }

    function NewReceta(emp,nombre,codigo,ingredientes,receta,objeto,tipo,callback){
        var data={
            nombre:nombre,
            codigo:codigo,
            ingredientes:ingredientes,
            id:emp,
            receta:receta,
            objeto:objeto,
            tipo:tipo
        };
        $http({
            method:'POST',
            url:'/add/recetas/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response)
        })
    }

    function Sendemail(from,to,subject,text,callback){
        var data={
            from:from,
            to:to,
            subject:subject,
            text:text,
        };
        $http({
            method:'POST',
            url:'/email/enviar/',
            data:data
        }).
        then(function(response){
            console.log(response);
            callback(response)
        })
    }

    //headers
    this.usuario =function(nombre,email,cedula,telefono,nivel,user,password,empresa,callback){
        crearuser(nombre,email,cedula,telefono,nivel,user,password,empresa,callback)
    }

    this.cliente = function(Cnombre,lat,lng,region,empresa,callback){
        crearcln(Cnombre,lat,lng,region,empresa,callback)
    }

    this.archiving= function(id,rule,propiedad,tolerancia,modulo,ref,operator,master,callback){
        Setarchiving(id,rule,propiedad,tolerancia,modulo,ref,operator,master,callback)
    }

    this.dispositivo=function(unidad,name,gps,odo,gas,km,temp,ibt,panic,vs,dall,ROnf,Acc,Canb,voz,others,tipo,model,callback){
        creardispositivo(unidad,name,gps,odo,gas,km,temp,ibt,panic,vs,dall,ROnf,Acc,Canb,voz,others,tipo,model,callback)
    }

    this.mensaje=function(sender,reciver,asunto,mensj,callback){
        console.log(sender,reciver,asunto,mensj)
        Nuevomensaje(sender,reciver,asunto,mensj,callback)
    }
    this.transporte=function(emp,transporte,callback){
        NuevoTransporte(emp,transporte,callback)
    }

    this.categoria=function(emp,cat,tipo,callback){
        NuevaCategoria(emp,cat,tipo,callback)
    }

    this.objeto=function(emp,colleccion,nombre,precio,cantidad,codigo,exentos,descripcion,Incatalogo,unidad,Dolares,contenidoNeto,convertFactor,min,max,contenidoNeto,convertFactor,callback){
        crearObjeto(emp,colleccion,nombre,precio,cantidad,codigo,exentos,descripcion,Incatalogo,unidad,Dolares,contenidoNeto,convertFactor,min,max,contenidoNeto,convertFactor,callback)
    }

    this.catalogo=function(empresa,description,objeto,catalogo,exento,formData,callback){
        crearcatalog(empresa,description,objeto,catalogo,exento,formData,callback)
    }

    this.Image=function(empresa,objeto,formData,callback){
        uploadimage(empresa,objeto,formData,callback)
    }

    this.archivo=function(empresa,nombre,formData,callback){
        uploadFile(empresa,nombre,formData,callback)
    }

    this.archivoParse=function(empresa,nombre,formData,callback){
        uploadParseFile(empresa,nombre,formData,callback)
    }

    this.pedido=function(emp,agente,reservas,contado,abono,transporte,total,cliente,resposable,callback){
        Makepedido(emp,agente,reservas,contado,abono,transporte,total,cliente,resposable,callback)
    }

    this.receta=function(emp,nombre,codigo,ingredientes,receta,objeto,tipo,callback){
        NewReceta(emp,nombre,codigo,ingredientes,receta,objeto,tipo,callback)
    }

    this.email=function(from,to,subject,text,callback){
        Sendemail(from,to,subject,text,callback)
    }
}])