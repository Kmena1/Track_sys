app.service('editar',['$http','$cookies', function($http,$cookies){
    //funciones
    function editarUser(id,nombre,email,cedula,numero_tel,tipo,user,password,callback){
        var data={
            id:id,
            nombre:nombre,
            email:email,
            cedula:cedula,
            numero_tel:numero_tel,
            tipo:tipo,
            user:user,
            password:password,
        };
        $http({
            method:'POST',
            url:'/edit/usuario/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function editarEmpresa(emp,name,ced,phone,email,Cname,Cid,Cphone,Cemail,callback){
        var data={
            empresa:emp,
            Name:name,
            Ced_J:ced,
            phone:phone,
            email:email,
            Cont_Name:Cname,
            Cont_ID:Cid,
            Cont_phone:Cphone,
            Cont_email:Cemail,
        };
        $http({
            method:'POST',
            url:'/edit/empresa/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response)
        })
    }

    function editarcliente(id,Cnombre,lat,lng,region,empresa,callback){
        var cliente={
            nombre:Cnombre,
            lat:lat,
            lon:lng,
            region:region
        }
        //datos a enviar
        var data={
            id:id,
            datos:cliente,
            empresa:empresa
        };
        console.log(data);
        $http({
            method:'POST',
            url:'/clientes/editar',
            data:data
        }).
        then(function(response){
            callback(response)
        })
    }

    function asignarCliente(id,clnt,model,callback){
        var data={
            usuario:id,
            cliente:clnt,
            state:model
        };
        console.log(data);
        $http({
            method:'POST',
            url:'/agentes/asignar',
            data:data
        }).
        then(function(response){
            callback(response);
        })
    }

    function ElimiarCliente(id,callback){
        var data={
            cliente:id._id,
            region:id.region,
            id:$cookies.get('empresa')
        };
        $http({
            method:'POST',
            url:'/cliente/delete',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response)
        })
    }

    function asignarDispositivo(user,id,callback){
        var data={
            'user':user,
            'divice':id
        };
        $http({
            method:'POST',
            url:'/assignar/divice',
            data:data
        }).
        then(function(response){
            callback(response)
        })
    }

    function elimiarReservas(objeto,reserva,pedido,callback){
        var data={
            objeto:objeto,
            reserva:reserva,
            pedido:pedido,
        };
        $http({
            method:'POST',
            url:'/reserva/cancelar',
            data:data
        }).
        then(function(response){
            callback(response)
        })
    }

    function eliminarPedido(empresa,objeto,reserva,pedido,callback){
        var data={
            empresa:empresa,
            objeto:objeto,
            reserva:reserva,
            pedido:pedido
        };
        console.log(data);
        $http({
            method:'POST',
            url:'/reserva/cancelar/',
            data:data
        }).
        then(function(response){
            callback(response)
        })
    }

    function MensajeLeido(mensaje,user,callback){
        var data={
            mensaje:mensaje,
            user:user
        };
        $http({
            method:'POST',
            url:'/Mensaje/leido/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function RemoveTransp(emp,transporte,callback){
        var data={
            emp:emp,
            transportes:transporte
        };
        $http({
            method:'POST',
            url:'/empresa/eliminar/Transporte',
            data:data
        }).
        then(function(response){
            console.log(response);
            callback(response)
        })
    }

    //inverntario
    function Editcatalog(empresa,description,objeto,catalogo,exento,formData,callback){
        $http({
            method:'POST',
            url:'/empresa/editar/catalogo?username='+String(empresa)+'&descripcion='+description+'&name='+objeto+'&catalogo='+catalogo+'&exento='+String(exento),
            data:formData,
            headers:{
                "Content-type": undefined
            }, 
            transformRequest: angular.identity
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function editarObjeto(objeto,precio,cantidad,nombre,codigo,exentos,descripcion,Incatalogo,unidad,Dolares,contenidoNeto,convertFactor,puntos,user,callback){
        var data={
            objeto:objeto,
            precio:precio,
            cantidad:cantidad,
            nombre:nombre,
            codigo:codigo,
            exentos: exentos,
            descripcion : descripcion,
            Incatalogo: Incatalogo,
            unidad:unidad,
            Dolares:Dolares,
            contenidoNeto:contenidoNeto,
            convertFactor:convertFactor,
            puntos:puntos,
            user:user
        };
        $http({
            method:'POST',
            url:'/editar/objeto',
            data:data
        }).
        then(function(response){
            //console.log(response.data);
            callback(response)
        })
    }

    function reservarObjeto(carrito,callback){
        var data=carrito;
        $http({
            method:'POST',
            url:'/reservar/objeto',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response)
        })
    }

    function ChangeEstadoOrd(tipo,estado,orden,callback){
        $http({
            method:'GET',
            url:'/orden/estado/'+String(orden)+'/'+String(tipo)+'/'+String(estado)
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function AutOrdenes(ord,usr,tipo,callback){
        var data={
            tipo:tipo,
            id:ord,
            usr:usr
        };
        $http({
            method:'POST',
            url:'/ordenes/aprovar/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    function EditStandBy(objeto,add,value,callback){
        var data={
            objeto:objeto,
            add:add,
            value:value,
        };
        $http({
            method:'POST',
            url:'/change/standBy/',
            data:data
        }).
        then(function(response){
            callback(response);
        })
    }

    this.User=function(id,nombre,email,cedula,numero_tel,tipo,user,password,callback){
        editarUser(id,nombre,email,cedula,numero_tel,tipo,user,password,callback)
    }

    this.cliente= function(id,Cnombre,lat,lng,region,empresa,callback){
        editarcliente(id,Cnombre,lat,lng,region,empresa,callback)
    }

    this.AsgCliente= function(id,clnt,model,callback){
        asignarCliente(id,clnt,model,callback)
    }

    this.deletecliente=function(id,callback){
        ElimiarCliente(id,callback);
    }

    this.AsgDispositivo =function(user,id,callback){
        asignarDispositivo(user,id,callback)
    }

    this.DeleteReservas=function(objeto,reserva,pedido,callback){
        elimiarReservas(objeto,reserva,pedido)
    }

    this.DeletePedido = function(empresa,objeto,reserva,pedido,callback){
        eliminarPedido(empresa,objeto,reserva,pedido,callback)
    }

    this.aceptarpedido=function(id,pedido,reservas,precio,callback){
        var data={
            empresa:$cookies.get('empresa'),
            id:id,
            pedido:pedido,
            reservas:reservas,
            total:precio
        };
        $http({
            method:'POST',
            url:'/aceptar/pedido/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    this.leido=function(mensaje,user,callback){
        MensajeLeido(mensaje,user,callback)
    }

    this.deleteTransporte=function(emp,transporte,callback){
        RemoveTransp(emp,transporte,callback)
    }

    this.catalogo=function(empresa,description,objeto,catalogo,exento,formData,callback){
        Editcatalog(empresa,description,objeto,catalogo,exento,formData,callback)
    }

    this.objeto=function(objeto,precio,cantidad,nombre,codigo,exentos,descripcion,Incatalogo,unidad,Dolares,contenidoNeto,convertFactor,puntos,user,callback){
        editarObjeto(objeto,precio,cantidad,nombre,codigo,exentos,descripcion,Incatalogo,unidad,Dolares,contenidoNeto,convertFactor,puntos,user,callback)
    }

    this.reserva=function(carrito,callback){
        reservarObjeto(carrito,callback)
    }

    this.empresa=function(emp,name,ced,phone,email,Cname,Cid,Cphone,Cemail,callback){
        editarEmpresa(emp,name,ced,phone,email,Cname,Cid,Cphone,Cemail,callback)
    }

    this.ordenesEstado=function(tipo,estado,orden,callback){
        ChangeEstadoOrd(tipo,estado,orden,callback)
    }

    this.AutorizarOrden=function (ord,usr,tipo,callback){
        AutOrdenes(ord,usr,tipo,callback)
    }

    this.minmax = function(objeto,min,max,callback){
        $http({
            method:'GET',
            url:'/trigger/'+String(objeto)+'/'+String(min)+'/'+String(max)
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    this.editVhc = function(id,name,unid,callback){
        var data={
            id:id,
            unid:unid,
            name:name
        };
        $http({
            method:'POST',
            url:'/edit/divice/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            callback(response.data)
        })
    }

    this.ChangeSetArray = function(ArrayChang,callback){
        for (let i = 0; i < ArrayChang.length; i++) {
            const producto = ArrayChang[i];
            EditStandBy(ArrayChang.producto,ArrayChang.cantidad,true,function(response){
                console.log(response)
            })
        }
        callback()
    }
}])