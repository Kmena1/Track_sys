app.constant('links',{
    links: [
        {
            id:'',
            img:'https://image.flaticon.com/icons/png/512/660/660611.png',
            menu:[
                {
                    'nombre':'Inicio',
                    'link':'/empresa/index.html',
                }
            ]
        },
        {
            id:'',
            img:'https://image.flaticon.com/icons/svg/1244/1244688.svg',
            menu:[
                {
                    'nombre':'Mapas',
                    'link':'/empresa/assets/htmls/clientes.html',
                },
                {
                    'nombre':'Clientes',
                    'link':'/empresa/assets/htmls/clientes_edit.html',
                },
                {
                    'nombre':'Rutas',
                    'link':'/empresa/assets/htmls/Zonas.html',
                },
            ]
        },
        {
            id:'',
            img:'https://image.flaticon.com/icons/svg/265/265729.svg',
            menu:[
                {
                    'nombre':'Inventario',
                    'link':'/empresa/assets/htmls/inventario.html',
                },
                {
                    'nombre':'Formulas',
                    'link':'/empresa/assets/htmls/Recetas.html',
                }
            ]
        },
        {
            id:'',
            img:'https://image.flaticon.com/icons/svg/138/138280.svg',
            menu:[
                {
                    'nombre':'Catalogo',
                    'link':'/empresa/assets/htmls/catalogo.html',
                },
                {
                    'nombre':'Calculo costos',
                    'link':'/empresa/assets/htmls/Costos.html',
                },
            ]
        },
        {
            id:'',
            img:'https://image.flaticon.com/icons/svg/1524/1524855.svg',
            menu:[
                {
                    'nombre':'Pedidos',
                    'link':'/empresa/assets/htmls/facturas.html',
                },
                {
                    'nombre':'Ordenes',
                    'link':'/empresa/assets/htmls/Ordenes.html',
                },
                {
                    'nombre':'Estado de ordenes',
                    'link':'/empresa/assets/htmls/Produccion.html',
                },
            ]
        },
        {
            id:'bottom',
            img:'https://image.flaticon.com/icons/svg/148/148912.svg',
            menu:[
                {
                    'nombre':'Configuracion',
                    'link':'/empresa/assets/htmls/editar.html',
                },
                /*{
                    'nombre':'Mensajes',
                    'link':'/empresa/assets/htmls/mensajes.html',
                },*/
            ]
        }
    ],

    /*links: [
        {icon:'https://image.flaticon.com/icons/svg/126/126486.svg',text:'ver agentes',link:'/empresa/index.html'},
        {icon:'https://image.flaticon.com/icons/svg/126/126486.svg',text:'Clientes',link:'/empresa/assets/htmls/clientes.html'},
        {icon:'https://image.flaticon.com/icons/svg/149/149346.svg',text:'Reportes',link:'/empresa/assets/htmls/reportes.html'},
        {icon:'https://image.flaticon.com/icons/svg/149/149393.svg',text:'Inventario',link:'/empresa/assets/htmls/inventario.html'},
        {icon:'https://image.flaticon.com/icons/svg/215/215517.svg',text:'Pedidos',link:'/empresa/assets/htmls/facturas.html'},
        {icon:'https://image.flaticon.com/icons/svg/1042/1042456.svg',text:'Catalago',link:'/empresa/assets/htmls/catalogo.html'},
        {icon:'https://image.flaticon.com/icons/svg/131/131154.svg',text:'Mensajes',link:'/empresa/assets/htmls/mensajes.html'},
        {icon:'https://image.flaticon.com/icons/svg/114/114997.svg',text:'Recetas',link:'/empresa/assets/htmls/Recetas.html'},
        {icon:'https://image.flaticon.com/icons/svg/1187/1187584.svg',text:'ordenes',link:'/empresa/assets/htmls/Ordenes.html'}
    ],*/

    Fgoto:function(windows,i,where){
        switch (i) {
            case 0:
                window.open('/empresa/index.html','_self')
                break;
            case 1:
                window.open('/empresa/assets/htmls/editar.html','_self')
                break;
            case 2:
                window.open(where,'_self')
                break
            case 3:
                window.open('/empresa/assets/htmls/mensajes.html','_self')
                break
            default:
                break;
        }
    }
})