var comandosST300=[
    {
        'nombre':'activar salida 1',
        'comand':';02;Enable1',
        'explicacion':'Activar la salida 1 del dispositivo'
    },
    {
        'nombre':'activar salida 2',
        'comand':';02;Enable2',
        'explicacion':'Activar la salida 2 del dispositivo'
    },

    {
        'nombre':'desactivar salida 1',
        'comand':';02;Disable1',
        'explicacion':'Apagar la salida 1 del dispositivo'
    },
    {
        'nombre':'desactivar salida 2',
        'comand':';02;Disable2',
        'explicacion':'Apagar la salida 2 del dispositivo'
    },
    {
        'nombre':'Solicitar RPM',
        'comand':';02;ReqRPMU',
        'explicacion':'Solicita el ultimo valor de las revoluciones del dispositivo'
    },
    {
        'nombre':'Solicitar odometro',
        'comand':';02;ReqOdoU',
        'explicacion':'Solicita el valor del odometro'
    },

]

var comandosST500=[
    {
        'nombre':'Solictar OBDs',
        'comand':';03;ReqOBDinfDTC',
        'explicacion':'Activar la salida 1 del dispositivo'
    },
    {
        'nombre':'OBDs disponibles',
        'comand':';03;ReqOBDAvailPID',
        'explicacion':'Activar la salida 2 del dispositivo'
    },

]

app.constant('dispositivos',{
    dispositivos:{
        'ST910':{
            '02':{
                'descripcion':"Lorem ipsum dolor sit amet consectetur adipiscing elit auctor et imperdiet, senectus praesent netus tristique proin etiam sodales feugiat rhoncus, id varius eget luctus eu nulla sociis cubilia elementum. Sollicitudin feugiat donec consequat varius a scelerisque mattis class vivamus ultricies accumsan ornare neque sagittis, tempus commodo iaculis natoque erat tortor rutrum hendrerit enim molestie porta morbi. Fringilla curae fames dapibus nec lobortis consequat ac lectus, eros nisi sociis quis pellentesque hendrerit fusce donec rutrum, conubia mauris purus dignissim fermentum nostra hac.",
                'comandos':[
                ]
            }
        },
        'ST300H':{
            '04':{
                'descripcion':"Dispositivo de localizacion marca Suntech",
                'comandos':comandosST300
            }
        },
        'ST500':{
            '07':{
                'descripcion':"Dispositivo de localizacion marca Suntech",
                'comandos':comandosST500
            }
        }
    }
})