app.controller('crear',['$scope','$http','$cookies','$window',function($scope,$http,$cookies,$window){
    console.log($cookies.getAll());
    $scope.links = [
        {icon:'https://image.flaticon.com/icons/svg/126/126486.svg',text:'Empresa',link:'/root/assets/htmls/empresa.html'},
        {icon:'https://image.flaticon.com/icons/svg/126/126486.svg',text:'Usuario',link:'/root/assets/htmls/usuarios.html'},
        {icon:'https://image.flaticon.com/icons/svg/149/149346.svg',text:'Trackers',link:'/root/assets/htmls/trakcs.html'},
    ]

    $scope.goto =function(i,where){
        switch (i) {
            case 0:
                $window.open('/root/index.html','_self')
                break;
            case 1:
                $window.open('/root/assets/htmls/trakcs.html','_self')
                break;
            case 2:
                $window.open(where,'_self')
                break
            default:
                break;
        }
    }
    
    $scope.crearUser = function(nivel,empresa){
        data={
            'nombre':$scope.nombre,
			'email':$scope.email,
			'cedula':$scope.cedula,
			'numero_tel':$scope.telefono,
			'tipo': nivel,
            'user'		: $scope.user,
            'password'	: $scope.password,
            'empresa'   : empresa
        }
        console.log(data);
        $http({
            method: 'POST',
            url: 'http://34.237.241.0:2500/crear/usuario/',
            data: data
        }).
        then(function(response){
            console.log(response.data)
            
        })
    }

    $scope.crear =function(){
        var data={
            'Name':$scope.Compania,
			'Ced_J':$scope.CedJ,
			'phone'       : $scope.phone,
			'email'       : $scope.correo,
			'datei'   :   $scope.datei,
            'Cont_Name': $scope.nombre, 
            'Cont_ID' :  $scope.cedula,
            'Cont_phone': $scope.telefono,
            'Cont_email': $scope.email,
        };
        console.log(data);
        $http({
            method: 'POST',
            url: 'http://34.237.241.0:2500/crear/empresa/',
            data: data
        }).then(function(response){
            console.log(response)
            $scope.crearUser(1,response.data.empresa)
        })
    }

    $scope.init =function(){
        $http({
            method:'GET',
            url:'/undivices/users'
        }).
        then(function(response){
            console.log(response.data);
            $scope.undUsr=response.data;
        })
        $http({
            method:'GET',
            url:'/struc/all'
        }).
        then(function(response){
            console.log(response.data);
            info=response.data;
            $scope.trackers=info;
        })
    }
    $scope.init();

    $scope.asg=function(id,Asgdiv){
        console.log(id,JSON.parse(Asgdiv))
        var data={
            "header": 
                {
                    "tipo":JSON.parse(Asgdiv).tipo,
                    "model_ID":JSON.parse(Asgdiv).model_ID
                }
        };
        console.log(data)
        $http({
            method:'POST',
            url:'/new/device',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            var data={
                user:id,
                divice:response.data._id
            };
            $http({
                method:'POST',
                url:'/assignar/divice',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.init();
            })
            
        })
        
    }
}])