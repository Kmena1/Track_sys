var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('alertas',['$scope','$http','$cookies','$window','$timeout','leafletData','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,$timeout,leafletData,crear,busquedas,editar,links){
    //arreglos
    $scope.divices=[]
    $scope.markers=[];
    $scope.center= {
        lat: 9.8755948,
        lng: -84.0380218,
        zoom: 10
    }

    $scope.resetAlertas=function(){
        $scope.alertas=[
            //overspeed
            {
                nombre:'Sobre velocidad',
                alertas:[]
            },
            //geofenceout
            {
                nombre:'Geo-cerca',
                alertas:[]
            },
            //batteryError batteryDown
            {
                nombre:'bateria',
                alertas:[]
            },
            //schocked
            {
                nombre:'coliciones',
                alertas:[]
            },
            //gas
            {
                nombre:'Gasolina',
                alertas:[]
            },
            //rutaDefinidain rutaDefinidaout
            {
                nombre:'Ruta',
                alertas:[]
            },
            //Engine
            {
                nombre:'Motor',
                alertas:[]
            },
            //DPA
            {
                nombre:'DPA',
                alertas:[]
            },
            //stop
            {
                nombre:'Tiempo inmóvil',
                alertas:[]
            },
            //Emergency
            {
                nombre:'Emergencias',
                alertas:[]
            },
            //RPM
            {
                nombre:'Revoluciones',
                alertas:[]
            },

        ]
    }
    
    $scope.resetAlertas()

    //links
    $scope.menu=[]
    $scope.links=links.links

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    $scope.goto=function(link){
        $window.open(link,'_self')
    }

    //funciones

    $scope.alertasArray=function(){
        var alt=$scope.dispositivo.Alt
        //sobrevelocidad
        $scope.alertas[0].alertas=$scope.alertas[0].alertas.concat(alt.overspeed)
        $scope.alertas[0].alertas=$scope.alertas[0].alertas.concat(alt.overspeedFinish)
        //geocerca
        $scope.alertas[1].alertas=$scope.alertas[1].alertas.concat(alt.geofencein)
        $scope.alertas[1].alertas=$scope.alertas[1].alertas.concat(alt.geofenceout)
        //bateria
        $scope.alertas[2].alertas=$scope.alertas[2].alertas.concat(alt.batteryDown)
        $scope.alertas[2].alertas=$scope.alertas[2].alertas.concat(alt.batteryError)
        //shocked
        $scope.alertas[3].alertas=$scope.alertas[3].alertas.concat(alt.schocked)
        //gas
        $scope.alertas[4].alertas=$scope.alertas[4].alertas.concat(alt.gas)
        //ruta
        $scope.alertas[5].alertas=$scope.alertas[5].alertas.concat(alt.rutaDefinidain)
        $scope.alertas[5].alertas=$scope.alertas[5].alertas.concat(alt.rutaDefinidaout)
        //Engine
        $scope.alertas[6].alertas=$scope.alertas[6].alertas.concat(alt.Engine.Excced_Speed)
        $scope.alertas[6].alertas=$scope.alertas[6].alertas.concat(alt.Engine.Violation_Speed)
        $scope.alertas[6].alertas=$scope.alertas[6].alertas.concat(alt.Engine.Coolant)
        $scope.alertas[6].alertas=$scope.alertas[6].alertas.concat(alt.Engine.oil)
        $scope.alertas[6].alertas=$scope.alertas[6].alertas.concat(alt.Engine.RPM)
        $scope.alertas[6].alertas=$scope.alertas[6].alertas.concat(alt.Engine.Break)
        $scope.alertas[6].alertas=$scope.alertas[6].alertas.concat(alt.Engine.DTC)
        $scope.alertas[6].alertas=$scope.alertas[6].alertas.concat(alt.Engine.Service)
        //DPA
        $scope.alertas[7].alertas=$scope.alertas[7].alertas.concat(alt.DPA.fast_acelation)
        $scope.alertas[7].alertas=$scope.alertas[7].alertas.concat(alt.DPA.fast_acelation2)
        $scope.alertas[7].alertas=$scope.alertas[7].alertas.concat(alt.DPA.sharp_turn)
        $scope.alertas[7].alertas=$scope.alertas[7].alertas.concat(alt.DPA.overspeed)
        //detenido
        $scope.alertas[8].alertas=$scope.alertas[8].alertas.concat(alt.stop.on_Ndrive)
        $scope.alertas[8].alertas=$scope.alertas[8].alertas.concat(alt.stop.Stoped)
        $scope.alertas[8].alertas=$scope.alertas[8].alertas.concat(alt.stop.stop_ignitOn)
        $scope.alertas[8].alertas=$scope.alertas[8].alertas.concat(alt.stop.moving)
        //emergencia
        $scope.alertas[9].alertas=$scope.alertas[9].alertas.concat(alt.Emergency.panic)
        $scope.alertas[9].alertas=$scope.alertas[9].alertas.concat(alt.Emergency.parking_lock)
        $scope.alertas[9].alertas=$scope.alertas[9].alertas.concat(alt.Emergency.remove_power)
        $scope.alertas[9].alertas=$scope.alertas[9].alertas.concat(alt.Emergency.anti_theft)
        $scope.alertas[9].alertas=$scope.alertas[9].alertas.concat(alt.Emergency.antitheft_door)
        $scope.alertas[9].alertas=$scope.alertas[9].alertas.concat(alt.Emergency.motion)
        $scope.alertas[9].alertas=$scope.alertas[9].alertas.concat(alt.Emergency.antitheft_shock)
        //aceleracion
        $scope.alertas[10].alertas=$scope.alertas[10].alertas.concat(alt.RPM.SDHR)
        $scope.alertas[10].alertas=$scope.alertas[10].alertas.concat(alt.RPM.FDHR)
        $scope.alertas[10].alertas=$scope.alertas[10].alertas.concat(alt.RPM.HSLR)
        $scope.alertas[10].alertas=$scope.alertas[10].alertas.concat(alt.RPM.LSHR)
        console.log($scope.alertas)
    }

    //busquedas.reverserGeoJson(Number($scope.lastData.lat),Number($scope.lastData.lon),20,function(response)
    $scope.ShowAlerta=function(i){
        console.log(i)
        $scope.markers=[];
        $scope.SelcAlerta=$scope.Alt[i]
        $scope.center= {
            lat: Number($scope.Alt[i].lat),
            lng: Number($scope.Alt[i].lon),
            zoom: 10
        }
        $scope.markers.push({
            lat: Number($scope.Alt[i].lat),
            lng: Number($scope.Alt[i].lon),
            message:'ultima posicion',
            icon: {
                iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
            }
        })
        busquedas.reverserGeoJson(Number($scope.Alt[i].lat),Number($scope.Alt[i].lon),20,function(response){
            $scope.localizacion=response.data.display_name;
        })

       
    }

     $scope.$watch("filtDate",function(newValue,oldValue) {
        
        console.log(newValue,oldValue)
    });

    $scope.eliminarAlerta=function(x,i){
        console.log($scope.dispositivo,$scope.Type,x,i)
        if($window.confirm('Desea eliminar la alerta '+x.Descripcion)){
            var data={
                key1:$scope.Type,
                id:x._id,
                div:$scope.dispositivo._id
            };
            $http({
                method:'POST',
                url:'/eliminar/alerta/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                if(response.data==0) {
                    $scope.Alt.splice(i,1);
                    switch ($scope.Type) {
                        case 'DPA':
                            $scope.alertas[7].alertas.splice(i,1);
                            break;
                    
                        default:
                            break;
                    }
                }
            })
        }
    }


    $scope.getAlertas=function(x){
        console.log(x)
        if(typeof x==='object')x=x
        else x=JSON.parse(x)
        $scope.Type=x.nombre
        if(x.alertas.length>0){
            var data={
                alertas:x.alertas
            };
            $http({
                method:'POST',
                url:'/get/alertas/array/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.Alt=response.data.data
                if($scope.Alt.length>0)$scope.ShowAlerta(0)
            })
        }
    }

    $scope.getDivice=function(){
        busquedas.dispositivo($cookies.get('deviceId'),function(response){
            console.log(response)
            $scope.dispositivo=response
            $scope.imies=response.imei
            //$scope.getlast()
            $scope.alertasArray()
        })        
    }

    $scope.chgDivice=function(x){
        $cookies.put('deviceId',x)
        $scope.resetAlertas()
        $scope.getDivice()
    }

    //busca todos los dispositivos
    $scope.getdivices = function(i){
        busquedas.divices($scope.users[i].id,function(response){
            console.log(response);
            for (let i = 0; i < response.length; i++) {
                $scope.divices.push(response[i]);
            }
            if($scope.users.length>i+1)$scope.getdivices(i+1)
            else {
                console.log($scope.divices)
                if($scope.divices.length>0){
                    if($cookies.get('deviceId')==undefined || $cookies.get('deviceId')==null) $cookies.put('deviceId',$scope.divices[0].id,{'path': '/'})
                    $scope.getDivice()
                }
            }
        })
    }

    //init
    $scope.init=function(){
        busquedas.init(function(users,empresa,mesaj){
            console.log(empresa,users,mesaj)
            $scope.nombre=empresa.nombre
            $scope.users=users
            $scope.getdivices(0)
            //if($cookies.get('deviceId')==undefined || $cookies.get('deviceId')==null) $cookies.put('deviceId',users[0].dispositivos,{'path': '/'})
        }) 
    }
    $scope.init() 
}])

app.filter('plusnine',function(){
    return function(number){
        if(number>9) return '+9'
        else return number
    }
})
app.filter('fecha',function(){
    return function(date){
        //console.log(date,hora)
        //var horas=hora.split(':')
        var fecha=Date.UTC(Number(date.slice(0,4)),Number(date.slice(4,6)),Number(date.slice(6,8)),Number(date.slice(8,10)),Number(date.slice(11,13)),Number(date.slice(14,16)))
        return fecha
    }
})