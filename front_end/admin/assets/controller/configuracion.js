var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('configuracion',['$scope','$http','$cookies','$window','$timeout','leafletData','crear','busquedas','editar','links','dispositivos',function($scope,$http,$cookies,$window,$timeout,leafletData,crear,busquedas,editar,links,dispositivos){
    //arreglos
    $scope.divices=[]
    $scope.imeisData=[]

    //links
    $scope.menu=[]
    $scope.links=links.links
    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    $scope.chgDivice=function(id){
        $scope.imeisData=[]
        $cookies.put('deviceId',id,{'path': '/'})
        $scope.getDivice()
    }

    $scope.getAllimeis=function(i){
        console.log($scope.imies[i])
        busquedas.imei($scope.imies[i],function(response){
            console.log(response)
            $scope.imeisData.push(response.data)
            if($scope.imies.length>i+1) $scope.getAllimeis(i+1)
            else console.log($scope.imeisData)
        })
    }

    $scope.getDivice=function(){
        busquedas.dispositivo($cookies.get('deviceId'),function(response){
            console.log(response)
            $scope.dispositivo=response
            $scope.imies=response.imei
            $scope.getAllimeis(0)
        })
        
    }

    $scope.SendComand=function(imei,trama){
        console.log(imei,trama);
        
        /*$http({
            method:'GET',
            url:'http://34.237.241.0:2100/write/'+String(imei)+'/'+trama
        }).
        then(function(response){
            console.log(response.data);
        })*/
    }

    //busca todos los dispositivos
    $scope.getdivices = function(i){
        busquedas.divices($scope.users[i].id,function(response){
            console.log(response);
            for (let i = 0; i < response.length; i++) {
                $scope.divices.push(response[i]);
            }
            if($scope.users.length>i+1)$scope.getdivices(i+1)
            else {
                console.log($scope.divices)
                if($scope.divices.length>0){
                    if($cookies.get('deviceId')==undefined || $cookies.get('deviceId')==null) $cookies.put('deviceId',$scope.divices[0].id,{'path': '/'})
                    $scope.getDivice()
                }
            }
        })
    }

    //init
    $scope.init=function(){
        busquedas.init(function(users,empresa,mesaj){
            console.log(empresa,users,mesaj)
            $scope.nombre=empresa.nombre
            $scope.users=users
            $scope.getdivices(0)
            //if($cookies.get('deviceId')==undefined || $cookies.get('deviceId')==null) $cookies.put('deviceId',users[0].dispositivos,{'path': '/'})
        }) 
    }
    $scope.init() 

}])