var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('admin',['$scope','$http','$cookies','$window','$timeout','leafletData','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,$timeout,leafletData,crear,busquedas,editar,links){
    //arreglos
    $scope.markers=[];
    $scope.center= {
        lat: 9.8755948,
        lng: -84.0380218,
        zoom: 10
    }
    $scope.divices=[]
    $scope.alerts={}
    $scope.styleWork='z-index=1'

    $scope.veropcion=function(x){
        $cookies.put('deviceId',x.id, {'path': '/'});
        console.log(x)
        $scope.goto('/admin/assets/htmls/dispositivo.html')
    }

    //links
    $scope.goto=function(link){
        $window.open(link,'_self')
    }

    $scope.menu=[]
    $scope.links=links.links

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    //cookies
    console.log($cookies.getAll());

    $scope.go_to =function(lat,lon,z){
        console.log(lat,lon,z);
        $scope.center={
            lat: Number(lat),
            lng: Number(lon),
            zoom: Number(z)
        };
    } 

    $scope.last=function(i){
        console.log($scope.divices)
        $http({
            method:'GET',
            url:'/divice/keys/'+$scope.divices[i].id+'/date,time,lat,lon/1/null'
        }).
        then(function(response){
            console.log(response.data);
            $scope.markers.push({
                lat:Number(response.data.data[0].lat),
                lng:Number(response.data.data[0].lon),
                message:String($scope.divices[i].unidad),
                icon: {
                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
                }
            })
            $scope.divices[i]['lat']=Number(response.data.data[0].lat)
            $scope.divices[i]['lng']=Number(response.data.data[0].lon)
            console.log($scope.markers, $scope.divices[i])
            if($scope.divices.length>i+1)$scope.last(i+1)
        })
    }

    $scope.alertas=function(i){
        busquedas.alertas($scope.divices[i].Alertas_last,function(alert){
            console.log(alert);
            if(alert==null)$scope.alerts[$scope.divices[i].id]=[]
            else{
                $scope.alerts[$scope.divices[i].id]=alert
            }
            if($scope.divices.length>i+1)$scope.alertas(i+1)
        }) 
    }

    $scope.getdivices = function(i){
        busquedas.divices($scope.users[i].id,function(response){
            console.log(response);
            for (let i = 0; i < response.length; i++) {
                $scope.divices.push(response[i]);
            }
            if($scope.users.length>i+1)$scope.getdivices(i+1)
            else {
                $scope.alertas(0)
                $scope.last(0)
            }
        })
    }

    $scope.init=function(){
        busquedas.init(function(users,empresa,mesaj){
            console.log(empresa,users,mesaj)
            $scope.nombre=empresa.nombre
            $scope.users=users
            $scope.getdivices(0)
        }) 
    }
    $scope.init()   
}])