var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('reportes',['$scope','$http','$cookies','$window','$timeout','leafletData','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,$timeout,leafletData,crear,busquedas,editar,links){
    $scope.NumberShow=0
    $scope.Optooltip=0
    var flag1=true
    //links
    $scope.menu=[]
    $scope.links=links.links
    $scope.goto=function(link){
        $window.open(link,'_self')
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }
    //cookies
    console.log($cookies.getAll());
    //arreglos
    $scope.divices=[]
    $scope.markers=[];
    $scope.center= {
        lat: 9.8755948,
        lng: -84.0380218,
        zoom: 10
    }
    $scope.geojson={
        data: {
            "type": "FeatureCollection",
            "features": []
        },
        style: {
            fillColor: "green",
            weight: 2,
            opacity: 1,
            dashArray: '3',
            fillOpacity: 0.7
        }
    }

    $scope.makelines=function(data){
        var line=[]
        $scope.geojson.data.features=[]
        if(data.length>0){
            line.push([Number(data[0].lon),Number(data[0].lat)]);
            for (let i = 1; i < data.length; i++) {
                 var radio=110950.58*Math.sqrt((data[i].lat-data[i-1].lat)*(data[i].lat-data[i-1].lat)+(data[i].lon-data[i-1].lon)*(data[i].lon-data[i-1].lon));
                if(radio>20000){
                    //console.log(line)
                    $scope.geojson.data.features.push({
                        "type": "Feature",
                        "properties": {
                            "color": "green"
                        },
                        "geometry": {
                            "type": "LineString",
                            "coordinates": line
                        }
                    })
                    line=[]
                }
                line.push([Number(data[i].lon),Number(data[i].lat)]);
            }
            $scope.geojson.data.features.push({
                "type": "Feature",
                "properties": {
                    "color": "green"
                },
                "geometry": {
                    "type": "LineString",
                    "coordinates": line
                }
            })
        }
    }

    $scope.historialPos =function(){
        console.log($scope.hist);
        var hst=JSON.parse($scope.hist)
        if(hst._id){
            busquedas.gethistorial(hst._id,'lat,lon,date,time,speed,dist,rpm,power',function(response){
                console.log(response)
                $scope.NumberShow=0;
                $scope.HistPos=response.data.data
                if($scope.HistPos.length>0){
                    $scope.lastData.dist=$scope.HistPos[0].dist-$scope.HistPos[$scope.HistPos.length-1].dist
                    if($scope.markers.length>1){
                        $scope.markers[1].lat=Number($scope.HistPos[$scope.HistPos.length-1].lat)
                        $scope.markers[1].lng=Number($scope.HistPos[$scope.HistPos.length-1].lon)
                    }
                    else{
                        $scope.markers.push({
                            lat:Number($scope.HistPos[$scope.HistPos.length-1].lat),
                            lng:Number($scope.HistPos[$scope.HistPos.length-1].lon),
                            message:'ultima posicion',
                            icon: {
                                iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png',
                            }
                        })
                    }
                }
                $scope.makelines(response.data.data)
            })
        }
        else{
            if(hst.fechas=='actual'){
                $http({
                    method:'GET',
                    url:'/search/keys/from/lat,lon,date,time,speed,dist,rpm,power/'+hst.final+'/'+hst.inicio
                }).
                then(function(response){
                    console.log(response)
                    $scope.NumberShow=0;
                    $scope.HistPos=response.data.data
                    if($scope.HistPos.length>0){
                        $scope.lastData.dist=$scope.HistPos[0].dist-$scope.HistPos[$scope.HistPos.length-1].dist
                        if($scope.markers.length>1){
                            $scope.markers[1].lat=Number($scope.HistPos[$scope.HistPos.length-1].lat)
                            $scope.markers[1].lng=Number($scope.HistPos[$scope.HistPos.length-1].lon)
                        }
                        else{
                            $scope.markers.push({
                                lat:Number($scope.HistPos[$scope.HistPos.length-1].lat),
                                lng:Number($scope.HistPos[$scope.HistPos.length-1].lon),
                                message:'ultima posicion',
                                icon: {
                                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png',
                                }
                            })
                        }
                    }
                    $scope.makelines(response.data.data)
                })
            }
        }
    }  

    $scope.getlast=function(id){
        $http({
            method:'GET',
            url:'/divice/keys/'+id+'/date,time,lat,lon,OSR,OST,rpm,mode,drive,io,speed2,dist,power,volt,_id/1/null'
        }).
        then(function(response){
            //console.log(response.data)
            $scope.lastData=response.data.data[0]
            console.log($scope.lastData)
            $scope.center= {
                lat: Number($scope.lastData['lat']),
                lng: Number($scope.lastData['lon']),
                zoom: 10
            }
            if($scope.markers.length==0){
                $scope.markers.push({
                    lat:Number(response.data.data[0].lat),
                    lng:Number(response.data.data[0].lon),
                    message:'ultima posicion',
                    icon: {
                        iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
                    }
                })
            }
            else{
                $scope.markers[0]={
                    lat:Number(response.data.data[0].lat),
                    lng:Number(response.data.data[0].lon),
                    message:'ultima posicion',
                    icon: {
                        iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
                    }
                }
            }
            busquedas.allhistorial(id,function(response){
                console.log(response.data);
                $scope.historials=response.data.data
                //si existe historial
                if($scope.historials){
                    var actual={
                        inicio:$scope.historials[0].final,
                        final:$scope.lastData._id,
                        fechas:"actual"
                    }
                    $scope.historials.push(actual)
                }
            })
        })
    }

    $scope.getDivice=function(id){
        busquedas.dispositivo(id,function(response){
            console.log(response)
            $scope.dispositivo=response
            $scope.imies=response.imei
            $scope.getlast(id)
        })
        
    }

    //cambiar de dispositivo
    $scope.chgDivice=function(id){
        console.log(id)
        $cookies.put('deviceId',id,{'path': '/'})
        $scope.getDivice(id)
    }

    //busca todos los dispositivos
    $scope.getdivices = function(i){
        busquedas.divices($scope.users[i].id,function(response){
            console.log(response);
            for (let i = 0; i < response.length; i++) {
                $scope.divices.push(response[i]);
            }
            if($scope.users.length>i+1)$scope.getdivices(i+1)
            else {
                console.log($scope.divices)
                if($scope.divices.length>0){
                    if($cookies.get('deviceId')==undefined || $cookies.get('deviceId')==null) $cookies.put('deviceId',$scope.divices[0].id,{'path': '/'})
                    $scope.getDivice($cookies.get('deviceId'))
                }
            }
        })
    }

    //init
    $scope.init=function(){
        busquedas.init(function(users,empresa,mesaj){
            console.log(empresa,users,mesaj)
            $scope.nombre=empresa.nombre
            $scope.users=users
            $scope.getdivices(0)
            //if($cookies.get('deviceId')==undefined || $cookies.get('deviceId')==null) $cookies.put('deviceId',users[0].dispositivos,{'path': '/'})
        }) 
    }
    $scope.init() 

    $scope.opacityControl=function(){
        if(!$scope.onTooltip){
            $scope.Optooltip=$scope.Optooltip-0.1;
            console.log($scope.Optooltip)
            if($scope.Optooltip>0){
                flag1=true
                $timeout(function(){
                    $scope.opacityControl()
                },3000)
            }
        }
    }

    $scope.arrowCntr=function(i){
        console.log(i,$scope.NumberShow)
        if(Number($scope.NumberShow)+i>0 && Number($scope.NumberShow)+i<$scope.HistPos.length){
            $scope.NumberShow=Number($scope.NumberShow)+i
        }
    }

    $scope.$watch("NumberShow",function(n,b){
        //console.log(b,n)
        $scope.tooltipValue=$scope.HistPos[$scope.HistPos.length-n]
        $scope.Optooltip=0.9
        $scope.onTooltip=false
        $scope.markers[1].lat=Number($scope.HistPos[$scope.HistPos.length-n].lat)
        $scope.markers[1].lng=Number($scope.HistPos[$scope.HistPos.length-n].lon)
        if(flag1){
            flag1=false
            $timeout(function(){
                $scope.opacityControl()
            },3000)
        }
    })
}])

app.filter('fecha',function(){
    return function(date,hora){
        //console.log(date,hora)
        var horas=hora.split(':')
        var fecha=Date.UTC(Number(date.slice(0,4)),Number(date.slice(4,6)),Number(date.slice(6,8)),Number(horas[0]),Number(horas[1]),Number(horas[2]))
        return fecha
    }
})

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return null
    }
})

app.filter('mode',function(){
    return function(modo,date){
        //console.log(Date.now()-Date.UTC(Number(date.slice(0,4)),Number(date.slice(4,6)),Number(date.slice(6,8))))
        switch (Number(modo)) {
            case 1:
                return 'Parqueado'
                break;
            case 2:
                if(Date.now()-Date.UTC(Number(date.slice(0,4)),Number(date.slice(4,6)),Number(date.slice(6,8)))<-86400000) return 'Inhabilitado'
                else return 'Conduciendo'
            case 3:
                return 'Desconocido'
            case 4:
                return 'Distancia'
            case 5:
                return 'Otro'
            default:
                return 'Indefinido'
                break;
        }
    }
})

app.filter('DateFormat',function(){
    return function(date){
        return date.slice(0,4)+'/'+date.slice(4,6)+"/"+date.slice(6,8) 
    }
})