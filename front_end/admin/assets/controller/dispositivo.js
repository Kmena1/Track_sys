var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('dispositivo',['$scope','$http','$cookies','$window','$timeout','leafletData','crear','busquedas','editar','links','dispositivos',function($scope,$http,$cookies,$window,$timeout,leafletData,crear,busquedas,editar,links,dispositivos){
    //arreglos
    $scope.markers=[];
    $scope.center= {
        lat: 9.8755948,
        lng: -84.0380218,
        zoom: 10
    }
    $scope.divices=[]
    $scope.alerts={}
    $scope.styleWork='z-index=1'
    $scope.imeisData=[]

    $scope.descripcion=''
    $scope.comandos=[]
    $scope.explicacion=''
    //links
    $scope.goto=function(link){
        $window.open(link,'_self')
    }

    $scope.menu=[]
    $scope.links=links.links

    //data
    $scope.data=dispositivos.dispositivos
    $scope.sendComand=function(imei,comad){
        switch ($scope.Tipo){
            case 'ST300H':
                header='ST300CMD;';
                break
            case 'ST500':
                header='ST500CMD;';
                break
            default:
                header='ST300CMD;';
                break
        }
        console.log('http://34.237.241.0:2100/write/'+String(imei)+'/'+header+String(imei)+comad)
        $http({
            method:'GET',
            url:'http://34.237.241.0:2100/write/'+String(imei)+'/'+header+String(imei)+comad
        }).
        then(function(response){
            console.log(response.data);
        })
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    //editar el nombre del dispositivo y la unidad
    $scope.editVhc=function(i){
        switch (i) {
            //nombre del dispositivo
            case 0:
                var response=$window.prompt("nuevo nombre del vehiculo")
                console.log(response)
                if(response){
                    editar.editVhc($scope.dispositivo._id,response,$scope.dispositivo.unidad,function(response){
                        console.log(response)
                        $scope.dispositivo=response
                    })
                }
                break;
            //nombre del dispositivo
            case 1:
                var response=$window.prompt("numbero de la unidad del vehiculo")
                console.log(response)
                if(!isNaN(Number(response))){
                    editar.editVhc($scope.dispositivo._id,$scope.dispositivo.nombre,response,function(response){
                        console.log(response)
                        $scope.dispositivo=response
                    })
                }
                break;
            default:
                break;
        }
    }

    $scope.select=function(x){
        console.log(x)
        if($scope.data[x.tipo][x.model_ID]){
            $scope.descripcion=$scope.data[x.tipo][x.model_ID].descripcion
            $scope.comandos=$scope.data[x.tipo][x.model_ID].comandos
            $scope.actualImei=x.imei
            $scope.Tipo=x.tipo
        }
    }

    $scope.cambioExplicacion = function(x){
        $scope.explicacion=x.explicacion
        $scope.comand=x.comand
    }

    //cookies
    console.log($cookies.getAll());

    $scope.getAllimeis=function(i){
        console.log($scope.imies[i])
        busquedas.imei($scope.imies[i],function(response){
            console.log(response)
            $scope.imeisData.push(response.data)
            if($scope.imies.length>i+1) $scope.getAllimeis(i+1)
            else console.log($scope.imeisData)
        })
    }
    $scope.GetLast=function(x){
        $http({
            method:'GET',
            url:'/divice/keys/'+x+'/date,time,lat,lon,OSR,OST,rpm,mode,drive,io,speed2,dist/1/null'
        }).
        then(function(response){
            console.log(response.data)
            if(response.data.data.length>0){
                $scope.lastData=response.data.data[0]
                $scope.input=[]
                $scope.output=[]
                busquedas.reverserGeoJson(Number($scope.lastData.lat),Number($scope.lastData.lon),20,function(response){
                    console.log(response.data)
                    $scope.loc = response.data.address;
                    $scope.displayname = response.data.display_name
                    if($scope.lastData.io){
                        $scope.ignition=Boolean(Number($scope.lastData.io[0]))
                        for (let i = 0; i < $scope.lastData.io.length-2; i++) {
                            $scope.input.push({
                                Number:i,
                                value:Boolean(Number($scope.lastData.io[i]))
                            })
                        }
                        for (let i = $scope.lastData.io.length-2; i < $scope.lastData.io.length; i++) {
                            $scope.output.push({
                                Number:i,
                                value:Boolean(Number($scope.lastData.io[i]))
                            })
                        }
                    }    
                })
            }
        })
    }

    $scope.getAlldivices = function(i){
        busquedas.divices($scope.users[i].id,function(response){
            console.log(response);
            for (let i = 0; i < response.length; i++) {
                $scope.divices.push(response[i]);
            }
            if($scope.users.length>i+1)$scope.getAlldivices(i+1)
            else {
                console.log($scope.divices)
                //if($scope.divices.length>0){
                //    if($cookies.get('deviceId')==undefined || $cookies.get('deviceId')==null) $cookies.put('deviceId',$scope.divices[0].id,{'path': '/'})
                //    $scope.getDivice()
                //}
            }
        })
    }

    $scope.getDivices=function(x){
        busquedas.dispositivo(x,function(response){
            console.log(response)
            $scope.dispositivo=response
            $scope.imies=response.imei
            $scope.getAllimeis(0)
        })
        
    }

    $scope.changeDiv=function(x){
        $cookies.put('deviceId',x, {'path': '/'});
        //$cookies.put('deviceId',x,{'path': '/'})
        console.log($cookies.get('deviceId'),x)
        $scope.init(x)
    }

    $scope.init=function(x){
        busquedas.init(function(users,empresa,mesaj){
            console.log(empresa,users,mesaj)
            $scope.nombre=empresa.nombre
            $scope.users=users
            $scope.divices=[]
            $scope.imeisData=[]
            $scope.getAlldivices(0)
            $scope.GetLast(x)
            $scope.getDivices(x)
        }) 
    }
    $scope.init($cookies.get('deviceId'))  

}])

app.filter('fecha',function(){
    return function(date,hora){
        //console.log(date,hora)
        var horas=hora.split(':')
        var fecha=Date.UTC(Number(date.slice(0,4)),Number(date.slice(4,6)),Number(date.slice(6,8)),Number(horas[0]),Number(horas[1]),Number(horas[2]))
        return fecha
    }
})

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return null
    }
})

app.filter('mode',function(){
    return function(modo,date){
        //console.log(Date.now()-Date.UTC(Number(date.slice(0,4)),Number(date.slice(4,6)),Number(date.slice(6,8))))
        switch (Number(modo)) {
            case 1:
                return 'Parqueado'
                break;
            case 2:
                if(Date.now()-Date.UTC(Number(date.slice(0,4)),Number(date.slice(4,6)),Number(date.slice(6,8)))<-86400000) return 'Inhabilitado'
                else return 'Conduciendo'
            case 3:
                return 'Desconocido'
            case 4:
                return 'Distancia'
            case 5:
                return 'Otro'
            default:
                return 'Indefinido'
                break;
        }
    }
})