var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('OBD',['$scope','$http','$cookies','$window','$timeout','leafletData','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,$timeout,leafletData,crear,busquedas,editar,links){
    $scope.NumberShow=0
    $scope.Optooltip=0
    var flag1=true
    //links
    $scope.menu=[]
    $scope.links=links.links
    $scope.goto=function(link){
        $window.open(link,'_self')
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }
    //cookies
    console.log($cookies.getAll());
    //arreglos
    $scope.divices=[]
    $scope.markers=[];
    
    $scope.ShowInfo="MIL"

    $scope.getlast=function(){
        if($scope.dispositivo.OBD_Pos){
            $http({
                method:'GET',
                url:'/get/obd/'+$scope.dispositivo.OBD_Pos
            }).
            then(function(response){
                console.log(response.data);
                $scope.lastData=response.data
            })
        }
    }

    $scope.getDivice=function(){
        busquedas.dispositivo($cookies.get('deviceId'),function(response){
            console.log(response)
            $scope.dispositivo=response
            $scope.imies=response.imei
            $scope.getlast()
        })
        
    }

    //cambiar de dispositivo
    $scope.chgDivice=function(id){
        $cookies.put('deviceId',id,{'path': '/'})
        $scope.getDivice()
    }

    //busca todos los dispositivos
    $scope.getdivices = function(i){
        busquedas.divices($scope.users[i].id,function(response){
            console.log(response);
            for (let i = 0; i < response.length; i++) {
                $scope.divices.push(response[i]);
            }
            if($scope.users.length>i+1)$scope.getdivices(i+1)
            else {
                console.log($scope.divices)
                if($scope.divices.length>0){
                    if($cookies.get('deviceId')==undefined || $cookies.get('deviceId')==null) $cookies.put('deviceId',$scope.divices[0].id,{'path': '/'})
                    $scope.getDivice()
                }
            }
        })
    }

    //init
    $scope.init=function(){
        busquedas.init(function(users,empresa,mesaj){
            console.log(empresa,users,mesaj)
            $scope.nombre=empresa.nombre
            $scope.users=users
            $scope.getdivices(0)
            //if($cookies.get('deviceId')==undefined || $cookies.get('deviceId')==null) $cookies.put('deviceId',users[0].dispositivos,{'path': '/'})
        }) 
    }
    $scope.init() 

    $scope.opacityControl=function(){
        if(!$scope.onTooltip){
            $scope.Optooltip=$scope.Optooltip-0.1;
            console.log($scope.Optooltip)
            if($scope.Optooltip>0){
                flag1=true
                $timeout(function(){
                    $scope.opacityControl()
                },3000)
            }
        }
    }

    $scope.arrowCntr=function(i){
        console.log(i,$scope.NumberShow)
        if(Number($scope.NumberShow)+i>0 && Number($scope.NumberShow)+i<$scope.HistPos.length){
            $scope.NumberShow=Number($scope.NumberShow)+i
        }
    }

    $scope.$watch("NumberShow",function(n,b){
        //console.log(b,n)
        $scope.tooltipValue=$scope.HistPos[$scope.HistPos.length-n]
        $scope.Optooltip=0.9
        $scope.onTooltip=false
        $scope.markers[1].lat=Number($scope.HistPos[$scope.HistPos.length-n].lat)
        $scope.markers[1].lng=Number($scope.HistPos[$scope.HistPos.length-n].lon)
        if(flag1){
            flag1=false
            $timeout(function(){
                $scope.opacityControl()
            },3000)
        }
    })
}])

app.filter('tiempoConvert',function(){
    return function(time){
        if(time){
            if(time<60) return time+' s'
            else if(time<3600) return Math.floor(time/60)+' min'
            else if(time<86400) return Math.floor(time/3600)+''
        }
        else return 'no especificado'
    }   
})

app.filter('Especificado',function(){
    return function(data){
        if(data==null)return 'No especificado'
        else return data
    }
})

app.filter('splitIndent',function(){
    return function(data,i){
        if(data){
            return data.split(",")[i]
        }
        else return 'No especificado'
    }
})