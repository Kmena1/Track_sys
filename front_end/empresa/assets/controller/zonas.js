var app =angular.module("app",['ui.bootstrap','ngCookies','ui-leaflet']);
app.controller('recetas',['$scope','$http','$cookies','$window','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,crear,busquedas,editar,links){
    console.log($cookies.getAll());
    $scope.menu=[]
    $scope.links = links.links
    $scope.NumberSide=1
    $scope.SelcPT=true

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    $scope.AgentesToZona=function(){
        for (let i = 0; i < $scope.rutas.length; i++) {
            var ruta=$scope.rutas[i].region
            for (let j = 0; j < $scope.agentes.length; j++) {
                if($scope.agentes[j].clientes.indexOf(ruta)!=-1){
                    $scope.rutas[i]['agente']=$scope.agentes[j].user
                }
                
            }
        }
    }

    $scope.getRutas=function(){
        $http({
            method:'GET',
            url:'/empresa/zonas/'+$cookies.get('empresa')
        }).
        then(function(response){
            console.log(response.data);
            $scope.rutas=response.data.id
            $scope.zona=response.data.zonas
            $scope.AgentesToZona()
        })
    }

    $scope.newZone=function(){
        var nombre=$window.prompt('Nombre de la zona')
        var codigo=$window.prompt('Codigo de la zona')
        if(nombre && codigo){
            var data={
                emp:$cookies.get('empresa'),
                region:nombre,
                codigo:codigo,
            };
            $http({
                method:'POST',
                url:'/new/zona/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.rutas=response.data.id
                $scope.zona=response.data.zonas
            })
        }
    }

    $scope.SelccionSide=function(y,i){
        console.log(y,i)
        $scope.nombre=y.region
        $scope.dataruta=y
        $scope.page=i
    }

    $scope.setid=function(){
        var data={
            emp:$cookies.get('empresa'),
            region:$scope.nombre,
            codigo:$scope.dataruta.codigo,
        };
        $http({
            method:'POST',
            url:'/change/id/region/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.getRutas()
        })
    }

    $scope.setZona=function(region,zona){
        console.log(region)
        var data={
            emp:$cookies.get('empresa'),
            region:region,
            zona:zona,
        };
        $http({
            method:'POST',
            url:'/change/zona/region/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.rutas=response.data.id
            $scope.zona=response.data.zonas
        })
    }

    $scope.addZona=function(){
        if($scope.dataruta.clientes.indexOf($scope.subZ)==-1)$scope.dataruta.clientes.push($scope.subZ)
        $scope.subZ=null
    }

    $scope.clientesaddZona=function(){
        if($scope.agent){
            var a=JSON.parse($scope.agent)
            if(a.clientes.indexOf($scope.nombre)==-1){
                a.clientes.push($scope.nombre)
                var data={
                    id:a.id,
                    clientes:a.clientes
                };
                $http({
                    method:'POST',
                    url:'/user/act/clientes/',
                    data:data
                }).
                then(function(response){
                    $http({
                        method:'GET',
                        url:'/usuarios/empresa/'+$cookies.get('empresa')+'/'+2
                    }).
                    then(function(response){
                        console.log(response.data);
                        $scope.agentes=response.data.datos
            $scope.AgentesToZona()
                    })
                })
            }
        }
    }

    $scope.removeZona=function(zona){
        $scope.dataruta.clientes.splice($scope.dataruta.clientes.indexOf(zona),1);
    }

    $scope.actClientes=function(){
        var data={
            id:$scope.dataruta.id,
            clientes:$scope.dataruta.clientes
        };
        $http({
            method:'POST',
            url:'/user/act/clientes/',
            data:data
        }).
        then(function(response){
            //console.log(response.data);
            $scope.findAgentes()
        })
    }

    $scope.findAgentes=function(){
        $http({
            method:'GET',
            url:'/usuarios/empresa/'+$cookies.get('empresa')+'/'+2
        }).
        then(function(response){
            console.log(response.data);
            $scope.agentes=response.data.datos
            $scope.getRutas()
        })
    }

    //init
    $scope.init=function(){
        busquedas.init(function(datos,extra,mensaje){
            $scope.users= datos;
            $scope.empresa= extra;
            $scope.mensaje=mensaje
            $http({
                method:'GET',
                url:'/find/company/'+extra.id
            }).
            then(function(response){
                //console.log(response.data.ExtEmail);
                $scope.email=response.data.ExtEmail
                $scope.email.push(response.data.email);
                //console.log($scope.email)
            })
            $scope.findAgentes()
        })
    }    

    $scope.init()

}])