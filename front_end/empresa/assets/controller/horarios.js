app.controller('horarios',['$scope','$http','$cookies','$window','leafletData','links',function($scope,$http,$cookies,$window,leafletData,links){
    //init var
    //saltos de pagina
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    console.log($cookies.getAll());
    $scope.meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Setiembre','Octubre','Noviembre','Diciembre']
    $scope.dias=[];
    $scope.tareas={};
    $scope.organizacion=[]
    $scope.horas=[]
    //arreglos
    $scope.show=false;
    $scope.fecha=new Date()
    $scope.tipoTarea=0;
    $scope.modalType = "crear";
    //$scope.fechaAct=new Date();
    $scope.inicio=new Date(new Date().setHours(new Date().getHours(),0,0,0))
    $scope.final=new Date(new Date().setHours(new Date().getHours()+1,0,0,0))
    console.log($scope.inicio,$scope.final)

    $scope.goto =function(i,where){
        switch (i) {
            case 0:
                $window.open('/empresa/index.html','_self')
                break;
            case 1:
                $window.open('/empresa/assets/htmls/editar.html','_self')
                break;
            case 2:
                $window.open(where,'_self')
                break
            default:
                break;
        }
    }

    $scope.crearActividad = function(){
        var fecha=new Date($scope.fechaAct);
        var data={
            usr:$scope.usr,
            mes:fecha.getMonth(),
            dia:fecha.getDate(),
            year:fecha.getYear(),
            tipo:$scope.tipoTarea,
            descripcion:$scope.Actividad,
            inicio:$scope.inicio,
            final:$scope.final
        };
        $http({
            method:'POST',
            url:'/crear/tarea/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
        })
    }

    $scope.MakeTareasSemana=function(){
        var l= $scope.tareas.length
        //console.log(l);
        for (let i = 0; i < l; i++) {
            console.log(i);
            var c= $scope.dias.indexOf($scope.tareas[i].dia);
            if(c>-1){
                var b =$scope.tareas[i].Hdescription;
                console.log(c,b)
                for (let j = 0; j < b.length; j++) {
                    var horario = b[j];
                    var Ainicio=horario.inicio.split(':')
                    var Afinal=horario.final.split(':')
                    if(Number(Ainicio[1])>30)var start=Number(Ainicio[0])*2+1
                    else var start=Number(Ainicio[0])*2
                    if(Number(Afinal[1])>30)var end=Number(Afinal[0])*2+1
                    else var end=Number(Afinal[0])*2
                    console.log(i,j,c,start,end);
                    for (let k = start; k < end; k++) {
                        console.log($scope.tareasSemana[c][k],new Array(horario.descripcion));
                        $scope.tareasSemana[c][k]=$scope.tareasSemana[c][k].concat(new Array(horario.descripcion))
                    }
                }
            }
        }
        console.log($scope.tareasSemana)
    }

    $scope.buscarTareas=function(i){
        var data={
            tareas:$scope.tareas[i].horario
        };
        $http({
            method:'POST',
            url:'/get/tareas/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.tareas[i]['Hdescription']=response.data.data;
            var data={tareas:$scope.tareas[i].tareas};
            $http({
                method:'POST',
                url:'/get/tareas/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.tareas[i]['HTescription']=response.data.data;
                if(i+1<$scope.tareas.length)  $scope.buscarTareas(i+1)
                else {
                    console.log($scope.tareas)
                    $scope.MakeTareasSemana()
                }
            })
        })
    }

    $scope.buscarHorarios=function(mes){
        console.log(mes)
        $scope.organizacion=new Array($scope.dias.length).fill([])
        if($scope.HorarioMeses[String(mes)]!=undefined){
            $http({
                method:'GET',
                url:'/get/horarios/'+$scope.HorarioMeses[String(mes)]
            }).
            then(function(response){
                console.log(response.data);
                $scope.tareas=response.data.data;
                if($scope.tareas.length>0) $scope.buscarTareas(0)
            })
        }
        else console.log(null)
    }

    $scope.getHorario = function() {
        $http({
            method:'GET',
            url:'/get/calendario/'+$cookies.get('userSch')+'/'+String($scope.fecha.getYear())
        }).
        then(function(response){
            console.log(response.data);
            $scope.HorarioMeses=response.data
            $scope.buscarHorarios($scope.fecha.getMonth())
        })
    }

    $scope.makeCalendar = function(){
        if($scope.fecha==undefined) $scope.fecha=new Date()
        var dia = $scope.fecha.getDay();
        $scope.dias=new Array(7).fill(dia);
        for (let i = dia; i >= 0; i--) {
            diferencia=dia-i;
            console.log(diferencia,i)
            let a= new Date($scope.fecha);
            a.setDate(a.getDate()-diferencia)
            $scope.dias[i]=a.getDate()
        }
        for (let i = dia; i <= 6; i++) {
            diferencia=dia-i;
            console.log(diferencia,i)
            let a= new Date($scope.fecha);
            a.setDate(a.getDate()-diferencia)
            $scope.dias[i]=a.getDate()
        }
        console.log($scope.dias)
        $scope.getHorario()
    }

    $scope.MakeHours = function(){
        for (let i = 0; i < 24; i++) {
            $scope.horas.push(String(i)+':00') 
            $scope.horas.push(String(i)+':30')         
        }
        $scope.tareasSemana = new Array(7).fill(new Array(48).fill([]))
    }

    //inicio
    $scope.init=function(){
        $scope.MakeHours()
        $http({
            method:'GET',
            url:'/find/empresa/'+String($cookies.get('empresa'))
        }).
        then(function(response){
            console.log('empresa y usuarios',response.data);
            $scope.users= response.data.datos;
            $scope.empresa= response.data.extra;
            if($scope.users.length>0){
                $scope.usr = $scope.users[0].id;
                $cookies.put('userSch',$scope.users[0].id,{'path': '/'})
                $scope.makeCalendar()
            }
            else $scope.crearDias(String(new Date().getMonth()))
        });
    }
    $scope.init()
}])