var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
//var moment = require('moment');

app.controller('reports',['$scope','$http','$cookies','$window','leafletData','busquedas','links',function($scope,$http,$cookies,$window,leafletData,busquedas,links){
    console.log($cookies.getAll());
    $scope.user=$cookies.get('userSch');
    $scope.lastid='null'
    $scope.datos=new Array(0);
    $scope.fechas={
        kmin:[new Date(),'00:00:00'],
        kmax:[new Date(),'00:00:00'],
    }
    $scope.rutas = []

    $scope.markers=[]

    //saltos de pagina
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }
    var gaugeOptions = {
        chart: {
            type: 'solidgauge'
        },
        title: null,
        pane: {
            center: ['50%', '80%'],
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },
        tooltip: {
            enabled: false
        },
        // the value axis
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },
        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    }

    // The speed gauge
    var chartSpeed = Highcharts.chart('velocidadProm', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 200,
            title: {
                text: 'Speed'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Speed',
            data: [80],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                    '<span style="font-size:12px;color:silver">km/h</span></div>'
            },
            tooltip: {
                valueSuffix: ' km/h'
            }
        }]

    }));

    var bateria = Highcharts.chart('bateria', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Bateria'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Speed',
            data: [80],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                    '<span style="font-size:12px;color:silver">%</span></div>'
            },
            tooltip: {
                valueSuffix: ' %'
            }
        }]

    }));

    var gasolina = Highcharts.chart('gasolina', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'gasolina'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Speed',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                    '<span style="font-size:12px;color:silver">L</span></div>'
            },
            tooltip: {
                valueSuffix: ' L'
            }
        }]

    }));

    //init values
    $scope.keywords=[
        {'key':'drive', 'nombre':'distancia'},        
        {'key':'speed', 'nombre':'velocidad'},
        {'key':'power', 'nombre':'poder'},
        {'key':'distance', 'nombre':'recorrido'},
        {'key':'battery','nombre':'bateria'}

    ]
    $scope.keys=['drive','speed'];
    $scope.space='alertas';

    busquedas.init(function(user,empresa){
        console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
    });
    //console.log(datos);
    //$scope.users= datos.datos;
    //$scope.empresa= datos.extra;

    $scope.resetMap=function(){
        $scope.markers=[];
        $scope.center= {
                    lat: 9.8755948,
                    lng: -84.0380218,
                    zoom: 10
                }

        $scope.geojson={
            data: {
                "type": "FeatureCollection",
                "features": []
            },
            style: {
                fillColor: "green",
                weight: 2,
                opacity: 1,
                dashArray: '3',
                fillOpacity: 0.7
            }
        }
    }
    $scope.resetMap()

    $scope.mapMakeLine=function(data,last,color){
        color=Number(color)
        //console.log(color)
        var paint="#000000"
        if(color<10){
            paint="#00FF00"
        }
        else{
            if(color<20){
                paint="#198f00"
            }
            else{
                if(color<30){
                    paint="#338500"
                }
                else{
                    if(color<40){
                        paint="#597500"
                    }
                    else{
                        if(color<50){
                            paint="#806600"
                        }
                        else{
                            if(color<60){
                                paint="#995c00"
                            }
                            else{
                                if(color<70){
                                    paint="#bf2600"
                                }
                                else{
                                    if(color<80){
                                        paint="#d91700"
                                    }
                                    else{
                                        if(color<90){
                                            paint="#e60f00"
                                        }
                                        else{
                                            if(color<100){
                                                paint="#b80f00"
                                            }
                                            else{
                                                if(color<110){
                                                    paint="#c20800"
                                                }
                                                else{
                                                    if(color<120){
                                                        paint="#cc0000"
                                                    }
                                                    else{
                                                        paint="#FF0000"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(data.length>1) $scope.geojson.data.features.push({
            "type": "Feature",
            "properties": {
                "color": "green"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": data
            }
        });
        $scope.datos = new Array(1).fill(last)
    }

    $scope.mapPoints=function(data){
        $scope.Makemarkers(data[1],data[data.length-1])
        console.log(data)
        //crear puntos en el mapa
        $scope.datos=[]
        var distancia =0;
        for (let i = 1; i < data.length; i++) {
            //si es un dato valido
            //console.log(data[i]);
            if(data[i].lat != null && data[i].lon != null){
                //si el radio es mayor a 10K se crear la linea por separado
                var radio=110950.58*Math.sqrt((data[i].lat-data[i-1].lat)*(data[i].lat-data[i-1].lat)+(data[i].lon-data[i-1].lon)*(data[i].lon-data[i-1].lon));
                //console.log(String(Number(data[i].speed))[0],String(Number(data[i-1].speed))[0])
                if(radio>1000 /*|| String(Number(data[i].speed))[0]!=String(Number(data[i-1].speed))[0]*/){
                    $scope.mapMakeLine($scope.datos,[data[i].lon,data[i].lat],data[i].speed)
                }
                else {
                    $scope.datos.push([data[i].lon,data[i].lat]);
                    distancia=distancia+radio
                }
            }
        }
        $scope.dist=Math.floor(distancia)/1000;
        $scope.mapMakeLine($scope.datos,[],0)
        if(data.length>0) {
            $scope.center= {
                lat: Number(data[1].lat),
                lng: Number(data[1].lon),
                zoom: 15
            }
        }
        else{
            $scope.center= {
                lat: 9.8755948,
                lng: -84.0380218,
                zoom: 15
            }
        }
        $scope.loading=false;
    };

    $scope.Makemarkers=function(first,last){
        $scope.markers.push({
                lat:Number(first.lat),
                lng:Number(first.lon),
                message:'Ultima posicion, \nEl '+first.date+' a las '+first.time,
                icon: {
                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                }
            })
            $scope.markers.push({
                lat:Number(last.lat),
                lng:Number(last.lon),
                message:'Primera posicion, El '+last.date+' a las '+last.time,
                icon: {
                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png',
                }
            })
    }

    $scope.filterDate = function(){
        /*
        $scope.dat=datos;
        $scope.lastid=id
        $scope.mapPoints(datos);
        $scope.prom(datos); 
        b= a.slice(0,4)+"-"+ a.slice(4,6)+"-"+ a.slice(6,8)
        */
        console.log(new Date($scope.fechai).getTime(),new Date($scope.fechao).getTime())
        s=new Date($scope.fechai).getTime();
        f=new Date($scope.fechao).getTime()
        $scope.resetMap()
        $scope.loading=true;
        var datosFilter=[];
        //console.log($scope.dat)
        for (let i = 1; i < $scope.dat.length; i++) {
            let b= new Date($scope.dat[i].date.slice(0,4)+"-"+ $scope.dat[i].date.slice(4,6)+"-"+ $scope.dat[i].date.slice(6,8)).getTime()
            if(s<=b && f>=b){
                datosFilter.push($scope.dat[i])
            }
        }
        console.log(datosFilter)
        if(datosFilter.length>0){
            $scope.mapPoints(datosFilter);
            $scope.prom(datosFilter); 
        }
    }

    $scope.prom=function(data){
        //console.log(moment(data[1].Date,'YYYYMMDD'))
        a=data[1].date
        b= a.slice(0,4)+"-"+ a.slice(4,6)+"-"+ a.slice(6,8)
        a=data[data.length-1].date
        c= a.slice(0,4)+"-"+ a.slice(4,6)+"-"+ a.slice(6,8)
        console.log(b,c)
        $scope.fechai=new Date(c);
        $scope.fechao=new Date(b);
        bateria.series[0].setData([Number(data[1].battery)])
        //$scope.dist=Math.abs((Number(data[1].distance)-Number(data[data.length-1].distance))/1000)
        console.log(Number(data[1].distance),Number(data[data.length-1].distance))
        var velocidad=0;
        chartSpeed.series[0].setData([Math.round(velocidad)])
    };

    $scope.findmore=function(){
        if($scope.loading==false){
            $scope.resetMap()
            $scope.loading=true;
            busquedas.divices($scope.user,function(div){
                busquedas.consultas('speed,distance,battery,tf,lat,lon',div,0,0,$scope.dat,$scope.lastid,function(datos,id){
                    console.log(datos,id)
                    $scope.dat=datos;
                    $scope.lastid=id
                    $scope.mapPoints(datos);
                    $scope.prom(datos);
                })
            })
        }
    }

    $scope.consultas = function(extras,i){
        busquedas.divices($scope.user,function(div){
            busquedas.consultas('speed,distance,battery,tf,'+extras,div,0,0,new Array(0),null,function(datos,id){
                console.log(datos,id)
                $scope.dat=datos;
                $scope.lastid=id
                if(extras==',lat,lon' || extras==',lon,lat')$scope.mapPoints(datos);
                $scope.prom(datos);
            })
        })
    }

    //inicia espacios de historicos, alertas y promedios 
    $scope.init=function(){
        $scope.resetMap()
        $scope.loading=true;
        //si el agente existe se toman datos
        if($cookies.get('userSch')!= null && $cookies.get('userSch')!= undefined){
            busquedas.divices(String($cookies.get('userSch')),function(div){
                console.log(div);
                if(div!=null){
                    busquedas.alertas(div[0].Alertas_last,function(alert){
                        console.log(alert);
                        $scope.Alerts=alert;
                    }) 
                    busquedas.allhistorial(div[0].id,function(response){
                        console.log(response.data)
                        if(response.data != null){
                            $scope.rutas = response.data.data;
                        }
                        else{
                            $scope.rutas = []
                        }
                    })
                }
            })
            //conseguir los historicos y datos relevantes para los promedios
            $scope.consultas(',lat,lon',0);
        }
        else{
            if($scope.users.length>0){
                $cookies.put('userSch',$scope.user,{'path': '/'});
                $scope.init();
            }
        }
    }

    $scope.$watch('user',function(newCollection, oldCollection){
        console.log(newCollection);
        if(newCollection != undefined){
            $cookies.put('userSch',newCollection,{'path': '/'})
            $scope.space='alertas';
        }
        $scope.datos=[];
        $scope.init();
    })

    $scope.$watch('Ruta',function(newCollection, oldCollection){
        console.log(newCollection);
        if(newCollection != undefined){
            $scope.datos=[];
            busquedas.gethistorial(newCollection,'date,time,speed,distance,battery,tf,lat,lon,_id',function(response) {
                console.log(response)
                if(response.data.data.length!=0){
                    $scope.resetMap()
                    $scope.loading=true;
                    $scope.dat=response.data.data;
                    $scope.lastid=response.data.data[response.data.itr-1]._id;
                    $scope.mapPoints($scope.dat);
                    $scope.prom($scope.dat);
                }
            })
            
        }
    })
}])

