var app =angular.module("app",['ui.bootstrap','ngCookies','ui-leaflet','ngJsonExportExcel']);
app.controller('inventario',['$scope','$http','$cookies','$window','busquedas','editar','crear','links',function($scope,$http,$cookies,$window,busquedas,editar,crear,links){
    $scope.clientes=[]
    //saltos de pagina
    $scope.menu=[]
    $scope.links = links.links

    $scope.barsize={
        'width': '1%',
        'height': '30px',
        'background-color': 'green',
    }

//cambio de pagina
    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }
//funciones
    //encuentra los pedidos de cada fatura par determinar los productos
    function findpedido(facturas,i,send,callback){
        $http({
            method:'GET',
            url:'/get/pedido/'+facturas[i].pedido
        }).
        then(function(response){
            //console.log(response.data);
            pedido=response.data
            var search=[]
            for (let i = 0; i < pedido.reservas.length; i++) {
                search.push(pedido.reservas[i].objeto)
            }
            var data={
                productos:search
            };
            $http({
                method:'POST',
                url:'/productos/array/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                productos=response.data.data
                if(pedido.contado) cont='Contado'
                else cont='Credito'
                var fecha=new Date(facturas[i].fecha)
                for (let j = 0; j < pedido.reservas.length; j++) {
                    const ped = pedido.reservas[j];
                    if(productos[ped.objeto]){
                        send.push({
                            Producto:productos[ped.objeto].nombre.replace(',',''),
                            Cod_Producto:productos[ped.objeto].codigo,
                            cantidad:ped.cantidad,
                            Factura:facturas[i].numero,
                            Saldo_de_Factura:facturas[i].monto,
                            Monto_de_Factura:pedido.total,
                            Fecha_de_Facturacion:String(fecha.getDay())+'/'+String(fecha.getMonth())+'/'+String(fecha.getYear()),
                            Credito_o_Contado:cont,
                        })
                    }
                    else{
                        send.push({
                            Producto:'Desconocido',
                            CodProducto:'0000',
                            cantidad:ped.cantidad,
                            Factura:facturas[i].numero,
                            Saldo_de_Factura:facturas[i].monto,
                            Monto_de_Factura:pedido.total,
                            Fecha_de_Facturacion:String(fecha.getDay())+'/'+String(fecha.getMonth())+'/'+String(fecha.getYear()),
                            Credito_o_Contado:cont,
                        })
                    }
                }
                if(i+1<facturas.length) findpedido(facturas,i,send,callback)
                else callback(send)
            })
        })
    }

    //encuentra las facuturas de los clientes
    function findfacturas(clientes,send,callback){
        $http({
            method:'GET',
            url:'/find/facturas/'+clientes._id
        }).
        then(function(response){
            //console.log(response.data.data,clientes.nombre)
            if(response.data.data.length>0) {
                findpedido(response.data.data,0,[],function(response){
                    for (let j = 0; j < response.length; j++) {
                        response[j]['Nombre_del_Cliente']= clientes.nombre.replace(',','')
                        response[j]['Codigo_Cliente']= clientes.codigo
                        response[j]['Ruta']= clientes.region
                        response[j]['Cod_ruta']=clientes.region.replace('.0','')
                    }
                    send=send.concat(response)
                    //console.log(clientes.nombre,send)
                    callback(send)
                    
                })
            }
            else{
                callback(send)
            }
        })
    }

    function ProssClient(client,i,send,callback){
        //console.log(client[i],i+1<client.length)
        if(i+1<client.length){
            const cliente= client[i];
            findfacturas(cliente,[],function(response){
                send=send.concat(response)
                //console.log(cliente.nombre,send)
                if(i+1<client.length)ProssClient(client,i+1,send,callback)
                else callback(send)
            })
        }
        else callback(send)
    }

//resumen
    function resumen(i){
        $scope.barsize.width=String(i*100/$scope.users.length)+'%'
        $http({
            method:'GET',
            url:'/clientes/agentes/get/'+$scope.users[i].id+'/'+$cookies.get('empresa')+'/'
        }).
        then(function(response){
            //console.log($scope.users[i].user,response.data.data)
            ProssClient(response.data.data,0,[],function(response){
                for (let j = 0; j < response.length; j++) {
                    response[j]['Agente']=$scope.users[i].nombre
                }
                $scope.FactRes=$scope.FactRes.concat(response)
                if(i+1<$scope.users.length)resumen(i+1)
                else{
                    //$scope.FactRes=response
                    console.log($scope.FactRes)
                    $scope.barsize={
                        'width': '100%',
                        'background-color': 'blue',
                        'height': '30px'
                    }
                    resumeFinal=true
                }
            })
        })
    }

    $scope.makeresumen=function(){
        $scope.FactRes=[]
        resumeFinal=false
        resumen(0)
    }
//facturar
    $scope.set=function(i){
        switch (Number($scope.accion)) {
            case 0:
                console.log($scope.facturas[i])
                if($scope.facturas[i].data>0){
                    if($scope.facturas[i].monto>$scope.facturas[i].data){
                        var data={
                            factura:$scope.facturas[i]._id,
                            monto:$scope.facturas[i].monto-$scope.facturas[i].data
                        };
                        $http({
                            method:'POST',
                            url:'/pagar/factura/',
                            data:data
                        }).
                        then(function(response){
                            console.log(response.data);
                            var data={
                                cliente:JSON.parse($scope.client)._id,
                                Monto:-$scope.facturas[i].data,
                            };
                            $http({
                                method:'POST',
                                url:'/aumentar/deuda/',
                                data:data
                            }).
                            then(function(response){
                                if($scope.facturas.length>i+1)$scope.set(i+1)
                                else $scope.SearchFact($scope.client)
                            })
                        })
                    }
                    else{
                        if($scope.facturas.length>i+1)$scope.set(i+1)
                        else $scope.SearchFact($scope.client)
                    }
                }
                else{
                    if($scope.facturas.length>i+1)$scope.set(i+1)
                    else $scope.SearchFact($scope.client)
                }
                break;
            case 1:
                 if($scope.facturas[i].data>0){
                    if(100>$scope.facturas[i].data){
                        var data={
                            factura:$scope.facturas[i]._id,
                            NewMonto:$scope.facturas[i].monto*(1-$scope.facturas[i].data/100),
                            tipo:false
                        };
                        $http({
                            method:'POST',
                            url:'/nota/credito/',
                            data:data
                        }).
                        then(function(response){
                            var data={
                                cliente:JSON.parse($scope.client)._id,
                                Monto:-$scope.facturas[i].monto*$scope.facturas[i].data/100,
                            };
                            $http({
                                method:'POST',
                                url:'/aumentar/deuda/',
                                data:data
                            }).
                            then(function(response){
                                //console.log(response.data);
                                if($scope.facturas.length>i+1)$scope.set(i+1)
                                else $scope.SearchFact($scope.client)
                            })
                            
                        })
                    }
                    else{
                        if($scope.facturas.length>i+1)$scope.set(i+1)
                        else $scope.SearchFact($scope.client)
                    }
                }
                else{
                    if($scope.facturas.length>i+1)$scope.set(i+1)
                    else $scope.SearchFact($scope.client)
                }
                break;
            case 2:
                 if($scope.facturas[i].data>0){
                    if(100>$scope.facturas[i].data){
                        var data={
                            factura:$scope.facturas[i]._id,
                            NewMonto:$scope.facturas[i].monto*(1+$scope.facturas[i].data/100),
                            tipo:false
                        };
                        $http({
                            method:'POST',
                            url:'/nota/credito/',
                            data:data
                        }).
                        then(function(response){
                            var data={
                                cliente:JSON.parse($scope.client)._id,
                                Monto:$scope.facturas[i].monto*$scope.facturas[i].data/100,
                            };
                            $http({
                                method:'POST',
                                url:'/aumentar/deuda/',
                                data:data
                            }).
                            then(function(response){
                                //console.log(response.data);
                                if($scope.facturas.length>i+1)$scope.set(i+1)
                                else $scope.SearchFact($scope.client)
                            })
                        })
                    }
                    else{
                        if($scope.facturas.length>i+1)$scope.set(i+1)
                        else $scope.SearchFact($scope.client)
                    }
                }
                else{
                    if($scope.facturas.length>i+1)$scope.set(i+1)
                    else $scope.SearchFact($scope.client)
                }
                break;
            case 3:
                monto=0
                //console.log($scope.pedido.reservas)
                for (let i = 0; i < $scope.pedido.reservas.length; i++) {
                    const reserva = $scope.pedido.reservas[i];
                    if($scope.productos[reserva.objeto].exentos)monto=monto+reserva.Ncant*$scope.productos[reserva.objeto].precio
                    else monto=monto+reserva.Ncant*$scope.productos[reserva.objeto].precio+reserva.cantidad*$scope.productos[reserva.objeto].precio*0.13
                    var data={
                        objeto:reserva.objeto,
                        cantidad:reserva.cantidad-reserva.Ncant,
                        user:$cookies.get('user')
                    };
                    $http({
                        method:'POST',
                        url:'/editar/cantidad/',
                        data:data
                    }).
                    then(function(response){
                        //console.log(response.data);
                        $scope.productos[response.data._id]=response.data
                    })
                    $scope.pedido.reservas[i]['Ocantidad']=$scope.pedido.reservas[i].cantidad
                    $scope.pedido.reservas[i].cantidad=$scope.pedido.reservas[i].Ncant
                }
                var diferencia=$scope.pedido.total-monto
                console.log(diferencia)
                var dataAumento={
                    cliente:JSON.parse($scope.client)._id,
                    Monto:-diferencia
                };
                var data={
                    factura:JSON.parse($scope.fct)._id,
                    NewMonto:monto,
                    tipo:true,
                    reserva:$scope.pedido.reservas
                };
                $http({
                    method:'POST',
                    url:'/nota/credito/',
                    data:data
                }).
                then(function(response){
                    console.log(dataAumento)
                    $http({
                        method:'POST',
                        url:'/aumentar/deuda/',
                        data:dataAumento
                    }).
                    then(function(response){
                        console.log(response.data);
                        $scope.SearchFact($scope.client)
                        var newWimdows=$window.open('/empresa/assets/htmls/Devolucion_producto.html')
                        newWimdows.data={
                            empresa:$scope.empresa,
                            Nota:0,
                            Documento:0,
                            fecha:new Date(),
                            cliente:JSON.parse($scope.client),
                            Pedido:$scope.pedido,
                            Factura:JSON.parse($scope.fct),
                            productos:$scope.productos
                        }
                    })
                    
                })
                break;
            default:
                break;
        }
    }
//facturas clientexcliente
    $scope.CxC=function(clientes,i,data){
        console.log(clientes[i],i)
        $http({
            method:'GET',
            url:'/find/facturas/'+clientes[i]
        }).
        then(function(response){
            if(data[clientes[i]]){
                data[clientes[i]]['facts']=response.data.data
                $scope.factCxC.push(data[clientes[i]])
                if(i+1<clientes.length) $scope.CxC(clientes,i+1,data)
                else console.log($scope.factCxC)
            }
            else console.log($scope.factCxC)    
        })
    }
//busquedas
    $scope.SearchFact=function(client){
        //onsole.log(client)
        var cliente=JSON.parse(client)
        if(cliente.facturas){
            $http({
                method:'GET',
                url:'/find/facturas/'+cliente._id
            }).
            then(function(response){
                console.log(response.data);
                $scope.facturas=response.data.data
            })
        }
    }

    $scope.Buscar_pedido=function(pedido){
        pedido=JSON.parse(pedido).pedido
        console.log(pedido)
        $http({
            method:'GET',
            url:'/get/pedido/'+pedido
        }).
        then(function(response){
            console.log(response.data);
            $scope.pedido=response.data
            var search=[]
            for (let i = 0; i < $scope.pedido.reservas.length; i++) {
                search.push($scope.pedido.reservas[i].objeto)
            }
            var data={
                productos:search
            };
            $http({
                method:'POST',
                url:'/productos/array/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.productos=response.data.data
            })
        })
    }

    $scope.FindZona=function(){
        $http({
            method:'GET',
            url:'/empresa/zonas/'+$cookies.get('empresa')
        }).
        then(function(response){
            console.log(response.data);
            $scope.zona=response.data.id
        })
    }

    $scope.clientesSearch=function(init){
        busquedas.clientes($cookies.get('empresa'),null,String(init),function(response){
            //console.log(response.max == response.last,response.max,response.last);
            if (response.max == response.last) {
                //llena el scope de respaldo
                $scope.clientes=$scope.clientes.concat(response.datos);
                console.log($scope.clientes)
                $scope.FindZona()
            } 
            else 
            {
                $scope.clientes=$scope.clientes.concat(response.datos);
                $scope.clientesSearch(response.last)
            }
        })
    }
    $scope.clientesSearch(0)
    $scope.LoadData=function(data){
        if(typeof(data) == 'string')data = JSON.parse(data)
        //console.log(data,typeof(data))
        var data={
            clientes:data.clientes
        };
        $http({
            method:'POST',
            url:'/clientes/array/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.factCxC=[]
            $scope.CxC(data.clientes,0,response.data.data)
        })
    }

//inicio
    busquedas.init(function(user,empresa){
        //console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
    })
}])

app.filter('vigencia',function(){
    return function(pedido){
        if(pedido){
            if(pedido.cancelada){
                return 'Cancelada'
            }
            else{
                diferencia=new Date().getTime()-new Date(pedido.fecha).getTime()
                var contdias = Math.round(diferencia/(1000*60*60*24));
                if(contdias>99) return 'Pendiente: +99 dias'
                else return 'Pendiente:'+contdias+' dias'
            }
        }
    }
})

app.filter('deudaCxC',function(){
    return function(facts,dias){
        var ret=0
        for (let i = 0; i < facts.length; i++) {
            const factura = facts[i];
            if(!factura.cancelada){
                diferencia=new Date().getTime()-new Date(factura.fecha).getTime()
                if(Math.round(diferencia/(1000*60*60*24))>=dias.i && Math.round(diferencia/(1000*60*60*24))<dias.f){
                    ret=ret+factura.monto
                }
            }
        }
        return ret
    }
})