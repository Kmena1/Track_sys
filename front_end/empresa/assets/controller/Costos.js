var imp1=13
var imp2=2

var app =angular.module("app",['ui.bootstrap','ngCookies','ui-leaflet']);
app.controller('recetas',['$scope','$http','$cookies','$window','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,crear,busquedas,editar,links){
    console.log($cookies.getAll());
//valores iniciales
    $scope.menu=[]
    $scope.links = links.links
    $scope.NumberSide=1
    $scope.Dolar=650

    $scope.costos={
        horas:2,
        electricidad:{
            KwH:180.69,
            consumo:10,
        },
        agua:{
            m3:1.669,
            consumo:100
        },
        alquiler:{
            mensual:300000,
            dias:20,
            horas:10,            
        },
        empleados:[]
    }

    $scope.empleados=[
        {
            nombre:'Jefe',
            costo:1000
        },
        {
            nombre:'supervisor',
            costo:1000
        },
        {
            nombre:'quimico',
            costo:1000
        },
        {
            nombre:'empleado',
            costo:1000
        },
        {
            nombre:'asistente',
            costo:1000
        },
        {
            nombre:'limpieza',
            costo:1000
        },
        {
            nombre:'bodegero',
            costo:1000
        }
    ]

    $scope.productosT={}
    $scope.MateriaP={}
    $scope.etiquetas={}
    $scope.envases={}
     $scope.Tapas={}
    $scope.recetas={}
    $scope.NombreObjetos={}
    $scope.AllRectas={}
    $scope.OtrosCostos=[]
//cambio de pagina
    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    //obtinene los costos de la empresa
     $scope.buscarEmpresa=function(){
        $http({
            method:'GET',
            url:'/find/company/'+$cookies.get('empresa')
        }).
        then(function(response){
            console.log(response.data);
            $scope.costos.electricidad=response.data.costos.electricidad
            $scope.costos.agua=response.data.costos.agua
            $scope.costos.alquiler=response.data.costos.alquiler
            if(response.data.costos.empleados.length>0)$scope.empleados=response.data.costos.empleados
        })
    }
    $scope.buscarEmpresa()
//costos de mano de obra
    $scope.addEmp=function(){
        $scope.Empleado=JSON.parse($scope.Empleado)
        if($scope.Empleado){
            $scope.costos.empleados.push({
                nombre:$scope.Empleado.nombre,
                costo:$scope.Empleado.costo,
                cantidad:1
            })
            $scope.Empleado=''
            console.log($scope.costos.empleados)
        }
    }

    $scope.removeEmp=function(i){
        $scope.costos.empleados.splice(i,1);
    }

    $scope.actualizarWork=function(){
        var data={
            id:$scope.sideSelection._id,
            workforce:$scope.costos.empleados
        };
        $http({
            method:'POST',
            url:'/actulizar/workforce/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.actRct(response.data)
            $window.alert('Costos Actualizados')
        })
    }
//seleccion del costado
    $scope.SelctionProducto=function(){
        $scope.Productos=JSON.parse($scope.Productos)
        //console.log($scope.envases[$scope.Productos.envase.objeto])
        if($scope.Productos.envase.objeto && $scope.Productos.tapa.objeto){
            if($scope.productosT[$scope.Productos.asignado].unidad == "KG")$scope.productosT[$scope.Productos.asignado].convertFactor=1
            else{
                if($scope.productosT[$scope.Productos.asignado].convertFactor)$scope.productosT[$scope.Productos.asignado].convertFactor=$scope.productosT[$scope.Productos.asignado].convertFactor
                else $scope.productosT[$scope.Productos.asignado].convertFactor=1
            }
        }
        //factor de conversion
        switch($scope.productosT[$scope.Productos.asignado].unidad){
            case 'G':
                $scope.productosT[$scope.Productos.asignado]['CFactor']=$scope.productosT[$scope.Productos.asignado].convertFactor/1000
                break
            case 'ML':
                $scope.productosT[$scope.Productos.asignado]['CFactor']=$scope.productosT[$scope.Productos.asignado].convertFactor/1000
                break
            case 'GL':
                $scope.productosT[$scope.Productos.asignado]['CFactor']=$scope.productosT[$scope.Productos.asignado].convertFactor/3.78541
                break
            default:
                $scope.productosT[$scope.Productos.asignado]['CFactor']=$scope.productosT[$scope.Productos.asignado].convertFactor
                break
        }
        $scope.competencia=$scope.productosT[$scope.Productos.asignado].competencia
        console.log($scope.competencia)
    }

    $scope.SelccionSide=function(x){
        console.log(x)
        $scope.sideSelection=x
        $scope.Productos=null
        $scope.competencia=null
        for (let i = 0; i < $scope.sideSelection.ingredientes.length; i++) {
            const objt= $scope.MateriaP[$scope.sideSelection.ingredientes[i].objeto]
            //correcion del factor de correcion
            if(objt){
                if(objt.unidad=="KG") $scope.MateriaP[$scope.sideSelection.ingredientes[i].objeto].convertFactor=1
                else{
                    if(!objt.convertFactor) $scope.MateriaP[$scope.sideSelection.ingredientes[i].objeto].convertFactor=1
                }
            }
            //correcion del precio, todo en dolares
            //console.log($scope.MateriaP[$scope.sideSelection.ingredientes[i].objeto])
        }
    }
//tandas
    $scope.loadTanda=function(){
        var Tanda=JSON.parse($scope.Tanda)
        console.log(Tanda)
        $scope.Tanda=Tanda
        $scope.costos.empleados=Tanda.workforce
        $scope.sizeTest=Tanda.cantidad
        $scope.costos.horas=Tanda.expenses.Alquiler
        $scope.costos.electricidad.consumo=Tanda.expenses.Electricidad
        $scope.costos.agua.consumo=Tanda.expenses.Agua
    }

    $scope.ActualizarTanda=function(){
        if($window.confirm("Desea actualizar los gastos de la tanda "+$scope.sizeTest)){
            var data={
                rct:$scope.sideSelection._id,
                size:$scope.sizeTest,
                workforce:$scope.costos.empleados,
                expenses:{
                    Alquiler:$scope.costos.horas,
                    Electricidad:$scope.costos.electricidad.consumo,
                    Agua:$scope.costos.agua.consumo,
                }
            };
            $http({
                method:'POST',
                url:'/formula/tanda/',
                data:data
            }).
            then(function(response){
                console.log(response.data);

            })
        }
    }

//recetas
    $scope.actRct=function(data){
        for (let i = 0; i < $scope.recetasArray.length; i++) {
            const rct = $scope.recetasArray[i];
            if(rct._id==data._id){
                $scope.recetasArray[i]=data
                break
            }
        }
    }
//competencia
    $scope.actualizarCompetencia=function(){
        if($window.confirm("Desea actualizar los precios de la competencia")){
            console.log($scope.competencia)
            var data={
                obj:$scope.productosT[$scope.Productos.asignado]._id,
                competencia:$scope.competencia,
            };
            $http({
                method:'POST',
                url:'/objeto/competencia/',
                data:data
            }).
            then(function(response){
                $scope.productosT[$scope.Productos.asignado].competencia=$scope.competencia
                console.log(response.data);
            })
        }
    }
//gastos fijos
    $scope.ActualizarGastos=function(){
        var data={
            id:$scope.sideSelection._id,
            Electricidad:$scope.costos.electricidad.consumo,
            Agua:$scope.costos.agua.consumo,
            tanda:$scope.sizeTest,
            Alquiler:$scope.costos.horas
        };
        $http({
            method:'POST',
            url:'/actualizar/gastos/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.actRct(response.data)
            $window.alert('Gastos Actualizados')
        })
    }
//inicio
    //ordenado de elementos del inventario
    $scope.ClasicacionInvt = function(keys,j,data){
        //console.log(keys[j],data[keys[j]],data['keys'])
        for (let i = 0; i < data['keys'].length; i++) {
            //console.log(keys[j],data['keys'][i].nombre)
            if(keys[j] == data['keys'][i].nombre){
                //console.log(data['keys'][i].tipo)
                switch (data['keys'][i].tipo) {
                    case "PT":
                        for (let k = 0; k < data[keys[j]].length; k++) {
                            const info = data[keys[j]][k];
                          $scope.productosT[info._id]=info  
                        }
                        //$scope.productosT=$scope.productosT.concat(data[keys[j]])
                        break;
                    case "MP":
                        for (let k = 0; k < data[keys[j]].length; k++) {
                            const info = data[keys[j]][k];
                          $scope.MateriaP[info._id]=info  
                        }
                        //$scope.MateriaP.push(data[keys[j]])
                        break;
                    case "ET":
                        for (let k = 0; k < data[keys[j]].length; k++) {
                            const info = data[keys[j]][k];
                          $scope.etiquetas[info._id]=info  
                        }
                        //$scope.etiquetas.push(data[keys[j]])
                        break;
                    case "EN":
                        for (let k = 0; k < data[keys[j]].length; k++) {
                            const info = data[keys[j]][k];
                          $scope.envases[info._id]=info  
                        }
                        //$scope.envases.push(data[keys[j]])
                        break;
                    case "TP":
                        for (let k = 0; k < data[keys[j]].length; k++) {
                            const info = data[keys[j]][k];
                           $scope.Tapas[info._id]=info 
                        }
                         //$scope.Tapas.push(data[keys[j]])
                        break;
                    default:
                        break;
                }
                break;
            }
        }
        if(j+1<keys.length)$scope.ClasicacionInvt(keys,j+1,data)
        else {
            console.log($scope.productosT,$scope.MateriaP,$scope.etiquetas,$scope.envases)

        }
    }

    //renombrado de objetos
    $scope.Nobjeto = function(keys,inventario){
        for (let i = 0; i < keys.length; i++) {
            for (let j = 0; j < inventario[keys[i]].length; j++) {
                $scope.NombreObjetos[inventario[keys[i]][j]._id]=inventario[keys[i]][j].nombre;   
            }
        }
        console.log($scope.NombreObjetos)
    }

    //Parser recetas
    $scope.ParserRecetas = function(recetas){
        for (let i = 0; i < recetas.length; i++) {
            $scope.AllRectas[recetas[i]._id] = recetas[i];
            for (let j = 0; j < $scope.AllRectas[recetas[i]._id].ingredientes.length; j++) {
                if(!$scope.AllRectas[recetas[i]._id].ingredientes[j])$scope.ingredientes.splice(j,1);
                else{
                    $scope.AllRectas[recetas[i]._id].ingredientes[j].cantidad=$scope.AllRectas[recetas[i]._id].ingredientes[j].cantidad*100;
                }
            }
        }
        console.log($scope.AllRectas)
    }

    //buscar recetas
    $scope.GetRecetas = function(){
        busquedas.recetas($cookies.get('empresa'),null,1000,function(response){
            //console.log(response.data)
            $scope.recetasArray=response.data.data
            console.log($scope.recetasArray)
            //$scope.AllRectas={}
            //$scope.ParserRecetas(response.data.data)
        })
    }

    $scope.inventarioGet = function(){
        busquedas.inventario($cookies.get('empresa'),function(keys,data){
            //console.log(keys, data)
            $scope.collections=data['keys']
            $scope.ClasicacionInvt(keys,1,data)
            $scope.Nobjeto(keys,data)
            $scope.GetRecetas()
        })
    }

    //reset
    $scope.reset=function(){
        $scope.producto=null
        $scope.seleccionado=false
        $scope.productosT=[]
        $scope.MateriaP=[]
        $scope.etiquetas=[]
        $scope.envases=[]
        $scope.inventarioGet()
    }

    //init
    $scope.init=function(){
        busquedas.init(function(datos,extra,mensaje){
            $scope.users= datos;
            $scope.empresa= extra;
            $scope.mensaje=mensaje
            $scope.reset()
        })
    }
    $scope.init()
//dolar
    $scope.cambiarDolar=function(){
        var dolar=$window.prompt('nuevo valor del dolar')
        console.log(dolar)
        if(dolar)$scope.Dolar=dolar
    }
//calculos finales
    $scope.calcularCostos=function(){
        var precioMateria=0
        var dolar=$scope.Dolar
        var tanda=$scope.sizeTest
        var unidades=$scope.sizeTest/($scope.productosT[$scope.Productos.asignado].contenidoNeto*$scope.productosT[$scope.Productos.asignado].CFactor)
        var productoEvaluar=$scope.productosT[$scope.Productos.asignado]
        let tapa=$scope.Tapas[$scope.Productos.tapa.objeto]
        let envase=$scope.envases[$scope.Productos.envase.objeto]
        var etiquetas=[]
        //calculo de la materia prima
        for (let i = 0; i < $scope.sideSelection.ingredientes.length; i++) {
            const x = $scope.sideSelection.ingredientes[i];
            impuesto=0
            if($scope.MateriaP[x.objeto]){
                if(!$scope.MateriaP[x.objeto].exentos) impuesto=$scope.MateriaP[x.objeto].precio*imp1/100
                else impuesto=$scope.MateriaP[x.objeto].precio*imp2/100
                //console.log(size,material[x.objeto].convertFactor,x.cantidad,material[x.objeto].precio)
                if($scope.MateriaP[x.objeto].Dolares) precioMateria=precioMateria+tanda*$scope.MateriaP[x.objeto].convertFactor*x.cantidad*($scope.MateriaP[x.objeto].precio+impuesto)
                else precioMateria=precioMateria+tanda*$scope.MateriaP[x.objeto].convertFactor*x.cantidad*($scope.MateriaP[x.objeto].precio+impuesto)/dolar
            }
        }
        precioMateria=Math.floor(precioMateria*Math.pow(10,3))/Math.pow(10,3)
        //calculo de costos
        var costosFinales={
            electircidad:$scope.costos.electricidad.KwH*$scope.costos.electricidad.consumo/dolar,
            agua:$scope.costos.agua.m3*$scope.costos.agua.consumo/dolar,
            alquiler:$scope.costos.horas*$scope.costos.alquiler.mensual/(dolar*$scope.costos.alquiler.dias*$scope.costos.alquiler.horas),
            manoObra:0
        }
        //calculo de la mano de obra
        for (let i = 0; i < $scope.costos.empleados.length; i++) {
            const empleado = $scope.costos.empleados[i];
            costosFinales.manoObra=costosFinales.manoObra+empleado.costo*empleado.cantidad
        }
        costosFinales.manoObra=costosFinales.manoObra/dolar
        //calculos de etiquetas
        var etiquetas={
            cantidad:0,
            precio:0
        }
        for (let i = 0; i < $scope.Productos.etiqueta.length; i++) {
            const x = $scope.Productos.etiqueta[i];
            impuesto=0
            if($scope.etiquetas[x.objeto]){
                if(!x.exentos) impuesto=$scope.etiquetas[x.objeto].precio*imp1/100
                else impuesto=$scope.etiquetas[x.objeto].precio*imp2/100
                etiquetas.cantidad=etiquetas.cantidad+1
                if($scope.etiquetas[x.objeto].Dolares)etiquetas.precio=etiquetas.precio+($scope.etiquetas[x.objeto].precio+impuesto)*x.cantidad
                else etiquetas.precio=etiquetas.precio+($scope.etiquetas[x.objeto].precio+impuesto)*x.cantidad/dolar
            }
        }
        //costo de fabrica
        //console.log(precioMateria,costosFinales.electircidad,costosFinales.alquiler,costosFinales.agua,costosFinales.manoObra,unidades)
        var Cunitario=(precioMateria+costosFinales.electircidad+costosFinales.alquiler+costosFinales.agua+costosFinales.manoObra)/unidades
        var Cfabrica=0
        impuesto=0
        if(tapa){
            if(!tapa.exentos)impuesto=tapa.precio*imp1/100
            else impuesto=tapa.precio*imp2/100
            //tapa.precio=tapa.precio+impuesto
            if(tapa.Dolares)Cfabrica=Cfabrica+$scope.Productos.tapa.cantidad*(tapa.precio+impuesto)
            else Cfabrica=Cfabrica+$scope.Productos.tapa.cantidad*(tapa.precio+impuesto)/dolar
            InfTapa={
                Dolares:tapa.Dolares,
                precio:tapa.precio+impuesto
            }
        }
        else{
            InfTapa={
                Dolares:false,
                precio:0
            }
        }
        //console.log(Cfabrica)
        impuesto=0
        if(envase){
            if(!envase.exentos)impuesto=envase.precio*imp1/100
            else impuesto=envase.precio*imp2/100
            //envase.precio=envase.precio+impuesto
            if(envase.Dolares)Cfabrica=Cfabrica+$scope.Productos.envase.cantidad*(envase.precio+impuesto)
            else Cfabrica=Cfabrica+$scope.Productos.envase.cantidad*(envase.precio+impuesto)/dolar
            InfEnv={
                Dolares:envase.Dolares,
                precio:envase.precio+impuesto
            }
        }
        else{
            InfEnv={
                Dolares:false,
                precio:0
            }
        }
        //console.log(Cfabrica)
        Cfabrica=Cfabrica+etiquetas.precio
        console.log(Cunitario,Cfabrica)
        //console.log(precioMateria,tanda,unidades,productoEvaluar,tapa,envase,$scope.costosFinales)
        $scope.infocosto={
            materia:precioMateria,
            tanda:tanda,
            unidades:unidades,
            producto:productoEvaluar,
            tapa2:InfTapa,
            envase2:InfEnv,
            tapa:tapa,
            envase:envase,
            etiquetas:etiquetas,
            costos:costosFinales,
            dolar:dolar,
            Cunitario:Cunitario,
            Cfabrica:Cfabrica,
            Cfinal:Cunitario+Cfabrica,
            extras:{
                fabrica:{
                    utilidad:30,
                    impuesto:10,
                    impuestoV:13,
                },
                distribuidor:{
                    gasto:0,
                    comision:13,
                    utilidad:30,
                    impuesto:13,
                },
                detallista:{
                    utilidad:30,
                    impuesto:13,
                }
            }
        }
        $scope.recalImps()
        console.log($scope.infocosto)
    }
    //impuestos
    $scope.recalImps=function(){
        var PF=$scope.infocosto.Cfinal;
        var extras=$scope.infocosto.extras
        //fabrica
        $scope.UF=PF*extras.fabrica.utilidad/100
        $scope.CF=(PF+$scope.UF)*extras.fabrica.impuesto/100
        $scope.PV=PF+$scope.UF+$scope.CF
        //$scope.IF=($scope.infocosto.Cfinal+$scope.UF+$scope.CF)*$scope.infocosto.extras.fabrica.impuestoV/100
        //Distribuidor
        var otros=extras.distribuidor.gasto    
        $scope.DU=($scope.PV+otros)*(extras.distribuidor.utilidad)/100   
        $scope.PD=($scope.PV+otros+$scope.DU)/(1-extras.distribuidor.comision/100)
        $scope.CU=$scope.PD-$scope.DU-$scope.PV-otros
        //Detallista
        $scope.DTU=$scope.PD*extras.detallista.utilidad/100
        $scope.DTI=($scope.PD+$scope.DTU)*extras.detallista.impuesto/100
        //precio venta
        $scope.Pfinal=$scope.PD+$scope.DTU+$scope.DTI
    }
    //imprimir el calculo de costos
    $scope.sendPrint=function(){
        var material=[]
        for (let i = 0; i < $scope.sideSelection.ingredientes.length; i++) {
            const x = $scope.sideSelection.ingredientes[i];
            impuesto=0
            let precio=0
            if($scope.MateriaP[x.objeto]){
                if(!$scope.MateriaP[x.objeto].exentos)impuesto=$scope.MateriaP[x.objeto].precio*imp1/100
                else impuesto=$scope.MateriaP[x.objeto].precio*imp2/100
                if($scope.MateriaP[x.objeto].Dolares)precio=$scope.MateriaP[x.objeto].precio
                else precio=$scope.MateriaP[x.objeto].precio/$scope.Dolar
                material.push({
                    ingrediente:$scope.MateriaP[x.objeto].nombre,
                    porcentaje:Math.floor(x.cantidad*10000)/100,
                    unidad:$scope.MateriaP[x.objeto].unidad,
                    peso:Math.floor($scope.sizeTest*$scope.MateriaP[x.objeto].convertFactor*x.cantidad*10000)/10000,
                    precio:Math.floor(precio*10000)/10000,
                    Total:Math.floor($scope.sizeTest*$scope.MateriaP[x.objeto].convertFactor*x.cantidad*precio*10000)/10000
                })
            }
        }
        var etiqueta=[]
        for (let i = 0; i < $scope.Productos.etiqueta.length; i++) {
            const x = $scope.Productos.etiqueta[i];
            precio=0
            impuesto=0
            if($scope.etiquetas[x.objeto]){
                if(!$scope.etiquetas[x.objeto].exentos) impuesto=$scope.etiquetas[x.objeto].precio*imp1/100
                else impuesto=$scope.etiquetas[x.objeto].precio*imp2/100
                if($scope.etiquetas[x.objeto].Dolares)precio=($scope.etiquetas[x.objeto].precio+impuesto)
                else precio=($scope.etiquetas[x.objeto].precio+impuesto)/$scope.Dolar
                etiqueta.push({
                    nombre:$scope.etiquetas[x.objeto].nombre,
                    Cantidad:x.cantidad,
                    precio:precio,
                    total:x.cantidad*precio
                })
            }
        }
        if($scope.Tapas[$scope.Productos.tapa.objeto]){
            tapa={
                nombre:$scope.Tapas[$scope.Productos.tapa.objeto].nombre,
                cantidad:$scope.Productos.tapa.cantidad,
            }
            impuesto=0
            if(!$scope.Tapas[$scope.Productos.tapa.objeto].exentos) impuesto=$scope.Tapas[$scope.Productos.tapa.objeto].precio*imp1/100
            else impuesto=$scope.Tapas[$scope.Productos.tapa.objeto].precio*imp2/100
            if($scope.Tapas[$scope.Productos.tapa.objeto].Dolares){
                tapa['precio']=($scope.Tapas[$scope.Productos.tapa.objeto].precio+impuesto)
                tapa['total']=$scope.Productos.tapa.cantidad*($scope.Tapas[$scope.Productos.tapa.objeto].precio+impuesto)
            }
            else{
                tapa['precio']=($scope.Tapas[$scope.Productos.tapa.objeto].precio+impuesto)/$scope.Dolar
                tapa['total']=$scope.Productos.tapa.cantidad*($scope.Tapas[$scope.Productos.tapa.objeto].precio+impuesto)/$scope.Dolar
            }
        }
        else{
            tapa={
                nombre:'indefinido',
                cantidad:0,
                precio:0,
                total:0
            }
        }
        if($scope.envases[$scope.Productos.envase.objeto]){
            envase={
                nombre:$scope.envases[$scope.Productos.envase.objeto].nombre,
                cantidad:$scope.Productos.envase.cantidad,
            }
            impuesto=0
            if(!$scope.envases[$scope.Productos.envase.objeto].exentos)impuesto=$scope.Tapas[$scope.Productos.tapa.objeto].precio*imp1/100
            else impuesto=$scope.Tapas[$scope.Productos.tapa.objeto].precio*imp2/100
            if($scope.envases[$scope.Productos.envase.objeto].Dolares){
                envase['precio']=($scope.envases[$scope.Productos.envase.objeto].precio+impuesto)
                envase['total']=$scope.Productos.envase.cantidad*($scope.envases[$scope.Productos.envase.objeto].precio+impuesto)
            }
            else{
                envase['precio']=($scope.envases[$scope.Productos.envase.objeto].precio+impuesto)/$scope.Dolar
                envase['total']=$scope.Productos.envase.cantidad*($scope.envases[$scope.Productos.envase.objeto].precio+impuesto)/$scope.Dolar
            }
        }
        else{
            envase={
                nombre:'indefinido',
                cantidad:0,
                precio:0,
                total:0
            }
        }
        if($scope.infocosto.producto){
            if(!$scope.infocosto.producto.Dolares) precioProd=$scope.infocosto.producto.precio/$scope.infocosto.dolar
        }
        else precioProd=0
        var send={
            Dolar:$scope.Dolar,
            nombre:$scope.productosT[$scope.Productos.asignado].nombre,
            presentacion:String($scope.productosT[$scope.Productos.asignado].contenidoNeto)+String($scope.productosT[$scope.Productos.asignado].unidad),
            size:$scope.sizeTest,
            materiaP:$scope.infocosto.materia,
            FijosP:$scope.infocosto.costos.electircidad+$scope.infocosto.costos.alquiler+$scope.infocosto.costos.agua+$scope.infocosto.costos.manoObra,
            FabricaP:$scope.infocosto.Cfabrica,
            precioProd:precioProd,
            materia:material,
            unidad: $scope.infocosto.unidades,
            costos: $scope.infocosto.costos,
            manodeObra: $scope.costos.empleados,
            producto:{ 
                nombre:$scope.productosT[$scope.Productos.asignado].nombre,
                contenido:String($scope.productosT[$scope.Productos.asignado].contenidoNeto)+String($scope.productosT[$scope.Productos.asignado].unidad),
                Factor:$scope.productosT[$scope.Productos.asignado].CFactor,
                cantidad:$scope.infocosto.unidades
            },
            envase:{
                tapa:tapa,
                envase:envase,
                etiquetas:etiqueta,
            },
            extras:$scope.infocosto.extras,
            calculos:{
                PCF:Math.floor(($scope.infocosto.Cunitario+$scope.infocosto.Cfabrica)*10000)/10000,
                VFU:Math.floor(($scope.UF)*10000)/10000,
                VFI:Math.floor(($scope.CF)*10000)/10000,
                VFV:0,
                //VFV:Math.floor((($scope.infocosto.Cfinal+$scope.PFC+($scope.infocosto.Cfinal+$scope.PFC)*$scope.infocosto.extras.fabrica.impuesto/100)*$scope.infocosto.extras.fabrica.impuestoV/100)*10000)/10000,
                PVF:Math.floor($scope.PV*10000)/10000,
                DC:Math.floor($scope.CU*10000)/10000,
                DU:Math.floor($scope.DU*10000)/10000,
                DI:0,
                //DI:Math.floor($scope.PD*1300/1.13)/10000,
                PVD:Math.floor($scope.PD*10000)/10000,
                DTU:Math.floor($scope.DTU*10000)/10000,
                DTI:Math.floor($scope.DTI*10000)/10000,
                PF:Math.floor($scope.Pfinal*10000)/10000,
            }
        }
        console.log(send)
        var newWimdows=$window.open('/empresa/assets/htmls/ViewCostos.html')
        newWimdows.data=send
    }
    //enviar al calculo inverso
    $scope.reverse=function(){
        if($scope.infocosto.producto.Dolares){
            PrecioFinal=$scope.infocosto.producto.precio*(1+$scope.infocosto.extras.detallista.impuesto/100)*(1+$scope.infocosto.extras.detallista.utilidad/100)
        }
        else{
            PrecioFinal=$scope.infocosto.producto.precio*(1+$scope.infocosto.extras.detallista.impuesto/100)*(1+$scope.infocosto.extras.detallista.utilidad/100)/$scope.infocosto.dolar
        }
        var send={
            Dolar:$scope.Dolar,
            nombre:$scope.sideSelection.nombre,
            producto:$scope.productosT[$scope.Productos.asignado],
            size:$scope.sizeTest,
            extras:$scope.infocosto.extras,
            competencia:$scope.competencia,
            calculos:{
                PCF:Math.floor(($scope.infocosto.Cunitario+$scope.infocosto.Cfabrica)*10000)/10000,
                VFU:Math.floor(($scope.UF)*10000)/10000,
                VFI:Math.floor(($scope.CF)*10000)/10000,
                VFV:0,
                //VFV:Math.floor((($scope.infocosto.Cfinal+$scope.PFC+($scope.infocosto.Cfinal+$scope.PFC)*$scope.infocosto.extras.fabrica.impuesto/100)*$scope.infocosto.extras.fabrica.impuestoV/100)*10000)/10000,
                PVF:Math.floor($scope.PV*10000)/10000,
                DC:Math.floor($scope.CU*10000)/10000,
                DU:Math.floor($scope.DU*10000)/10000,
                DI:0,
                //DI:Math.floor($scope.PD*1300/1.13)/10000,
                PVD:Math.floor($scope.PD*10000)/10000,
                DTU:Math.floor($scope.DTU*10000)/10000,
                DTI:Math.floor($scope.DTI*10000)/10000,
                PF:Math.floor($scope.Pfinal*10000)/10000,
                PFR:PrecioFinal,
            }
        }
        console.log(send)
        var newWimdows=$window.open('/empresa/assets/htmls/ReverseCostos.html')
        newWimdows.data=send
    }
}])

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return null
    }
})

app.filter('impuesto',function(){
    return function(value,imp){
        //console.log(value,imp)
        if(imp)return value*(1+imp2/100)
        else return value*(1+imp1/100)
    }
})

app.filter('priceCal',function(){
    return function(ingredientes,data){
        //console.log(ingredientes,data)
        var dolar=data.dolar
        var material=data.material
        var size=data.size
        var colones=data.colones
        let Total=0
        if(ingredientes){
            for (let i = 0; i < ingredientes.length; i++) {
                const x = ingredientes[i];
                let impuesto=0
                if(material[x.objeto]){
                    if(!material[x.objeto].convertFactor)material[x.objeto].convertFactor=1
                    if(!material[x.objeto].exentos) impuesto=material[x.objeto].precio*imp1/100
                    else impuesto=material[x.objeto].precio*imp2/100
                    //precio=size*material[x.objeto].convertFactor*x.cantidad*(material[x.objeto].precio+impuesto)
                    //console.log(precio,Total)
                    //console.log(size,material[x.objeto].convertFactor,x.cantidad,material[x.objeto].precio)
                    if(material[x.objeto].Dolares) Total=Total+size*material[x.objeto].convertFactor*x.cantidad*(material[x.objeto].precio+impuesto)
                    else Total=Total+size*material[x.objeto].convertFactor*x.cantidad*(material[x.objeto].precio+impuesto)/dolar
                }
            }
            if(!colones)return Math.floor(Total*Math.pow(10,data.k))/Math.pow(10,data.k)
            else return Math.floor(Total*dolar*Math.pow(10,data.k))/Math.pow(10,data.k)
        }
        else return 0
    }
})

app.filter('calEmp',function(){
    return function(emp,dolar){
        manoObra=0
        for (let i = 0; i < emp.length; i++) {
            const empleado = emp[i];
            manoObra=manoObra+empleado.costo*empleado.cantidad
        }
        return Math.floor(manoObra/dolar*10000)/10000
    }
})