var app =angular.module("app",['ui.bootstrap','ngCookies','ui-leaflet','ngJsonExportExcel']);
app.controller('inventario',['$scope','$http','$cookies','$window','busquedas','editar','crear','links',function($scope,$http,$cookies,$window,busquedas,editar,crear,links){
    console.log($cookies.getAll());
    console.log($window.location.href.split('/')[2])
    if($cookies.get('empresa')==null) $window.open('/login/index.html')
    $scope.idobject = null;
    $scope.filt = ''
    $scope.edit=false;
    $scope.Asignar=true;
    $scope.msg=false;
    $scope.carrito=[];
    $scope.Jclient={};
    $scope.Ncliente=null;
    $scope.preciofinal=0;
    $scope.reservasArray=[];
    $scope.filterCat =[];
    $scope.DescType=1;
    $scope.contado=false;
    $scope.Abono=0;
    $scope.CrearArchivo=false;
    $scope.CatType='PT';
    var colect=[]
    $scope.Archivo = {
        name:null
    }

    $scope.barsize={
        'width': '1%',
        'height': '30px',
        'background-color': 'green',
    }
    $scope.reportfields={
        'codigo':'CODIGO DEL PRODUCTO',
        'nombre':'PRODUCTO',
        'cantidad':'STOCK EN KG',
        'Dolares':'DOLARES/COLONES',
        'precio':'PRECIO',
        'monto':'MONTO',
        'trigger.min':'MINIMO',
        'trigger.max':'MAXIMO',
        'contenidoNeto':'PESO',
        'unidad':'UNIDAD',
        'familia':'COLECCION',
        '_id':'IDENTIFICADOR'
    }

    /*busquedas.getCurrency(function(response){
        console.log(response)
        $scope.currency=response.val;
    })*/
    $scope.currency=600

    busquedas.init(function(user,empresa){
        console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
    });
    $scope.ChangeSet =function(Ncat){
        console.log(Ncat)
        $scope.ctl=Ncat;
    }

    //saltos de pagina
    $scope.menu=[]
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    //modal 
    $scope.page=function(col,page){
        $scope.edit=false
        console.log(col,page);
        switch (Number(page)) {
            case 0:
                $scope.reset();
                $scope.msg=false;
                $scope.colc = true;
                $scope.objeto= false;
                $scope.catl=false;
                $scope.resv =false
                $scope.loadData=false
                break;
            case 1:
                $scope.reset();
                $scope.msg=false;
                $scope.colc = false;
                $scope.objeto = true;
                $scope.catl=false;
                $scope.resv =false
                $scope.colleccion=col;
                $scope.loadData=false
                $scope.SendArchivo=false;
                break;
            case 2:
                $scope.msg=false;
                $scope.colc = false;
                $scope.objeto = false;
                $scope.catl=true;
                $scope.resv =false
                $scope.objt();
                $scope.loadData=false
                $scope.SendArchivo=false;
                break;
            case 3:
                $scope.reset();
                $scope.msg=false;
                $scope.colc = false;
                $scope.objeto= false;
                $scope.catl=false;
                $scope.resv =true;
                $scope.loadData=false
                $scope.SendArchivo=false;
                break;
            case 4:
                $scope.msg=true;
                $scope.colc = false;
                $scope.objeto= false;
                $scope.catl=false;
                $scope.resv =false;
                $scope.loadData=false
                $scope.SendArchivo=false;
                break;
            case 5:
                $scope.msg=false;
                $scope.colc = false;
                $scope.objeto= false;
                $scope.catl=false;
                $scope.resv =false;
                $scope.loadData=true
                $scope.SendArchivo=false;
                if($scope.keys){
                    var popup = document.getElementsByClassName("modalActive")[0];
                    //console.log(popup)
                    //for show model
                    popup.click()
                    $scope.MakeArchivo()
                }
                else $window.alert('No hay informacion, porfavor espere que la informacion se cargue en la pagina para descargar los reportes del inventario')
                break
            case 6:
                $scope.msg=false;
                $scope.colc = false;
                $scope.objeto= false;
                $scope.catl=false;
                $scope.resv =false;
                $scope.loadData=false;
                $scope.SendArchivo=true;
            default:
                break;
        }
                
    }

    $scope.filter =function(){
        var Filtrado =[]
        if($scope.sercher == undefined || $scope.sercher == '') $scope.filt =false;
        else{
            $scope.filt =true;
            for (let i = 0; i < $scope.CatKey.length; i++) {
                for (let j = 0; j < $scope.catalogo[$scope.CatKey[i]].length; j++) {
                    if($scope.catalogo[$scope.CatKey[i]][j].nombre.toLowerCase().indexOf($scope.sercher.toLowerCase())>=0 || $scope.catalogo[$scope.CatKey[i]][j].descripcion.toLowerCase().indexOf($scope.sercher.toLowerCase())>=0){
                        Filtrado.push($scope.catalogo[$scope.CatKey[i]][j])
                    }
                }
            }
        }
        $scope.filterCat = Filtrado;
        console.log(Filtrado);
    }

    $scope.setCat = function(data){
        $scope.agregar =true;
        console.log(data);
        $scope.Cprecio=data.precio;
        $scope.data = data;
        $scope.Cnombre = data.nombre;
        $scope.description = data.descripcion;
        $scope.imagen=data.imagen;
        $scope.CatId=data.objeto
        if(data.exentos==undefined)$scope.exento=false;
        else $scope.exento=data.exentos;
    }

    $scope.editCat = function(data){
        console.log(data)
        var formData = new FormData();
        formData.append("name", name);
        formData.append("file", $scope.file); 
        //console.log('/empresa/editar/catalogo?username='+String($cookies.get('empresa'))+'&descripcion='+$scope.description+'&name='+data.objeto+'&catalogo='+data.catalogo+'&exento='+String($scope.exento))
        editar.catalogo(String($cookies.get('empresa')),$scope.description,data.objeto,data.catalogo,String($scope.exento),formData,function(response){
            console.log(response);
            $scope.init();
        })
    }

    $scope.eliminarCat=function(){
        var data={
            empresa:$cookies.get('empresa'),
            cat:$scope.col
        };
        $http({
            method:'POST',
            url:'/eliminar/cat',
            data:data
        }).
        then(function(response){
            console.log(response.data)
            if(response.data.err==0){
                $scope.init()                
                $scope.reset();
            }
        })
    }

    $scope.eliminarItem=function(id){
        console.log(id)
        var i=$scope.i;
        user=$window.prompt('Escriba su nombre de usuario')
        password=$window.prompt('Escriba la contraseña')
        data={
            'user':user,
            'password':password
        }
        $http({
            method:'POST',
            url:'http://34.237.241.0:2500/login/',
            data:data
        }).
        then(function(response){
            //console.log(response.data);
            info = response.data;
            console.log(info);
            switch (info.err) {
                case 0:
                    if($window.confirm('Desea eliminar el producto')){
                        var data={
                            objeto:id,
                            empresa:$cookies.get('empresa'),
                            cat:$scope.col
                        };
                        $http({
                            method:'POST',
                            url:'/eliminar/objeto',
                            data:data
                        }).
                        then(function(response){
                            console.log(response.data);
                            if($scope.filt=='')$scope.inventario[$scope.col].splice(i,1);
                            $scope.init()                
                            $scope.reset();
                            
                        })
                    }
                    break
                case 1:
                    $window.alert('Error de usuario');
                    break
                case 2:
                    $window.alert('Error de contraseña')
                    break
            }
        })
    }

    $scope.windowalrt=function(texto){
        $window.alert(texto)
    }

    $scope.AddDist=function(objeto,proveedor){
        var data={
            objeto:objeto,
            proveedor:proveedor
        };
        $http({
            method:'POST',
            url:'/add/proveedor',
            data:data
        }).
        then(function(response){
            console.log(response.data); 
            $scope.ActualObject(response.data)
        })
    }

    $scope.AddProve=function(){
        if($scope.dist.indexOf($scope.newProv)==-1){
            console.log($scope.newProv,$scope.idobject)
            $scope.dist.push($scope.newProv)
            $scope.AddDist($scope.idobject,$scope.newProv)
        }
        else $window.alert("El distribuidor ya se encuentra asignado a el producto")
    }

    $scope.removeDist=function(i){
        $scope.dist.splice(i,1);
        var data={
            objeto:$scope.idobject,
            proveedor:$scope.dist
        };
        $http({
            method:'POST',
            url:'/add/proveedor',
            data:data
        }).
        then(function(response){
            $scope.ActualObject(response.data)
        })
    }

    $scope.crearDist=function(distribuidor){
        if($window.confirm('Desea crear al distribuidor '+distribuidor)){
            var data={
                empresa:$cookies.get('empresa'),
                nombre:distribuidor
            };
            $http({
                method:'POST',
                url:'/crear/proveedor',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.LookDist()
            })
        }    
    }

    $scope.ActualObject=function(y){
        console.log(y,$scope.filt)
        //$scope.inventario[$scope.col][$scope.i]=y
        if(!$scope.filt){
            for (let i = 0; i < $scope.inventario[$scope.col].length; i++) {
                if($scope.inventario[$scope.col][i]._id==$scope.idobject){
                    $scope.inventario[$scope.col][i]=y
                    $scope.indexAllInvt()         
                    break
                }
            }
        }
        else{
            var fin=false
            for (let i = 0; i < $scope.keys.length; i++) {
                const key = $scope.keys[i].nombre;
                //console.log(key)
                for (let j = 0; j < $scope.inventario[key].length; j++) {
                    if($scope.inventario[key][j]._id==$scope.idobject){
                        $scope.inventario[key][j]=y
                        fin=true
                        $scope.indexAllInvt()         
                        break
                    }
                }
                if(fin)break
            }
        }
    }

    $scope.set = function(){
        //si se cre una categoria
        if($scope.colc){
            console.log('creando categoria');
            crear.categoria($cookies.get('empresa'),$scope.nombre,$scope.CatType,function(response){
                $scope.colleccion=$scope.nombre
                console.log(response.data);
                //$scope.init()
                $scope.page($scope.nombre,1)
            })
        }
        if($scope.objeto){
            //crear un objeto
            if($scope.edit == false){
                console.log('creando objeto',$cookies.get('empresa'),$scope.colleccion,$scope.nombre,$scope.precio,$scope.cantidad,$scope.codigo,$scope.Execento,$scope.description,$scope.Incatalogo);
                crear.objeto($cookies.get('empresa'),$scope.colleccion,$scope.nombre,$scope.precio,$scope.cantidad,$scope.codigo,$scope.Execento,$scope.description,$scope.Incatalogo,$scope.unidad,$scope.dolares,$scope.Contenido,$scope.convertionFac,$scope.TrgMin,$scope.TrgMax,$scope.Contenido,$scope.convertionFac,function(response){
                    console.log(response.data);
                    if(response.data.err==0){
                        var Object=response.data
                        if($scope.file != undefined){
                            var formData = new FormData();
                            formData.append("name", name);
                            formData.append("file", $scope.file);
                            console.log(formData);
                            crear.Image($cookies.get('empresa'),response.data.data._id,formData,function(response){
                                $scope.init()
                                $scope.reset();
                            })
                        }
                        else{
                            $scope.init()              
                            $scope.reset();
                        }
                    }
                    else{
                        $window.alert('Error creando el objeto')
                    }
                })
            }
            //edita un objeto
            else{
                console.log($scope.idobject,$scope.precio,$scope.cantidad,$scope.nombre,$scope.codigo,$scope.Execento,$scope.description,$scope.Incatalogo)
                editar.objeto($scope.idobject,$scope.precio,$scope.cantidad,$scope.nombre,$scope.codigo,$scope.Execento,$scope.description,$scope.Incatalogo,$scope.unidad,$scope.dolares,$scope.Contenido,$scope.convertionFac,$scope.puntos,$cookies.get('user'),function(response){
                    console.log(response.data);
                    var objet=response.data
                    if($scope.file != undefined){
                        var formData = new FormData();
                        formData.append("name", name);
                        formData.append("file", $scope.file);
                        console.log(formData);
                        crear.Image($cookies.get('empresa'),response.data._id,formData,function(response){
                            $scope.ActualObject(objet)
                            $scope.page(null,4)
                        })
                    }
                    else{
                        $scope.ActualObject(objet)           
                    }
                })
            }
        }
        if($scope.catl){
                //console.log(JSON.parse(data))
                //data=JSON.parse(data);
                //envio del archivo
                var formData = new FormData();
                formData.append("name", name);
                formData.append("file", $scope.file); 
                console.log(formData);
                crear.catalogo(String($cookies.get('empresa')),$scope.description,$scope.objet,null,String($scope.exento),formData,function(response){
                    console.log(response);
                    $scope.init();
                })
        }
    }

    $scope.precrear=function(inCat){
        $scope.Incatalogo=Boolean(inCat==1)
        $scope.objeto=true;
        $scope.set()
    }

    $scope.cambiarcantidad = function(cantidad){
        console.log(cantidad)
        editar.objeto($scope.idobject,$scope.precio,$scope.cantidad,$scope.nombre,$scope.codigo,$scope.Execento,$scope.description,$scope.Incatalogo,$scope.unidad,$scope.dolares,$scope.Contenido,$scope.convertionFac,$cookies.get('user'),function(response){
            console.log(response.data);
            $scope.init();
        })
    }

    $scope.SetMinMax=function(){
        if($scope.TrgMin>$scope.TrgMax){
            var aux=$scope.TrgMin;
            $scope.TrgMin=$scope.TrgMax
            $scope.TrgMax=aux;
        }
        $http({
            method:'GET',
            url:'/trigger/'+$scope.idobject+'/'+$scope.TrgMin+'/'+$scope.TrgMax
        }).
        then(function(response){
            console.log(response.data);
            $scope.ActualObject(response.data)
        })
    }

    $scope.findlotes=function(lotes){
        var data={
            lotes:lotes
        };
        $http({
            method:'POST',
            url:'/find/lotes/',
            data:data
        }).
        then(function(response){
            console.log(response.data.data);
            $scope.lotes=response.data.data
        })
    }

    $scope.editar=function(data,col,i){
        console.log(data,col);
        $scope.empresa=$cookies.get('empresa')
        $scope.idobject=data._id;
        $scope.colleccion=col;
        $scope.nombre=data.nombre;
        $scope.precio=data.precio;
        $scope.cantidad=data.cantidad;
        $scope.codigo=data.codigo;
        $scope.description=data.descripcion,
        $scope.Execento=data.exentos,
        $scope.Contenido=data.contenidoNeto;
        $scope.unidad=data.unidad
        $scope.convertionFac=data.convertFactor
        $scope.TrgMin=data.trigger.min
        $scope.TrgMax=data.trigger.max
        $scope.dolares=data.Dolares
        $scope.msg=false;
        $scope.colc = false;
        $scope.objeto = true;
        $scope.catl=false;
        $scope.colleccion=col;
        $scope.edit=true;
        $scope.Incatalogo=data.Incatalogo;
        $scope.dist=data.distribuidores;
        $scope.puntos=data.puntos
        $scope.i=i
        $scope.findlotes(data.lotes)
    }

    //busca los objetos sin catalogo
    $scope.objt = function(){
        $scope.ObtWcat = new Array(0)
        for (let i = 0; i < $scope.collections.length; i++) {
            var objs = $scope.inventario[$scope.collections[i]];
            for (let j = 0; j < objs.length; j++) {
                if(objs[j].catalogo==null) $scope.ObtWcat.push(objs[j])
            }
        }
        console.log($scope.ObtWcat)
    }

    //agregar al carrito, almacena los cambios del carrito
    $scope.agregarCarrito = function(data){
        //conversion del valor a colones
        if(data.Dolares){
            data.precio=$scope.currency*data.precio;
            data.Dolares=false
        }
        switch (Number($scope.DescType)) {
            case 1:
                if($scope.Descuento<$scope.AmontReser) var descuento=data.precio*$scope.Descuento;
                else var descuento=0;
                break;
            case 2:
                if($scope.Descuento<100) var descuento=Number($scope.AmontReser)*Number(data.precio)*$scope.Descuento/100;
                else var descuento=0;
                break;
            case 3:
                if($scope.Descuento<Number($scope.AmontReser)*Number(data.precio))var descuento=$scope.Descuento
                else var descuento=0;
                break
            default:
                var descuento=0;
                break;
        }
        console.log($scope.Descuento,$scope.AmontReser,descuento,$scope.DescType)
        //let info =JSON.parse($scope.cliente);
        if($scope.cliente != undefined && $scope.AmontReser != undefined && $scope.AmontReser>0){
            console.log(data)
            if(data.exentos){
                $scope.carrito.push({
                    objeto:CatId,
                    nombre:$scope.Cnombre,
                    cantidad:$scope.AmontReser,
                    descuento:descuento,
                    //cliente:info._id,
                    //Ncliente:info.nombre,
                    total:Number($scope.AmontReser)*Number(data.precio)-descuento,
                    precio:data.precio,
                    impuesto:0,
                    agente:$cookies.get('id')
                })
                $scope.preciofinal+=(Number($scope.AmontReser)*Number(data.precio))-descuento
            }
            else{
                $scope.carrito.push({
                    objeto:data.objeto,
                    nombre:$scope.Cnombre,
                    cantidad:$scope.AmontReser,
                    descuento:descuento,
                    //cliente:info._id,
                    //Ncliente:info.nombre,
                    total:(Math.floor(Number($scope.AmontReser)*Number(data.precio)*1.13*1000)/1000)-descuento,
                    precio:data.precio,
                    impuesto:Math.floor(data.precio*0.13*1000)/1000,
                    agente:$cookies.get('id')
                })            
                $scope.preciofinal+=(Number($scope.AmontReser)*Math.floor(data.precio*1.13*1000)/1000)-descuento
            }
            $scope.AmontReser=0;
            $scope.Descuento=0;
        }
    }

    //eliminar del carrito
    $scope.removecarrito = function(i){
        console.log(i);
        $scope.preciofinal-=Number($scope.carrito[i].precio)*Number($scope.carrito[i].cantidad);
        $scope.carrito.splice(i,1);
    }

    //enviar cuenta
    $scope.envioCuenta=function(i){
        if($scope.Transporte != undefined){
            $scope.carrito[i]['cliente']=JSON.parse($scope.cliente)._id;
            console.log('reservando...')
            editar.reserva($scope.carrito[i],function(response){
                console.log(response.data);
                $scope.reservasArray.push(response.data._id)
                if($scope.carrito.length>i+1)$scope.envioCuenta(i+1);
                else{
                    console.log('haciendo pedido');
                    crear.pedido($cookies.get('empresa'),$cookies.get('id'),$scope.reservasArray,$scope.contado,$scope.Abono,$scope.Transporte,$scope.preciofinal,function(response){
                        console.log(response.data);
                        $scope.carrito=[]
                        $scope.reservasArray=[]
                        $scope.preciofinal=0;
                        $scope.abono=0;
                    })
                }
            })
        }
    }

    //agragar nombre de cliente a las reservas
    $scope.NombreResrvas=function(){
        for (let i = 0; i < $scope.resvObj.length; i++) {
            $scope.resvObj[i].Ncliente= $scope.Jclient[$scope.resvObj[i].cliente];
        }
    }

    //se obtienen las reservas
    $scope.obtenerReservas=function(id){
        $scope.page(null,3);
        busquedas.reservas(id,function(reservas){
            $scope.resvObj=reservas;
            $scope.NombreResrvas();
        })
    }


    $scope.resumen = function(){
        $scope.objCant = 0;
        $scope.Stock = 0;
        if($scope.collections.length>0){
            console.log($scope.ctl)
            if(!$scope.ctl)$scope.ctl=$scope.collections[0];
            console.log($scope.ctl)
            for (let i = 0; i < $scope.collections.length; i++) {
                for (let j = 0; j < $scope.inventario[$scope.collections[i]].length; j++) {
                    $scope.objCant = $scope.objCant+$scope.inventario[$scope.collections[i]][j].cantidad;
                }
                $scope.Stock=$scope.Stock+$scope.inventario[$scope.collections[i]].length
            }
            $scope.catCan=0;
            for (let i = 0; i < $scope.CatKey.length; i++) {
                for (let j = 0; j < $scope.catalogo[$scope.CatKey[i]].length; j++) {
                    //console.log($scope.catalogo[$scope.CatKey[i]][j])
                    console.log($scope.catalogo[$scope.CatKey[i]][j].exentos)
                    $scope.catCan = $scope.catCan+1;
                }
            }
        }
    }

    $scope.indexAllInvt=function(){
        $scope.AllInvt=[]
        for (let i = 0; i < $scope.keys.length; i++) {
            const key = $scope.keys[i].nombre;
            for (let j = 0; j < $scope.inventario[key].length; j++) {
                $scope.AllInvt.push($scope.inventario[key][j]);
                
            }
        }
    }

    $scope.colleccionfilter=function(type){
        console.log($scope.inventario)
        if(Object.keys($scope.inventario).length>0){
            $scope.keys= $scope.inventario['keys']
            if(!$scope.col || $scope.CatType!=type){
                //si no hay coleccion definida
                for (let i = 0; i < $scope.keys.length; i++) {
                    //console.log(keys[i].tipo,type,$scope.collections)
                    if($scope.keys[i].tipo==type){
                        $scope.col=$scope.keys[i].nombre
                        break
                    }
                }
            }
        }
        $scope.CatType=type;
        $scope.indexAllInvt()
    }

    $scope.chgColeccition = function(x){
        $scope.col=x
    }

    $scope.chgCollCat = function(x){
        $scope.key=x
    }

    $scope.reset= function(){
        $scope.msg=false;
        $scope.colc = false;
        $scope.objeto= false;
        $scope.catl=false;
        $scope.resv =false;
        $scope.msg=false;
        $scope.empresa=null
        $scope.idobject=null;
        $scope.colleccion=null;
         $scope.nombre=null
        $scope.precio=null
        $scope.cantidad=null
        $scope.codigo=null
        $scope.description=null
        $scope.Execento=null
        $scope.Contenido=null
        $scope.unidad=null
        $scope.convertionFac=null
        $scope.TrgMin=null
        $scope.TrgMax=null
        $scope.dolares=null
        $scope.file=undefined;
        $http({
            method:'GET',
            url:'/get/logs/emp/'+$cookies.get('empresa')
        }).
        then(function(response){
            console.log(response.data);
            $scope.regs=response.data.data
        })
    }

    $scope.LookDist = function(){
        $http({
            method:'GET',
            url:'/get/provedores/'+$cookies.get('empresa')
        }).
        then(function(response){
            $scope.Distribuidores=response.data.data.keys
            console.log($scope.Distribuidores)
        })
    }
    $scope.LookDist()

    busquedas.transporte($cookies.get('empresa'),function(response){
        console.log(response)
        $scope.transporte=response;
    })
    busquedas.initmensajes($cookies.get('id'),function(response){
        console.log(response)
    })

    //init
    $scope.init=function(){
        $scope.reset()
        busquedas.inventario($cookies.get('empresa'),function(col,invt){
            console.log(col,invt)
            $scope.colectionALL=col
            col.splice(col.indexOf('keys'),1);
            colect=col
            $scope.inventario= invt;
            $scope.colleccionfilter($scope.CatType);
        })
        //$scope.client();
    }

    $scope.init();

    //cambiar de tipo un categoria
    $scope.ChangCat=function(cat,type){
        //console.log(cat,type)
        $http({
            method:'GET',
            url:'/cambiar/cat/'+$cookies.get('empresa')+'/'+cat+'/'+type
        }).
        then(function(response){
            console.log(response.data);
        })
    }

    //creacion del archivo
    $scope.MakeArchivo=function(){
        var porcentaje=0
        $scope.AllDataPT=[]
        $scope.AllDataMP=[]
        $scope.AllDataTP=[]
        $scope.AllDataET=[]
        $scope.AllDataEN=[]
        if($scope.keys){
            for (let i = 0; i < $scope.keys.length; i++) {
                const keys = $scope.keys[i];
                //console.log(keys)
                for (let j = 0; j < $scope.inventario[keys.nombre].length; j++) {
                    const producto = $scope.inventario[keys.nombre][j];
                    producto['familia']=keys.nombre
                    switch (keys.tipo) {
                        case 'PT':
                            $scope.AllDataPT.push(producto)
                            break;
                        case 'MP':
                            $scope.AllDataMP.push(producto)
                            break;
                        case 'EN':
                            $scope.AllDataEN.push(producto)
                            break;
                        case 'ET':
                            $scope.AllDataET.push(producto)
                            break
                        case 'TP':
                            $scope.AllDataTP.push(producto)
                            break; 
                        default:
                            break;
                    }
                    console.log(i*100/$scope.keys.length,j*(1/$scope.keys.length)*100/$scope.keys.length)
                    $scope.barsize['width']=String(i*100/$scope.keys.length+j*(1/$scope.keys.length)*100/$scope.keys.length)+'%'
                }
            }
            $scope.barsize={
                'width': '100%',
                'height': '30px',
                'background-color': 'blue',
            }
        }
        else{
            $scope.barsize={
                'width': '0%',
                'height': '30px',
                'background-color': 'green',
            }
        }
        
        //console.log($scope.AllDataMP)
    }

    //manejo del archivo
    $scope.printArchivo=function(){
        console.log($scope.Archivo,$scope.idobject);
        var r= $window.confirm("Seguro que desea subir el archivo "+$scope.Archivo.name+" como archivo de inventario")
        console.log(r)
        if(r){
            var formData = new FormData();
            $scope.confirmSend=false
             $scope.barsize={
                'width': '1%',
                'height': '30px',
                'background-color': 'green',
            }
            formData.append("name", $scope.Archivo.name);
            formData.append("file", $scope.Archivo); 
            crear.Image($cookies.get('empresa'),$scope.idobject,formData,function(response){
                //respuesta en crudo: response
                console.log(response)
                $scope.DataArchivo=response
                $scope.page(null,6)
                var popup = document.getElementsByClassName("modalActive")[0];
                //console.log(popup)
                //for show model
                popup.click()
            })
        }
    }

    $scope.EditFile=function(i){
        $scope.confirmSend=true;
        var data={
            objeto:$scope.DataArchivo[i]['IDENTIFICADOR'],
            precio:$scope.DataArchivo[i]['PRECIO'],
            cantidad:$scope.DataArchivo[i]['STOCK EN KG'],
            min:$scope.DataArchivo[i]['MINIMO'],
            max:$scope.DataArchivo[i]['MAXIMO'],
            contenido:$scope.DataArchivo[i]['PESO'],
            user:$cookies.get('user')
        };
        //console.log(data)
        $http({
            method:'POST',
            url:'/simple/edit',
            data:data
        }).
        then(function(response){
            console.log(i+1,$scope.DataArchivo.length);
            $scope.barsize['width']=String((i+1)*100/$scope.DataArchivo.length)+'%'
            if(i+1<$scope.DataArchivo.length) $scope.EditFile(i+1)
            else{
                $scope.barsize={
                    'width': '100%',
                    'height': '30px',
                    'background-color': 'blue',
                }
                $scope.init()
            }
        })
    }
}])

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return null
    }
})

app.directive('uploaderModel', ["$parse", function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) 
		{
			iElement.on("change", function(e)
			{
				$parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
                console.log('parseado', iAttrs.name)
                if(iAttrs.name=="Archivo")scope.printArchivo()
			});
		}
	};
}])
