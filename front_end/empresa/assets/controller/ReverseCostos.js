//var dolat

app.controller('view',['$scope','$http','$cookies','$window','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,crear,busquedas,editar,links){
    console.log($cookies.getAll());
    console.log($window.data)
    $scope.fecha=new Date()
    $scope.AllData=$window.data
    $scope.Dol=false
    dolar=$scope.AllData.Dolar
    //if($scope.AllData){
    //    if($window.confirm('Desea imprimir esta factura')){
    //        $window.print()
    //    }
    //    //$window.close()
    //}
    //else $window.close()

    $scope.calcular=function(){
        //impuesto venta detallista x
        $scope.IVD=$scope.PF*(1-1/(1+$scope.AllData.extras.detallista.impuesto/100));
        //utilidad del detallista x
        $scope.UD=$scope.PF/(1+$scope.AllData.extras.detallista.impuesto/100)*(1-1/(1+$scope.AllData.extras.detallista.utilidad/100))
        //Precio venta distribuidor x
        $scope.PVD=$scope.PF-$scope.UD-$scope.IVD
        //Precio de venta al distribuididor x
        //console.log(($scope.PVD+$scope.CD+$scope.AllData.extras.distribuidor.gasto),(1-$scope.AllData.extras.distribuidor.comision/100))
        $scope.DPV=$scope.PVD*(1-$scope.AllData.extras.distribuidor.comision/100)
        //comision del distribuidor x
        //console.log(($scope.PVF+$scope.AllData.extras.distribuidor.gasto),(($scope.AllData.extras.distribuidor.comision/100)))
        $scope.CD=($scope.PVD-$scope.DPV)
        //impuesto venta distribuidor
        //$scope.IVDT=$scope.PVD*(1-1/(1+$scope.AllData.extras.distribuidor.impuesto/100));
        //precio venta fabrica *
        $scope.PVF=($scope.DPV)/(1+$scope.AllData.extras.distribuidor.utilidad/100)-$scope.AllData.extras.distribuidor.gasto
        //Comision de la fabrica
        //$scope.CF=($scope.PVF-$scope.PVF)*($scope.AllData.extras.fabrica.impuestoV);
        //utilidad del ditribuidor *
        $scope.DU=($scope.PVF+$scope.AllData.extras.distribuidor.gasto)*($scope.AllData.extras.distribuidor.utilidad/100)
        //Precio costo fabrica *
        $scope.PCF=($scope.PVF)/((1+$scope.AllData.extras.fabrica.utilidad/100)*(1+$scope.AllData.extras.fabrica.impuesto/100))
        //utilidad de la fabrica *
        $scope.U2=$scope.PVF/($scope.AllData.calculos.PCF*(1+$scope.AllData.extras.fabrica.impuesto/100))-1
        $scope.UF=($scope.AllData.calculos.PCF)*($scope.U2);
        //impuesto de la fabrica
        //console.log(1-1/(1+$scope.AllData.extras.fabrica.impuestoV/100))
        $scope.IF=($scope.AllData.calculos.PCF+$scope.UF)*($scope.AllData.extras.fabrica.impuesto/100);
    }
    $scope.PF=Math.floor($scope.AllData.calculos.PFR*10000)/10000;
    $scope.PFC=$scope.PF*dolar
    $scope.calcular()

    $scope.calcularDol=function(){
        //console.log($scope.PFC)
        if($scope.PF==''){
            $scope.PFC=0
            $scope.calcular()
        }
        else{
            if(Number($scope.PF)){
                //$scope.PF=Number($scope.PFC)
                $scope.PFC=($scope.PF*dolar)
                $scope.calcular()

            }
            else{
                $scope.PFC=0
                $scope.PF=0
                $scope.calcular()
            }
        }
        //console.log($scope.PF,$scope.PFC)
    }

    $scope.precalcular=function(){
        console.log($scope.PFC)
        if($scope.PFC==''){
            $scope.PF=0
            $scope.calcular()
        }
        else{
            if(Number($scope.PFC)){
                //$scope.PF=Number($scope.PFC)
                $scope.PF=Number($scope.PFC/dolar)
                $scope.calcular()

            }
            else{
                $scope.PFC=0
                $scope.PF=0
                $scope.calcular()
            }
        }
        //console.log($scope.PF,$scope.PFC)
    }
}])

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return 0
    }
})