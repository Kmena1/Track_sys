var app =angular.module("app",['ui.bootstrap','ngCookies','ui-leaflet','ngJsonExportExcel']);
app.controller('MisPedidos',['$scope','$http','$cookies','$window','busquedas','editar','crear','links',function($scope,$http,$cookies,$window,busquedas,editar,crear,links){
    console.log($cookies.getAll())
    $scope.clientes=[]
    $scope.filterBy={
        nombre:'',
        region:'',
    }

    $scope.excelParm={
        _id: '_id', 
        'nombre': 'nombre', 
        codigo: 'codigo',
        Razon:'Razon social',
        Tdoc:'Tipo de documento',
        cedula:'Cedula',
        correoFact:'Correo para factura',
        correoCortesia:'Correo de cortesia',
        lat:'Latitud',
        lon:'Longitud',
        Provincia:'Provincia',
        Canton:'Canton',
        Distrito:'Distrito',
        direccion:'direccion',
        'pago.transporte':'Transporte',
        Actividad:'Actividad',
        size:'Tamaño',
        tipo:'tipo',
        'pedidosSelc.Representante.nombre':'Representante',
        'pedidosSelc.Representante.ced':'Representante cedula',
        'pedidosSelc.Representante.cargo':'Representante cargo',
        'pedidosSelc.Representante.email':'Representante email',
        'pedidosSelc.Representante.tel':'Representante telefono',
        'pedidosSelc.Representante.celular':'Representante celular',
        'pedidosSelc.Representante.direccion':'Representante direccion',
        'pago.contado':'Contado',
        'pago.dias':'Dias plazo',
        'credito.limite':'Limite de credito',
        'credito.deuda':'Deuda Actual',
    }

    $scope.reverse=false;
//cambio de pagina
    $scope.menu=[]
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }
//contro del lateral
    $scope.invertRevers=function(){
        $scope.reverse=!$scope.reverse
    }
    $scope.selectoption=function(x,i){
        if(i==1) {
            x=JSON.parse(x)
        }
        if(i==2){ 
            //x=$scope.Solicitudes[x]
            $scope.editar=false
        }
        else $scope.editar=true
        console.log(x)
        $scope.pedidosSelc=x
        $scope.region=x.region
    }
//filtros
    $scope.filterCheck=function(){
        if($scope.filterBy.codigo==null)$scope.filterBy.codigo=''
        //if($scope.filterBy.)
    }
//indefinidas
    $scope.UserName=function(name){
        console.log(name)
        if($scope.Jusers[name]==undefined){
            $http({
                method:'GET',
                url:'/find/user/'+name
            }).
            then(function(response){
                console.log(response.data);
                $scope.Jusers[name]=response.data.nombre
                $scope.Naprvby= response.data.nombre
            })
        }
        else $scope.Naprvby= null
    }

//edicion
    $scope.editarCliente=function(){
        $scope.pedidosSelc.lat=Number($scope.pedidosSelc.lat);
        $scope.pedidosSelc.lon=Number($scope.pedidosSelc.lon);
        console.log($scope.pedidosSelc)
        var data={
            id:$scope.pedidosSelc._id,
            data:$scope.pedidosSelc,
        };
        $http({
            method:'POST',
            url:'/editar/cliente/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            if(!$scope.editar){
                var data={
                    id:$cookies.get('empresa'),
                    crear:true,
                    solicitud:response.data._id
                };
                $http({
                    method:'POST',
                    url:'/sumit/solicitud/',
                    data:data
                }).
                then(function(response){
                    console.log(response.data);
                    var data={
                        clientes:response.data
                    };
                    $http({
                        method:'POST',
                        url:'/clientes/get/array',
                        data:data
                    }).
                    then(function(response){
                        console.log(response.data);
                        $scope.Solicitudes=response.data.data
                        $scope.clientesSearch(0)
                    })
                })
            }
        })
    }

    $scope.aprovarReferencia=function(x){
        console.log($scope.pedidosSelc.referencias[x])
        ref=$scope.pedidosSelc.referencias[x]
        if(ref.nombre && ref.plazo && ref.Monto){
            if($window.confirm('Desea aprovar al contacto '+ref.nombre)){
                $scope.pedidosSelc.referencias[x].aprovadoBy=$cookies.get('user')
            }
            else{
                $scope.pedidosSelc.referencias[x].aprovado=false
            }
        }
        else{
            $scope.pedidosSelc.referencias[x].aprovado=false
        }
    }
//revers geojson
    $scope.localizacion=function(){
        if($scope.pedidosSelc.lat&&$scope.pedidosSelc.lon){
            busquedas.reverserGeoJson($scope.pedidosSelc.lat,$scope.pedidosSelc.lon,15,function(response){
                console.log(response)
                $scope.position=response.data.display_name
            })
        }
    }
//solicitudes
    $scope.rechazarSolicitud=function(id){
        var data={
            id:$cookies.get('empresa'),
            crear:false,
            solicitud:id
        };
        $http({
            method:'POST',
            url:'/sumit/solicitud/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            var data={
                clientes:response.data
            };
            $http({
                method:'POST',
                url:'/clientes/get/array',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.Solicitudes=response.data.data
                //$scope.clientesSearch(0)
            })
        })
    }
//zonas
    $scope.actualizarZona=function(){
        console.log($scope.pedidosSelc._id)
        if($scope.pedidosSelc && $scope.pedidosSelc.region != $scope.region){
            if($window.confirm('Desea actualizar la region')){
                for (let i = 0; i < $scope.szonas.length; i++) {
                    const zona = $scope.szonas[i];
                    //eliminar el cliente
                    if(zona.region==$scope.pedidosSelc.region){
                        //console.log($scope.szonas[i].clientes)
                        $scope.szonas[i].clientes.splice($scope.szonas[i].clientes.indexOf($scope.pedidosSelc._id),1);
                        //console.log($scope.szonas[i].clientes)
                    }
                    //agregar cliente
                    if(zona.region==$scope.region){
                        //console.log($scope.szonas[i].clientes)
                        $scope.szonas[i].clientes.push($scope.pedidosSelc._id)
                        idregion=zona.codigo
                        //console.log($scope.szonas[i].clientes)
                    }
                }
                var data={
                    id:$scope.pedidosSelc._id,
                    region:$scope.region,
                    regionId:idregion,
                    myclnid:$scope.szonas,
                    mycln:$scope.zonasid
                };
                $http({
                    method:'POST',
                    url:'/change/cliente/zona/',
                    data:data
                }).
                then(function(response){
                    console.log(response.data);
                    $scope.szonas=response.data.id
                    $scope.clientesSearch(0)
                })
            }
            else{
                $scope.region = $scope.pedidosSelc.region
            }
        }
    }
//archivo de clientes
    $scope.uploadFile=function(i){
        if($scope.pedidosSelc){
            //verifica si existe el nombre
            if($scope.pedidosSelc.archivos){
                switch (i) {
                    case 1:
                        if($scope.pedidosSelc.archivos.Arc1) {
                            nombre=$scope.pedidosSelc.archivos.Arc1.split(".")[0]
                            ext=$scope.pedidosSelc.archivos.Arc1.split(".")[1]
                        }
                        else{
                            nam=$scope.file.name.split(".")[0]
                            ext=$scope.file.name.split(".")[1]
                            nombre=nam+new Date().getTime()
                        }
                        $scope.pedidosSelc.archivos.Arc1=nombre+'.'+ext
                        $scope.pedidosSelc.archivos.Arc1ref=$scope.file.name
                        break;
                    case 2:
                        if($scope.pedidosSelc.archivos.Arc2) {
                            nombre=$scope.pedidosSelc.archivos.Arc2.split(".")[0]
                            ext=$scope.pedidosSelc.archivos.Arc2.split(".")[1]
                        }
                        else{
                            nam=$scope.file.name.split(".")[0]
                            ext=$scope.file.name.split(".")[1]
                            nombre=nam+new Date().getTime()
                        }
                        $scope.pedidosSelc.archivos.Arc2=nombre+'.'+ext
                        $scope.pedidosSelc.archivos.Arc2ref=$scope.file.name
                        break;
                    case 3:
                        if($scope.pedidosSelc.archivos.Arc3) {
                            nombre=$scope.pedidosSelc.archivos.Arc3.split(".")[0]
                            ext=$scope.pedidosSelc.archivos.Arc3.split(".")[1]
                        }
                        else{
                            nam=$scope.file.name.split(".")[0]
                            ext=$scope.file.name.split(".")[1]
                            nombre=nam+new Date().getTime()
                        }
                        $scope.pedidosSelc.archivos.Arc3=nombre+'.'+ext
                        $scope.pedidosSelc.archivos.Arc3ref=$scope.file.name
                        break;
                    default:
                        break;
                }
            }
            else{
                nam=$scope.file.name.split(".")[0]
                ext=$scope.file.name.split(".")[1]
                nombre=nam+new Date().getTime()
                $scope.pedidosSelc.archivos={
                    Arc1:null,
                    Arc2:null,
                    Arc3:null
                }
                switch(i){
                    case 1:
                        $scope.pedidosSelc.archivos.Arc1=nombre+'.'+ext
                        break
                    case 2:
                        $scope.pedidosSelc.archivos.Arc2=nombre+'.'+ext
                        break
                    case 3:
                        $scope.pedidosSelc.archivos.Arc3=nombre+'.'+ext
                        break    
                }
            }
            //envio del archivo
            var formData = new FormData();
            formData.append("name", name);
            formData.append("file", $scope.file); 
            console.log(formData);
            $http({
                method:'POST',
                url:'/empresa/cliente/archivo?username='+String($cookies.get('empresa'))+'&name='+nombre+'&header='+ext,
                data:formData,
                headers:{
                    "Content-type": undefined
                }, 
                transformRequest: angular.identity
            }).
            then(function(response){
                console.log(response.data);
                //actualizacion de los nombre de archivo
                var data={
                    id:$scope.pedidosSelc._id,
                    archivos:$scope.pedidosSelc.archivos
                };
                $http({
                    method:'POST',
                    url:'/editar/archivos/cliente/',
                    data:data
                }).
                then(function(response){
                    console.log(response.data);
                })
            })
        }
    }

    $scope.DescargarArchivo=function(nombre,filename){
        if(nombre && filename) $window.open('/look/file/'+$cookies.get('empresa')+'/'+nombre+'/'+filename)
    }

    $scope.multisave=function(data,i){
        if(data.length>i){
            $scope.info='Cargando informacion '+i+1+'/'+data.length
            if(data[i]._id){
                var info=data[i]
                //contado
                if(info.Contado=='false' || Number(info.Contado)==0)info.contado=false
                else info.Contado=true
                //tamano
                if(Number(info.Tamaño)){
                    if(info.Tamaño>3)info.Tamaño=3
                }
                else info.Tamaño=0
                //tipo
                if(Number(info.tipo)){
                    if(info.tipo>14)info.tipo=14
                }
                else info.tipo=0
                //
                if(Number(info['Tipo de documento'])){
                    if(info['Tipo de documento']>15)info['Tipo de documento']=15
                }
                else info['Tipo de documento']=0
                //envio de informacion
                var data={
                    data:info,
                };
                $http({
                    method:'POST',
                    url:'route',
                    data:data
                }).
                then(function(response){
                    //console.log(response.data.err);
                    switch (response.data.err) {
                        case 0:
                            $scope.multisave(data,i+1)
                            break;
                        case 1:
                            $scope.rechazados.push(data.nombre+':Cliente no existe')
                            $scope.multisave(data,i+1)
                        case 2:
                            $scope.rechazados.push(data.nombre+':Error en la busqueda del cliente')
                            $scope.multisave(data,i+1)
                        case 3:
                            $scope.rechazados.push(data.nombre+':infomacion no almacenada')
                            $scope.multisave(data,i+1)
                        default:
                            $scope.rechazados.push(data.nombre+':error desconocido')
                            $scope.multisave(data,i+1)
                            break;
                    }
                })
            }
            else{
                $scope.rechazados.push(data.nombre+':Falta de _id')
                $scope.multisave(data,i+1)
            }
        }
    }

    $scope.fileUploadSend=function(){
        console.log($scope.file)
        var formData = new FormData();
        formData.append("name", name);
        formData.append("file", $scope.Archivo); 
        console.log(formData);
        $http({
            method:'POST',
            url:'/empresa/clientes/excel?username='+String($cookies.get('empresa'))+'&name=Clientes_'+String($cookies.get('empresa')),
            data:formData,
            headers:{
                "Content-type": undefined
            }, 
            transformRequest: angular.identity
        }).
        then(function(response){
            console.log(response.data);
            /*$scope.rechazados=[]
            $scope.k=0;
            if($window.confirm('Desea actualizar ')){
                $scope.multisave(response.data,0)
            }*/
        })
    }
//inicio
    busquedas.init(function(user,empresa){
        //console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
        $http({
            method:'GET',
            url:'/find/company/'+$scope.empresa.id
        }).
        then(function(response){
            //console.log(response.data);
            $scope.empresa=response.data
            if($scope.empresa.SolCln.length>0){
                var data={
                    clientes:$scope.empresa.SolCln
                };
                $http({
                    method:'POST',
                    url:'/clientes/get/array',
                    data:data
                }).
                then(function(response){
                    console.log(response.data);
                    $scope.Solicitudes=response.data.data
                })
            }
        })
    });
    busquedas.transporte($cookies.get('empresa'),function(response){
        console.log(response)
        $scope.transporte=response;
    })

    $scope.clientesSearch=function(init){
        busquedas.clientes($cookies.get('empresa'),null,String(init),function(response){
            //console.log(response.max == response.last,response.max,response.last);
            if (response.max == response.last) {
                //llena el scope de respaldo
                $scope.clientes=$scope.clientes.concat(response.datos);
                console.log($scope.clientes)
            } 
            else 
            {
                $scope.clientes=$scope.clientes.concat(response.datos);
                $scope.clientesSearch(response.last)
            }
        })
    }
    $scope.clientesSearch(0)

    $scope.init=function(){
        $http({
            method:'GET',
            url:'/empresa/zonas/'+$cookies.get('empresa')
        }).
        then(function(response){
            $scope.szonas=response.data.id
            $scope.zonasid=response.data._id
            console.log($scope.szonas);
        })
    }
    $scope.init()

}])

app.directive('uploaderModel', ["$parse", function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) 
		{
			iElement.on("change", function(e)
			{
				$parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
                console.log('parseado', iAttrs)
                if(iAttrs.name=="Archivo")scope.fileUploadSend()
                else scope.uploadFile(Number(iAttrs.numero))
			});
		}
	};
}])

app.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return val != null ? parseInt(val, 10) : null;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
});

