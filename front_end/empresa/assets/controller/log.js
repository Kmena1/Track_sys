var imp1=13
var imp2=2

var app =angular.module("app",['ui.bootstrap','ngCookies','ui-leaflet']);
app.controller('log',['$scope','$http','$cookies','$window','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,crear,busquedas,editar,links){
    console.log($cookies.getAll());
    $scope.menu=[]
    $scope.links = links.links
//cambio de pagina
    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }
//buquedas
    $scope.getRegistros=function(){
        $http({
            method:'GET',
            url:'/get/logs/emp/'+$cookies.get('empresa')
        }).
        then(function(response){
            console.log(response.data);
            $scope.regs=response.data.data
        })
    }
//control del lateral
    $scope.SelccionSide=function(x){
        $scope.Regdata=x
    }
//inicio
    $scope.init=function(){
        busquedas.init(function(datos,extra,mensaje){
            $scope.users= datos;
            $scope.empresa= extra;
            $scope.mensaje=mensaje
            $scope.getRegistros()
            //$scope.reset()
        })
    }   
    $scope.init()

    
}])

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return null
    }
})

app.filter('impuesto',function(){
    return function(value,imp){
        //console.log(value,imp)
        if(imp)return value*(1+imp2/100)
        else return value*(1+imp1/100)
    }
})

app.filter('priceCal',function(){
    return function(ingredientes,data){
        //console.log(ingredientes,data)
        var dolar=data.dolar
        var material=data.material
        var size=data.size
        var colones=data.colones
        let Total=0
        if(ingredientes){
            for (let i = 0; i < ingredientes.length; i++) {
                const x = ingredientes[i];
                let impuesto=0
                if(material[x.objeto]){
                    if(!material[x.objeto].convertFactor)material[x.objeto].convertFactor=1
                    if(!material[x.objeto].exentos) impuesto=material[x.objeto].precio*imp1/100
                    else impuesto=material[x.objeto].precio*imp2/100
                    //precio=size*material[x.objeto].convertFactor*x.cantidad*(material[x.objeto].precio+impuesto)
                    //console.log(precio,Total)
                    //console.log(size,material[x.objeto].convertFactor,x.cantidad,material[x.objeto].precio)
                    if(material[x.objeto].Dolares) Total=Total+size*material[x.objeto].convertFactor*x.cantidad*(material[x.objeto].precio+impuesto)
                    else Total=Total+size*material[x.objeto].convertFactor*x.cantidad*(material[x.objeto].precio+impuesto)/dolar
                }
            }
            if(!colones)return Math.floor(Total*Math.pow(10,data.k))/Math.pow(10,data.k)
            else return Math.floor(Total*dolar*Math.pow(10,data.k))/Math.pow(10,data.k)
        }
        else return 0
    }
})

app.filter('calEmp',function(){
    return function(emp,dolar){
        manoObra=0
        for (let i = 0; i < emp.length; i++) {
            const empleado = emp[i];
            manoObra=manoObra+empleado.costo*empleado.cantidad
        }
        return Math.floor(manoObra/dolar*10000)/10000
    }
})