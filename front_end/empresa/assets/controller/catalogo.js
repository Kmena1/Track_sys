var imp1=13
var imp2=2

var app =angular.module("app",['ui.bootstrap','ngCookies','ui-leaflet']);
app.controller('catalogo',['$scope','$http','$cookies','$window','busquedas','editar','crear','links',function($scope,$http,$cookies,$window,busquedas,editar,crear,links){
//varibles de inicio
    $scope.filt=''
    $scope.carrito=[];
    $scope.clientes=[];
    $scope.preciofinal=0
    $scope.impuesto=0;

    var empresaID=$cookies.get('empresa')
    var userID=$cookies.get('id')

//saltos de pagina
    $scope.menu=[]
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    $scope.chgCollCat = function(x){
        console.log($scope.catalogo[x])
        $scope.col=x
    }
//busquedas iniciales
    busquedas.init(function(user,empresa){
        console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
    });
    busquedas.transporte($cookies.get('empresa'),function(response){
        console.log(response)
        $scope.transporte=response;
    })

//envio rapido
    $scope.agregadoRapido=function(){
        $scope.carrito=[]
        for (let i = 0; i < $scope.objtPed.length; i++) {
            objt=$scope.catCodigo[$scope.objtPed[i].cod]
            if(objt){
                if(objt.exentos) imp=imp2
                else imp=imp1
                let impuesto=(Math.floor((Number($scope.objtPed[i].cantidad)*Number(objt.precio))*1000)/1000-$scope.objtPed[i].bonificacion)*imp/100
                $scope.carrito.push({
                    objeto:objt.id,
                    codigo:objt.codigo,
                    Maximo:objt.cantidad,
                    nombre:objt.cantidad,
                    cantidad:$scope.objtPed[i].cantidad,
                    descuento:0,
                    Adescuento:$scope.objtPed[i].bonificacion,
                    Adescuento2:0,
                    Tdescuento:3,
                    //cliente:info._id,
                    //Ncliente:info.nombre,
                    total:(Math.floor((Number($scope.objtPed[i].cantidad)*Number(objt.precio))*1000)/1000-$scope.objtPed[i].bonificacion)*(1+imp/100),
                    precio:objt.precio,
                    excento:objt.exentos,
                    impuesto:impuesto,
                    agente:userID
                })
            }
        }
        console.log($scope.carrito)
        if($window.confirm('Seguro que desea enviar este pedido')){
            $scope.envioCuenta()
        }
    }

//control de agregar al carrito
    $scope.agregarCarrito = function(data){
        //conversion del valor a colones
        if(data.Dolares){
            data.precio=$scope.currency*data.precio;
            data.Dolares=false
        }
        ErrorDesc=false
        switch (Number($scope.DescType)) {
            case 1:
                if($scope.Descuento<$scope.AmontReser) {
                    var descuento=data.precio*$scope.Descuento;
                    var descuento2=0
                    //$scope.AmontReser=$scope.AmontReser+$scope.Descuento
                }
                else {
                    ErrorDesc=true;
                    $window.alert('Error en el descuento aplicado\nLa cantidad de la bonificacion no puede se mayor que la cantidad solicitada')
                    var descuento=0;
                }
                break;
            case 2:
                if($scope.Descuento<100 && $scope.Descuento>0) var descuento=Number($scope.AmontReser)*Number(data.precio)*$scope.Descuento/100;
                else {
                    ErrorDesc=true;
                    $window.alert('Error en el descuento aplicado\nLa cantidad del porcentaje del descuento no puede ser mayor a 100% o menor al 0%')
                    var descuento=0;
                }
                break;
            case 3:
                if($scope.Descuento<Number($scope.AmontReser)*Number(data.precio))var descuento=$scope.Descuento
                else {
                    ErrorDesc=true;
                    $window.alert('Error en el descuento aplicado\nLa cantidad del descuento no puede ser mayo que '+Number($scope.AmontReser)*Number(data.precio))
                    var descuento=0;
                }
                break
            case 4:
                if($scope.Descuento<$scope.AmontReser && $scope.Descuento2<100 && $scope.Descuento2>0){
                    var descuento=data.precio*$scope.Descuento
                    var descuento2=Number($scope.AmontReser)*Number(data.precio)*$scope.Descuento2/100;
                    //$scope.AmontReser=$scope.AmontReser+$scope.Descuento
                }
                else{
                    ErrorDesc=true;
                    $window.alert('Error en el descuento aplicado')
                    var descuento=0;
                }
                break
            default:
                $scope.DescType=0
                var descuento=0;
                break;
        }
        //console.log($scope.Descuento,$scope.AmontReser,descuento,$scope.DescType)
        //let info =JSON.parse($scope.cliente);
        if($scope.AmontReser != undefined && $scope.AmontReser>0 && !ErrorDesc){
            console.log(data.exentos)
            if(data.exentos) imp=imp2
            else imp=imp1
                let impuesto=(Math.floor((Number($scope.AmontReser)*Number(data.precio))*1000)/1000-descuento)*imp/100
                //console.log(Number($scope.DescType),descuento,descuento2)
                if(Number($scope.DescType)==1 || Number($scope.DescType)==4){
                    $scope.carrito.push({
                        objeto:$scope.CatId,
                        codigo:$scope.CatCod,
                        Maximo:$scope.CatTotal,
                        nombre:$scope.Cnombre,
                        cantidad:$scope.AmontReser,
                        descuento:descuento2,
                        Adescuento:$scope.Descuento,
                        Adescuento2:$scope.Descuento2,
                        Tdescuento:Number($scope.DescType),
                        //cliente:info._id,
                        //Ncliente:info.nombre,
                        total:(Math.floor((Number($scope.AmontReser)*Number(data.precio))*1000)/1000-descuento)*(1+imp/100),
                        precio:data.precio,
                        excento:data.exentos,
                        impuesto:impuesto,
                        agente:userID
                    })
                    if(descuento>0){
                        $scope.carrito.push({
                            objeto:$scope.CatId,
                            codigo:$scope.CatCod,
                            Maximo:$scope.CatTotal,
                            nombre:$scope.Cnombre,
                            cantidad:$scope.Descuento,
                            descuento:descuento,
                            Adescuento:$scope.Descuento,
                            Adescuento2:$scope.Descuento2,
                            Tdescuento:5,
                            //cliente:info._id,
                            //Ncliente:info.nombre,
                            total:0,
                            precio:data.precio,
                            excento:data.exentos,
                            impuesto:0,
                            agente:userID
                        })
                    }
                }    
                else{
                    $scope.carrito.push({
                        objeto:$scope.CatId,
                        codigo:$scope.CatCod,
                        Maximo:$scope.CatTotal,
                        nombre:$scope.Cnombre,
                        cantidad:$scope.AmontReser,
                        descuento:descuento,
                        Adescuento:$scope.Descuento,
                        Adescuento2:$scope.Descuento2,
                        Tdescuento:Number($scope.DescType),
                        //cliente:info._id,
                        //Ncliente:info.nombre,
                        total:(Math.floor(Number($scope.AmontReser)*Number(data.precio)*1000)/1000-descuento)*(1+imp/100),
                        precio:data.precio,
                        excento:data.exentos,
                        impuesto:impuesto,
                        agente:userID
                    })
                }            
                $scope.preciofinal+=(Number($scope.AmontReser)*Math.floor(data.precio*(1+imp/100)*1000)/1000)-descuento*(1+imp/100)
            
            console.log($scope.carrito)
            $scope.AmontReser=0;
            $scope.Descuento=0;
            $scope.ActualizarCarrito()
        }
    }

    $scope.autosave=function(){
        var data={
            id:$scope.userID,
            pedido:{
                reservas:$scope.carrito,
    	        resposable:$scope.Responsable,
		        contado:$scope.contado,
		        Abono:$scope.Abono,
		        total:$scope.preciofinal,
    	        transporte:$scope.Transporte,
                impuesto:$scope.impuesto,
            }
        };
        $http({
            method:'POST',
            url:'/save/pedido',
            data:data
        }).
        then(function(response){
            console.log(response.data);
        })
    }

    $scope.ActualizarCarrito=function(){
        $scope.impuesto=0
        $scope.preciofinal=0
        for (let i = 0; i < $scope.carrito.length; i++) {
            const orden = $scope.carrito[i];
            switch (orden.Tdescuento) {
                case 0:
                    $scope.carrito[i].descuento=0
                    break;
                case 1:
                    //$scope.carrito[i].descuento=$scope.carrito[i].precio*$scope.carrito[i].Adescuento
                    break;
                case 2:
                    $scope.carrito[i].descuento=$scope.carrito[i].precio*$scope.carrito[i].cantidad*$scope.carrito[i].Adescuento/100
                    break
                case 3:
                    $scope.carrito[i].descuento=$scope.carrito[i].Adescuento
                    break
                case 4:
                  //$scope.carrito[i].descuento=$scope.carrito[i].precio*($scope.carrito[i].Adescuento+orden.cantidad*orden.Adescuento2/100)
                    $scope.carrito[i].descuento=$scope.carrito[i].precio*(orden.cantidad*orden.Adescuento2/100)
                    break
                default:
                    break;
            }
            if(orden.Tdescuento!=5)$scope.carrito[i].total=$scope.carrito[i].precio*$scope.carrito[i].cantidad-$scope.carrito[i].descuento
            //console.log(orden)
            if(orden.excento) imp=imp2
            else imp=imp1
            console.log($scope.impuesto,$scope.carrito[i].total*imp/100)
            $scope.impuesto=$scope.impuesto+($scope.carrito[i].total)*imp/100
            $scope.carrito[i].impuesto=$scope.carrito[i].total*imp/100
            $scope.carrito[i].total=$scope.carrito[i].total*(1+imp/100)
            $scope.preciofinal=$scope.preciofinal+$scope.carrito[i].total
            $scope.autosave();
        }
    }

    $scope.removecarrito=function(i){
        if($scope.carrito.length>1){
            $scope.preciofinal=Math.floor(($scope.preciofinal-$scope.carrito[i].total)*10)/10
            $scope.impuesto=$scope.impuesto-$scope.carrito[i].impuesto
        }
        else {
            $scope.impuesto=0
            $scope.preciofinal=0
        }
        $scope.carrito.splice(i,1);
        $scope.autosave();
    }

    $scope.addClient=function(x){
        console.log(x.pago)
        $scope.Transporte =x.pago.transporte;
        $scope.contado=x.pago.contado;
        $scope.AvailCredito=x.pago.contado;
        $scope.credito=x.credito
        $scope.Ncliente=x.nombre;
        $scope.ClientId=x._id
    }

    $scope.newline=function(){
        if($scope.objtPed[$scope.objtPed.length-1].cod && $scope.objtPed[$scope.objtPed.length-1].cantidad){
            $scope.objtPed.push({
                cod:'',
                cantidad:0,
                bonificacion:0,
            })
        }
    }

    $scope.setCat = function(data){
        $scope.agregar =true;
        //console.log(data);
        $scope.Cprecio=data.precio;
        $scope.data = data;
        $scope.Cnombre = data.nombre;
        $scope.description = data.descripcion;
        $scope.CatTotal=data.cantidad;
        $scope.CatId=data.id
        $scope.CatCod=data.codigo
        if(data.exentos==undefined)$scope.exento=false;
        else $scope.exento=data.exentos;
    }

//enviar cuenta
    $scope.envioCuenta=function(){
        if($scope.ClientId != undefined){
            $scope.reservasArray=[]
            $scope.alerta=[]
            for (let i = 0; i < $scope.carrito.length; i++) {
                if($scope.carrito[i].cantidad>$scope.carrito[i].Maximo){
                    $scope.alerta.push($scope.carrito[i].nombre+'\tCantidad Solicitada:'+$scope.carrito[i].cantidad+'\tCantidad Disponible'+$scope.carrito[i].Maximo)
                }
                $scope.reservasArray.push({
                    objeto:$scope.carrito[i].objeto,
                    cantidad:$scope.carrito[i].cantidad,
                    descuento:$scope.carrito[i].descuento,
                    Tdescuento:$scope.carrito[i].Tdescuento
                }) 
            }
            console.log($scope.alerta)
            if($scope.alerta.length>0){
                texto='Advertencia!\n No existe suficiente productos en el inventario para suplir la demamda de las siguiente ordenes \n'
                for (let i = 0; i < $scope.alerta.length; i++) {
                    //console.log($scope.alerta[i])
                    texto =texto+$scope.alerta[i]+'\n'
                }
                texto=texto+'De ser aprobado el pedido alguno de sus pedidos quedaria congelado hasta rellenar la existencia \n Desea Enviar la orde de todas maneras?'
                if($window.confirm(texto)){
                    crear.pedido(empresaID,userID,$scope.reservasArray,$scope.contado,$scope.Abono,$scope.Transporte,$scope.preciofinal,$scope.ClientId,$scope.Responsable,function(response){
                        console.log(response.data);
                        $scope.carrito=[]
                        $scope.reservasArray=[]
                        $scope.preciofinal=0;
                        $scope.abono=0;
                        $scope.impuesto=0
                        $window.alert('Se realizo el pedido adecuadamente\nSe encuentra disponible para su seguimiento en la patalla Mis pedidos')
                    })
                }
            }
            

                else{
                    crear.pedido(empresaID,userID,$scope.reservasArray,$scope.contado,$scope.Abono,$scope.Transporte,$scope.preciofinal,$scope.ClientId,$scope.Responsable,function(response){
                        //console.log(response.data);
                        $scope.carrito=[]
                        $scope.reservasArray=[]
                        $scope.preciofinal=0;
                        $scope.abono=0;
                        $scope.impuesto=0
                        $window.alert('Se realizo el pedido adecuadamente\nSe encuentra disponible para su seguimiento en la patalla Mis pedidos')
                    })
                }
            //console.log($scope.Responsable)
            //console.log($cookies.get('empresa'),userID,$scope.reservasArray,$scope.contado,$scope.Abono,$scope.Transporte,$scope.preciofinal,$scope.ClientId,)
            
        }
        else $window.alert('Seleccione el cliente')
    }

    $scope.OtherTransp=function(){
        if($scope.Transporte=='otro'){
            var transp=$window.prompt("Defina el medio de transporte deseado")
            if(transp) {
                if($scope.transporte.indexOf(transp)==-1)$scope.transporte.push(transp)
                $scope.Transporte=transp;
            }
            else $scope.Transporte=null
        }
    }

//init
    $scope.codCat=function(){
        $scope.catCodigo={}
        for (let i = 0; i < $scope.CatKey.length; i++) {
            const key = $scope.CatKey[i];
            for (let j = 0; j < $scope.catalogo[key].length; j++) {
                $scope.catCodigo[$scope.catalogo[key][j].codigo] = $scope.catalogo[key][j];
                
            }
            
        }
        console.log($scope.catCodigo)
    }

    $scope.clientesSearch=function(init){
        console.log(empresaID,$cookies.get('empresa'))
        busquedas.clientes(empresaID,null,String(init),function(response){
            //console.log(response.max == response.last,response.max,response.last);
            if (response.max == response.last) {
                //llena el scope de respaldo
                $scope.clientes=$scope.clientes.concat(response.datos);
                console.log($scope.clientes)
                busquedas.transporte(empresaID,function(response){
                    console.log(response)
                    $scope.transporte=response;
                })
            } 
            else 
            {
                $scope.clientes=$scope.clientes.concat(response.datos);
                $scope.clientesSearch(response.last)
            }
        })
    }

    $scope.getAutosave=function(userID){
        $http({
            method:'GET',
            url:'/get/autosave/'+userID
        }).
        then(function(response){
            console.log(response.data);
            if(response.data){
                $scope.carrito=response.data.reservas;
                $scope.Responsable=response.data.resposable;
                $scope.contado=response.data.contado;
                $scope.Abono=response.data.Abono;
                $scope.preciofinal=response.data.total;
                $scope.Transporte=response.data.transporte;
                $scope.impuesto=response.data.impuesto;
            }
        })
    }

    $scope.init=function(){
        if($cookies.get('empresa')){
            $scope.objtPed=[{
                cod:'',
                cantidad:0,
                bonificacion:0,
            }]
            busquedas.catalogo($cookies.get('empresa'),function(keys,catalogo){
                console.log(keys,catalogo)
                empresaID=$cookies.get('empresa')
                userID=$cookies.get('id')
                $scope.userID=$cookies.get('id')
                $scope.CatKey=keys
                $scope.col=keys[0]
                $scope.catalogo=catalogo 
                $scope.clientesSearch(0)
                $scope.codCat()
            })
            $scope.getAutosave($cookies.get('id'))
        }
        else{
            var user=$window.prompt('Lo sentimos, ocurrio un error con su identificacion\nIngrese su usuario nuevamete')
            var pass=$window.prompt('Lo sentimos, ocurrio un error con su identificacion\nIngrese su contraseña nuevamete')
             data={
                'user':user,
                'password':pass
            }
            $http({
                method:'POST',
                url:'http://34.237.241.0:2500/login/',
                data:data
            }).
            then(function(response){
                //console.log(response.data);
                info = response.data;
                console.log(info);
                switch (info.err) {
                    case 0:
                        busquedas.catalogo(info.data.empresa,function(keys,catalogo){
                            //console.log(keys,catalogo)
                            empresaID=info.data.empresa
                            userID=info.data.id
                            $scope.userID=info.data.id;
                            $scope.CatKey=keys
                            $scope.col=keys[0]
                            $scope.catalogo=catalogo    
                            $scope.getAutosave(userID)
                            $scope.clientesSearch(0)
                        })
                        break
                    default:
                        $window.alert('Lo sentimos no pudimos realizar la autentificacion')
                        $window.open('/login/','_self')
                        break;
                }
            })
        }
    }
    $scope.init()
}])

app.filter('MilDot',function(){
    return function(input){
        if(input){
            entrada=String(input).split('.')
            console.log(entrada)
            if(entrada[1]) response='.'+entrada[1]
            else response='.00'
            for (let i = 0; i < entrada[0].length; i=i+3) {
                if(entrada[0].length-(i+3)>0){
                    response=','+entrada[0].slice(entrada[0].length-(i+3),entrada[0].length-i)+response
                    console.log(entrada[0].slice(entrada[0].length-(i+3),entrada[0].length-i),entrada[0].length-(i+3),entrada[0].length-i);
                }
                else {
                    response=entrada[0].slice(0,entrada[0].length-i)+response
                    console.log(entrada[0].slice(0,entrada[0].length-i),0,entrada[0].length-i);
                }
                //console.log(entrada[0].slice(i,i+3),i,i+3);
            }
            return response
        }
        else return '0.00'
    }
})

app.filter('cantidadShow',function(){
    //console.log(p1)
    return function(x,y){
        //console.log(x,y[x.objeto])
        if((x.Tdescuento==1 || x.Tdescuento==4) && x.descuento>0) return '+'+String(Math.floor(x.descuento/x.precio))
        //if(x.Tdescuento==1) return 
    }
})

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return 0
    }
})

app.filter('clientName',function(){
    return function(name,l){
        if(name){
            if(name.length<l)return name
            else return name.slice(0,l)+'...'
        }
        else return 'Cliente desconcido'
    }
})