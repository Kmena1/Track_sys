var app =angular.module("app",['ui.bootstrap','ngCookies','ui-leaflet']);
app.controller('recetas',['$scope','$http','$cookies','$window','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,crear,busquedas,editar,links){
    console.log($cookies.getAll());
    $scope.menu=[]
    $scope.links = links.links
    $scope.NumberSide=1

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    //init values
    $scope.productosT=[]
    $scope.MateriaP=[]
    $scope.etiquetas=[]
    $scope.envases=[]
     $scope.Tapas=[]
    $scope.recetas={}
    $scope.producto=null
    $scope.seleccionado=false
    $scope.NombreObjetos={}
    $scope.AllRectas={}
    $scope.porcent =0
    $scope.CrearExcel=false
    $scope.SelcPT=true

    $scope.removeIng=function(x){
        console.log(x)
        var id=x._id
        for (let i = 0; i < $scope.ingredientes.length; i++) {
            if($scope.ingredientes[i]._id==id){
                $scope.ingredientes.splice(i,1);
                $scope.recalcularTotal()
                break
            }
            
        }
        //$scope.ingredientes.splice(i,1);
        //$scope.recalcularTotal()
    }

    $scope.recalcularTotal = function(){
        console.log($scope.ingredientes)
        $scope.total=0
        for (let i = 0; i < $scope.ingredientes.length; i++) {
            $scope.total=$scope.total+Number($scope.ingredientes[i].cantidad)
            if($scope.ingredientes[i].cantidad>0.001 && $scope.ingredientes[i].cantidad!=0){
                $scope.ingredientes[i].cantidad=Math.floor($scope.ingredientes[i].cantidad*1000)/1000
            }
        }
        console.log($scope.total)
        $scope.total=Math.floor($scope.total*1000)/1000
        if($scope.total==99.999 || $scope.total==100.001) $scope.total=100
    }

    //porcentajes
    $scope.porcentajes=function(){
        $scope.total=0
        for (let i = 0; i < $scope.ingredientes.length; i++) {
            $scope.total=$scope.total+Number($scope.ingredientes[i].cantidad)
        }
        $scope.total=Math.floor($scope.total*100)/100
        if($scope.total==99.99 || $scope.total==100.01) $scope.total=100
        console.log($scope.total)
    }

    $scope.RctSelect=function(){
        $scope.Asignar=true
        $scope.producto=$scope.AllRectas[$scope.recetaAsignada];
        var id=$scope.producto.producto
        $scope.producto['producto']=id
        $scope.producto['receta']=$scope.producto._id
        //console.log($scope.recetaAsignada,recetaAsg)
        $scope.Nombre=$scope.producto.nombre;
        $scope.Codigo=Number($scope.producto.codigo);
        $scope.ingredientes=$scope.AllRectas[$scope.recetaAsignada].ingredientes
        $scope.recalcularTotal()
        console.log(id,$scope.producto)
    }

    $scope.SelccionSideE=function(y,index){
        if($scope.NumberSide!=index){
            $scope.NumberSide=index
            console.log(y)
            $scope.Nombre=y.nombre;
            $scope.Codigo=y.codigo;
            $scope.ingredientes=y.ingredientes
            $scope.porcentajes()
        }
    }

    //sidebar controler
    $scope.SelccionSide = function(y){
        console.log(y)
        $scope.selected=y
        $scope.recetaAsignada=null
        $scope.Asignar=false
        $scope.seleccionado=true
        if(y.receta==null){
            $scope.TipoPorcent=false
            $scope.total=0
            $scope.producto=y;
            $scope.producto['producto']=y._id
            $scope.ingredientes=[]
            $scope.Nombre=$scope.producto.nombre;
            $scope.Codigo=Number($scope.producto.codigo)
            $scope.envase={
                    asignado:y._id,
                    tapa:{
                        objeto:null,
                        cantidad:1,
                    },
                    envase:{
                        objeto:null,
                        cantidad:1,
                    },
                    etiqueta:[{
                        objeto:null,
                        cantidad:1,
                    }],
                }
        }
        else{
            $scope.producto=$scope.AllRectas[y.receta];
            $scope.producto['receta']=y.receta
            $scope.Nombre=$scope.producto.nombre;
            $scope.TipoPorcent=$scope.producto.tipo;
            $scope.Codigo=Number($scope.producto.codigo);
            $scope.ingredientes=$scope.AllRectas[y.receta].ingredientes
            //envases
            $scope.envase=null
            //console.log($scope.AllRectas[y.receta])
            for (let i = 0; i < $scope.AllRectas[y.receta].envase.length; i++) {
                const envase = $scope.AllRectas[y.receta].envase[i];
                if(envase.asignado==y._id) {
                    $scope.envase=envase
                    break;
                }
            }
            if($scope.envase==null){
                $scope.envase={
                    asignado:y._id,
                    tapa:{
                        objeto:null,
                        cantidad:1,
                    },
                    envase:{
                        objeto:null,
                        cantidad:1,
                    },
                    etiqueta:[{
                        objeto:null,
                        cantidad:1,
                    }],
                }
            }
            //console.log($scope.envase)
            $scope.porcentajes()
        }
        console.log($scope.ingredientes)
        $scope.porcent=0
        for (let i = 0; i < $scope.ingredientes.length; i++) {
            const ingrediente = $scope.ingredientes[i];
            $scope.porcent=$scope.porcent+ingrediente.cantidad
        }
        $scope.producto['articulo']=y._id
        //console.log($scope.producto)
        $scope.recalcularTotal()
    }

    $scope.AddEtiqueta=function(){
        if($scope.envase.etiqueta){
            $scope.envase.etiqueta.push({
                objeto:null,
                cantidad:1,
            })
        }
        else {
            $scope.envase.etiqueta={
                objeto:null,
                cantidad:1,
            }
        }
    }

    $scope.RemoveEtq=function(i){
        $scope.envase.etiqueta.splice(i,1);
    }

    //agregar ingrediente
    $scope.AddIngr = function(){
        //console.log($scope.newIngridient,$scope.cantidad)
        $scope.ingredientes.push({
            objeto:$scope.newIngridient._id,
            cantidad:Number($scope.cantidad)
        })
        $scope.recalcularTotal()
    }

    //Parser del nuevo ingrediente
    $scope.Jparser= function(){
        $scope.newIngridient=JSON.parse($scope.productongrediente)
    }

    //renombrado de objetos
    $scope.Nobjeto = function(keys,inventario){
        for (let i = 0; i < keys.length; i++) {
            for (let j = 0; j < inventario[keys[i]].length; j++) {
                $scope.NombreObjetos[inventario[keys[i]][j]._id]=inventario[keys[i]][j].nombre;   
            }
        }
        console.log($scope.NombreObjetos)
    }

    $scope.actulizarCantidad=function(x,i){
        var cantidad=$window.prompt('Introduzca el nuevo porcentaje del ingrediente '+$scope.NombreObjetos[x.objeto])
        if(Number(cantidad)){
            $scope.ingredientes[i].cantidad=Number(cantidad)
            $scope.recalcularTotal()
        }
    }

    //crear receta
    $scope.crearReceta=function(){
        //retorna los ingrediante a valor porcentual
        for (let i = 0; i < $scope.ingredientes.length; i++) {
            $scope.ingredientes[i].cantidad=$scope.ingredientes[i].cantidad/100;
        //console.log(ingredientes)
        }
        //Modal de carga
        $scope.loading=true
        var popup = document.getElementsByClassName("modalActive")[0];
        popup.click()
        if($scope.producto.receta==null){
            $scope.InfoText='Creando la formula '+$scope.Codigo+'-'+$scope.Nombre
        }
        else{
            $scope.InfoText='Editando la formula '+$scope.Codigo+'-'+$scope.Nombre
        }
        //console.log($cookies.get('empresa'),$scope.Nombre,$scope.Codigo,$scope.ingredientes,$scope.producto.receta,$scope.producto.articulo)
        crear.receta($cookies.get('empresa'),$scope.Nombre,$scope.Codigo,$scope.ingredientes,$scope.producto.receta,$scope.producto._id,$scope.TipoPorcent,function(response){
            console.log(response)
            //revision de las etiquetas
            for (let i = 1; i < $scope.envase.etiqueta.length; i++) {
                if(!$scope.envase.etiqueta[i].objeto) {
                    $scope.envase.etiqueta.splice(i,1);
                    i=i-1
                }
            }
            console.log($scope.envase)
            var data={
                receta:response.data._id,
                data:$scope.envase
            };
            $http({
                method:'POST',
                url:'/receta/add/envace/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                
                if($scope.producto.receta==null){
                    for (let i = 0; i < $scope.email.length; i++) {
                        crear.email('info@passarte.com',$scope.email[i],'Alerta de creacion de formula','El usuario '+$cookies.get('user')+' creo la formula '+$scope.Codigo+'-'+$scope.Nombre+" del producto "+$scope.producto.nombre+' el dia '+new Date(),function(){})
                    }//$scope.InfoText='Creando la formula '+$scope.Codigo+'-'+$scope.Nombre
                }
                else{
                    for (let i = 0; i < $scope.email.length; i++) {
                        crear.email('info@passarte.com',$scope.email[i],'Alerta de edicion de formula','El usuario '+$cookies.get('user')+' edito la formula '+$scope.Codigo+'-'+$scope.Nombre+" del producto "+$scope.producto.nombre+' el dia '+new Date(),function(){})
                    }//$scope.InfoText='Editando la formula '+$scope.Codigo+'-'+$scope.Nombre
                }
                //var popup = document.getElementsByClassName("closeModal")[0];
                //popup.click()
                //$scope.loading=false
                $scope.seleccionado=false;
                $scope.productosT=[]
                $scope.MateriaP=[]
                $scope.etiquetas=[]
                $scope.envases=[]
                $scope.inventarioGet()
            })
        })
    }

    $scope.CerrarExcel=function(){
        $scope.CrearExcel=false
        $scope.init()
    }

    //crear receta
    $scope.crearFormula=function(){
        //retorna los ingrediante a valor porcentual
        for (let i = 0; i < $scope.ingredientes.length; i++) {
            $scope.ingredientes[i].cantidad=$scope.ingredientes[i].cantidad/100;
        //console.log(ingredientes)
        }
        //console.log($cookies.get('empresa'),$scope.Nombre,$scope.Codigo,$scope.ingredientes,$scope.producto.receta,$scope.producto.articulo)
        crear.receta($cookies.get('empresa'),$scope.Nombre,$scope.Codigo,$scope.ingredientes,null,null,function(response){
            console.log(response)
            $scope.formulasE.splice($scope.NumberSide,1);
            if($scope.formulasE.length>0)$scope.SelccionSideE($scope.formulasE[0],0)
            else $scope.CerrarExcel()
        })
    }

    $scope.AsignarReceta=function(){
        console.log($scope.producto)
        var data={
            objeto:$scope.selected._id,
            receta:$scope.recetaAsignada
        };
        $http({
            method:'POST',
            url:'/asignar/receta/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.crearReceta()
            for (let i = 0; i < $scope.email.length; i++) {
                crear.email('info@passarte.com',$scope.email[i],'Alerta de asignacion de formula','El usuario '+$cookies.get('user')+' asigno la formula al producto'+$scope.selected.codigo+'-'+$scope.selected.nombre+' el dia '+new Date(),function(){})
            }
        })
    }

    //ordenado de elementos del inventario
    $scope.ClasicacionInvt = function(keys,j,data){
        //console.log(keys[j],data[keys[j]],data['keys'])
        for (let i = 0; i < data['keys'].length; i++) {
            //console.log(keys[j],data['keys'][i].nombre)
            if(keys[j] == data['keys'][i].nombre){
                //console.log(data['keys'][i].tipo)
                switch (data['keys'][i].tipo) {
                    case "PT":
                        $scope.productosT.push({
                            'nombre':keys[j],
                            'data':data[keys[j]]
                        })
                        break;
                    case "MP":
                        $scope.MateriaP.push({
                            'nombre':keys[j],
                            'data':data[keys[j]]
                        })
                        break;
                    case "ET":
                        $scope.etiquetas.push({
                            'nombre':keys[j],
                            'data':data[keys[j]]
                        })
                        break;
                    case "EN":
                        $scope.envases.push({
                            'nombre':keys[j],
                            'data':data[keys[j]]
                        })
                        break;
                    case "TP":
                         $scope.Tapas.push({
                            'nombre':keys[j],
                            'data':data[keys[j]]
                        })
                        break;
                    default:
                        break;
                }
                break;
            }
        }
        if(j+1<keys.length)$scope.ClasicacionInvt(keys,j+1,data)
        else {
            console.log($scope.productosT,$scope.MateriaP,$scope.etiquetas,$scope.envases)

        }
    }

    $scope.desAsignar=function(){
        console.log($scope.selected)
        if ($window.confirm("Desea des-asignar la formula "+$scope.AllRectas[$scope.selected.receta].nombre+" del producto "+$scope.selected.nombre)){
            //console.log('eliminando')
            $scope.loading=true
            var popup = document.getElementsByClassName("modalActive")[0];
            popup.click()
            //if($scope.producto.receta==null){
            $scope.InfoText='Desasignado la formula '+$scope.AllRectas[$scope.selected.receta].nombre+" del producto "+$scope.selected.nombre
            //}
            var data={
                objeto:$scope.selected._id,
                formula:$scope.selected.receta
            };
            $http({
                method:'POST',
                url:'/deasignar/formulas/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                for (let i = 0; i < $scope.email.length; i++) {
                    crear.email('info@passarte.com',$scope.email[i],'Alerta de desasignacion de formula','El usuario '+$cookies.get('user')+' desasigno la formula '+$scope.AllRectas[$scope.selected.receta].nombre+" del producto "+$scope.selected.nombre+' el dia '+new Date(),function(){})
                }
                //var popup = document.getElementsByClassName("closeModal")[0];
                //popup.click()
                //$scope.loading=false
                $scope.seleccionado=false;
                $scope.productosT=[]
                $scope.MateriaP=[]
                $scope.etiquetas=[]
                $scope.envases=[]
                $scope.inventarioGet()
            })
        }
    }

    $scope.eliminarFormula = function(x){
        //console.log()
        console.log(x)
        if($window.confirm("Desea eliminar la formula "+x.codigo+'-'+x.nombre)){
            var data={
                id:$cookies.get('empresa'),
                receta:x._id
            };
            $http({
                method:'POST',
                url:'/delete/receta',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                //$scope.recetasArray.splice(i,1);
                $window.alert('Formula eliminada, en un momento se actualizaran las formulas en la pagina')
                $scope.SelccionSide=null
                for (let i = 0; i < $scope.email.length; i++) {
                    crear.email('info@passarte.com',$scope.email[i],'Alerta de eliminacion de formula','El usuario '+$cookies.get('user')+' elimino la formula '+x.codigo+'-'+x.nombre+' el dia '+new Date(),function(){})
                }
                $scope.GetRecetas()
            })
        }
    }

    //Parser recetas
    $scope.ParserRecetas = function(recetas){
        //Creacion del JSon de envases
        $scope.JSONenv={};
        for (let i = 0; i < recetas.length; i++) {
            $scope.AllRectas[recetas[i]._id] = recetas[i];
            //Vuelve los valores a formato 100
            for (let j = 0; j < $scope.AllRectas[recetas[i]._id].ingredientes.length; j++) {
                if(!$scope.AllRectas[recetas[i]._id].ingredientes[j])$scope.ingredientes.splice(j,1);
                else{
                    $scope.AllRectas[recetas[i]._id].ingredientes[j].cantidad=$scope.AllRectas[recetas[i]._id].ingredientes[j].cantidad*100;
                }
            }
            for (let k = 0; k < recetas[i].envase.length; k++) {
                const env = recetas[i].envase[k];
                if(env.envase.objeto && env.tapa.objeto && env.etiqueta.length>0){
                    $scope.JSONenv[env.asignado]=true
                } 
                else $scope.JSONenv[env.asignado]=false
            }
        }
        console.log($scope.JSONenv)
        $scope.InfoText="Recetas Actualizadas, Gracias por la espera"
        var popup = document.getElementsByClassName("closeModal")[0];
        popup.click()
        $scope.loading=false
    } 

    //buscar recetas
    $scope.GetRecetas = function(){
        $scope.InfoText='Actualizando recetas, favor espere un momento'
        busquedas.recetas($cookies.get('empresa'),null,1000,function(response){
            console.log(response.data)
            $scope.recetasArray=response.data.data
            $scope.AllRectas={}
            $scope.ParserRecetas(response.data.data)
        })
    }

    //busquedas del inventario
    $scope.inventarioGet = function(){
        $scope.InfoText='Obteniendo productos, favor espere un momento'
        busquedas.inventario($cookies.get('empresa'),function(keys,data){
            console.log(keys, data)
            $scope.collections=data['keys']
            $scope.ClasicacionInvt(keys,1,data)
            $scope.Nobjeto(keys,data)
            $scope.GetRecetas()
        })
    }

    //modal control
    $scope.page=function(i){
        switch (i) {
            //coleccion
            case 0:
                $scope.colc =true;
                $scope.objeto =false;
                $scope.Ncol =false
                break;
            case 1:
                $scope.colc =false;
                $scope.objeto =true;
                $scope.Ncol =false
                break;
            default:
                break;
        }
    }

    //modal footer
    $scope.CrearColeccion=function(){
        crear.categoria($cookies.get('empresa'),$scope.nombre,$scope.CatType,function(response){
            console.log(response);
            $scope.colleccion = $scope.nombre;
            $scope.page(1)
        })
    }

    $scope.crearProducto = function(){
        crear.objeto($cookies.get('empresa'),$scope.colleccion,$scope.nombre,$scope.precio,$scope.cantidad,$scope.codigo,$scope.Execento,$scope.description,$scope.Incatalogo,$scope.unidad,$scope.dolares,function(response){
            console.log(response.data);
            if($scope.file != undefined){
                var formData = new FormData();
                formData.append("name", name);
                formData.append("file", $scope.file);
                console.log(formData);
                crear.Image($cookies.get('empresa'),response.data.data._id,formData,function(response){
                    $scope.init()
                    $scope.reset();
                })
            }
            else{
                $scope.init()                
                $scope.reset();
            }
        })
    }

    //reset
    $scope.reset=function(){
        $scope.producto=null
        $scope.seleccionado=false
        $scope.productosT=[]
        $scope.MateriaP=[]
        $scope.etiquetas=[]
        $scope.envases=[]
        $scope.inventarioGet()
    }

    //init
    $scope.init=function(){
        busquedas.init(function(datos,extra,mensaje){
            $scope.users= datos;
            $scope.empresa= extra;
            $scope.mensaje=mensaje
            $http({
                method:'GET',
                url:'/find/company/'+extra.id
            }).
            then(function(response){
                //console.log(response.data.ExtEmail);
                $scope.email=response.data.ExtEmail
                $scope.email.push(response.data.email);
                //console.log($scope.email)
            })
            $scope.reset()
        })
    }    

    $scope.init()

    //manejo del archivo
    $scope.CheckFormula=function(){
        for (let i = 0; i < $scope.formulasE.length; i++) {
            total=0
            for (let j = 0; j < $scope.formulasE[i].ingredientes.length; j++) {
                const ingrediente = $scope.formulasE[i].ingredientes[j];
                total=Math.floor((total+ingrediente.cantidad)*100)/100
                $scope.formulasE[i].ingredientes[j].cantidad=$scope.formulasE[i].ingredientes[j].cantidad*100
            }
            if(total!=1) $scope.formulasE[i]['err']=0
            else $scope.formulasE[i]['err']=1
        }
        console.log($scope.formulasE)
        $scope.SelccionSideE($scope.formulasE[0],0)
    }


    $scope.FormulasExcel=function(formulas){
        //json guardar codigo de productos necesarios
        var objects={}
        //arrglo para guardar las formulas
        $scope.formulasE=[]
        //busqueda de los productos necesarios en as formulas 
        var producto=Object.keys(formulas)
        $scope.CrearExcel=true
        //console.log(producto)
        for (let i = 0; i < producto.length; i++) {
            $scope.formulasE.push({
                nombre:producto[i],
                codigo:i,
                ingredientes:[]
            })
            for (let j = 0; j < formulas[producto[i]].length; j++) {
                const prodct = formulas[producto[i]][j];
                //console.log(prodct['Código'])
                var cod=String(prodct['Código']).replace(' ','')
                if(cod){
                    if(objects[cod]){
                        $scope.formulasE[i].ingredientes.push({
                            objeto:objects[String(prodct['Código']).replace(' ','')],
                            cantidad:prodct['Formulación']/100
                        })
                    }
                    //busqueda de productos en materia prima
                    else{
                        for (let k = 0; k < $scope.MateriaP.length; k++) {
                            for (let l = 0; l < $scope.MateriaP[k].data.length; l++) {
                                //codigo sin espacio
                                const codigo = $scope.MateriaP[k].data[l].codigo.replace(' ','');
                                if(codigo==String(prodct['Código']).replace(' ','')){
                                    objects[codigo]=$scope.MateriaP[k].data[l]._id
                                    $scope.formulasE[i].ingredientes.push({
                                        objeto:objects[String(prodct['Código']).replace(' ','')],
                                        cantidad:prodct['Formulación']/100
                                    })
                                    k=$scope.MateriaP.length+1
                                    break
                                }
                            }
                            
                        }
                    }
                }
                else{
                    console.log(prodct)
                }
            }
        }
        //console.log($scope.formulasE)
        $scope.CheckFormula()
    }

    $scope.printArchivo=function(){
        console.log($scope.Archivo);
        var formData = new FormData();
        formData.append("name", $scope.Archivo.name);
        formData.append("file", $scope.Archivo);
        $http({
            method:'POST',
            url:'/upload2/parser/page?username='+$cookies.get('empresa')+'&name='+'formulas_'+$cookies.get('empresa'),
            data:formData,
            headers:{
                "Content-type": undefined
            }, 
            transformRequest: angular.identity
        }).
        then(function(response){
            console.log(response.data);
            $scope.FormulasExcel(response.data)
        })
    }   
}])

app.filter('EstadoFormula', function(){
    return function(x){
        if(x){
            return 'completa'
        }
        else return 'SinF'
    }
})


app.directive('uploaderModel', ["$parse", function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) 
		{
			iElement.on("change", function(e)
			{
				$parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
                console.log('parseado', iAttrs.name)
                if(iAttrs.name=="Archivo")scope.printArchivo()
			});
		}
	};
}])