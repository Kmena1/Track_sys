app.controller('view',['$scope','$http','$cookies','$window','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,crear,busquedas,editar,links){
    console.log($cookies.getAll());
    console.log($window.data)
    $scope.Ahora=new Date()
    console.log($scope.Ahora)
    impuesto1=13
    impuesto2=2
    $scope.AllData=$window.data
    $scope.subTotal=0
    $scope.imp=0
    $scope.descuento=0
    $scope.AllProductos=[]
    if($scope.AllData){
        $scope.direccion=$scope.AllData.direccion
        var reserva=$scope.AllData.pedidos.reservas
        var salida=$scope.AllData.salida.prductos
        $scope.productos=[]
        $scope.excentos=0
        $scope.gravados=0
        for (let i = 0; i < salida.length; i++) {
            const PSal = salida[i].producto;
            for (let j = 0; j < reserva.length; j++) {
                const PRes = reserva[j].objeto;
                if(PSal==PRes && salida[i].cantidad==reserva[j].cantidad ){
                    //calculo del impuesto
                    if(reserva[j].Tdescuento!=5){
                        if($scope.AllData.objeto[PSal].exentos){
                            impuesto=($scope.AllData.objeto[PSal].precio*salida[i].cantidad-reserva[j].descuento)*impuesto2/100
                            $scope.excentos=$scope.excentos+$scope.AllData.objeto[PSal].precio*salida[i].cantidad
                        }
                        else{
                            impuesto=($scope.AllData.objeto[PSal].precio*salida[i].cantidad-reserva[j].descuento)*impuesto1/100
                            $scope.gravados=$scope.gravados+$scope.AllData.objeto[PSal].precio*salida[i].cantidad
                        }
                        $scope.productos.push({
                            producto:PSal,
                            cantidad:salida[i].cantidad,
                            descuento:reserva[j].descuento,
                            impusto:impuesto,
                            Tdescuento:reserva[j].Tdescuento
                        })
                        $scope.subTotal=$scope.subTotal+$scope.AllData.objeto[PSal].precio*salida[i].cantidad
                        $scope.imp=$scope.imp+impuesto
                        $scope.descuento=$scope.descuento+reserva[j].descuento
                    }
                    else{
                        $scope.productos.push({
                            producto:PSal,
                            cantidad:salida[i].cantidad,
                            descuento:reserva[j].descuento,
                            impusto:0,
                            Tdescuento:reserva[j].Tdescuento
                        })
                        //$scope.descuento=$scope.descuento+reserva[j].descuento
                    }
                    if($scope.productos.length==7){
                        $scope.AllProductos.push($scope.productos)
                        
                        $scope.productos=[]
                    }
                    j=reserva.length+1
                    //console.log($scope.descuento,reserva[j].descuento)
                }
            }
        }
        $scope.AllProductos.push($scope.productos)
        console.log($scope.AllProductos,$scope.productos)
        $scope.productos=[]
            if($window.confirm('Desea imprimir esta factura')){
                $window.print()
            }
        //$window.close()
    }
    else $window.close()
}])

app.filter('TypeAprv',function(){
    return function(input){
        if(input){
            console.log(input)
            var response='Aprobado'
            for (let i = 0; i < input.reservas.length; i++) {
                if(!input.reservas[i].Aprovado){
                    response='Parcialmente Aprobado'
                    break;
                }
            }
            return response
        }
        return null
    }
})

app.filter('cantidadShow',function(){
    //console.log(p1)
    return function(x,y){
        console.log(x,y[x.objeto])
        if(x.Tdescuento==5) return "Bonificacion"
        else return x.cantidad
        //if(x.Tdescuento==1) return 
    }
})

app.filter('total',function(){
    return function(pedidos,objetos){
        console.log(pedidos,objetos)
        var total=0
        for (let i = 0; i < pedidos.length; i++) {
            const pedido = pedidos[i];
            if(pedido.Aprovado) {
                var obj=objetos[pedido.objeto]
                if(obj.exentos)total=total+obj.precio*pedido.cantidad-pedido.descuento
                else total=total+(obj.precio*pedido.cantidad-pedido.descuento)*1.13
            }
        }
        return total
    }
})

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return 0
    }
})

app.filter('fechaShow',function(){
    return function(date,tipo){
        var fecha = new Date(date);
        console.log(fecha)
        if(tipo==0)return String(fecha.getHours())+':'+String(fecha.getMinutes())
        else return String(fecha.getDate())+'/'+String(fecha.getMonth())+'/'+String(fecha.getFullYear())
    }
})

app.filter('filterDirection',function(){
    return function(entrada){
        if(entrada) return entrada.replace('Provincia','').replace('Cantón','')
    }
})