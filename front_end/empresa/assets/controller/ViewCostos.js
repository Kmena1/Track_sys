app.controller('view',['$scope','$http','$cookies','$window','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,crear,busquedas,editar,links){
    console.log($cookies.getAll());
    console.log($window.data)
    $scope.fecha=new Date()
    $scope.AllData=$window.data
    if($scope.AllData){
        $window.print()
    }
    else $window.close()
}])

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return null
    }
})