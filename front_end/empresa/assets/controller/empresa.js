var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('company',['$scope','$http','$cookies','$window','$timeout','leafletData','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,$timeout,leafletData,crear,busquedas,editar,links){
    console.log($cookies.getAll());
//variables de inicio
    $scope.landing=true;
    $scope.shw=false;
    if($cookies.get('empresa')==null) $window.open('/login/','_self');
    //arreglos
    $scope.markers=[];
    $scope.center= {
        lat: 9.8755948,
        lng: -84.0380218,
        zoom: 10
    }
    $scope.geojson={
            data: {
                "type": "FeatureCollection",
                "features": []
            },
            style: {
                fillColor: "green",
                weight: 2,
                opacity: 1,
                color: 'red',
                dashArray: '3',
                fillOpacity: 0.7
            }
        }
//saltos de pagina
    $scope.links = links.links
    $scope.menu=[]

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

//inicio

    $scope.init=function(){
        $scope.UserShw=true;
        busquedas.init(function(datos,extra){
            $scope.nombre=extra.nombre
            $scope.users= datos;
            $scope.empresa= extra;
            $scope.poslatlon=[];
            $scope.keepTracking()
        });
        busquedas.estrcuturas(function(response){
            console.log(response.data);
            $scope.strc=response.data;
        })
    }
    $scope.init();

//crear usuario
    $scope.crearUser = function(){
        crear.usuario($scope.nombre,$scope.email,$scope.cedula,$scope.telefono,2,$scope.user,$scope.password,$cookies.get('empresa'),function(response){
            console.log(response);
            $scope.userChange=response.data.usuario;
            $scope.UserShw=false
        })
    }
//crear dispositivo
    $scope.creardisp=function(){
        crear.dispositivo($scope.unid,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,$scope.strc.tipo,$scope.strc.model_ID,function(response){
            console.log(response.data);
            editar.AsgDispositivo($scope.userChange,response.data._id,function(response){
                console.log(response.data);
                $scope.init();
            })
        })
    }
//cookies
    $scope.userSave=function(id){
        console.log(id);
        $cookies.put('userSch',id, {'path': '/'});
        $scope.template='/empresa/assets/htmls/Alertas.html';   
    }
    $scope.reportes =function(id){
        //console.log(id)
        $cookies.put('userSch',id,{'path': '/'});
        $window.open('/empresa/assets/htmls/reportes.html',"_self");
    }
    
//mapa
    $scope.go_to =function(i){
        console.log($scope.center,$scope.poslatlon,i);
        lat=$scope.poslatlon[i].lat;
        lon=$scope.poslatlon[i].lon;
        $scope.center={
            lat: Number(lat),
            lng: Number(lon),
            zoom: 15
        };
        console.log($scope.center);
    }

    $scope.show=function(i){
        console.log(i);
    }

    $scope.geoHAndler=function(tipo,pos,name){
        console.log(tipo,pos);
         switch (tipo) {
            case 'point':
                data={
                    "type": "Feature",
                    "properties": {
                        "name":name
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": pos
                    }
                }
                break;
            case 'line':
                data={
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "LineString",
                        "coordinates": pos
                    }
                }
                break;
            default:
                break;
        }
        $scope.geojson.data.features.push(data);
        console.log($scope.geojson);
    }

    $scope.last=function(user,i,markers){
        console.log(user,i,markers)
        if(user.length!=i){
            busquedas.divices(user[i].id,function(response){
                if(response!=null){
                    console.log(response,user,i);
                    //console.log('lat,lon',response,10,0)
                    busquedas.consultaslast('lat,lon',response,10,0,new Array(0),null,function(datos,id){
                        //console.log(datos,user[i]);
                        //let info=response.data;
                        let itr=datos.length-1;
                        if(datos.length>0) $scope.poslatlon[i] = datos[datos.length-1];
                        if(datos[itr].lat!=null && datos[itr].lon!=null) {
                            let pos=new Array(0);
                            pos.push(datos[itr].lon);
                            pos.push(datos[itr].lat);
                            //console.log(pos,datos[itr].lat,datos[itr].lon)
                            //$scope.getReverseGeocodingData(info.data[info.itr].lat,info.data[info.itr].lon);
                            //$scope.geoHAndler('point',pos,response.data,user[i].nombre)
                            markers.push({
                                lat:Number(datos[itr].lat),
                                lng:Number(datos[itr].lon),
                                message:user[i].nombre
                            });
                            //console.log(markers)
                        }
                        $scope.last(user,i+1,markers);
                    })
                }
                else{
                    $scope.last(user,i+1,markers);
                }
            })
        }
        else{
            console.log(markers)
            $scope.markers=markers
        }
    }

    $scope.keepTracking=function(){
        $scope.last($scope.users,0,[])
        $timeout(function(){
            $scope.keepTracking(); 
        },5000);
    }
    //pages
}])