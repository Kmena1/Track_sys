app.controller('ordenes',['$scope','$http','$cookies','$window','leafletData','busquedas','crear','editar','links',function($scope,$http,$cookies,$window,leafletData,busquedas,crear,editar,links){
    console.log($cookies.getAll())
        $scope.verOrden=false;
        $scope.productos=[];
        $scope.filter=''
        $scope.filCod=''
        $scope.Mpage=''
        $scope.Producir=[]
        $scope.solicitar=[]

        $scope.Nformula={}
        $scope.cantidadSol={}
        $scope.SolToKg={}
        $scope.Total=[]

        $scope.formulasArray=[]
        $scope.provedoresArray=[]
        $scope.AllRectas=[]

        $scope.ProductosFor=[]

        $scope.emailFrom= 'kmena281@gmail.com';
        $scope.emailTo='';
        
        $scope.ToProduce={}

        var lista=[]

        $scope.tipo='PT'

        //saltos de pagina
        $scope.menu=[]
        $scope.links = links.links

        $scope.MpSol={}

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    //buscar recetas
    $scope.GetRecetas = function(){
        busquedas.recetas($cookies.get('empresa'),null,1000,function(response){
            console.log(response.data)
            $scope.AllRectas=response.data.data
            //$scope.ParserRecetas(response.data.data)
        })
    }

    $http({
        method:'GET',
        url:'/last/orden/'+$cookies.get('empresa')
    }).
    then(function(response){
        console.log(response.data);
        $scope.consecutivo=Number(response.data.numero)+1
    })

    $scope.producidosExtra=function(){
        $scope.extraCnt={}
        $scope.ordenesPrdct={}
        //$scope.ordenID={}
        for (let i = 0; i < $scope.ordenes.length; i++) {
            const orden = $scope.ordenes[i];
            if(orden.estado!=3 && orden.estado!=6){
                //$scope.ordenID[orden._id]=orden
                for (let j = 0; j < orden.prductos.length; j++) {
                    const prdt = orden.prductos[j];
                    if($scope.extraCnt[prdt.producto]){
                        $scope.extraCnt[prdt.producto]=$scope.extraCnt[prdt.producto]+prdt.cantidad
                        $scope.ordenesPrdct[prdt.producto].push(orden._id)
                    }
                    else{
                        $scope.ordenesPrdct[prdt.producto]=[
                            orden._id
                        ]
                        $scope.extraCnt[prdt.producto]=prdt.cantidad
                    }
                }
            }
            
        }
        console.log($scope.extraCnt,$scope.ordenID)
    }
    $scope.makeExtra=function(){
        busquedas.ordenes(function(response){
            $scope.ordenes=response.Produccion
            console.log($scope.ordenes)
            $scope.producidosExtra()
            //$scope.busqueda_clientes()
        })
    }

    $scope.eliminarFormula = function(x){
        //console.log()
        console.log(x)
        if($window.confirm("Desea eliminar la formula "+x.nombre)){
            $scope.formula=false
            var data={
                id:$cookies.get('empresa'),
                receta:x._id
            };
            $http({
                method:'POST',
                url:'/delete/receta',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                //$scope.recetasArray.splice(i,1);
                $scope.GetRecetas()
            })
        }
    }

    $scope.Convertion=function(x,y){
        console.log(x,y)
        if(x.id)x['_id']=x.id
        if(x.contenido_neto)x['contenidoNeto']=x.contenido_neto
        if(!x.convertFactor)x.convertFactor=1
        if(!x.contenidoNeto)x.contenidoNeto=1
        cantidad=$scope.ToProduce[$scope.prdt.receta].productos[x._id].cantidad
        //console.log(cantidad,x.unidad)
        switch(x.unidad.toUpperCase()){
            case 'KG':
                kilos=cantidad*x.contenidoNeto;
                break
            case 'G':
                kilos=cantidad*x.contenidoNeto/1000;
                break
            case 'L':
                kilos=cantidad*x.contenidoNeto*x.convertFactor;
                break
            case 'ML':
                kilos=cantidad*x.contenidoNeto*x.convertFactor/1000;
                break
            case 'GL':
                kilos=cantidad*x.contenidoNeto*x.convertFactor/3.78541;
                break
            default:
                kilos=cantidad*x.contenidoNeto*x.convertFactor;
                break
        }
        //console.log(x._id)
        $scope.ToProduce[$scope.prdt.receta].productos[x._id]['kilos']=kilos
        $scope.ActualizarTotal()
    }

    $scope.AddProveedor=function(){
        console.log('hola')
        if($scope.Distri!=''){
            if($scope.provedoresArray.indexOf($scope.Distri)==-1){
                $scope.provedoresArray.push($scope.Distri)
            }
            $scope.Distri=''
        }
        $scope.prove=true;
        $scope.formula=false;
        $scope.index=$scope.provedoresArray.length-1
        console.log($scope.provedoresArray)
    }

    $scope.AddFormula=function(){
        $scope.receta=JSON.parse($scope.receta)
        if($scope.receta){
            if($scope.formulasArray.indexOf($scope.receta._id)==-1){
                $scope.formulasArray.push($scope.receta._id)
                $scope.Nformula[$scope.receta._id]=$scope.receta.nombre
            }
            $scope.receta=null
            //console.log($scope.formulasArray,$scope.Nformula)
            $scope.prove=false;
            $scope.formula=true;
            $scope.index=$scope.formulasArray.length-1
        }
    }

    /*$scope.autofill=function(j){
        //console.log($scope.receta.envase)
        //for (let j = 0; j < $scope.receta.envase.length; j++) {
        if($scope.receta.envase[j]){
            var id=$scope.receta.envase[j].asignado
            //si los productos tiene la receta asignada y estan en catalogo
            if($scope.envaseFor[id]){
                if($scope.envaseFor[id].receta==$scope.receta._id){
                    console.log($scope.envaseFor[id].trigger,$scope.envaseFor[id].cantidad)
                    //si posee maximo y la cantidad es menor que el maximo establecido
                    if($scope.envaseFor[id].trigger.max && $scope.envaseFor[id].trigger.max>$scope.envaseFor[id].cantidad){
                        //si no se ha asignado nada
                        if($scope.cantidadSol[$scope.receta._id]){
                            if($scope.cantidadSol[$scope.receta._id][id])$scope.cantidadSol[$scope.receta._id][id]['cantidad']=$scope.envaseFor[id].trigger.max-$scope.envaseFor[id].cantidad;
                            else{
                                $scope.cantidadSol[$scope.receta._id][id]={
                                    'cantidad':$scope.envaseFor[id].trigger.max-$scope.envaseFor[id].cantidad
                                }
                            }
                        }
                        else{
                            $scope.cantidadSol[$scope.receta._id]={}
                            $scope.cantidadSol[$scope.receta._id][id]={
                                'cantidad':Number($scope.envaseFor[id].trigger.max-$scope.envaseFor[id].cantidad)
                            }
                            $scope.cantidadSol[$scope.receta._id]['total']=0
                        }
                        console.log($scope.cantidadSol,$scope.receta._id,id)
                        $scope.envaseFor[id]['id']=$scope.envaseFor[id]._id
                        $scope.envaseFor[id]['']=$scope.envaseFor[id].contenidoNeto
                        $scope.Convertion($scope.envaseFor[id],$scope.receta._id,function(){
                            if(j+1 < $scope.receta.envase.length)$scope.autofill(j+1)
                        })
                    }
                    else $scope.autofill(j+1)
                }
            }
            else{
                if(j+1 < $scope.receta.envase.length)$scope.autofill(j+1)
            }
        }
        else{
            if(j+1 < $scope.receta.envase.length)$scope.autofill(j+1)
        }
    }*/

    $scope.SelectSide=function(x,i){
        $scope.prdt=x;
        //busqueda de la receta
        $http({
            method:'GET',
            url:'/find/formula/'+x.receta
        }).
        then(function(response){
            //console.log(response.data);
            $scope.formula=true
            $scope.receta=response.data
            //ingredientes de la formula
            var ingredientes=[]
            for (let i = 0; i < $scope.receta.ingredientes.length; i++) {
                ingredientes.push($scope.receta.ingredientes[i].objeto)
            }
            var data={
                productos:ingredientes
            };
            $http({
                method:'POST',
                url:'/productos/array/',
                data:data
            }).
            then(function(response){
                //console.log(response.data);
                $scope.ingredientes=response.data.data
            })
            //busqueda del envase
            var send=[]
            for (let j = 0; j < $scope.receta.envase.length; j++) {
                send.push($scope.receta.envase[j].asignado)
                send.push($scope.receta.envase[j].envase.objeto);
                send.push($scope.receta.envase[j].tapa.objeto);
                for (let i = 0; i < $scope.receta.envase[j].etiqueta.length; i++) {
                    send.push($scope.receta.envase[j].etiqueta[i].objeto)
                    
                }
            }
            var data={
                productos:send
            };
            $http({
                method:'POST',
                url:'/productos/array/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.envaseFor=response.data.data
                //$scope.autofill(0)
                //$scope.ProductosFor=[]
                //$scope.prdt=x.id
                $scope.AgregarProducto()
            })
        })
    }

    $scope.ActualizarTotal=function(){
        //console.log($scope.cantidadSol)
        receta=Object.keys($scope.ToProduce)
        productos=Object.keys($scope.ToProduce[$scope.receta._id].productos)
        console.log(receta,productos)
        total=0
        for (let i = 0; i < productos.length; i++) {
            const prdc = productos[i];
            if(prdc!='total'){
                total=total+$scope.ToProduce[$scope.prdt.receta].productos[prdc].kilos
            }
        }
        $scope.ToProduce[$scope.prdt.receta].total=total
    }

    $scope.AgregarProducto=function(){
        /*id=$scope.prdt
        agregar=true
        for (let i = 0; i < $scope.ProductosFor.length; i++) {
            if($scope.ProductosFor[i]._id==$scope.prdt){
                agregar=false
                break
            }
        }
        if($scope.envaseFor[id] && agregar){
            if($scope.envaseFor[id].id)$scope.envaseFor[id]['_id']=$scope.envaseFor[id].id
            $scope.ProductosFor.push($scope.envaseFor[id])
            if($scope.envaseFor[id].receta==$scope.receta._id){
                //console.log($scope.envaseFor[id].trigger,$scope.envaseFor[id].cantidad)
                //si posee maximo y la cantidad es menor que el maximo establecido
                if($scope.envaseFor[id].trigger.max && $scope.envaseFor[id].trigger.max>$scope.envaseFor[id].cantidad){
                    //si no se ha asignado nada
                    if($scope.cantidadSol[$scope.receta._id]){
                        if($scope.cantidadSol[$scope.receta._id][id])$scope.cantidadSol[$scope.receta._id][id]['cantidad']=$scope.envaseFor[id].trigger.max-$scope.envaseFor[id].cantidad;
                        else{
                            $scope.cantidadSol[$scope.receta._id][id]={
                                'cantidad':$scope.envaseFor[id].trigger.max-$scope.envaseFor[id].cantidad
                            }
                        }
                    }
                    else{
                        $scope.cantidadSol[$scope.receta._id]={}
                        $scope.cantidadSol[$scope.receta._id][id]={
                            'cantidad':Number($scope.envaseFor[id].trigger.max-$scope.envaseFor[id].cantidad)
                        }
                        $scope.cantidadSol[$scope.receta._id]['total']=0
                    }
                    console.log($scope.cantidadSol,$scope.receta._id,id)
                    $scope.envaseFor[id]['id']=$scope.envaseFor[id]._id
                    $scope.envaseFor[id]['']=$scope.envaseFor[id].contenidoNeto
                    $scope.Convertion($scope.envaseFor[id],$scope.receta._id)
                }
                //else $scope.autofill(j+1)
            }
        }*/
        //verifica si la recta ya fue incluida
        //if(typeof($scope.prdt)=='string')$scope.prdt=JSON.parse($scope.prdt)
        console.log( $scope.prdt)
        if($scope.ToProduce[$scope.prdt.receta]){
            //se verifica si el producto dentro de la receta
            if(!$scope.ToProduce[$scope.prdt.receta].productos[$scope.prdt.id]){
                $scope.ProductosFor.push($scope.prdt)
                //si faltan productos para producir
                if($scope.prdt.cantidad+$scope.extraCnt[$scope.prdt._id]<$scope.prdt.trigger.max){
                    $scope.ToProduce[$scope.prdt.receta].productos[$scope.prdt.id]={
                        cantidad:-($scope.prdt.cantidad+$scope.extraCnt[$scope.prdt._id])+$scope.prdt.trigger.max
                    }
                }
                else{
                    $scope.ToProduce[$scope.prdt.receta].productos[$scope.prdt.id]={
                        cantidad:0
                    }
                }
                $scope.calKilos($scope.prdt.receta,$scope.prdt)
            }
        }
        //en otro caso se crea el espacio para la receta
        else{
            $scope.ProductosFor.push($scope.prdt)
            $scope.ToProduce[$scope.prdt.receta]={
                productos:{},
                total:0
            }
            //si faltan productos para producir
            if($scope.prdt.cantidad<$scope.prdt.trigger.min){
                $scope.ToProduce[$scope.prdt.receta].productos[$scope.prdt.id]={
                    cantidad:-$scope.prdt.cantidad+$scope.prdt.trigger.max
                }
            }
            else{
                $scope.ToProduce[$scope.prdt.receta].productos[$scope.prdt.id]={
                    cantidad:0
                }
            }
            $scope.calKilos($scope.prdt.receta,$scope.prdt)
        }
    }

    $scope.ParcheProducto=function(){
        $scope.prdt=JSON.parse($scope.prdtslc)
        $scope.AgregarProducto()
    }

    //Realiza el primer calculo de los productos
    $scope.calKilos=function(receta,prdc){
        //parche
        if(prdc.id)prdc['_id']=prdc.id
        if(prdc.contenido_neto)prdc['contenidoNeto']=prdc.contenido_neto
        if(!prdc.convertFactor)prdc.convertFactor=1
        if(!prdc.contenidoNeto)prdc.contenidoNeto=1
        cantidad=$scope.ToProduce[$scope.prdt.receta].productos[$scope.prdt.id].cantidad
        //Calculo de la cantidad de kilos
        switch(prdc.unidad.toUpperCase()){
            case 'KG':
                kilos=cantidad*prdc.contenidoNeto;
                break
            case 'G':
                kilos=cantidad*prdc.contenidoNeto/1000;
                break
            case 'L':
                kilos=cantidad*prdc.contenidoNeto*prdc.convertFactor;
                break
            case 'ML':
                kilos=cantidad*prdc.contenidoNeto*prdc.convertFactor/1000;
                break
            case 'GL':
                kilos=cantidad*prdc.contenidoNeto*prdc.convertFactor/3.78541;
                break
            default:
                kilos=cantidad*prdc.contenidoNeto*prdc.convertFactor;
                break
        }
        $scope.ToProduce[$scope.prdt.receta].productos[$scope.prdt.id]['kilos']=kilos
        $scope.ToProduce[$scope.prdt.receta].total=$scope.ToProduce[$scope.prdt.receta].total+kilos
        console.log($scope.ToProduce)
    }

    $scope.clasificador = function(){
        for (let i = 0; i < $scope.colect.length; i++) {
            var key = $scope.colect[i]
                //console.log(key.nombre)
                //console.log($scope.inventario[$scope.colect[i]])
                for (let j = 0; j < $scope.inventario[key].length; j++) {
                    const prdc = $scope.inventario[key][j];
                    //console.log(j,prdc)
                    prdc['tipo']=key.tipo
                    $scope.productos.push(prdc)
                }//$scope.colect[i];
        }
        console.log($scope.productos)
    }

    $scope.FilterIsIn=function(item,look){
        if(look){
            console.log(item,look)
            return item.distribuidores.indexOf(look)>-1
        }
        else return false
    }

    $scope.LookDist = function(){
        $http({
            method:'GET',
            url:'/get/provedores/'+$cookies.get('empresa')
        }).
        then(function(response){
            $scope.Distribuidores=response.data.data.keys
            console.log($scope.Distribuidores)
        })
    }

    $scope.init=function(){
        busquedas.catalogo($cookies.get('empresa'),function(col,invt){
                col.splice(col.indexOf('keys'),1);
                $scope.colect=col
                $scope.inventario= invt;
                console.log($scope.colect,$scope.inventario)
                $scope.clasificador()
                $scope.LookDist()
        })
        $http({
            method:'GET',
            url:'/get/materia/'+$cookies.get('empresa')
        }).
        then(function(response){
            console.log(response.data);
            $scope.AllFM=response.data.keys;
            $scope.mp=[]//response.data
            for (let i = 0; i < response.data.keys.length; i++) {
                for (let j = 0; j < response.data.data[response.data.keys[i]].length; j++) {
                    response.data.data[response.data.keys[i]][j]['familia']=response.data.keys[i];
                }
                $scope.mp=$scope.mp.concat(response.data.data[response.data.keys[i]]);
                
            }

            $scope.makeExtra()
            console.log($scope.mp)
        })
    }
    $scope.init()


    $scope.page=function(i){
        $scope.day=new Date()
        switch (i) {
            case 0:
                $scope.Mpage='min-max';
                break;
            case 1:
                $scope.Mpage='reabastecer';
                break;
            case 2:
                /*console.log($scope.cantidadSol[$scope.receta._id])
                $scope.solitud=[]
                sear=[]
                for (let i = 0; i < Object.keys($scope.cantidadSol[$scope.receta._id]).length-1; i++) {
                    const key = Object.keys($scope.cantidadSol[$scope.receta._id])[i];
                    $scope.solitud.push({
                        producto:key,
                        cantidad:$scope.cantidadSol[$scope.receta._id][key]['cantidad'],
                        prioridad:1,
                    })
                    sear.push(key)
                }
                var data={
                    productos:sear
                };
                $http({
                    method:'POST',
                    url:'/productos/array/',
                    data:data
                }).
                then(function(response){
                    console.log(response.data);
                    $scope.objetos=response.data.data
                })*/
                $scope.Mpage='show';
                break;
            case 2:
                $scope.Mpage='MatPrm';
                break;
            case 4:
                for (let i = 0; i < $scope.mp.length; i++) {
                    const mp = $scope.mp[i];
                    if(mp.trigger.min>mp.cantidad){
                        $scope.MpSol[mp.id]=mp.trigger.max-mp.cantidad
                    }
                }
                $scope.Mpage='AutoMatP';
                break;
            default:
                break;
        }
    }

    $scope.setMin=function(x,i){
        $scope.page(0)
        $scope.ProductoSet=x
        $scope.i=i
    }

    $scope.OrdenarMas=function(x,i){
        $scope.page(1)
        $scope.ProductoSet=x
        $scope.i=i
    }

    $scope.changeMin=function(){
        var min=$scope.min
        var max=$scope.max
        if(min>max){
            var aux=min;
            min=max
            max=aux
        }
        console.log($scope.ProductoSet._id,min,max)
        editar.minmax($scope.ProductoSet._id,min,max,function(response){
            $scope.productos[$scope.i].trigger.min=min
            $scope.productos[$scope.i].trigger.max=max

        })
    }

    $scope.showProducir=function(){
        busquedas.productosArray(lista,function(response){
            console.log(response)
            $scope.page(2)
            $scope.productosindex=response.data
        })
    }

    $scope.EliminarSolicitud=function(w,i){
        console.log($scope.ProductosFor)
        var producto=''
        if(w==0) {
            for (let j = 0; j < $scope.ProductosFor.length; j++) {
                if($scope.ProductosFor[j]._id==i){
                    //producto=$scope.ProductosFor[j].producto
                    n=j
                    break
                }
            }
            $scope.ToProduce[$scope.receta._id].productos[i].kilos=0
            $scope.ToProduce[$scope.receta._id].productos[i].cantidad=0
            $scope.ProductosFor.splice(n,1);
        }
        else{
            producto=$scope.solicitar[i].producto
            $scope.solicitar.splice(i,1);
        }
        lista.splice(lista.indexOf(producto),1);
        $scope.ActualizarTotal()
    }

    $scope.pushProducir=function(x){
        if(lista.indexOf(x._id)==-1){
            lista.push(x._id)
            if(x.receta != null){
                $scope.Producir.push({
                    producto:x._id,
                    cantidad:$scope.cantidad
                })
            }
            else{
                $scope.solicitar.push({
                    producto:x._id,
                    cantidad:$scope.cantidad
                })
            }
        }
        else{
            if(x.receta != null){
                for (let i = 0; i < $scope.Producir.length; i++) {
                    if($scope.Producir[i].producto==x._id)$scope.Producir[i].cantidad=$scope.cantidad
                    break
                }
            }
            else{
                for (let i = 0; i < $scope.solicitar.length; i++) {
                    if($scope.solicitar[i].producto==x._id)$scope.solicitar[i].cantidad=$scope.cantidad
                    break
                }
            }
        }
        console.log($scope.Producir)
    }

    $scope.reset=function(){
        $scope.verOrden=false;
        $scope.productos=[];
        $scope.filter=''
        $scope.filCod=''
        $scope.Mpage=''
        $scope.Producir=[]
        $scope.solicitar=[]
        $scope.emailFrom= 'kmena281@gmail.com';
        $scope.emailTo='';
        var lista=[]
        $scope.tipo='PT'
        $scope.init()
    }

    $scope.enviarMp=function(){
        texto='Productos a solicitar\n'
        for (let i = 0; i < $scope.mp.length; i++) {
            if($scope.MpSol[$scope.mp[i].id]){
                texto=texto+'Producto:'+$scope.mp[i].nombre+'cantidad'+$scope.MpSol[$scope.mp[i].id]+$scope.mp[i].unidad+'\t';
                texto=texto+'Proveedores:';
                if($scope.mp[i].distribuidores.length==0)texto=texto+'Desconocidos'
                else{
                    texto=texto+$scope.mp[i].distribuidores[0]
                    for (let i = 1; i < $scope.mp[i].distribuidores.length; i++) {
                        texto=texto+$scope.mp[i].distribuidores[i];
                    }
                }
                texto=texto+'\n'
            }
        }
        var correo=$window.prompt('Correo al cual enviar el recordatorio')
        crear.email($scope.emailFrom,correo,'recordario de solitud de pedido',texto,function(response){
            console.log(response)
        })
    }

    $scope.sendSolicitar= function(){
        var texto = 'Recordario de parte de PassArte \n Se le recuerda que debe solicitar:\n'
        for (let i = 0; i < $scope.solicitar.length; i++) {
            const solitud = $scope.solicitar[i];
            texto=texto+solitud.nombre+':'+solitud.cantidad+'\n'
        }
        crear.email($scope.emailFrom,$scope.emailTo,'recordario de solitud de pedido',texto,function(response){
            console.log(response)
            $scope.reset()
        })
    }

    $scope.crearOrden = function(){
        $scope.solitud=[]
        var usuario = $window.prompt('Ingrese su nombre de usario')
        var password= $window.prompt('Ingrese su contraseña')
        data={
            'user':usuario,
            'password':password
        }
        $http({
            method:'POST',
            url:'/login/',
            data:data
        }).
        then(function(response){
            console.log(response.data)
            if(response.data.err==0){
                //reduccion de ingredientes
                for (let i = 0; i < $scope.ProductosFor.length; i++) {
                    const x = $scope.ProductosFor[i];
                    console.log($scope.ToProduce[$scope.prdt.receta].productos,$scope.prdt.receta,x._id)
                    $scope.solitud.push({
                        producto:x._id,
                        cantidad:$scope.ToProduce[$scope.prdt.receta].productos[x._id].cantidad,
                        prioridad:x.prioridad,
                    })
                }
                var send=[]
                for (let i = 0; i < $scope.receta.ingredientes.length; i++) {
                    const mp = $scope.receta.ingredientes[i];
                    send.push({
                        object:mp.objeto,
                        cantidad:mp.cantidad*$scope.ToProduce[$scope.receta._id]['total']
                    })
                }
                //reduccion de etiquetas
                for (let j = 0; j < $scope.receta.envase.length; j++) {
                    const envase = $scope.receta.envase[j];
                    if($scope.ToProduce[$scope.receta._id].productos[envase.asignado]){
                        send.push({
                            object:envase.tapa.objeto,
                            cantidad:$scope.ToProduce[$scope.receta._id].productos[envase.asignado]['cantidad']*envase.tapa.cantidad
                        })
                        send.push({
                            object:envase.envase.objeto,
                            cantidad:$scope.ToProduce[$scope.receta._id].productos[envase.asignado]['cantidad']*envase.envase.cantidad
                        })
                        for (let k = 0; k < envase.etiqueta.length; k++) {
                            const etiqueta = envase.etiqueta[k];
                            send.push({
                                object:etiqueta.objeto,
                                cantidad:$scope.ToProduce[$scope.receta._id].productos[envase.asignado]['cantidad']*etiqueta.cantidad
                            })
                        }
                    }
                }
                var data={
                    'emp':$cookies.get('empresa'),
                    'tipo':1,
                    'cliente':null,
                    'Transporte':'correos',
                    'productos':JSON.stringify($scope.solitud),
                    'Aprv':1,
                    'Apr_By':response.data.data.id,
                    'MP':send,
                    'numero':$scope.consecutivo,
                    'formula':$scope.receta.nombre,
                    'comentario':$scope.comentarioP
                };
                console.log(data)  
                $http({
                    method:'POST',
                    url:'/crear/orden/',
                    data:data
                }).
                then(function(response){
                    console.log(response.data);
                    $scope.comentarioP=null
                    $scope.ToProduce[$scope.receta._id]={
                        productos:{},
                        total:0
                    }
                    $scope.reset()
                    $window.alert('Orden creada con el consecutivo '+$scope.consecutivo)
                    //editar.ChangeSetArray($scope.Producir,function(){
                    //  $scope.sendSolicitar()
                    //})
                })
            }
            else $window.alert('Error de identificacion')
        })
        
    }
}])

app.filter("rev",function(){
    return function(x,y){
        if(y.cantidad){
            console.log(x,y)
            ret =false
            for (let i = 0; i < y.receta.ingredientes.length; i++) {
                const rct = y.receta.ingredientes[i];
                if(x[rct.objeto].cantidad<=y.cantidad*rct.cantidad){
                    ret=true
                    break
                }
            }
            if(ret) return ret
            //verifica si hay sufuciente envases
            else{
                //verifica todos los productos agregados
                for (let j = 0; j < y.Prodts.length; j++) {
                    const id = y.Prodts[j]._id;
                    //barre todos los envases buscando el que le pertenece a la formula
                    for (let k = 0; k < y.receta.envase.length; k++) {
                        const envase = y.receta.envase[k];
                        if(envase.asignado==id){
                            //si hay suficientes tapas y envase
                            if(y.envaseFor[envase.tapa.objeto].cantidad>=envase.tapa.cantidad*y.ToProduce[y.receta._id].productos[id].cantidad &&
                                y.envaseFor[envase.envase.objeto].cantidad>=envase.envase.cantidad*y.ToProduce[y.receta._id].productos[id].cantidad){
                                //si hay suficientes etiquetas
                                for (let m = 0; m < envase.etiqueta.length; m++) {
                                    const etq = envase.etiqueta[m];
                                    if(y.envaseFor[etq.objeto].cantidad<=etq.cantidad*y.ToProduce[y.receta._id].productos[id].cantidad){
                                        ret=true
                                        break
                                    }                              
                                }
                            }
                            else{
                                ret=true
                            }
                            if(ret) break
                        }                        
                    }
                    if(ret) break
                }
                return ret
            }
            return ret           
        }
        else return true
    }
})
