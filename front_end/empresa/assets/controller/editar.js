var app = angular.module("app", ['ngCookies','ui.bootstrap']);
app.controller('editar',['$scope','$http','$cookies','$window','$modal','busquedas','crear','editar','links',function($scope,$http,$cookies,$window,$modal,busquedas,crear,editar,links){    
    console.log('editar cookies',$cookies.getAll())
    //saltos de pagina
    $scope.menu=[]
    $scope.links = links.links
    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }
//init var
    $scope.creando=false;
    $scope.userShow =true;
    $scope.modalShow="archiving";
    $scope.rules=["moduled_tolerance","simple_tolerance","fixed_tolerance","time","radio","alerta","Almacenar"];
    $scope.propertys=["lat","lon","speed","dist","battery","date"];
    $scope.operators=["ge","le","l","g","e","ne"];
    $scope.operator="ge";
    $scope.modulo=0;
    $scope.master=false;
    $scope.diviceId=$cookies.get('deviceId');
    $scope.controllerModal='archive';
    $scope.templateModal='/empresa/assets/htmls/archive.html';


//cambios de modal
    $scope.modalSet = function(id,where){
        console.log(id,where);
        switch (where) {
            case 'Arch':
                $scope.controllerModal='archive';
                $scope.templateModal='/empresa/assets/htmls/archive.html';
                $scope.modal="archiving";
                $cookies.put('deviceId',id,{'path':'/'});
                $scope.ModalHeader="nuevo criterio de guardado"
                break;
            default:
                break;
        }
    }
    
    $scope.check =function(dato){
        console.log('checkeando',$scope.propertys)
        if($scope.rule=="time"){
            $scope.propertys=["Y","M","D","H","m","s"];
        }
        else if($scope.rule=="alerta"){
            $scope.propertys=["geocerca","Static"];
        }
        else{
            $scope.propertys=["lat","lon","speed","dist","battery","date"];;
        }

    }
//creacion
    $scope.crearusuario=function(){
        if($scope.UNombre!=undefined && $scope.Uemail!=undefined && $scope.Ucedula!=undefined && $scope.Utelefono!=undefined && Number($scope.Univel)!=undefined && $scope.Uuser!=undefined && $scope.Upassword!=undefined ){
            $scope.creando=true
            crear.usuario($scope.UNombre,$scope.Uemail,$scope.Ucedula,$scope.Utelefono,Number($scope.Univel),$scope.Uuser,$scope.Upassword,$cookies.get('empresa'),function(response){
                console.log(response)
                $scope.creando=false
                if(response.data.err==0){
                    $scope.UNombre=null
                    $scope.Uemail=null
                    $scope.Ucedula=null
                    $scope.Utelefono=null
                    $scope.Univel=null
                    $scope.Uuser=null
                    $scope.Upassword=null                    
                }
                else{
                    switch (response.data.err) {
                        case 1:
                            $window.alert('No se pudo guardar el usuario debio a un error del sistema \nPor favor trate mas tarde');
                            break;
                        case 2:
                            $window.alert('No se reconoce la empresa de usuario que realiazo el login\nReinicie seccion nuevamente para proceder');
                            break;
                        case 3:
                             $window.alert('El usuario ya existe\nPruebe con otro nombre de usuario')
                        default:
                            break;
                    }
                }
            })
        }
        //crear.usuario(nombre,email,cedula,telefono,nivel,user,password,empresa)
    }
//edidicion
    $scope.set=function(){
        console.log($scope.rule,$scope.property,$scope.operator,$scope.master);
        crear.archiving($scope.diviceId,$scope.rule,$scope.property,$scope.tolerancia,$scope.modulo,$scope.ref,$scope.operator,$scope.master,function(response){
            console.log(response.data);
        })
    }

    $scope.ChmEdit=function(){
        $scope.userShow =false;
        $scope.EUNombre=$scope.dataUsr.nombre
        $scope.EUusuario=$scope.dataUsr.usuario
        $scope.EUcedula=Number($scope.dataUsr.cedula)
        $scope.EUtelefono=Number($scope.dataUsr.telefono)
        $scope.EUcorreo=$scope.dataUsr.correo
        $scope.EUcod=$scope.dataUsr.cod
    }

    $scope.ChmEmp=function(){
        $scope.userShow =false;
        $scope.EUNombre=$scope.empresaData.nombre
        $scope.EUcedula=Number($scope.empresaData.cedula)
        $scope.EUtelefono=Number($scope.empresaData.phone)
        $scope.EUcorreo=$scope.empresaData.email
        $scope.EUFax=$scope.empresaData.fax
        $scope.EUdireccion=$scope.empresaData.direccion
        $scope.FactC=$scope.empresaData.Facturacion.codigo
        $scope.FactI=$scope.empresaData.Facturacion.informacion
        $scope.FactL=$scope.empresaData.Facturacion.legal

    }
    //edicion de concepto legal de las facturas
    $scope.pushLegal=function(){
        $scope.FactL.push("Nueva linea")
        console.log($scope.FactL)
    }
    $scope.editarLegalPromt=function(i){
        var legal=$window.prompt('Dato legal numero '+i)
        if(legal) {
            if($scope.FactL.indexOf(legal)==-1) $scope.FactL[i]=legal;
        }
    }
    //esdicion de los contactos innmediatos
    $scope.changeCI=function(){
        console.log(JSON.parse($scope.contI))
        var cont=JSON.parse($scope.contI)
        busquedas.usuarios(cont.id,function(response){
            console.log(response.data);
            //$scope.dataUsr = response.data;
            $scope.empresaData.Cont_Cont_imm={
                cedula:response.data.cedula,
                correo:response.data.correo,
                nombre:response.data.nombre,
                telefono:response.data.telefono,
            }
        });
        
    }
    //edidicion de usuario
    $scope.editarUser=function(){
        var data={
            id:$scope.user,
            nombre:$scope.EUNombre,
            email:$scope.EUcorreo,
            cedula:$scope.EUcedula,
            numero_tel:$scope.EUtelefono,
            tipo:2,
            cod:$scope.EUcod,
            user:$scope.EUusuario,
        };
        $http({
            method:'POST',
            url:'/edit/usuario/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            })
    }
    //correos de contacto
    $scope.addextemail=function(){
        var email=$window.prompt('Inserte el nuevo correo de contacto')
        if(email){
            $scope.empresaData.ExtEmail.push(email)
        }
    }
    $scope.eliminarEmail=function(i){
        $scope.empresaData.ExtEmail.splice(i,1);
    }
    //edicion de datos de la empresa
    $scope.editarEmp=function(){
        var data={
            empresa:$cookies.get('empresa'),
            Name:$scope.EUNombre,
            Ced_J:$scope.EUcedula,
            phone:$scope.EUtelefono,
            email:$scope.EUcorreo,
            fax:$scope.EUFax,
            direccion:$scope.EUdireccion,
            Cont_Name:$scope.empresaData.nombre,
            Cont_ID:$scope.empresaData.cedula,
            Cont_phone:$scope.empresaData.telefono,
            Cont_email:$scope.empresaData.correo,
            extemail:$scope.empresaData.ExtEmail,
        };
        $http({
            method:'POST',
            url:'/edit/empresa/',
            data:data
        }).
        then(function(response){
            $http({
                method:'GET',
                url:'/find/company/'+$cookies.get('empresa')
            }).
            then(function(response){
                console.log(response.data);
                $scope.userShow =true;
                $scope.empresaData=response.data
            })
        })
    }
//edicion de datos para facturar
    $scope.editarFact=function(){
        var data={
            empresa:$cookies.get('empresa'),
            codigo:$scope.FactC,
            informacion:$scope.FactI,
            legal:$scope.FactL,
        };
        $http({
            method:'POST',
            url:'/editar/facturacion/empresa/',
            data:data
        }).
        then(function(response){
            $http({
                method:'GET',
                url:'/find/company/'+$cookies.get('empresa')
            }).
            then(function(response){
                $scope.empresaData=response.data
                $scope.ChmEmp()
            })
        })
    }
    $scope.agregarempleado=function(){
        if($scope.empleado && $scope.salario>0){
            $scope.costos.empleados.push({
                nombre:$scope.empleado,
                costo:$scope.salario,
            })
        }    
        $scope.empleado=null
        $scope.salario=0
    }
    $scope.eliminarempleado=function(i){
        $scope.costos.empleados.splice(i,1);
    }
    $scope.actualizarCostos=function(){
        var costoE=$scope.costos.electricidad.KwH
        var costoA=$scope.costos.agua.m3
        $scope.costos.electricidad.KwH=$scope.costos.electricidad.KwH/($scope.costos.alquiler.dias*$scope.costos.alquiler.horas)
        $scope.costos.agua.m3=$scope.costos.agua.m3/($scope.costos.alquiler.dias*$scope.costos.alquiler.horas)
        var data={
            empresa:$cookies.get('empresa'),
            costos:$scope.costos
        };
        console.log(data)
        $http({
            method:'POST',
            url:'/actualizar/costos',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.costos=response.data
            $scope.costos.electricidad.KwH=costoE
            $scope.costos.agua.m3=costoA
        })
    }

    $scope.chguser=function(user){
        if(typeof(user)=='string')user=JSON.parse(user)
        console.log(user)
        $scope.userShow=true
        $scope.data(user.id)
    }

//busquedas
    $scope.clientes=function(){
        $http({
            method:'GET',
            url:'/clientes/agentes/get/'+$scope.dataUsr.id+'/'+$cookies.get('empresa')+'/'
        }).
        then(function(response){
            $scope.mycln = response.data.data;
            console.log($scope.mycln)
        })
    }

    $scope.data=function(user){
        //$scope.mycln=new Array(0);
        if(user==undefined){
            if($scope.users.length>0){
            }
        }
        else{
            $scope.user = user;
            busquedas.usuarios(user,function(response){
                console.log(response.data);
                $scope.dataUsr = response.data;
                $scope.clientes()
            });
        }
    }
    $scope.findAllusers=function(level){
        //console.log(level)
        $http({
            method:'GET',
            url:'/usuarios/empresa/'+$cookies.get('empresa')+'/'+String(level)
        }).
        then(function(response){
            console.log(response.data);
            $scope.allUsers=$scope.allUsers.concat(response.data.datos)
            if(level<3) $scope.findAllusers(level+1)
            else {
                console.log($scope.allUsers)
                $scope.data($scope.allUsers[0].id)
            }
        })
    }
    $scope.buscarEmpresa=function(){
        $http({
            method:'GET',
            url:'/find/company/'+$cookies.get('empresa')
        }).
        then(function(response){
            console.log(response.data);
            $scope.empresaData=response.data
            $scope.costos=response.data.costos
            $scope.costos.electricidad.KwH=$scope.costos.electricidad.KwH*($scope.costos.alquiler.dias*$scope.costos.alquiler.horas)
            $scope.costos.agua.m3=$scope.costos.agua.m3*($scope.costos.alquiler.dias*$scope.costos.alquiler.horas)
            $scope.allUsers=[]
            $scope.findAllusers(1)
        })
    }
//transporte
    $scope.CrearNuevoTransporte=function(){
        if($scope.transportes.indexOf($scope.NewTransporte)==-1){
            crear.transporte($cookies.get('empresa'),$scope.NewTransporte,function(respuesta){
                console.log(respuesta)
                $scope.transportes=respuesta;        
            })
        }
        $scope.NewTransporte='';
    }

    $scope.RemoveTransporte=function(i){
        console.log($scope.transportes[i])
        editar.deleteTransporte($cookies.get('empresa'),$scope.transportes[i],function(response){
            console.log(response.data)
            $scope.transportes=response.data;
        })
    }
//eliminar 
    $scope.eliminarUsuario=function(){
        //console.log($scope.dataUsr)
        if($window.confirm('Desea eliminar el usuario '+$scope.dataUsr.user)){
            var data={
                empresa:$cookies.get('empresa'),
                usuario:$scope.dataUsr.id
            };
            $http({
                method:'POST',
                url:'/user/eliminar/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.allUsers=[]
                $scope.findAllusers(1)
            })
        }
    }
//inicio
    $scope.init=function(){
        busquedas.init(function(datos,extra){
            console.log('empresa y usuarios',datos,extra);
            $scope.users= datos;
            $scope.empresa= extra;
            //$scope.data($cookies.get('userSch'));
            $scope.buscarEmpresa()
        });
        busquedas.transporte($cookies.get('empresa'),function(response){
            console.log(response)
            $scope.transportes=response;
        })
    }
    $scope.init()

//watch
    $scope.$watch('rule',function(newCollection, oldCollection){
        console.log('rule',newCollection);
    })
    $scope.$watch('user',function(newCollection, oldCollection){
        console.log(newCollection);
        if(newCollection != undefined){
            $cookies.put('userSch',newCollection,{'path': '/'})
            //$scope.data($scope.user);
        }
        $scope.space = 'datos';
    })
}])

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        input=Number(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return null
    }
})