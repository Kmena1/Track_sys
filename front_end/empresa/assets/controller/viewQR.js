var app =angular.module("app",['ui.bootstrap','ngCookies']);
app.controller('view',['$scope','$http','$cookies','$window','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,crear,busquedas,editar,links){
    console.log($cookies.getAll());
    //console.log($window.data)
    $scope.fecha=new Date()
    var empresa=$window.location.href.split('/')[4]
    var lote=$window.location.href.split('/')[5]
    var num=$window.location.href.split('/')[6]
    var puntos=$window.location.href.split('/')[7]
    console.log(empresa,lote,num)
    $scope.clientes=[]

    $scope.bloqueado=false

    $scope.selectCln=function(){
        var cln=JSON.parse($scope.client)
        $scope.user=undefined
        $scope.puntos=cln.puntos
        console.log($scope.client,$scope.puntos)
    }

    $scope.AgregarDependiente=function(){
        var cln = JSON.parse($scope.client)
        var dep=$window.prompt('Nombre del dependiente');
        var cor=$window.prompt('Correo del dependiente');
        if($window.confirm('Desea crea el dependiente\nNombre:'+dep+'\nCorreo:'+cor)){
            var data={
                cln:cln._id,
                correo:cor,
                dependiente:dep
            };
            $http({
                method:'POST',
                url:'/agregar/dependiente/',
                data:data
            }).
            then(function(response){
                console.log(response.data)
                $scope.client=response.data
            })
        }
    }

    $scope.autorizar=function(){
        if($scope.user!=undefined){
            console.log($scope.client)
            var cln = JSON.parse($scope.client)
            var data={
                cln:cln._id,
                i:$scope.user,
                num:num,
                id:lote,
                puntos:puntos
            };
            $http({
                method:'POST',
                url:'/agregar/puntos/dependiente/',
                data:data
            }).
            then(function(response){
                $scope.bloqueado=true
                //console.log(response.data);
                $window.alert('Punto agregado con exito')
                $window.close()
            })
        }
        else{
            $window.alert('Seleccione un dependiente para autorizar los puntos')
        }
    }

    $scope.clientesSearch=function(init){
        //console.log(empresa)
        busquedas.clientes(empresa,null,String(init),function(response){
            //console.log(response.max == response.last,response.max,response.last);
            if (response.max == response.last) {
                //llena el scope de respaldo
                $scope.clientes=$scope.clientes.concat(response.datos);
                console.log($scope.clientes)
            } 
            else 
            {
                $scope.clientes=$scope.clientes.concat(response.datos);
                $scope.clientesSearch(response.last)
            }
        })
    }

    $scope.init=function(){
        $http({
            method:'GET',
            url:'/get/lote/'+lote
        }).
        then(function(response){
            console.log(response.data);
            if($cookies.get('id')){
                var lt=response.data
                if(lt.qr.indexOf(num)==-1 && lt.cantidad>num){
                    if(puntos>0) $scope.clientesSearch(0)
                    else $window.alert('El producto escaneado no se encuentra en campaña')
                }
                else{
                    $scope.bloqueado=true
                    $window.alert('Ese codigo qr ya fue registrado o no es valido')
                    $window.close()
                }
            }
            else{
                var usuario = $window.prompt('Ingrese su nombre de usario')
                var password= $window.prompt('Ingrese su contraseña')
                data={
                    'user':usuario,
                    'password':password
                }
                $http({
                    method:'POST',
                    url:'/login/',
                    data:data
                }).
                then(function(response){
                    switch(response.data.err){
                        case 0:
                            $cookies.put('id',response.data.data.id, {'path': '/'});
                            $cookies.put('empresa',response.data.data.empresa, {'path': '/'});
                            $scope.init()
                            break
                        default:
                            $scope.bloqueado=true
                            $window.alert('error de identificacion')
                            break
                    }
                })
            }
        })
    }
    $scope.init()
}])