
var imp1=13
var imp2=2

app.controller('facturas',['$scope','$http','$cookies','$window','$timeout','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,$timeout,crear,busquedas,editar,links){
    console.log($cookies.getAll());
    if($cookies.get('empresa')==null) $window.open('/login/index.html')
    $scope.idobject = null;
    $scope.Nstats="empleados";
    $scope.stat=0;
    $scope.colect=null;
    $scope.edit=false;
    $scope.carrito=[];
     $scope.clientes=[]
    $scope.Jclient={};
    $scope.credito={}
    $scope.Jusers={};
    $scope.Jobjeto={};
    $scope.JPrecio={}
    $scope.reservas=[];
    $scope.pedidosArray=[];
    $scope.Lastid='null'
    $scope.Rechazo={}
    $scope.stat=0;
    $scope.Revizados=false
    $scope.Fagente=null;
    $scope.Fcliente=null;


     $scope.menu=[]
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }
    
    //busquedas.getCurrency(function(response){
    //     console.log(response)
    //     $scope.currency=response.val;
    // })
    //modal
    $scope.page=function(page){
        console.log(page)
        switch (Number(page)) {
            case 0:
                $scope.facturas = true;
                $scope.stats= false;
                break;
            case 1:
                $scope.facturas = false;
                $scope.stats = true;
                break;
            default:
                break;
        }
                
    }

    //graficas
    var chart = Highcharts.chart({
        chart: {
            renderTo: 'container',
            margin: [150, 75, 75, 75],
            type: 'column',
        },
        title: {
            text: 'Estadisticas de '+$scope.Nstats
        },
        xAxis: {
            type: "category"
        },
        yAxis: {
            title: {
                text: 'cantidad'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.y}'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

        colors: ['#6CF', '#39F', '#06C', '#036', '#000'],

        series: [{
            name:"productos",
            data:[]
        }]
    })

    $scope.changeAgenteFilter=function(x){
        console.log(x)
        $scope.Fagente=x
    }

    $scope.chardatos=function(){
        var data=[];
        console.log($scope.stat);
        switch(Number($scope.stat)){
            case 0:
                for (let i = 0; i < $scope.users.length; i++) {
                    let user = $scope.users[i];
                    data.push([user.nombre,0])
                    for (let j = 0; j < $scope.reservas.length; j++) {
                        let resvr = $scope.reservas[j];
                        if(resvr.Nagente==user.nombre) data[i][1]+=Number(resvr.cantidad);
                    }
                }
                chart.setTitle({text: "Estadisticas de Agentes"});
                break
            case 1:
                if ($scope.colect ==null) $scope.colect=$scope.collections[0]
                for (let i = 0; i < $scope.inventario[$scope.colect].length; i++) {
                    let objeto = $scope.inventario[$scope.colect][i];
                    data.push([objeto.nombre,0])
                    for (let j = 0; j < $scope.reservas.length; j++) {
                        let resvr = $scope.reservas[j];
                        if(resvr.Nobjeto==objeto.nombre) data[i][1]+=Number(resvr.cantidad);
                    }
                }
                chart.setTitle({text: "Estadisticas de Producto"});
                break;
            case 2:
                for (let i = 0; i < $scope.clientes.length; i++) {
                    let cliente = $scope.clientes[i];
                    data.push([cliente.nombre,0])
                    for (let j = 0; j < $scope.reservas.length; j++) {
                        let resvr = $scope.reservas[j];
                        if(resvr.Ncliente==cliente.nombre) data[i][1]+=Number(resvr.cantidad);
                    }
                }
                chart.setTitle({text: "Estadisticas de Clientes"});
                break;
        }
        console.log(data)
        chart.series[0].setData(data)
    }

    //filter
    $scope.filterreservas = function(){
        $scope.filteResv=[];
        if($scope.filter != undefined && $scope.filter != ''){
            let busqueda=$scope.filter;
            for (let i = 0; i < $scope.reservas.length; i++) {
                let reserva=$scope.reservas[i]
                console.log(reserva.Nagente.indexOf(busqueda),reserva.Ncliente.indexOf(busqueda),reserva.Nobjeto.indexOf(busqueda))
                if(reserva.Nagente.indexOf(busqueda)>-1 || reserva.Ncliente.indexOf(busqueda)>-1 || reserva.Nobjeto.indexOf(busqueda)>-1) $scope.filteResv.push(reserva)
                
            }
        }
        else $scope.filteResv=$scope.reservas;
        $scope.chardatos()
    }

    $scope.resumeClients=function(){
        $scope.ClientesFilters=[];
        //console.log($scope.clientes)
        if($scope.filter != undefined && $scope.filter != ''){
            let busqueda=$scope.filter.toUpperCase();
            for (let i = 0; i < $scope.clientes.length; i++) {
                let cliente=$scope.clientes[i].nombre.toUpperCase();
                //console.log(reserva.Nagente.indexOf(busqueda),reserva.Ncliente.indexOf(busqueda),reserva.Nobjeto.indexOf(busqueda))
                if(cliente.indexOf(busqueda)>-1) $scope.ClientesFilters.push($scope.clientes[i])
                
            }
        }
        else $scope.ClientesFilters=$scope.clientes;
    }

    //agragar nombre de cliente a las reservas
    $scope.NombreResrvas=function(id,objeto,i,j){
        for (let i = 0; i < $scope.resvObj.length; i++) {
            $scope.resvObj[i]["Ncliente"]= $scope.Jclient[$scope.resvObj[i].cliente];
            $scope.resvObj[i]["Nagente"]= $scope.Jusers[$scope.resvObj[i].agente];
            $scope.resvObj[i]["objeto"]=id;
            $scope.resvObj[i]["Nobjeto"]=$scope.Jobjeto[id];
        }
        console.log($scope.resvObj)
        $scope.reservas=$scope.reservas.concat($scope.resvObj);
        $scope.AllResv(i,j+1)
    }

    //se obtienen las reservas
    $scope.obtenerReservas=function(id,objeto,i,j){
        console.log(id);
        busquedas.reservas(id,function(response){
            //console.log(response.data);
            $scope.resvObj=response.data.data
            $scope.NombreResrvas(id,objeto,i,j);
        })
    }

    //todas las reservas
    $scope.AllResv = function(i,j){
        let key = $scope.collections[i];
        if(j< $scope.inventario[key].length){
            let objeto = $scope.inventario[key][j];
            console.log(objeto);
            $scope.obtenerReservas(objeto._id,objeto.nombre,i,j);
        }
        else{
            if($scope.collections.length>i+1) $scope.AllResv(i+1,0)
            else $scope.filterreservas()//console.log($scope.reservas)
        }
    }

    //elimanar una reserva
    $scope.eliminar=function(objeto,reserva,pedido){
        editar.DeleteReservas(objeto,reserva,pedido,function(response){
            console.log(response.data);
            $scope.pedidosArray=[]
            $scope.reservas = [];
            $scope.ObtenerPedidos()
        })
    }

    $scope.dataRechazo=function(x){
        $scope.RechazoModel=true;
        console.log(x);
        $scope.Rechazo=x
    }

    $scope.RechazarPedido=function(x){
        $scope.RechazoModel=false;
        //let reservasA=$scope.pedid$scope.pedidosArray[i].reservasindex[j];
        console.log(x);
        crear.mensaje($cookies.get('id'),x.agente,'Rechazo del pedido al cliente '+x.Ncliente,'Se rechzo el pedido :' +x._id+',Por los motivos'+$scope.mensjRechazo+'\nFavor edite el pedido e intentelo de nuevo',function(response){
            console.log(response)
            editar.aceptarpedido($cookies.get('id'), x._id,[],0,function(response){
                console.log(response)
                $scope.reset()
            })
        })
        /*editar.DeletePedido($cookies.get('empresa'),$scope.reservas[reservasA].objeto,$scope.reservas[reservasA]._id,$scope.reservas[reservasA].pedido,function(response){
            console.log(response.data);
            if($scope.pedidosArray[i].reservasindex.length > j+1) $scope.eliminarPedido(i,j+1)
            else{
                $scope.pedidosArray=[]
                $scope.reservas = [];
                $scope.ObtenerPedidos()
            }
        })*/
        //console.log($cookies.get('empresa'),$scope.reservas[reservasA].objeto,$scope.reservas[reservasA]._id,$scope.reservas[reservasA].pedido)
    }

    $scope.datafactura=function(x){
        $scope.facturaShowing = x;
    }

    $scope.dataAceptar=function(x){
        $scope.aprovados=[]
        $scope.seleccionados=[]
        $scope.AceptarModel=true;
        $scope.FinalPrice=0;
        for (let i = 0; i < x.reservasindex.length; i++) {
            const index = x.reservasindex[i];
            if($scope.reservas[index].aprovado){
                $scope.seleccionados.push(true)
                $scope.aprovados.push($scope.reservas[index]._id)
                $scope.FinalPrice=$scope.FinalPrice+$scope.reservas[index].precio*$scope.reservas[index].cantidad-$scope.reservas[index].descuento
            }   
            else $scope.seleccionados.push(false)
        }
        console.log(x,$scope.reservas,$scope.catalogo);
        $scope.Rechazo=x
    }

    $scope.changeFinal=function(x,i){
        console.log(x)
        console.log($scope.aprovados.indexOf(x._id))
        //existe en los aprovados
        $scope.seleccionados[i]=!$scope.seleccionados[i]
        if($scope.aprovados.indexOf(x._id)>-1){
            $scope.aprovados.splice($scope.aprovados.indexOf(x._id),1);
            $scope.FinalPrice=$scope.FinalPrice-(x.precio*x.cantidad-x.descuento)
        }
        //si se quiere agregar a aprovados
        else{
            $scope.aprovados.push(x._id)
            $scope.FinalPrice=$scope.FinalPrice+x.precio*x.cantidad-x.descuento
        }
    }

    $scope.aceptarPedido=function(){
        //console.log(mensaje)
        console.log($scope.pedidosSelc.reservas)
        var data={
            pedido:$scope.pedidosSelc._id,
            id:$cookies.get('id'),
            transporte:$scope.pedidosSelc.transporte,
            finalPrice:$scope.Total_Aprv,
            reservas:$scope.pedidosSelc.reservas,
            mensaje:$scope.mensjRechazo,
            empresa:$cookies.get('empresa'),
            cliente:$scope.pedidosSelc.cliente,
            otros:$scope.pedidosSelc.otros
        };
        $http({
            method:'POST',
            url:'/aceptar/pedido/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
                var data={
                    cliente:$scope.pedidosSelc.cliente,
                    Monto:$scope.Total_Aprv,
                };
                $http({
                    method:'POST',
                    url:'/aumentar/deuda/',
                    data:data
                }).
                then(function(response){
                    //console.log(response.data);
                    credito[response.data._id]=response.data.credito
                })
            $scope.pedidosArray=[];
            $scope.Lastid='null'
            $scope.mensjRechazo='null'
            $scope.ObtenerPedidos()
        })
    }

    $scope.nombrepedidos=function(reservas,i){
        console.log(reservas,i)
        //se asigni el nombre del agente a el pedido
        let pedido=$scope.pedidosArray[i]
        $scope.pedidosArray[i]['Nagente']=$scope.Jusers[pedido.agente];
        for (let k = 0; k < reservas.length; k++) {
            //const element = reservas[k];
            reservas[k]["Ncliente"]= $scope.Jclient[reservas[k].cliente];
            $scope.pedidosArray[i]["Ncliente"]= $scope.Jclient[reservas[k].cliente];
            reservas[k]["Nagente"]= $scope.Jusers[reservas[k].agente];
            reservas[k]["objeto"]=reservas[k].objeto;
            reservas[k]["precio"]=$scope.JPrecio[reservas[k].objeto];
            reservas[k]["Nobjeto"]=$scope.Jobjeto[reservas[k].objeto];
            reservas[k]["pedido"]=$scope.pedidosArray[i]._id;
            $scope.pedidosArray[i].reservasindex.push($scope.reservas.length+k);
        }
        console.log($scope.pedidosArray[i].reservasindex)
        $scope.reservas=$scope.reservas.concat(reservas);
        $scope.buscarreservas(i+1);
    }

    $scope.reservafind=function(array,i){
        busquedas.arrayReservas(array,function(response){
            console.log(response.data);
            $scope.nombrepedidos(response.data.data,i)
        })
    }

    $scope.buscarreservas=function(i){
        if($scope.pedidosArray.length > i){
            $scope.pedidosArray[i].reservasindex=[];
            $scope.reservafind($scope.pedidosArray[i].reservas,i)
        }
        else {
            console.log($scope.pedidosArray,$scope.reservas)
            $scope.chardatos()
        }
    }

    $scope.KeepLooking=function(){
        $http({
            method:'GET',
            url:'/actualizar/pedidos/'+$scope.lastOne
        }).
        then(function(response){
            if(response.data.err==0){
                for (let i = 0; i < response.data.data.length; i++) {
                    response.data.data[i]['new']=true;
                    
                }
                $scope.pedidosArray=$scope.pedidosArray.concat(response.data.data);
                $window.alert('Hay '+response.data.data.length+' pedidos nuevos\nSe agragaron a la lista de pedidos, seleccionelos para obtener informacion de ellos')
                $scope.lastOne=response.data.data[response.data.data.length-1]._id
            }
            else{
                console.log('No hay pedidos nuevos')
            }
            $timeout($scope.KeepLooking,60000);
        })
    }

    $scope.ObtenerPedidos=function(){
        console.log($cookies.get('empresa'),$scope.Lastid)
        busquedas.pedidos($cookies.get('empresa'),$scope.Lastid,function(response){
            $scope.pedidosArray=$scope.pedidosArray.concat(response.data.data);
            $scope.Lastid=response.data.last
            console.log($scope.pedidosArray);
            if(response.data.err != 0) $scope.ObtenerPedidos()
            else {
                //$scope.pedidosArray[$scope.pedidosArray.length-1]['new']=true
                $scope.lastOne=$scope.pedidosArray[$scope.pedidosArray.length-1]._id
                //console.log($scope.pedidosArray[$scope.pedidosArray.length-1])
            }
        })
        $timeout($scope.KeepLooking,60000);
    }

    $scope.Tobjetos = function(){
        for (let i = 0; i < $scope.CatKey.length; i++) {
            let cat = $scope.CatKey[i];
            for (let j = 0; j < $scope.catalogo[cat].length; j++) {
                let objeto = $scope.catalogo[cat][j];
                $scope.Jobjeto[objeto.id] = objeto; 
                if (objeto.Dolares) $scope.JPrecio[objeto.objeto] = Math.round(objeto.precio*$scope.currency)
                else $scope.JPrecio[objeto.objeto] = objeto.precio
            }
        }
        console.log($scope.Jobjeto);
        $scope.ObtenerPedidos();
    }

    $scope.getcatalog=function(){
        busquedas.catalogo($cookies.get('empresa'),function(keys,data){
            $scope.CatKey = keys;
            $scope.catalogo= data;
            $scope.Tobjetos()
        })
    }

    //busca el inventario
    $scope.Getinventario=function(){
        //busquedas.inventario($cookies.get('empresa'),function(keys,data){
            //$scope.collections = keys;
            //$scope.inventario= data;
            //$scope.AllResv(0,0);
            
        //})
    }

    //crea un json de clientes para facilitar la traduccion de id a nombre
    $scope.Tclients=function(){
        for (let i = 0; i < $scope.clientes.length; i++) {
            //console.log($scope.clientes[i]._id)
            $scope.Jclient[$scope.clientes[i]._id] = $scope.clientes[i]   
            $scope.credito[$scope.clientes[i]._id] = $scope.clientes[i].credito
        }
        console.log($scope.Jclient)
        $scope.reservas=[]
        $scope.resumeClients()
        $scope.getcatalog()
    }

    //obtener clientes
    $scope.client=function(init){
        //$scope.clients=[];
        busquedas.clientes($cookies.get('empresa'),null,String(init),function(response){
            //console.log(response.max == response.last,response.max,response.last);
            if (response.max == response.last) {
                //llena el scope de respaldo
                $scope.clientes=$scope.clientes.concat(response.datos);
                console.log($scope.clientes)
                $scope.Tclients()
            } 
            else 
            {
                $scope.clientes=$scope.clientes.concat(response.datos);
                $scope.client(response.last)
            }
        })
    }

    //crea un json de usuaurios de empresa para facilitar la traduccion de id a nombre
    $scope.Tusers=function(){
        $scope.Cuser={}
        for (let i = 0; i < $scope.users.length; i++) {
            console.log($scope.users[i])
            $scope.Jusers[$scope.users[i].id] = $scope.users[i].user;  
            $scope.Cuser[$scope.users[i].id]= $scope.users[i].cod
        }
        $scope.client(0);
    }

    $scope.reset=function(){
        $scope.Jclient={};
        $scope.Jusers={};
        $scope.Jobjeto={};
        $scope.JPrecio={}
        $scope.reservas=[];
        $scope.pedidosArray=[];
        $scope.Lastid='null'
        $scope.Rechazo={}
        $scope.stat=0;
        $scope.Revizados=false
        $scope.Fagente=null;
        $scope.Fcliente=null;
        $scope.filter=''
        $scope.init()
    }

    //init
    $scope.init=function(){
        //se obtiene el inventario
        //se obtiene el catalogo
        busquedas.init(function(datos,extra,mensaje){
            console.log(mensaje)
            $scope.users=datos;
            $scope.empresa=extra;
            $scope.Tusers();
        })
    }
    $scope.page(0)
    $scope.reset();

    $scope.Cal_Total_Aprv=function(x){
        //console.log(x)
        $scope.Total_Ped=0
        $scope.Total_Aprv=0
        $scope.Total_Simp=0
        $scope.Total_imp=0
        for (let i = 0; i < x.reservas.length; i++) {
            const reserva = x.reservas[i];
            if($scope.Jobjeto[reserva.objeto]){
                //console.log($scope.Jobjeto[reserva.objeto])
                if($scope.Jobjeto[reserva.objeto].exentos) imp=imp2
                else imp=imp1
                $scope.Total_Ped=$scope.Total_Ped+Math.floor((reserva.cantidad*$scope.Jobjeto[reserva.objeto].precio-reserva.descuento)*(1+imp/100))
                if(reserva.Aprovado) {
                    if(reserva.Tdescuento!=5){
                        if($scope.Jobjeto[reserva.objeto].exentos) imps=imp2
                        else imps=imp1
                        console.log(imps)
                        $scope.Total_imp=Math.floor(($scope.Total_imp+reserva.cantidad*$scope.Jobjeto[reserva.objeto].precio*imps/100-reserva.descuento*imps/100)*1000)/1000
                        $scope.Total_Simp=$scope.Total_Simp+reserva.cantidad*$scope.Jobjeto[reserva.objeto].precio-reserva.descuento
                        $scope.Total_Aprv=Math.floor(($scope.Total_Aprv+reserva.cantidad*$scope.Jobjeto[reserva.objeto].precio*(1+imps/100)-reserva.descuento*(1+imps/100))*1000)/1000
                        
                    }
                }
            }
        }
        $scope.Total_imp=$scope.Total_imp+$scope.pedidosSelc.otros*0.13
        $scope.Total_Aprv=$scope.Total_Aprv+$scope.pedidosSelc.otros*1.13
        console.log($scope.Total_Aprv,$scope.Total_Simp,$scope.Total_imp)
    }

    $scope.UserName=function(name){
        console.log(name)
        if($scope.Jusers[name]==undefined){
            $http({
                method:'GET',
                url:'/find/user/'+name
            }).
            then(function(response){
                console.log(response.data);
                $scope.Jusers[name]=response.data.nombre
                $scope.Naprvby= response.data.nombre
            })
        }
        else $scope.Naprvby= $scope.Jusers[name]
    }

    $scope.selectoption=function(x){
        console.log(x)
        $scope.pedidosSelc=x
        $scope.Cal_Total_Aprv(x)
        if(x.aprovado) $scope.UserName(x.aprvby)
    }

    $scope.viewFactura=function(){
        console.log($scope.pedidosSelc)
        var data={
            concecutivo:$scope.pedidosSelc.consecutivo,
            emp:$cookies.get('empresa')
        };
        console.log(data)
        $http({
            method:'POST',
            url:'/get/concecutivo/ordenes/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            if(response.data.salida){
                $http({
                    method:'GET',
                    url:'/find/company/'+$cookies.get('empresa')
                }).
                then(function(empr){
                    var empresa=empr.data
                    if($scope.Jclient[$scope.pedidosSelc.cliente].lat && $scope.Jclient[$scope.pedidosSelc.cliente].lon){
                        busquedas.reverserGeoJson($scope.Jclient[$scope.pedidosSelc.cliente].lat , $scope.Jclient[$scope.pedidosSelc.cliente].lon,17,function(revers){
                            //$scope.direccion=response.data.address
                            var newWimdows=$window.open('/empresa/assets/htmls/ViewFactura.html')
                            newWimdows.data={
                                empresa:empresa,
                                direccion:revers.data.address,
                                salida:response.data.salida,
                                pedidos:$scope.pedidosSelc,
                                objeto:$scope.Jobjeto,
                                clientes:$scope.Jclient[$scope.pedidosSelc.cliente],
                                users:$scope.Jusers[$scope.pedidosSelc.agente],
                                aprovado:$scope.Naprvby
                            }
                        })
                    }
                    else{
                        var newWimdows=$window.open('/empresa/assets/htmls/ViewFactura.html')
                        newWimdows.data={
                            empresa:empresa,
                            direccion:null,
                            salida:response.data.salida,
                            pedidos:$scope.pedidosSelc,
                            objeto:$scope.Jobjeto,
                            clientes:$scope.Jclient[$scope.pedidosSelc.cliente],
                            users:$scope.Jusers[$scope.pedidosSelc.agente],
                            aprovado:$scope.Naprvby
                        }
                    }
                })
            }
        })
        /**/
    }
}])

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return 0
    }
})

app.filter('TypeAprv',function(){
    return function(input){
        if(input){
            //console.log(input)
            var response='Aprobado'
            for (let i = 0; i < input.reservas.length; i++) {
                if(!input.reservas[i].Aprovado){
                    response='Parcialmente Aprobado'
                    break;
                }
            }
            return response
        }
        return null
    }
})

app.filter('cantidadShow',function(){
    //console.log(p1)
    return function(x,y){
        //console.log(x,y[x.objeto])
        if(y[x.objeto]){
            if((x.Tdescuento==1 || x.Tdescuento==4) && x.descuento>0) return String(x.cantidad-Math.floor(x.descuento/y[x.objeto].precio))+'+'+String(Math.floor(x.descuento/y[x.objeto].precio))
            else return x.cantidad
        }
        else return 0
        //if(x.Tdescuento==1) return 
    }
})

app.filter('clientName',function(){
    return function(name,l){
        if(name){
            if(name.length<l)return name
            else return name.slice(0,l)+'...'
        }
        else return 'Cliente desconcido'
    }
})