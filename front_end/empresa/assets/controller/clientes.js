var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('clientes',['$scope','$http','$cookies','$window','leafletData','busquedas','crear','editar','links',function($scope,$http,$cookies,$window,leafletData,busquedas,crear,editar,links){
    /*
    Abrir el modal
        var popup = document.getElementsByClassName("modalActive")[0];
        popup.click()
    **/
    
    //init var
    console.log($cookies.getAll());
    //regiones default
//saltos de pagina
    $scope.menu=[]
    $scope.links = links.links
    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }
    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }
//init values
    $scope.region=1;
    //jsons
    $scope.events= {};
    $scope.center={
        lat: 9.9356124,
        lng: -84.1833856,
        zoom: 12
    };
    //arreglos
    $scope.markers=[];
    $scope.DeleteAlerta=false;
    //inico con el maximo del mapa
    $scope.crear=false;
//cambio en el mapa y el footer
    $scope.usuarioChg=function(x){
        $scope.usrCLN=JSON.parse($scope.usr).clientes
    }
    //reset
    $scope.ResetOptions=function(){
        $scope.assginar=false;
        $scope.CreateEdit=false;
        $scope.DeleteAlerta=false;
        $scope.Loading=false;
    }
    //cambia el cliente
    $scope.chgIndex=function(x){
        console.log(x)
        //$scope.cliente=Number(i)
        $scope.sercher=null
        if(x.lat&&x.lon)$scope.go_to(x.lat,x.lon,15)
        for (let i = 0; i < $scope.clientes.length; i++) {
            if($scope.clientes[i]._id==x._id){
                $scope.cliente=i
                break
            }
        }
    }
    //cierra el footer
    $scope.close =function(){
        $scope.crear=false;
        if($scope.CreateEdit==true){
            $scope.CreateEdit=false
            if($scope.tipoCreacion=="crear"){
                $scope.markers.splice($scope.l,1);
            }
        }
    }
    //se inicia puntero con valores basura
    $scope.init_map=function(){
        $scope.markers[$scope.l]={
            lat: $scope.center.lat,
            lng: $scope.center.lng,
            message: "new marker"
        };
    }
    //$scope.init_map();

//creacion y edicion de clientes
    $scope.crearcliente=function(){
        $scope.Loading=true;
        //datos del cliente
        if($scope.Cnombre!=undefined && $scope.markers[$scope.l].lat!=undefined && $scope.markers[$scope.l].lng!=undefined && $scope.region!=undefined && $cookies.get('empresa')!=undefined) {
            $scope.InfoShow="Creando el cliente"
            crear.cliente($scope.Cnombre,$scope.markers[$scope.l].lat,$scope.markers[$scope.l].lng,$scope.region,$cookies.get('empresa'),function(response){
                console.log(response.data);
                //reinicia el side bar
                $scope.InfoShow="Cliente Creado \n Solictando informacion de clientes\n Porfavor espere"
                $scope.clientes=[];
                $scope.clientes=[];
                //se cierra el footer
                $scope.crear=false;
                $scope.close()
                //se reinicia los valores del side bar
                $scope.start(0);
            })
        }
    }

    //edita un cliente
    $scope.editarCln = function(id){
        //datos del cliente
        editar.cliente(id,$scope.Cnombre,$scope.markers[$scope.l].lat,$scope.markers[$scope.l].lng,$scope.region,$cookies.get('empresa'),function(response){
            console.log(response.data);
            //se reinicia los valores del sidebar
            $scope.clientes=[];
            $scope.clientes=[];
            //cierra el footer
            $scope.crear=false;
            //rellena el sidebar
            $scope.start(0);
        })
    }

    //elimnar
    $scope.DeleteCliente=function(id){
        console.log(id);
        $scope.DeleteAlerta=false
        $scope.Loading=true
        $scope.InfoShow="Eliminando al cliente"
        editar.deletecliente(id,function(response){
            $scope.InfoShow="Cliente eliminado; Recolectando informacion de los clientes."
            console.log(response.data)
            $scope.cliente=0
            $scope.start(0);
        })

    }

    //crear clientes en el footer
    $scope.StrCrear =function(){
        $scope.assginar=false;
        $scope.CreateEdit=true;
        $scope.DeleteAlerta=false;
        //si se desea crear se abre el footer
        if(!$scope.crear){
            //se abre el footer
            $scope.crear=true;
            //se crea un marcardor vacio
            $scope.markers.push({
                lat: $scope.center.lat,
                lng: $scope.center.lng,
                message: "new marker"
            });
            //se actualiza el largo de los markers
            $scope.l=$scope.markers.length-1;
        }
        //si se cancela, se cierra el footer y se elimana el marker
        else{
            $scope.crear=false;
            if($scope.tipoCreacion=="crear"){
                $scope.markers.splice($scope.l,1);
            }
            $scope.close()
        }
        //se pone en modo de creacion
        $scope.tipoCreacion="crear";
        console.log($scope.tipoCreacion);
    }

    //edita un cliente
    $scope.editarCliente=function(id,index){
        console.log(id,index)
        if(id != undefined){
            $scope.assginar=false;
            $scope.CreateEdit=true;
            $scope.DeleteAlerta=false;
            console.log(id,index);
            //se setean los datos del cliente
            $scope.ClnId=id;
            $scope.Cnombre=$scope.clientes[index].nombre;
            $scope.go_to($scope.clientes[index].lat,$scope.clientes[index].lon,15)
            $scope.region=$scope.clientes[index].region;
            //se abre el footer
            if($scope.crear==true && $scope.tipoCreacion=="crear"){
                $scope.markers.splice($scope.l,1);
            }
            else{
                $scope.crear=true
            }
            //se pone el largo del markers en el
            $scope.l=index;
            //se pone en modo de edicion
            $scope.tipoCreacion="editar";
            console.log($scope.tipoCreacion);
            // se centra el mapa
            if($scope.clientes[index].lat&&$scope.clientes[index].lon)$scope.go_to($scope.clientes[index].lat,$scope.clientes[index].lon,15);
        }
    }

//asignar
    //bandera de asignacion
    $scope.asigg = function(client){
        $scope.assginar=true;
    }
    //accion de asignacion
    $scope.asignarStd =function(Asignar,data){
        if(Asignar){
            var data={
                usuario:JSON.parse($scope.usr).id,
                asignar:true,
                Zona:$scope.zona
            };
            console.log(data)
            $http({
                method:'POST',
                url:'/agentes/asignar/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                response.data['id']=response.data._id
                $scope.usr=JSON.stringify(response.data)
                $scope.usrCLN=response.data.clientes
            })
        }
        else{
            var data={
                usuario:JSON.parse($scope.usr).id,
                asignar:false,
                Zona:data
            };
            console.log(data)
            $http({
                method:'POST',
                url:'/agentes/asignar/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                response.data['id']=response.data._id
                $scope.usr=JSON.stringify(response.data)
                $scope.usrCLN=response.data.clientes
            })
        }
    }

//acciones del mapa
    $scope.SelctPos = function(x){
        console.log(x)
        $scope.showres=false;
        //se define el zoom
        if (x.type == 'city' || x.type == 'administrative') {
            zoom=11
        } else {
            zoom=13;
        } 
        //se centra el mapa
        $scope.go_to(x.lat,x.lon,zoom)
    }
    //buscar posiiciones
    $scope.serpos = function(){
        console.log($scope.centpos)
        if($scope.centpos != ''){
            busquedas.searchPos($scope.centpos,'5',function(response){
                console.log(response.data);
                $scope.showres=true;
                $scope.ResPos=response.data;
            })
        }
        else $scope.showres=false;
    }
    //centra el mapa
    $scope.go_to =function(lat,lon,z){
        console.log(lat,lon,z);
        $scope.center={
            lat: Number(lat),
            lng: Number(lon),
            zoom: Number(z)
        };
    }
//archivo
    $scope.multisave=function(data,i){
        busquedas.reverserGeoJson(data[i].lat,data[i].lng,'12',function(response){
            region=response.data.address.state;
            crear.cliente(data[i].Cliente,data[i].lat,data[i].lng,region,$cookies.get('empresa'),function(response){
                console.log(response.data);
                //reinicia el side bar
                if(i+1<data.length)$scope.multisave(data,i+1)
                else{
                    $scope.clientes=[];
                    $scope.clientes=[];
                    //se cierra el footer
                    $scope.crear=false;
                    //se reinicia los valores del side bar
                    $scope.start(0);
                }
            })
        })
    }

    $scope.fileUploadSend=function(){
        console.log($scope.file)
        var formData = new FormData();
        formData.append("name", name);
        formData.append("file", $scope.file); 
        console.log(formData);
        $http({
            method:'POST',
            url:'/empresa/clientes/excel?username='+String($cookies.get('empresa'))+'&name=Clientes_'+String($cookies.get('empresa')),
            data:formData,
            headers:{
                "Content-type": undefined
            }, 
            transformRequest: angular.identity
        }).
        then(function(response){
            console.log(response.data);
            //$scope.multisave(response.data,0)
        })
    }

//inicio
    //datos de la empresa y usuarios
    $scope.inicioUsr = function(){
        busquedas.init(function(datos,extras){
            console.log(datos)
            $scope.users= datos;
            $scope.empresa= extras;
        });
    }
    $scope.inicioUsr();

    $scope.MakeMarkers=function(){
        $scope.markers=[]
        for (let i = 0; i < $scope.clientes.length; i++) {
            const lat = $scope.clientes[i].lat;
            const lon = $scope.clientes[i].lon;
            $scope.markers.push({
                lat:Number(lat),
                lng:Number(lon),
                message:$scope.clientes[i].codigo+'-'+$scope.clientes[i].nombre
            })            
        }       
        $scope.InfoShow="Puntos creados\n Gracias por esperar" 
    }

    $scope.findZonas=function(){
        $http({
            method:'GET',
            url:'/empresa/zonas/'+$cookies.get('empresa')
        }).
        then(function(response){
            console.log(response.data);
            $scope.zonas=response.data.id
        })
    }

    //busca los clientes
    $scope.clientesSearch=function(init){
        busquedas.clientes($cookies.get('empresa'),null,String(init),function(response){
            //console.log(response.max == response.last,response.max,response.last);
            if (response.max == response.last) {
                $scope.InfoShow="Clientes obtenidos \nCreando puntos en el mapa\n Porfavor espere"
                //llena el scope de respaldo
                $scope.clientes=$scope.clientes.concat(response.datos);
                //console.log($scope.clientes)
                $scope.MakeMarkers()
                $scope.findZonas()
            } 
            else 
            {
                $scope.clientes=$scope.clientes.concat(response.datos);
                $scope.clientesSearch(response.last)
            }
        })
    }
    //busca los valores del side bar
    $scope.start =function(init){
        $scope.Loading=true
        var popup = document.getElementsByClassName("modalActive")[0];
        popup.click()
        $scope.InfoShow="Obteniendo cientes... Por favor espere"
        $scope.clientes=[];
        $scope.clientesSearch(0)
        
    }
    $scope.start(0);
//functions $watch
    $scope.$watchCollection('center', function(newCollection, oldCollection) {
        //console.log("El nuevo valor es:", newCollection);
        if(!$scope.crear){
            busquedas.reverserGeoJson(newCollection.lat,newCollection.lng,'12',function(response){
                console.log(response.data.address);
                $scope.region=response.data.address.state;
            })
        }
    });

    $scope.$watchCollection('markers',function(newCollection, oldCollection) {
        //console.log("El nuevo valor es:", newCollection);)
        if($scope.crear){
            console.log(newCollection[$scope.l]);
            busquedas.reverserGeoJson(newCollection[$scope.l].lat,newCollection[$scope.l].lng,'12',function(response){
                console.log(response.data.address);
                $scope.region=response.data.address.state;
            })
        }
    })

    $scope.$watch('file', function(newCollection, oldCollection) {
        if($scope.file != undefined){
            $scope.fileUploadSend()
        }    
    })

//scope on, directiva para crear markers
    $scope.$on("leafletDirectiveMap.click", function(event, args){
        var leafEvent = args.leafletEvent;
        console.log(leafEvent)
        if($scope.crear){
            $scope.markers[$scope.l]= {
                lat: leafEvent.latlng.lat,
                lng: leafEvent.latlng.lng,
                message: $scope.Cnombre
            };
            console.log($scope.markers);
        }
    });

    $scope.$on('leafletDirectiveMarker.click', function(event, args){
      // Access underlying marker on leaflet object as args.leafletObject.options
      var leafEvent = args.leafletEvent;
      console.log(leafEvent)
      for (let i = 0; i < $scope.markers.length; i++) {
          if($scope.markers[i].lat==leafEvent.latlng.lat && $scope.markers[i].lng==leafEvent.latlng.lng){
            console.log($scope.markers[i])
            $scope.cliente=i;
            $scope.go_to($scope.markers[i].lat,$scope.markers[i].lng,15)
          }
          
      }
    });
}])

app.directive('uploaderModel', ["$parse", function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) 
		{
			iElement.on("change", function(e)
			{
				$parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
                console.log('parseado')
			});
		}
	};
}])