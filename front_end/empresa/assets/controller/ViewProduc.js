app.controller('view',['$scope','$http','$cookies','$window','crear','busquedas','editar','links',function($scope,$http,$cookies,$window,crear,busquedas,editar,links){
    console.log($cookies.getAll());
    console.log($window.data)
    $scope.fecha=new Date()
    $scope.Nproductos=$window.data.Nproductos
    $scope.Seleccionado=$window.data.Seleccionado

    if($window.data){
        $window.print()
    }
    else $window.close()
}])

app.filter('filterDigits',function(){
    return function(input,k){
        //console.log(input)
        if(input){
            var dato=Math.floor(input*Math.pow(10,k))/Math.pow(10,k)
            let a=String(dato).split(".")
            //console.log(a)
            //correccion del digito final
            if(a[1]){
                if(a[1].length!=k){
                    a[1]=a[1]+'0'.repeat(k-a[1].length)
                }
            }
            else a[1]='0'.repeat(k)
            //divisor de millares
            var b=''
            var j=0
            for (let i = a[0].length-1; i >= 0; i=i-1) {
                b=a[0][i]+b;
                ++j
                if(j%3==0 && j>1 && i>0)b=','+b
            }
            return b+'.'+a[1]
        }
        else return null
    }
})

app.filter('Estados',function(){
    return function(i){
        console.log(i)
        switch (i) {
            case 0:
                return 'Sin iniciar'
                break;
            case 1:
                return 'Produciendo'
                break;
            case 2:
                return 'Envasando'
                break;
            case 3:
                return 'Etiquetando'
                break;
            case 4:
                return 'Finalizado'
                break;
            case 5:
                return 'Error'
                break;
            default:
                return 'Cancelado'
                break;
        }
    }
})

app.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return val != null ? parseInt(val, 10) : null;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
});