app.controller('ordenes',['$scope','$http','$cookies','$window','leafletData','busquedas','crear','editar','links',function($scope,$http,$cookies,$window,leafletData,busquedas,crear,editar,links){
    console.log($cookies.getAll())
    $scope.estados=['recibido','leido','En proceso','terminado','problemas','','Cancelado']
    //init
    $scope.tipo = 'Produccion';
    $scope.Atipo = 'Produccion';
    var clientes=[]
    $scope.Nclientes={};
    $scope.Nproductos={};
    $scope.modal_page='Produccion'
    $scope.Seleccionado=null;
    $scope.user={}
    $scope.final={}
    $scope.LotesF={}

    //saltos de pagina
    $scope.menu=[]
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    busquedas.init(function(datos,extra,mensaje){
            $scope.users= datos;
            $scope.empresa= extra;
            $scope.mensaje=mensaje
            $http({
                method:'GET',
                url:'/find/company/'+extra.id
            }).
            then(function(response){
                //console.log(response.data.ExtEmail);
                $scope.email=response.data.ExtEmail
                $scope.email.push(response.data.email);
                //console.log($scope.email)
            })
    });

    $scope.busqueda_clientes=function(){
        for (let i = 0; i < $scope.ordenes.salida.length; i++) {
            if(clientes.indexOf($scope.ordenes.salida[i].cliente)==-1) clientes.push($scope.ordenes.salida[i].cliente);   
        }
        for (let i = 0; i < $scope.ordenes.Produccion.length; i++) {
            if(clientes.indexOf($scope.ordenes.Produccion[i].cliente)==-1) clientes.push($scope.ordenes.Produccion[i].cliente);   
        }
        console.log(clientes)
        busquedas.clientesArray(clientes,function(response){
            console.log(response)
            $scope.Nclientes=response.data
        })
    }
    $scope.init=function(){
        busquedas.ordenes(function(response){
            $scope.ordenes=response
            console.log($scope.ordenes)
            $scope.busqueda_clientes()
        })
    }
    $scope.init()

    $scope.refreshOrdenes=function(){
        $scope.Seleccionado=null;
        busquedas.ordenes(function(response){
            $scope.ordenes=response
        })
    }

    $scope.ordenesEstado=function(tipo){
        if(tipo==4){
            err=$window.prompt('Especifique el tipo de error que hay con la orden')
            if(err){
                for (let i = 0; i < $scope.email.length; i++) {
                    crear.email('info@passarte.com',$scope.email[i],'Error en la produccion','El usuario '+$cookies.get('user')+' reporto un error en la produccion '+$scope.Seleccionado.numero+'-'+$scope.Seleccionado.Formula+' el dia '+new Date()+' con la especificacion: \n'+err,function(){})
                }
                editar.ordenesEstado($scope.Atipo,tipo,$scope.Seleccionado._id,function(response){
                    $scope.Seleccionado=response
                    $scope.refreshOrdenes()
                })
            }
        }
        else{
            editar.ordenesEstado($scope.Atipo,tipo,$scope.Seleccionado._id,function(response){
                console.log(response)
                $scope.Seleccionado=response
                if(response.estado==3){
                    console.log($scope.Seleccionado._id,$cookies.get('id'),$scope.Atipo)
                    var type =2 
                    if($scope.Atipo='salida') type=0
                    //editar.AutorizarOrden($scope.Seleccionado._id,$cookies.get('id'),type,function(response){
                        //console.log(response)
                        $scope.refreshOrdenes()
                    //})
                }
                else $scope.refreshOrdenes()
            })
        }
    }

    $scope.orderProdct=function(){
        $scope.modal_page='allOnes'
        $scope.ord=[]
        for (let i = 0; i < $scope.ordenes['Produccion'].length; i++) {
            const orden = $scope.ordenes['Produccion'][i];
            if(Number(orden.estado) != 3) {
                //console.log(orden)
                for (let j = 0; j < orden.prductos.length; j++) {
                    const x = orden.prductos[j];
                    $scope.ord.push({
                        OrdenId:orden._id,
                        Numero:orden.numero,
                        Formula:orden.formula,
                        Producto:x.producto,
                        Cantidad:x.cantidad,
                        Prioridad:x.prioridad
                    })
                }
                
            }
            
        }
        console.log($scope.ord)
        var productos=[]
        for (let i = 0; i < $scope.ord.length; i++) {
            if(productos.indexOf($scope.ord[i].Producto)==-1)productos.push($scope.ord[i].Producto)
        }
        console.log(productos)
        busquedas.productosArray(productos,function(response){
            console.log(response)
            //$scope.buscarReceta(response.data[productos[0]].receta)
            $scope.Nproductos=response.data
            //$scope.calKilos($scope.Seleccionado.prductos)
        })
    }

    $scope.actualizarPrioriadades=function(){
        $window.alert('Actualizando')
        var data={
            productos:$scope.ord
        };
        $http({
            method:'POST',
            url:'/actualizar/prioridad/ordenes/',
            data:data
        }).
        then(function(response){
            //console.log(response.data);
            $window.alert('Obteniendo ordenes actualizadas')
            busquedas.ordenes(function(response){
                $scope.ordenes=response
                $window.alert('Ordenes actualizadas')
            })
        })
    }

    $scope.ordenCancelar=function(){
        if($window.confirm('Desea eliminar la orden de produccion '+$scope.Seleccionado.numero)){
            var send=[]
            console.log($scope.KeyReceta)
            for (let i = 0; i < $scope.KeyReceta.length; i++) {
                const x = $scope.KeyReceta[i];
                send.push({
                    ingediente:x.objeto,
                    diferencia:x.cantidad*$scope.kilos,
                    usado:0,
                })
            }
            for (let j = 0; j < $scope.envase.length; j++) {
                const x = $scope.envase[j];
                //si se solicito
                if($scope.cantidad[x.asignado]){
                    //Tapa
                    send.push({
                        ingediente:x.tapa.objeto,
                        diferencia:$scope.cantidad[x.asignado]*x.tapa.cantidad,
                        usado:0,
                    })
                    //envase
                    send.push({
                        ingediente:x.envase.objeto,
                        diferencia:$scope.cantidad[x.asignado]*x.envase.cantidad,
                        usado:0,
                    })
                    for (let k = 0; k < x.etiqueta.length; k++) {
                        const y = x.etiqueta[k];
                        //etiqueta
                        send.push({
                            ingediente:y.objeto,
                            diferencia:$scope.cantidad[x.asignado]*y.cantidad,
                            usado:0,
                        })
                    }
                }
            }
            console.log(send)
            var usuario = $window.prompt('Ingrese su nombre de usario')
            var password= $window.prompt('Ingrese su contraseña')
            data={
                'user':usuario,
                'password':password
            }
            $http({
                method:'POST',
                url:'/login/',
                data:data
            }).
            then(function(response){
                var data={
                    orden:$scope.Seleccionado._id,
                    data:send,
                };
                $http({
                    method:'POST',
                    url:'/cancelar/production/',
                    data:data
                }).
                then(function(response){
                    $scope.refreshOrdenes()
                })
            })
        }
    }

    $scope.actualizarfecha=function(tipo){
        $scope.Seleccionado.fechas[tipo]=new Date()
    }

    $scope.actualizarEncargados=function(){
        var data={
            orden:$scope.Seleccionado._id,
            encargado:$scope.Seleccionado.encargado,
            fechas:$scope.Seleccionado.fechas
        };
        $http({
            method:'POST',
            url:'/orden/encargado/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.init()
        })
    }

    $scope.ordenesFinal=function(){
        //console.log($scope.final)
        var send=[]
        for (let i = 0; i < $scope.KeyReceta.length; i++) {
            const x = $scope.KeyReceta[i];
            send.push({
                ingediente:x.objeto,
                diferencia:x.cantidad*$scope.kilos-$scope.final[x.objeto],
                usado:$scope.final[x.objeto],
            })
        }
        for (let j = 0; j < $scope.envase.length; j++) {
            const x = $scope.envase[j];
            //si se solicito
            if($scope.cantidad[x.asignado]){
                //Tapa
                send.push({
                    ingediente:x.tapa.objeto,
                    diferencia:$scope.cantidad[x.asignado]*x.tapa.cantidad-$scope.final[x.tapa.objeto],
                    usado:$scope.final[x.tapa.objeto],
                })
                //envase
                send.push({
                    ingediente:x.envase.objeto,
                    diferencia:$scope.cantidad[x.asignado]*x.envase.cantidad-$scope.final[x.envase.objeto],
                    usado:$scope.final[x.envase.objeto],
                })
                for (let k = 0; k < x.etiqueta.length; k++) {
                    const y = x.etiqueta[k];
                    //etiqueta
                    send.push({
                        ingediente:y.objeto,
                        diferencia:$scope.cantidad[x.asignado]*y.cantidad-$scope.final[y.objeto],
                        usado:$scope.final[y.objeto],
                    })
                }
            }
        }
        console.log(send)
        var usuario = $window.prompt('Ingrese su nombre de usario')
        var password= $window.prompt('Ingrese su contraseña')
        data={
            'user':usuario,
            'password':password
        }
        $http({
            method:'POST',
            url:'/login/',
            data:data
        }).
        then(function(response){
            var data={
                orden:$scope.Seleccionado._id,
                encargado:$scope.Seleccionado.encargado,
                data:send,
                fechas:$scope.Seleccionado.fechas
            };
            $http({
                method:'POST',
                url:'/finalizar/production/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                var send=[]
                for (let i = 0; i < $scope.Seleccionado.prductos.length; i++) {
                    const x = $scope.Seleccionado.prductos[i];
                    send.push({
                        object:x.producto,
                        number:$scope.LotesF[x.producto]['lote'],
                        prove:$scope.empresa.nombre,
                        cantidad:$scope.LotesF[x.producto]['cantidad'],
                        vencimiento:null,
                    })
                }
                var data={
                    data:send
                };
                $http({
                    method:'POST',
                    url:'/assig/lote/PT/',
                    data:data
                }).
                then(function(response){
                    console.log(response.data);
                    $scope.refreshOrdenes()
                    $window.alert('Se mostrar la pagina que genera los codigos QR que identifica los articulos del lote\nEsta pagina solo se mostrara una vez por razones de seguridad\nSi no desea imprimirlas inmediatamenta se recomienda guardarlo en PDF')
                    var newWimdows=$window.open('/empresa/assets/htmls/imprimirQR.html')
                    newWimdows.data={
                        empresa:$cookies.get('empresa'),
                        lotes:response.data,
                        
                    }
                })

            })
        })
    }

    $scope.ordenesAct=function(){
        var usuario = $window.prompt('Ingrese su nombre de usario')
        var password= $window.prompt('Ingrese su contraseña')
        data={
            'user':usuario,
            'password':password
        }
        $http({
            method:'POST',
            url:'http://34.237.241.0:2500/login/',
            data:data
        }).
        then(function(response){
            switch(response.data.err){
                case 0:
                    var data={
                        orden:$scope.Seleccionado._id,
                        productos:$scope.Seleccionado.prductos,
                        user:response.data.data.id,
                    };
                    $http({
                        method:'POST',
                        url:'/orden/actualizar/',
                        data:data
                    }).
                    then(function(response){
                        console.log(response.data);
                        })
                    break
                case 1:
                    $window.alert('Error de usuario')
                    break
                case 2:
                    $window.alert('Error de contraseña')
                    break
            }
        })
    }

    $scope.finduser=function(user){
        console.log(user)
        $http({
            method:'GET',
            url:'/find/user/'+user
        }).
        then(function(response){
            console.log(response.data);
            $scope.user[user]=response.data
        })
    }

    $scope.buscarReceta= function(id){
        $http({
            method:'GET',
            url:'/find/formula/'+id
        }).
        then(function(response){
            $scope.formula=id
            console.log(response.data);
            $scope.infoRCT=response.data
            $scope.KeyReceta=response.data.ingredientes
            $scope.envase=response.data.envase
            var productos=[]
            for (let i = 0; i < response.data.ingredientes.length; i++) {
                productos.push(response.data.ingredientes[i].objeto);
            }
            for (let i = 0; i < response.data.envase.length; i++) {
                const envase = response.data.envase[i];
                productos.push(envase.envase.objeto)
                //productos.push(envase.etiqueta.objeto)
                productos.push(envase.tapa.objeto)
                for (let j = 0; j < envase.etiqueta.length; j++) {
                    const etiqueta = envase.etiqueta[j];
                    productos.push(etiqueta.objeto)
                }
            }
            //console.log(productos)
            busquedas.productosArray(productos,function(response){
                console.log(response.data)
                $scope.receta=response.data
            })
        })
    }

    $scope.DeleteEnvase=function(){
        var usuario = $window.prompt('Ingrese su nombre de usario')
        var password= $window.prompt('Ingrese su contraseña')
        data={
            'user':usuario,
            'password':password
        }
        $http({
            method:'POST',
            url:'http://34.237.241.0:2500/login/',
            data:data
        }).
        then(function(response){
            switch(response.data.err){
                case 0:
                    var data={
                        formula:$scope.formula,
                        envase:$scope.envase
                    };
                    $http({
                        method:'POST',
                        url:'/formula/envase/eliminar/',
                        data:data
                    }).
                    then(function(response){
                        for (let i = 0; i < $scope.email.length; i++) {
                            crear.email('info@passarte.com',$scope.email[i],'Elimanacion de un envase de formula','El usuario '+$cookies.get('user')+' eliminino un envase de la formula '+$scope.infoRCT.codigo+'-'+$scope.infoRCT.nombre+' el dia '+new Date(),function(){})
                        }
                        $scope.buscarReceta($scope.formula)
                    })
                    break
                case 1:
                    $window.alert('Error de usuario')
                    break
                case 2:
                    $window.alert('Error de contraseña')
                    break
            }
        })
    }

    $scope.imprimirOrdenP=function(){
        var newWimdows=$window.open('/empresa/assets/htmls/ViewProduccion.html')
        newWimdows.data={
            Nproductos:$scope.Nproductos,
            Seleccionado:$scope.Seleccionado,
        }
    }

    $scope.eliminarenvase=function(i,asignado){
        if($scope.Nproductos[asignado]){
            /*if($window.confirm('Seguro que desea eliminar el envase: '+$scope.Nproductos[asignado].nombre)){
                $scope.envase.splice(i,1);
                console.log(asignado,$scope.formula)
            }*/
        }
        else{
            if($window.confirm('El producto asignado ya no existe mas, desea eliminarlo ')){
                $scope.envase.splice(i,1);
                console.log(asignado,$scope.formula)
                $scope.DeleteEnvase()
            }
        }
    }

    $scope.calKilos=function(x){
        console.log(x[0])
        $scope.kilos=0
        for (let i = 0; i < x.length; i++) {
            producto = $scope.Nproductos[x[i].producto];
            console.log(producto)
            if(!producto.convertFactor)producto.convertFactor=1
            switch(producto.unidad.toUpperCase()) {
                case 'KG':
                    $scope.kilos=$scope.kilos+x[i].cantidad*producto.contenidoNeto
                    break;
                case 'G':
                    $scope.kilos=$scope.kilos+x[i].cantidad*producto.contenidoNeto/1000
                    break
                case 'ML':
                    $scope.kilos=$scope.kilos+x[i].cantidad*producto.contenidoNeto*producto.convertFactor/1000
                    break;
                case 'L':
                    $scope.kilos=$scope.kilos+x[i].cantidad*producto.contenidoNeto*producto.convertFactor
                    break;
                case 'GL':
                    $scope.kilos=$scope.kilos+x[i].cantidad*producto.contenidoNeto*producto.convertFactor/3.78541
                    break;
                
                default:
                    break;
            }
            //console.log($scope.kilos,x[i].cantidad,producto.contenidoNeto,producto.convertFactor)
        }
        //console.log($scope.kilos)
    }

    $scope.ChangsSelection = function(x){
        $scope.Atipo=$scope.tipo
        $scope.Seleccionado=x;
        $scope.Seleccionado['tipo']=$scope.tipo
        $scope.cantidad={}
        for (let i = 0; i < x.prductos.length; i++) {
            $scope.cantidad[x.prductos[i].producto]=x.prductos[i].cantidad            
        }
        if(x.estado==0) $scope.ordenesEstado(1)
        if(!$scope.user[x.Apr_By])$scope.finduser(x.Apr_By)
        //else console.log($scope.user,x.Apr_By)
        var productos=[]
        console.log(x)
        for (let i = 0; i < x.prductos.length; i++) {
           productos.push(x.prductos[i].producto);
        }
        console.log(productos)
        busquedas.productosArray(productos,function(response){
            console.log(response)
            if(response.data[productos[0]])$scope.buscarReceta(response.data[productos[0]].receta)
            $scope.Nproductos=response.data
            $scope.calKilos($scope.Seleccionado.prductos)
        })
    }

    $scope.Descongelar=function(i){
        var usuario = $window.prompt('Ingrese su nombre de usario')
        var password= $window.prompt('Ingrese su contraseña')
        data={
            'user':usuario,
            'password':password
        }
        $http({
            method:'POST',
            url:'http://34.237.241.0:2500/login/',
            data:data
        }).
        then(function(response){
            switch(response.data.err){
                case 0:
                    if(response.data.data.nivel==1){
                        var data={
                            concecutivo:$scope.Seleccionado.consecutivo,
                            i:i,
                        };
                        $http({
                            method:'POST',
                            url:'/descongelar/orden/',
                            data:data
                        }).
                        then(function(response){
                            $scope.Seleccionado=response.data
                            $scope.refreshOrdenes();
                        })
                    }
                    else $window.alert('Usuario no autorizado')
                    break
                case 1:
                    $window.alert('Error de usuario')
                    break
                case 2:
                    $window.alert('Error de contraseña')
                    break
            }
        })
    }

    $scope.AllOrders=function(tipo){
        $scope.modal_page='AllData'
        $scope.Allord=[]
        productos=[]
        for (let i = 0; i < $scope.ordenes[tipo].length; i++) {
            const orden = $scope.ordenes[tipo][i];
            for (let j = 0; j < orden.prductos.length; j++) {
                orden.prductos[j]['id']=orden._id;
                orden.prductos[j]['numero']=orden.numero;
                orden.prductos[j]['fecha']=orden.Fecha_creacion
                $scope.Allord.push(orden.prductos[j]);
                if(productos.indexOf(orden.prductos[j].producto)==-1)productos.push(orden.prductos[j].producto)
            }
        }
        busquedas.productosArray(productos,function(response){
            console.log(response)
            //$scope.buscarReceta(response.data[productos[0]].receta)
            $scope.Nproductos=response.data
            //$scope.calKilos($scope.Seleccionado.prductos)
        })
        console.log($scope.Allord)
    }

    $scope.actPrioridades=function(){
        var data={
            ordenes:$scope.Allord
        };
        $http({
            method:'POST',
            url:'/actualizar/prioridades/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.init()
        })
    }
}])

app.filter('Estados',function(){
    return function(i){
        console.log(i)
        switch (i) {
            case 0:
                return 'Sin iniciar'
                break;
            case 1:
                return 'Produciendo'
                break;
            case 2:
                return 'Envasando'
                break;
            case 3:
                return 'Etiquetando'
                break;
            case 4:
                return 'Finalizado'
                break;
            case 5:
                return 'Error'
                break;
            default:
                return 'Cancelado'
                break;
        }
    }
})

app.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return val != null ? parseInt(val, 10) : null;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
});

app.filter('palabraLength',function(){
    return function(w,l){
   // console.log(w)
        if(w.length>l){
            return w.slice(0,l)
        }
        else return w
    }
})