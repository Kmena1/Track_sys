app.controller('inventario',['$scope','$http','$cookies','$window','$timeout','busquedas','links',function($scope,$http,$cookies,$window,$timeout,busquedas,links){
    console.log($cookies.getAll());
    if($cookies.get('empresa')==null) $window.open('/login/index.html')
    $scope.idobject = null;
    $scope.edit=false;
    $scope.carrito=[];
    $scope.Jclient={};
    $scope.Ncliente=null;
    $scope.preciofinal=0;
    $scope.reservasArray=[]
    $scope.ModalItems=[]
    $scope.ItemAdd={}

    //saltos de pagina
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    var objetos= Highcharts.chart('graph', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Productos en el inventario'
        },
        xAxis: {
            categories: []
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'objetos',
            data: []
        },{
            name: 'reservas',
            data: []
        }]
    });

    busquedas.init(function(user,empresa){
        //console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
    });

    //agragar nombre de cliente a las reservas
    $scope.NombreResrvas=function(w){
        //$scope.reservasArray.push(0);
        console.log($scope.resvObj)
        for (let i = 0; i < $scope.resvObj.length; i++) {
            $scope.resvObj[i].Ncliente= $scope.Jclient[$scope.resvObj[i].cliente];
            if($scope.resvObj[i].aprovado)$scope.reservasArray[w]=$scope.reservasArray[w]+$scope.resvObj[i].cantidad
        }
        //$scope.reservasArray.push($scope.resvObj);
        objetos.series[1].setData($scope.reservasArray);
        console.log($scope.reservasArray)
    }

    //se obtienen las reservas
    $scope.obtenerReservas=function(id,i){
        //console.log(id,i)
        busquedas.reservas(id,function(reservas){
            $scope.resvObj=reservas;
            $scope.NombreResrvas(i);
        })
    }

    //crea un json de clientes para facilitar la traduccion de id a nombre
    $scope.Tclients=function(){
        for (let i = 0; i < $scope.clientes.length; i++) {
            //console.log($scope.clientes[i]._id)
            $scope.Jclient[$scope.clientes[i]._id] = $scope.clientes[i].nombre;   
        }
        //console.log($scope.Jclient)
    }

    //obtener clientes
    $scope.client=function(){
        $scope.clientes=[];
        busquedas.clientes($cookies.get('empresa'),'null',0,function(cliente){
            if (cliente.err == 0) {
                //llena el scope de respaldo
                $scope.clientes=$scope.clientes.concat(cliente.datos);
                $scope.Tclients()
                //filtra
                //$scope.filter()
            } 
            else {
                
            }
        })
    }

    $scope.$watch('cliente',function(n,o) {
        console.log(o,n)
        if(n!=undefined){
            $scope.Ncliente = JSON.parse(n).nombre
        }
    })

    $scope.MakeGraph = function(m){
        if(m*10>$scope.AllInvt.cantidad.length)m=0
        if((m+1)*10>$scope.AllInvt.cantidad.length)n=$scope.AllInvt.cantidad.length
        else n=(m+1)*10
        objetos.xAxis[0].setCategories($scope.AllInvt.names.slice(m*10,n));
        objetos.series[0].setData($scope.AllInvt.cantidad.slice(m*10,n));
        //$timeout($scope.MakeGraph(m+1),10000)
    }

    var Graphmaker = function(m){
        //console.log(m)
        $scope.MakeGraph(m)
        m=m+1
        $timeout(function(){
            Graphmaker(m)
        },10000)
    }

    $scope.addModal=function(y){
        y=JSON.parse(y)
        //if($scope.ModalItems.indexOf(y)==-1)$scope.ModalItems.push(y)
        //console.log($scope.ModalItems)
        $scope.ModalItems.push({
            obj:y._id,
            nombre:y.nombre,
        })
        console.log($scope.ModalItems)
    }

    $scope.agregarStock=function(i){
        console.log($scope.ModalItems,i);
        var data={
            //items:$scope.ItemAdd,
            factura:$scope.factura,
            obj:$scope.ModalItems[i].obj,
            numero:$scope.ModalItems[i].numero,
            proveedor:$scope.proveedor,
            FProduccion:$scope.ModalItems[i].FProduccion,
            FVencimiento:$scope.ModalItems[i].FVencimiento,
            Cas:$scope.ModalItems[i].Cas,
            cantidad:$scope.ModalItems[i].cantidad,
        };
        $http({
            method:'POST',
            url:'/make/lote/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            if(i+1>$scope.ModalItems.length)$scope.agregarStock
            else $scope.init()
        })
    }

    $scope.resumen = function(m){
        $scope.objCant = 0;
        $scope.Stock = 0;
        $scope.catCan=0;
        var objetosName =[];
        var ObjetosCant =[];
        $scope.collections.splice($scope.collections.indexOf('keys'),1);
        for (let i = 0; i < $scope.collections.length; i++) {
            for (let j = 0; j < $scope.inventario[$scope.collections[i]].length; j++) {
                if($scope.inventario[$scope.collections[i]][j].Incatalogo) $scope.catCan=$scope.catCan+1
                $scope.objCant = $scope.objCant+$scope.inventario[$scope.collections[i]][j].cantidad;
                //$scope.obtenerReservas($scope.inventario[$scope.collections[i]][j]._id)
                objetosName.push($scope.inventario[$scope.collections[i]][j].nombre)
                ObjetosCant.push($scope.inventario[$scope.collections[i]][j].cantidad)
            }
            $scope.Stock=$scope.Stock+$scope.inventario[$scope.collections[i]].length
        }
        $scope.AllInvt={
            names:objetosName,
            cantidad:ObjetosCant
        }
        console.log($scope.AllInvt)
        $scope.reservasArray=new Array(ObjetosCant.length).fill(0);
        Graphmaker(0)
        //console.log($scope.collections)
        /*for (let i = 0; i < $scope.collections.length; i++) {
            if($scope.collections[i]!="keys"){
                for (let j = 0; j < $scope.inventario[$scope.collections[i]].length; j++) {
                    console.log($scope.inventario[$scope.collections[i]][j])
                    $scope.obtenerReservas($scope.inventario[$scope.collections[i]][j]._id,k)
                    k=k+1;
                }
            }    
        }*/
        /*objetos.xAxis[0].setCategories(objetosName);
        objetos.series[0].setData(ObjetosCant);
        objetos.series[1].setData(ObjetosCant);
        $scope.catCan=0;
        for (let i = 0; i < $scope.CatKey.length; i++) {
            for (let j = 0; j < $scope.catalogo[$scope.CatKey[i]].length; j++) {
                //console.log($scope.catalogo[$scope.CatKey[i]][j])
                $scope.catCan = $scope.catCan+1;
            }
        }*/
    }

    $scope.LookDist = function(){
        $http({
            method:'GET',
            url:'/get/provedores/'+$cookies.get('empresa')
        }).
        then(function(response){
            $scope.Distribuidores=response.data.data.keys
            console.log($scope.Distribuidores)
        })
    }
    $scope.LookDist()

    //init
    $scope.init=function(){
        busquedas.inventario($cookies.get('empresa'),function(col,invt){
            $scope.collections = col;
            $scope.inventario= invt;
            busquedas.catalogo($cookies.get('empresa'),function(key,cat){
                $scope.CatKey = key;
                $scope.catalogo= cat;
                $scope.resumen();
            })
        })
        $scope.client();
    }

    $scope.init();

    $scope.parJson = function (json) {
        return JSON.parse(json);
    }     
}])

app.directive('uploaderModel', ["$parse", function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) 
		{
			iElement.on("change", function(e)
			{
				$parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
                console.log('parseado', iAttrs.name)
			});
		}
	};
}])