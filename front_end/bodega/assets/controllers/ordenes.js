app.controller('ordenes',['$scope','$http','$cookies','$window','leafletData','busquedas','crear','editar','links',function($scope,$http,$cookies,$window,leafletData,busquedas,crear,editar,links){
    console.log($cookies.getAll())
    $scope.estados=['recibido','leido','En proceso','terminado','problemas']
    //init
    $scope.tipo = 'salida';
    $scope.Atipo = 'salida';
    var clientes=[]
    $scope.Nclientes={};
    $scope.Nproductos={};
    $scope.modal_page='salida'
    $scope.Seleccionado=null;
    $scope.user={}
    
    //saltos de pagina
    $scope.menu=[]
    $scope.links = links.links

    $http({
        method:'GET',
        url:'/find/company/'+$cookies.get('empresa')
    }).
    then(function(response){
        //console.log(response.data);
        $scope.email=response.data.extemail
        $scope.email.push(response.data.email);
    })

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    $scope.busqueda_clientes=function(){
        for (let i = 0; i < $scope.ordenes.salida.length; i++) {
            if(clientes.indexOf($scope.ordenes.salida[i].cliente)==-1) clientes.push($scope.ordenes.salida[i].cliente);   
        }
        for (let i = 0; i < $scope.ordenes.Produccion.length; i++) {
            if(clientes.indexOf($scope.ordenes.Produccion[i].cliente)==-1) clientes.push($scope.ordenes.Produccion[i].cliente);   
        }
        console.log(clientes)
        busquedas.clientesArray(clientes,function(response){
            console.log(response)
            $scope.Nclientes=response.data
        })
    }
    $scope.init=function(){
        busquedas.ordenes(function(response){
            $scope.ordenes=response
            console.log($scope.ordenes)
            $scope.busqueda_clientes()
        })
    }
    $scope.init()

    $scope.refreshOrdenes=function(){
        busquedas.ordenes(function(response){
            $scope.ordenes=response
        })
    }

    $scope.ordenesEstado=function(tipo){
        console.log($scope.Seleccionado.prductos)
        var usuario = $window.prompt('Ingrese su nombre de usario')
        var password= $window.prompt('Ingrese su contraseña')
        data={
            'user':usuario,
            'password':password
        }
        $http({
            method:'POST',
            url:'http://34.237.241.0:2500/login/',
            data:data
        }).
        then(function(response){
            console.log(response.data.data)
            switch(response.data.err){
                case 0:
                    if(tipo=4){
                        var errorData=$scope.prompt('Especifique el error en la produccion');
                        if(errorData){
                            for (let i = 0; i < $scope.email.length; i++) {
                                crear.email('info@passarte.com',$scope.email[i],'Alerta de error en orden de produccion','El usuario '+$cookies.get('user')+' reporto un error en la produccion '+$scope.Seleccionado.numero+$scope.Seleccionado.formula+' el dia '+new Date(),function(){})
                            }
                            var data={
                                orden:$scope.Seleccionado._id,
                                estado:tipo,
                                user:response.data.data.id,
                                productos:$scope.Seleccionado.prductos
                            };
                            $http({
                                method:'POST',
                                url:'/actualizar/orden/salida/',
                                data:data
                            }).
                            then(function(response){
                                $scope.Seleccionado=response.data
                                if(response.estado==3){
                                    //console.log($scope.Seleccionado._id,$cookies.get('id'),$scope.Atipo)
                                    console.log(response.data)
                                    var type =2 
                                    if($scope.Atipo='salida') type=0
                                    //editar.AutorizarOrden($scope.Seleccionado._id,$cookies.get('id'),type,function(response){
                                        //console.log(response)
                                        $scope.refreshOrdenes()
                                    //})
                                }
                                else $scope.refreshOrdenes()
                            })
                        }
                    }
                    else{
                        var data={
                            orden:$scope.Seleccionado._id,
                            estado:tipo,
                            user:response.data.data.id,
                            productos:$scope.Seleccionado.prductos
                        };
                        $http({
                            method:'POST',
                            url:'/actualizar/orden/salida/',
                            data:data
                        }).
                        then(function(response){
                            $scope.Seleccionado=response.data
                            if(response.estado==3){
                                //console.log($scope.Seleccionado._id,$cookies.get('id'),$scope.Atipo)
                                console.log(response.data)
                                var type =2 
                                if($scope.Atipo='salida') type=0
                                //editar.AutorizarOrden($scope.Seleccionado._id,$cookies.get('id'),type,function(response){
                                    //console.log(response)
                                    $scope.refreshOrdenes()
                                //})
                            }
                            else $scope.refreshOrdenes()
                        })
                    }    
                    break
                case 1:
                    $window.alert('Error de usuario')
                    break
                case 2:
                    $window.alert('Error de contraseña')
                    break
            }
        })
        var data={
            orden:$scope.Seleccionado._id,
            estado:tipo,
            productos:$scope.Seleccionado.prductos
        };
        
    }

    $scope.finduser=function(user){
        console.log(user)
        $http({
            method:'GET',
            url:'/find/user/'+user
        }).
        then(function(response){
            console.log(response.data);
            $scope.user[user]=response.data
        })
    }

    $scope.ChangsSelection = function(x){
        $scope.Atipo=$scope.tipo
        $scope.Seleccionado=x;
        if(x.estado==0) $scope.ordenesEstado(1)
        //if(!$scope.user[x.Apr_By])$scope.finduser(x.Apr_By)
        //else console.log($scope.user,x.Apr_By)
        var productos=[]
        console.log(x.prductos)
        for (let i = 0; i < x.prductos.length; i++) {
           productos.push(x.prductos[i].producto);
        }
        console.log(productos)
        busquedas.productosArray(productos,function(response){
            console.log(response)
            $scope.Nproductos=response.data
        })
    }
}])

app.filter('clientName',function(){
    return function(name,l){
        if(name){
            if(name.length<l)return name
            else return name.slice(0,l)+'...'
        }
        else return 'Cliente desconcido'
    }
})

app.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return val != null ? parseInt(val, 10) : null;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
});