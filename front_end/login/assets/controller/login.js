app.controller('login',['$scope','$http','$cookies','$window',function($scope,$http,$cookies,$window){
    var info;
    console.log('login')

    $scope.Template = '';
    $scope.index=1;


    $scope.check=function(){
        if($scope.user != undefined && $scope.Password != undefined){
            data={
                'user':$scope.user,
                'password':$scope.Password
            }
            $http({
                method:'POST',
                url:'/login/',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                info = response.data;
                console.log(info);
                switch (info.err) {
                    case 0:
                        $scope.index=0;
                        $cookies.put('user',info.data.user, {'path': '/'});
                        $cookies.put('id',info.data.id, {'path': '/'});
                        $cookies.put('empresa',info.data.empresa, {'path': '/'});
                        switch (info.data.nivel) {
                            case 0:
                                $window.open('/root/index.html',"_self");
                                break;
                            case 1:
                                $window.open('/empresa/index.html',"_self");
                                break;
                            case 2:
                                $window.open('/user/index.html',"_self");
                                break;
                            case 3:
                                $window.open('/bodega/index.html',"_self");
                                break;
                            case 4:
                                $window.open('/admin/index.html',"_self");
                                break;
                            default:
                                break;
                        }
                        console.log($cookies.getAll())
                        break;
                    case 1:
                        $scope.index=1;
                        $scope.Template = '';
                        $cookies.remove('user');
                        $cookies.remove('id');
                        break;
                    case 2:
                        $scope.index=2;
                        $scope.Template = '';
                        $cookies.remove('user');
                        $cookies.remove('id');
                        break;
                    default:
                        break;
                }
            })
        }
    }

    $scope.mess=function(mess){
        $scope.check();
        switch (mess) {
            case 1:
                $scope.messg="Error de usuario"
                break;
            case 2:
                $scope.messg="Error de password"
                break;
            default:
                $scope.messg="";
                break;
        }
    }
}])