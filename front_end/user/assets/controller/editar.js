var app = angular.module("app", ['ngCookies','ui.bootstrap']);
app.controller('editar',['$scope','$http','$cookies','$window','$modal','busquedas','crear','editar','links',function($scope,$http,$cookies,$window,$modal,busquedas,crear,editar,links){    
    console.log('editar cookies',$cookies.getAll())
    $scope.menu=[]
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    //init var
    $scope.userShow =true;
    $scope.DatosEmp= true;

    //cambios de modal
    $scope.modalSet = function(id,where){
    }

    $scope.pos=function(){
        /*if ($window.navigator && $window.navigator.geolocation) {
            $window.navigator.geolocation.getCurrentPosition(
                position => {
                    this.geolocationPosition = position,
                        console.log(position)
                },
                error => {
                    switch (error.code) {
                        case 1:
                            console.log('Permission Denied');
                            break;
                        case 2:
                            console.log('Position Unavailable');
                            break;
                        case 3:
                            console.log('Timeout');
                            break;
                    }
                }
            );
        };*/
        busquedas.mypos(function(response){
            console.log(response)
        })
    }
    $scope.pos()

    $scope.clientes=function(where,i,data){
        $http({
            method:'GET',
            url:'/clientes/agentes/get/'+$cookies.get('id')+'/'+where+'/'+i*100+'/100'
        }).
        then(function(response){
            console.log(response.data.datos);
            data=data.concat(response.data.datos)
            if(response.data.last>=100*(1+i)){
                $scope.clientes(where,i+1)
            } 
            else{
                $scope.mycln = data;
                console.log($scope.mycln)
            }
        })
    }
    $scope.clientes('null',0, new Array(0));

    $scope.ChmEdit=function(){
        $scope.userShow =false;
        $scope.EUNombre=$scope.dataUsr.nombre
        $scope.EUusuario=$scope.dataUsr.usuario
        $scope.EUcedula=Number($scope.dataUsr.cedula)
        $scope.EUtelefono=Number($scope.dataUsr.telefono)
        $scope.EUcorreo=$scope.dataUsr.correo
    }

    $scope.ChmEditCompany=function(){
        $scope.userShow =false;
        $scope.CUNombre=$scope.dataCompany.nombre
        $scope.CUcedula=Number($scope.dataCompany.cedula)
        $scope.CUtelefono=Number($scope.dataCompany.phone)
        $scope.CUcorreo=$scope.dataCompany.email
    }

    $scope.editarUser=function(){
        editar.User($scope.user,$scope.EUNombre,$scope.EUcorreo,$scope.EUcedula,$scope.EUtelefono,$scope.dataUsr.nivel,$scope.EUusuario,null,function(response){
            console.log(response)
            $scope.init()
        })
    }

    $scope.editarComp=function(){
        editar.empresa($cookies.get('empresa'),$scope.CUNombre,$scope.CUcedula,$scope.CUtelefono,$scope.CUcorreo,$scope.dataCompany.Cont_imm.nombre,$scope.dataCompany.Cont_imm.cedula,$scope.dataCompany.Cont_imm.telefono,$scope.dataCompany.Cont_imm.correo,function(response){
            console.log(response)
            $scope.init()
        })
    }

    $scope.ActualizarContImn=function(){
        editar.empresa($cookies.get('empresa'),$scope.dataCompany.nombre,$scope.dataCompany.cedula,$scope.dataCompany.phone,$scope.dataCompany.email,$scope.dataUsr.nombre,$scope.dataUsr.cedula,$scope.dataUsr.telefono,$scope.dataUsr.correo,function(response){
            console.log(response)
            $scope.init()
        })
    }

    $scope.data=function(user){
        //$scope.mycln=new Array(0);
        if(user==undefined){
            if($scope.users.length>0){
                $scope.data($cookies.get('id'));
            }
        }
        else{
            $scope.user = user;
            busquedas.usuarios(user,function(response){
                console.log(response.data);
                $scope.dataUsr = response.data;
                busquedas.divices(user,function(response){
                    console.log(response);
                    $scope.disp = response
                })
            });
        }
    }

    $scope.init=function(){
        $scope.userShow =true;
        busquedas.init(function(datos,extra){
            console.log('empresa y usuarios',datos,extra);
            $scope.users= datos;
            $scope.empresa= extra;
            busquedas.company(extra.id,function(response){
                console.log(response.data)
                $scope.dataCompany=response.data
            })
            $scope.data($cookies.get('id'));
        });
    }
    $scope.init()
}])