var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('clientes',['$scope','$http','$cookies','$window','$timeout','busquedas','links','crear','editar','leafletData',function($scope,$http,$cookies,$window,$timeout,busquedas,links,crear,editar){
    
    $scope.center={
        lat: 9.9356124,
        lng: -84.1833856,
        zoom: 12
    };
    $scope.markers=[];

     //saltos de pagina
    $scope.menu=[]
    $scope.links = links.links

    console.log( links)

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    /*$scope.ActualPoscition=function(){
        if ($window.navigator.geolocation) {
            $window.navigator.geolocation.getCurrentPosition(function(position) {
                //$window.alert('it works');
            }, function(error) {
                //$window.alert('Error occurred. Error code: ' + error.code);         
            },{timeout:5000});
        }else{
            //$window.alert('no geolocation support');
        }
    }*/
    //$scope.ActualPoscition()

    $scope.ChnCln=function(i){
        $scope.cln=$scope.mycln[i]
        console.log($scope.cln)
        $scope.center={
            lat: $scope.cln.lat,
            lng: $scope.cln.lon,
            zoom: 12
        }
        if($scope.markers.length>0)$scope.markers[0]={
            lat: $scope.cln.lat,
            lng: $scope.cln.lon,
            message:$scope.cln.nombre,
            icon: {
                iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
            }
        }
        else{
            $scope.markers.push({
                lat: $scope.cln.lat,
                lng: $scope.cln.lon,
                message:$scope.cln.nombre,
                icon: {
                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                }
            })
        }
    }

    $scope.clientes=function(where,i,data){
        console.log('/clientes/agentes/get/'+$cookies.get('id')+'/'+$cookies.get('empresa')+'/')
        $http({
            method:'GET',
            url:'/clientes/agentes/get/'+$cookies.get('id')+'/'+$cookies.get('empresa')+'/'
        }).
        then(function(response){
            console.log(response.data);
            
                $scope.mycln = response.data.data;
                if($scope.mycln.length>0){
                    $scope.cln=$scope.mycln[0]
                    $scope.center={
                        lat: $scope.cln.lat,
                        lng: $scope.cln.lon,
                        zoom: 12
                    }
                    if($scope.markers.length>0)$scope.markers[0]={
                        lat: $scope.cln.lat,
                        lng: $scope.cln.lon,
                        message:$scope.cln.nombre,
                        icon: {
                            iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                        }
                    }
                    else{
                        $scope.markers.push({
                            lat: $scope.cln.lat,
                            lng: $scope.cln.lon,
                            message:$scope.cln.nombre,
                            icon: {
                                iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                            }
                        })
                    }
                }
            
        })
    }
    $scope.clientes('null',0, new Array(0));

    $scope.getcatalog=function(){
        console.log($cookies.get('empresa'))
        busquedas.catalogo($cookies.get('empresa'),function(keys,data){
            //console.log(keys,catalogo)
            //$scope.CatKey=keys
            //$scope.catalogo=data
            $scope.Nobject={}
            for (let i = 0; i < keys.length; i++) {
                for (let j = 0; j < data[keys[i]].length; j++) {
                    CatElm = data[keys[i]][j];
                    $scope.Nobject[CatElm.id] = CatElm;
                }
            }
            $scope.ObjectsTags=Object.keys($scope.Nobject)
            console.log($scope.ObjectsTags)
        })
    }
    $scope.getcatalog()

    $scope.addProducto=function(){
        $scope.cln.productos.push({
            producto:$scope.producto,
            cantidad:$scope.cantidad,
        })
    }

    $scope.actualizarproductos=function(){
        for (let i = 0; i < $scope.cln.productos.length; i++) {
            const cln = $scope.cln.productos[i];
            if(cln.actual) $scope.cln.productos.cantidad=Number($scope.cln.productos.actual)
        }
        console.log($scope.cln.productos)
        var data={
            id:$scope.cln._id,
            productos:$scope.cln.productos
        };
        $http({
            method:'POST',
            url:'/actualizar/objetos/cliente',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            })
    }

    $scope.data=function(user){
        $scope.user = user;
        busquedas.usuarios(user,function(response){
            console.log(response.data);
            $scope.dataUsr = response.data;
        });
    }

    $scope.init=function(){
        $scope.userShow =true;
        busquedas.init(function(datos,extra){
            console.log('empresa y usuarios',datos,extra);
            $scope.users= datos;
            $scope.empresa= extra;
            $scope.data($cookies.get('id'));
        });
    }
    $scope.init()
}])

app.filter('Decode',function(){
    return function(input,type){
        switch (type) {
            case 0:
                switch (input) {
                    ////0: small, 1: med, 2: grande, 3 indefinidad
                    case 0:
                        return 'pequeña'
                        break;
                    case 1:
                        return 'mediana'
                        break;
                    case 2:
                        return 'grande'
                        break;
                    default:
                        return 'indefinida'
                        break;
                }
                break;
            case 1:
                switch (input) {
                    //0: indefinido, 1: distribuidor 2. supermercado, 3. farmacia, 4. Macrobiotica, 5. personal, 6. veterinaria
                    case 1:
                        return 'distribuidor'
                        break;
                    case 2:
                        return 'Supermercado'
                        break;
                    case 3:
                        return 'farmacia'
                        break;
                    case 4:
                        return 'Macrobiotica'
                        break;
                    case 5:
                        return 'personal'
                        break;
                    case 6:
                        return 'veterinaria'
                        break;
                    default:
                        return 'otro'
                        break;
                }
                break
            case 2:
                if(input){
                    if(input.contado) return 'contado'
                    else  return 'credito a '+input.dias+' dias'
                }
                break
            default:
                return 'indefinido'
                break;
        }
    }
})