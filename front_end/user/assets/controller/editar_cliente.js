var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('clientes',['$scope','$http','$cookies','$window','leafletData','busquedas','crear','editar','links',function($scope,$http,$cookies,$window,leafletData,busquedas,crear,editar,links){
    console.log($cookies.getAll())
    $scope.clientes=[]
    $scope.filterBy={
        nombre:'',
        region:'',
    }

    $scope.events= {};
    $scope.center={
        lat: 9.9356124,
        lng: -84.1833856,
        zoom: 12
    };
    $scope.markers=[];
    $scope.position=null

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
            console.log(position)
            $scope.position=position
            $scope.center={
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                zoom: 17
            };
            $scope.markers.push({
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                icon: {
                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                }
            })
            busquedas.reverserGeoJson(position.coords.latitude,position.coords.longitude,17,function(response){
                console.log(response.data.display_name)
                $scope.localiazcion=response.data.display_name
            })
        })
    } 
    else {
        console.log('geolocation is not supported')
    }

    $scope.pedidosSelc={}
    
    $scope.menu=[]
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    busquedas.init(function(user,empresa){
        //console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
    });
    busquedas.transporte($cookies.get('empresa'),function(response){
        console.log(response)
        $scope.transporte=response;
    })

    $scope.clientesSearch=function(init){
        busquedas.clientes($cookies.get('empresa'),null,String(init),function(response){
            console.log(response.max == response.last,response.max,response.last);
            if (response.max == response.last) {
                //llena el scope de respaldo
                $scope.clientes=$scope.clientes.concat(response.datos);
                console.log($scope.clientes)
            } 
            else 
            {
                $scope.clientes=$scope.clientes.concat(response.datos);
                $scope.clientesSearch(response.last)
            }
        })
    }
    //$scope.clientesSearch(0)

    $scope.init=function(){
        
    }
    $scope.init()

    $scope.UserName=function(name){
        console.log(name)
        if($scope.Jusers[name]==undefined){
            $http({
                method:'GET',
                url:'/find/user/'+name
            }).
            then(function(response){
                console.log(response.data);
                $scope.Jusers[name]=response.data.nombre
                $scope.Naprvby= response.data.nombre
            })
        }
        else $scope.Naprvby= null
    }

    $scope.editarCliente=function(){
        console.log($scope.pedidosSelc)
        var data={
            id:$cookies.get('empresa'),
            data:$scope.pedidosSelc,
        };
        $http({
            method:'POST',
            url:'/make/solicitud/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
        })
    }

    $scope.selectoption=function(x,i){
        if(i==1) x=JSON.parse(x)
        console.log(x)
        $scope.pedidosSelc=x
        
    }

    $scope.$on('leafletDirectiveMap.click', function(event, args){
        // Access underlying marker on leaflet object as args.leafletObject.options
        var leafEvent = args.leafletEvent;
        console.log(leafEvent.latlng)
        console.log($scope.markers)
        if($scope.markers.length==0){
            $scope.markers.push({
                lat: leafEvent.latlng.lat,
                lng: leafEvent.latlng.lng,
                icon: {
                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                }
            })
        }
        else{
            $scope.markers[0]={
                lat: leafEvent.latlng.lat,
                lng: leafEvent.latlng.lng,
                icon: {
                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                }
            }
            console.log($scope.markers)
        }
    });

}])