app.controller('alert',['$scope','$http','$cookies','$window',function($scope,$http,$cookies,$window){
    //inicio
    $scope.links = [
        {icon:'https://image.flaticon.com/icons/svg/263/263115.svg',text:'Inicio',link:'/user/index.html'},
        {icon:'https://image.flaticon.com/icons/svg/149/149346.svg',text:'Reportes',link:'/user/assets/htmls/historics.html'},
        {icon:'https://image.flaticon.com/icons/svg/896/896530.svg',text:'Dispositivos',link:'/user/assets/htmls/divices.html'},
        {icon:'https://image.flaticon.com/icons/svg/1042/1042456.svg',text:'Catalago',link:'/user/assets/htmls/catalogo.html'}
    ]

    $scope.goto =function(i,where){
        switch (i) {
            case 0:
                $window.open('/user/index.html','_self')
                break;
            case 1:
                $window.open('/empresa/assets/htmls/editar.html','_self')
                break;
            case 2:
                $window.open(where,'_self')
                break
            default:
                break;
        }
    }

    console.log($cookies.getAll())
    $scope.user=$cookies.get('id');
    $scope.get_Alerts = function(div,i,data){
        if(div.length>0){
            $http({
                method:'GET',
                url:'/divice/alerts/'+div[i].Alertas_last+'/100'
            }).
            then(function(response){
                console.log(response.data);
                if(response.data.data!=null)data.push(response.data.data);
                if(div.length>i+1) $scope.get_Alerts(div,i+1,data)
                else{
                    $scope.Alerts=data
                }
            })
        }      
    }

    $scope.init = function(){
        $http({
            method:'GET',
            url:'/divice/of/'+String($cookies.get('id'))
        }).
        then(function(response){
            console.log(response.data);
            $scope.myDiv = response.data;
            $scope.get_Alerts($scope.myDiv, 0 , new Array(0)); 
        })
    }
    $scope.init();

}])