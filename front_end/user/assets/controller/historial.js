var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('reports',['$scope','$http','$cookies','$window','leafletData','busquedas',function($scope,$http,$cookies,$window,leafletData,busquedas){
    console.log($cookies.getAll());
    $scope.user=$cookies.get('id');
    $scope.lastid='null'
    $scope.datos=new Array(0);
    $scope.fechas={
        kmin:[new Date(),'00:00:00'],
        kmax:[new Date(),'00:00:00'],
    }
    //console.log($scope.fechas.kmax[0],$scope.fechas.kmin[0])
    //charts
    /*var impulso =new Highcharts.chart( {
        chart: {
            renderTo: 'impulso',
            margin: [150, 75, 75, 75],
            type: 'line'
        },
        title:{
            text: null
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: 
                '<td style="color:{series.color};padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        xAxis: {
            type: 'datetime',
            labels:{
                enabled: false
            },
        },

        plotOptions: {
        },
        series: [{
            data: []
        },]
    });
    var velocidad =new Highcharts.chart( {
        chart: {
            renderTo: 'velocidad',
            margin: [150, 75, 75, 75],
            type: 'line'
        },
        title:{
            text: null
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: 
                '<td style="color:{series.color};padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        xAxis: {
            type: 'datetime',
            labels:{
                enabled: false
            },
        },

        plotOptions: {
        },
        series: [{
            data: []
        },]
    });*/
    var gaugeOptions = {
        chart: {
            type: 'solidgauge'
        },
        title: null,
        pane: {
            center: ['50%', '80%'],
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },
        tooltip: {
            enabled: false
        },
        // the value axis
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },
        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    }

    // The speed gauge
    var chartSpeed = Highcharts.chart('velocidadProm', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 200,
            title: {
                text: 'Speed'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Speed',
            data: [80],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                    '<span style="font-size:12px;color:silver">km/h</span></div>'
            },
            tooltip: {
                valueSuffix: ' km/h'
            }
        }]

    }));

    var bateria = Highcharts.chart('bateria', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Bateria'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Speed',
            data: [80],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                    '<span style="font-size:12px;color:silver">km/h</span></div>'
            },
            tooltip: {
                valueSuffix: ' km/h'
            }
        }]

    }));

    var gasolina = Highcharts.chart('gasolina', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'gasolina'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Speed',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                    '<span style="font-size:12px;color:silver">km/h</span></div>'
            },
            tooltip: {
                valueSuffix: ' km/h'
            }
        }]

    }));

    //init values
    $scope.keywords=[
        {'key':'drive', 'nombre':'distancia'},        
        {'key':'speed', 'nombre':'velocidad'},
        {'key':'power', 'nombre':'poder'},
        {'key':'distance', 'nombre':'recorrido'},
        {'key':'battery','nombre':'bateria'}

    ]
    $scope.keys=['drive','speed'];
    $scope.space='alertas';

    busquedas.init(function(user,empresa){
        console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
    });
    //console.log(datos);
    //$scope.users= datos.datos;
    //$scope.empresa= datos.extra;

    $scope.resetMap=function(){
        $scope.center= {
                    lat: 9.8755948,
                    lng: -84.0380218,
                    zoom: 10
                }

        $scope.geojson={
            data: {
                "type": "FeatureCollection",
                "features": []
            },
            style: {
                fillColor: "green",
                weight: 2,
                opacity: 1,
                color: 'red',
                dashArray: '3',
                fillOpacity: 0.7
            }
        }
    }
    $scope.resetMap()

    $scope.mapMakeLine=function(data,last){
        if(data.length>1) $scope.geojson.data.features.push({
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "LineString",
                "coordinates": data
            }
        });
        $scope.datos = new Array(1).fill(last)
    }

    $scope.mapPoints=function(data){
        console.log($scope.lastid)
        //crear puntos en el mapa
        $scope.actual=[]
        $scope.datos=[]
        for (let i = 0; i < data.length; i++) {
            //si es un dato valido
            //console.log(data[i]);
            if(data[i].lat != null && data[i].lon != null){
                if($scope.actual.length==0){
                    $scope.actual=[data[i].lon,data[i].lat];
                    //console.log('actual:',$scope.actual)
                }
                //si el radio es mayor a 10K se crear la linea por separado
                if(110950.58*Math.sqrt((data[i].lat-data[i-1].lat)*(data[i].lat-data[i-1].lat)+(data[i].lon-data[i-1].lon)*(data[i].lon-data[i-1].lon))>10000){
                    $scope.mapMakeLine($scope.datos,[data[i].lon,data[i].lat])
                }
                else $scope.datos.push([data[i].lon,data[i].lat]);
            }
            
        }
        $scope.mapMakeLine($scope.datos,[])
        if($scope.actual.length!=0) {
            /*$scope.geojson.data.features.push({
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "Point",
                    "coordinates": $scope.actual
                }
            })*/
            $scope.center= {
                lat: Number($scope.actual[1]),
                lng: Number($scope.actual[0]),
                zoom: 15
            }
        }
        else{
            $scope.center= {
                lat: 9.8755948,
                lng: -84.0380218,
                zoom: 15
            }
        }
        /*if($scope.datos.length>1) $scope.geojson.data.features.push({
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "LineString",
                "coordinates": $scope.datos
            }
        });*/
        //console.log($scope.center,$scope.datos)
    };

    $scope.filterDate = function(){
        
    }

    $scope.MinMax = function(k1min,k1max,k0min,k0max){
        console.log(k1min,k1max,k0min,k0max);
        if(k1min!=null && k0min != null){
            if(k1min>k0min)  mindate=new Date(k0min);
            else  mindate=new Date(k1min);
            if(k1max<k0max)  maxdate=new Date(k0max);
            else  maxdate=new Date(k1max);
        }
        else{
            if(k1min!=null){
                 mindate=new Date(k1min);
                 maxdate=new Date(k1max);
                console.log('1',mindate,maxdate)
            }
            else{
                if(k0min!=null){
                     mindate=new Date(k0min);
                     maxdate=new Date(k0max);
                    console.log('2',mindate,maxdate)
                }
                else{
                     mindate=null;
                     maxdate=null;
                }
            }
        }
        if(mindate !=null & maxdate !=null){
            $scope.fechas.kmin=[new Date(String(mindate.getYear()+1900)+'-'+String(mindate.getMonth()+1)+'-'+String(mindate.getDate())),String(mindate.getHours())+":00:00"]
            $scope.fechas.kmax=[new Date(String(maxdate.getYear()+1900)+'-'+String(maxdate.getMonth()+1)+'-'+String(maxdate.getDate())),String(maxdate.getHours())+":00:00"]
        }
        console.log($scope.fechas)
    }

    $scope.prom=function(data){
        console.log(data.length)
        /*let dataK1 = []
        let dataK0 = []
        let promedios = [
            {
                'prom':0,
                'max':null,
                'min':null
            },
            {
                'prom':0,
                'max':null,
                'min':null
            },
        ]
        //recorre los valores de data
        for (let i = 0; i < data.length; i++) {
            //solo acepat valores validos
            if(data[i][$scope.keys[0]] != null){
                //crea los arrays del char
                dataK0.unshift([Date.UTC(Number(data[i].date.slice(0,4)),Number(data[i].date.slice(4,6))-1,Number(data[i].date.slice(6,8)),Number(data[i].time.slice(0,2)),Number(data[i].time.slice(3,5)),Number(data[i].time.slice(6,8))),Number(data[i][$scope.keys[0]])]);
                //categoriesK0.unshift(String(data[i].date +'  '+ data[i].time));
                //calculo del promedio
                promedios[0].prom = ((promedios[0].prom * (dataK0.length-1))+Number(data[i][$scope.keys[0]]))/dataK0.length;
                //calculo del maximo y el minimo
                //si es el primero
                if(promedios[0].min==null){
                    promedios[0].min=Number(data[i][$scope.keys[0]]);
                    promedios[0].max=Number(data[i][$scope.keys[0]]);
                }
                //sino pregunta si cambio el minimo o el maximo
                else{
                    if(promedios[0].min>Number(data[i][$scope.keys[0]]))promedios[0].min=Number(data[i][$scope.keys[0]]);
                    if(promedios[0].max<Number(data[i][$scope.keys[0]]))promedios[0].max=Number(data[i][$scope.keys[0]]);
                }
            }
            if(data[i][$scope.keys[1]] != null){
                //crea los arrays del char
                dataK1.unshift([Date.UTC(Number(data[i].date.slice(0,4)),Number(data[i].date.slice(4,6))-1,Number(data[i].date.slice(6,8)),Number(data[i].time.slice(0,2)),Number(data[i].time.slice(3,5)),Number(data[i].time.slice(6,8))),Number(data[i][$scope.keys[1]])]);
                //categoriesK1.unshift(String(data[i].date +'  '+ data[i].time));
                //calculo del promedio
                promedios[1].prom = ((promedios[1].prom * (dataK1.length-1))+Number(data[i][$scope.keys[1]]))/dataK1.length;
                //calculo del maximo y el minimo
                //si es el primero
                if(promedios[1].min==null){
                    promedios[1].min=Number(data[i][$scope.keys[1]]);
                    promedios[1].max=Number(data[i][$scope.keys[1]]);
                }
                else{
                    if(promedios[1].min>Number(data[i][$scope.keys[1]]))promedios[1].min=Number(data[i][$scope.keys[1]]);
                    if(promedios[1].max<Number(data[i][$scope.keys[1]]))promedios[1].max=Number(data[i][$scope.keys[1]]);
                }
            }
        }
        //ordena los datos en orden cornologico
        dataK0.sort(function(a, b){return a[0]-b[0]});
        dataK1.sort(function(a, b){return a[0]-b[0]});
        //crea el nuevo chart
        impulso.series[0].setData(dataK0);
        //impulso.xAxis[0].setCategories(categoriesK0);
        velocidad.series[0].setData(dataK1);
        //velocidad.xAxis[0].setCategories(categoriesK1);
        //actualiza la tabla
        $scope.promTable=promedios
        //console.log($scope.promTable);
        if(dataK0.length>0 && dataK1.length>0) $scope.MinMax(dataK1[0][0],dataK1[dataK1.length-1][0],dataK0[0][0],dataK0[dataK0.length-1][0])
        else{
            if(dataK0.length>0) $scope.MinMax(null,null,dataK0[0][0],dataK0[dataK0.length-1][0])
            else{
                if(dataK1.length>0) $scope.MinMax(dataK1[0][0],dataK1[dataK1.length-1][0],null,null)
                else $scope.MinMax(null,null,null,null)
            }
        }*/
        bateria.series[0].setData([Number(data[1].battery)])
        $scope.dist=Math.abs((Number(data[1].distance)-Number(data[data.length-1].distance))/1000)
        console.log(Number(data[1].distance),Number(data[data.length-1].distance))
        var velocidad=0;
        for (let i = 0; i < data.length; i++) {
            velocidad = (velocidad*i+Number(data[1].speed))/(i+1);
        }
        chartSpeed.series[0].setData([Math.round(velocidad)])
    };

    $scope.consultas = function(extras,i){
        busquedas.divices($scope.user,function(div){
            busquedas.consultas('speed,distance,battery,tf,'+extras,div,0,0,new Array(0),null,function(datos,id){
                console.log(datos,id)
                $scope.dat=datos;
                if(extras==',lat,lon' || extras==',lon,lat')$scope.mapPoints(datos);
                $scope.prom(datos);
            })
        })
    }

    //inicia espacios de historicos, alertas y promedios 
    $scope.init=function(){
        $scope.resetMap()
        //si el agente existe se toman datos
        if($cookies.get('id')!= null && $cookies.get('id')!= undefined){
            busquedas.divices(String($cookies.get('id')),function(div){
                console.log(div);
                if(div!=null){
                    busquedas.alertas(div[0].Alertas_last,function(alert){
                        console.log(alert);
                        $scope.Alerts=alert;
                    }) 
                }
            })
            //conseguir los historicos y datos relevantes para los promedios
            $scope.consultas(',lat,lon',0);
        }
        else $window.open('/login/index.html','_self')
    }

    $scope.$watchCollection('keys',function(newCollection, oldCollection){
        console.log(newCollection);
        $scope.lastid='null'
        $scope.dataK0=[];
        $scope.dataK1=[];
        $scope.datos=[];
        $scope.consultas('',0);
    })
}])

