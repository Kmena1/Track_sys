app.controller('divice',['$scope','$http','$cookies','$window',function($scope,$http,$cookies,$window){
    //init var
    $scope.modalShow="archiving";
    $scope.rules=["moduled_tolerance","simple_tolerance","fixed_tolerance","time","radio","alerta"];
    $scope.propertys=["lat","lon","speed","dist","battery"];
    $scope.propertysTime=["Y","M","D","H","m","s"];
    $scope.propertysAlert=["geocerca","Static"];
    $scope.operators=["ge","le","l","g","e","ne"];
    $scope.operator="ge";
    $scope.modulo=0;
    $scope.master=false;
    console.log($cookies.getAll())
    $scope.user=$cookies.get('id');
    

    $scope.links = [
        {icon:'https://image.flaticon.com/icons/svg/263/263115.svg',text:'Inicio',link:'/user/index.html'},
        {icon:'https://image.flaticon.com/icons/svg/149/149346.svg',text:'Reportes',link:'/user/assets/htmls/historics.html'},
        {icon:'https://image.flaticon.com/icons/svg/896/896530.svg',text:'Dispositivos',link:'/user/assets/htmls/divices.html'},
        {icon:'https://image.flaticon.com/icons/svg/1042/1042456.svg',text:'Catalago',link:'/user/assets/htmls/catalogo.html'}
    ]

    $scope.goto =function(i,where){
        switch (i) {
            case 0:
                $window.open('/user/index.html','_self')
                break;
            case 1:
                $window.open('/empresa/assets/htmls/editar.html','_self')
                break;
            case 2:
                $window.open(where,'_self')
                break
            default:
                break;
        }
    }
    //inicio
    $scope.init = function(){
        $http({
            method:'GET',
            url:'/struc/all'
        }).
        then(function(response){
            console.log(response.data);
            $scope.imeis = response.data
        })
        $http({
            method:'GET',
            url:'/divice/of/'+String($cookies.get('id'))
        }).
        then(function(response){
            console.log(response.data);
            $scope.myDiv = response.data;
            //$scope.get_Alerts($scope.myDiv, 0 , new Array(0)); 
        })
    }
    $scope.init();

    $scope.crearImei = function(id){
        var data={
            deviceId:id,
            header:{
                tipo:JSON.parse($scope.header).tipo,
                model_ID:JSON.parse($scope.header).model_ID,
            }
        };
        console.log(data);
        $http({
            method:'POST',
            url:'/divice/assing/imei',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.init();
        })
    }

    $scope.remover =function(div,imei){
        console.log(div,imei);
        var data={
            dispositivo:div,
            imei:imei
        };
        $http({
            method:'POST',
            url:'/imei/desinstall',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.init()
        })
    }

    $scope.page=function(j){
        switch (j) {
            case 0:
                $scope.mydiv=true
                $scope.arch=false
                $scope.comandos=false
                break;
            case 1:
                $scope.mydiv=false
                $scope.arch=true
                $scope.comandos=false
                break;
            case 2:
                $scope.mydiv=false
                $scope.arch=false
                $scope.comandos=true
                break;
            default:
                break;
        }
    }

        $scope.page(0)
    $scope.Archdata=function(dato){
        //const element = $scope.dataUsr[i];
        console.log($scope.ArchDisp)
        var data={
            'ArchSetts':JSON.parse($scope.ArchDisp).Archiving
        };
        console.log(data)
        $http({
            method:'POST',
            url:'/find/arch',
            data:data
        }).
        then(function(response){
            //console.log(response.data);
            $scope.dataAch=response.data.data
            console.log($scope.dataAch)
        })
    }

    $scope.set=function(id){
        console.log($scope.rule,$scope.property,$scope.operator,$scope.master, $scope.diviceId, $scope.tolerancia);
        if($scope.diviceId != undefined && $scope.operator!=undefined && $scope.tolerancia!=undefined  && $scope.property!=undefined){
            if($scope.ref==undefined)var reference=null
            else reference=$scope.ref
            if($scope.rule=="radio"){
                var property="radius";
            }
            else{
                var property=$scope.property
            }
            var data={
                deviceId:$scope.diviceId,
                settings:{
                    rule		 :$scope.rule,
                    property	 :property,
                    tolerance	 :$scope.tolerancia,
                    module		 :$scope.modulo,
                    reference    :{
                        ref1:reference,
                    },
                    operator     :$scope.operator,
                    master       :$scope.master
                }
            };
            console.log(data)
            $http({
                method:'POST',
                url:'http://34.237.241.0:2500/divice/set/achieve',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.Archdata(new Array(0));
            })
        }
    }

     $scope.check =function(dato){
        console.log('checkeando',$scope.propertys)
        if($scope.rule=="time"){
            $scope.propertys=["Y","M","D","H","m","s"];
        }
        else if($scope.rule=="alerta"){
            $scope.propertys=["geocerca","Static"];
        }
        else{
            $scope.propertys=["lat","lon","speed","dist","battery"];
        }

    }

}])