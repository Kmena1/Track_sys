var imp1=13
var imp2=2

var app =angular.module("app",['ui.bootstrap','ngCookies','ui-leaflet']);
app.controller('MisPedidos',['$scope','$http','$cookies','$window','busquedas','editar','crear','links',function($scope,$http,$cookies,$window,busquedas,editar,crear,links){
    console.log($cookies.getAll())
    $scope.Jusers={};
    $scope.filterBy={
        cliente:'',
        estado:''
    }
    

    $scope.menu=[]
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    busquedas.init(function(user,empresa){
        console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
    });
    busquedas.transporte($cookies.get('empresa'),function(response){
        console.log(response)
        $scope.transporte=response;
    })

    $scope.checkEstado=function(){
        console.log($scope.filestado)
    }

    $scope.clientLook=function(){
        var clientes=[]
        for (let i = 0; i < $scope.MisPedidos.length; i++) {
            cliente = $scope.MisPedidos[i].cliente;
            if(clientes.indexOf(cliente)==-1)clientes.push(cliente)
        }
        console.log(clientes)
        if(clientes.length>0){
            var data={
                clientes:clientes
            };
            $http({
                method:'POST',
                url:'/clientes/get/array',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                $scope.ClientsId={}
                for (let i = 0; i < response.data.data.length; i++) {
                    if(response.data.data[i]){
                    $scope.ClientsId[response.data.data[i]._id] = response.data.data[i];
                    }
                }
            })
        }
    }

    $scope.getcatalog=function(){
        console.log($cookies.get('empresa'))
        busquedas.catalogo($cookies.get('empresa'),function(keys,data){
            //console.log(keys,catalogo)
            //$scope.CatKey=keys
            //$scope.catalogo=data
            $scope.ProdctInfo={}
            $scope.Nprecio={}
            for (let i = 0; i < keys.length; i++) {
                for (let j = 0; j < data[keys[i]].length; j++) {
                    CatElm = data[keys[i]][j];
                    $scope.ProdctInfo[CatElm.id] = CatElm;
                }
            }
            console.log($scope.ProdctInfo)
        })
    }
    //$scope.getcatalog()

    $scope.init=function(){
        $http({
            method:'GET',
            url:'/get/pedidos/user/'+$cookies.get('id')
        }).
        then(function(response){
            //console.log(response.data);
            $scope.MisPedidos=response.data
            if($scope.MisPedidos.length>0){
                $scope.selectoption($scope.MisPedidos[0],0)
                for (let i = 0; i < $scope.MisPedidos.length; i++) {
                    const pedido = $scope.MisPedidos[i];
                    if(pedido.aprovado){
                         $scope.MisPedidos[i]['estado']=0
                        for (let j = 0; j < pedido.reservas.length; j++) {
                            if(!pedido.reservas[j].Aprovado){
                                $scope.MisPedidos[i]['estado']=2
                                break
                            }
                        }
                    }
                    else $scope.MisPedidos[i]['estado']=1
                }
            }
            console.log($scope.MisPedidos)
            $scope.clientLook()
        })
    }
    $scope.init()

    $scope.UserName=function(name){
        //console.log(name)
        if($scope.Jusers[name]==undefined){
            $http({
                method:'GET',
                url:'/find/user/'+name
            }).
            then(function(response){
                //console.log(response.data);
                $scope.Jusers[name]=response.data.nombre
                $scope.Naprvby= response.data.nombre
            })
        }
        else $scope.Naprvby= null
    }

    $scope.selectoption=function(x,i){
        if(i==1) x=JSON.parse(x)
        //console.log(x)
        $scope.pedidosSelc=x
        $scope.Dataorden()
        if(x.aprovado) {
            $scope.UserName(x.aprvby)
            if(x.consecutivo!=0){
                var data={
                    concecutivo:x.consecutivo
                };
                $http({
                    method:'POST',
                    url:'/get/concecutivo/ordenes/',
                    data:data
                }).
                then(function(response){
                    console.log(response.data);
                    $scope.ordenes=response.data
                })
            }
            else{
                $scope.ordenes={
                    congelado:null,
                    salida:null
                }
            }
        }
    }

    $scope.Dataorden=function(){
        //console.log($scope.pedidosSelc.reservas)
        productos=[]
        for (let i = 0; i < $scope.pedidosSelc.reservas.length; i++) {
            productos.push($scope.pedidosSelc.reservas[i].objeto)
            
        }
        var data={
            productos:productos
        };
        $http({
            method:'POST',
            url:'/productos/array/',
            data:data
        }).
        then(function(response){
            //console.log(response.data);
            $scope.ProdctInfo=response.data.data
        })
    }

    $scope.RedoPed = function(){
        $scope.PedidosRed={
            Abono:$scope.pedidosSelc.Abono,
            agente:$scope.pedidosSelc.agente,
            cliente:$scope.pedidosSelc.cliente,
            resposable:$scope.pedidosSelc.resposable,
            transporte:$scope.pedidosSelc.transporte,
            contado:$scope.pedidosSelc.contado,
            reservas:[],
        }
        //$scope.descuento=$scope.pedidosSelc.descuento
        //console.log($scope.pedidosSelc)
        $scope.total=0;
        $scope.impuesto=0;
        $scope.descuento=0;
        for (let i = 0; i < $scope.pedidosSelc.reservas.length; i++) {
            if(!$scope.pedidosSelc.reservas[i].Aprovado){
                $scope.PedidosRed.reservas.push($scope.pedidosSelc.reservas[i])
                $scope.descuento=$scope.descuento+$scope.pedidosSelc.reservas[i].descuento
            }
        }
        //console.log($scope.PedidosRed)
        $scope.RevRedo()
    }

    $scope.RevRedo=function(){
        $scope.total=0;
        $scope.impuesto=0;
        $scope.descuento=0
        $scope.err=false
        for (let i = 0; i < $scope.PedidosRed.reservas.length; i++) {
            const ped=$scope.PedidosRed.reservas[i]
            console.log(ped)
            var descuento=0
            switch(Number(ped.Tdescuento)){
                case 1:
                    //$scope.PedidosRed.reservas[i].cantidad=ped.cantidad+ped.descuento;
                    descuento=ped.descuento*$scope.ProdctInfo[ped.objeto].precio
                    break
                case 2:
                    descuento=$scope.ProdctInfo[ped.objeto].precio*ped.descuento/100
                    break
                case 3:
                    descuento=ped.descuento
                    break;
            }
            $scope.descuento=+descuento
            if($scope.ProdctInfo[ped.objeto].exentos)imp=imp1
            else imp=imp2
            $scope.total=$scope.total+($scope.ProdctInfo[ped.objeto].precio*ped.cantidad-descuento)*imp/100
            $scope.impuesto=$scope.impuesto+($scope.ProdctInfo[ped.objeto].precio*ped.cantidad-descuento)*imp/100
            
            $scope.PedidosRed.reservas[i]['total']=$scope.ProdctInfo[ped.objeto].precio*ped.cantidad-descuento
            if($scope.PedidosRed.reservas[i]['total']<0) $scope.err=true
        }
        console.log($scope.PedidosRed.reservas)
    }

    $scope.RevDesc=function(i){
        ped=$scope.PedidosRed.reservas[i]
        switch(Number(ped.Tdescuento)){
            case 1:
                $scope.PedidosRed.reservas[i].descuento=0
                $scope.PedidosRed.reservas[i].total=0
                $scope.PedidosRed.reservas[i].Tdescuento=5
                break
            case 2:
                if(ped.descuento>100 || ped.descuento<0){
                    $scope.PedidosRed.reservas[i].descuento=0
                }
                break
            case 3:
                if(ped.descuento>$scope.ProdctInfo[ped.objeto].precio*ped.cantidad){
                    $scope.PedidosRed.reservas[i].descuento=0
                }
                break
        }
        $scope.RevRedo()
    }

    $scope.envioCuenta=function(){
        //revision de los pedidos
        var err=false
        for (let i = 0; i < $scope.PedidosRed.reservas.length; i++) {
            const ped=$scope.PedidosRed.reservas[i]
            //console.log(ped)
            var descuento=0
            switch(Number(ped.Tdescuento)){
                case 1:
                    //$scope.PedidosRed.reservas[i].cantidad=ped.cantidad+ped.descuento;
                    $scope.PedidosRed.reservas[i].descuento=ped.descuento*$scope.ProdctInfo[ped.objeto].precio
                    break
                case 2:
                    $scope.PedidosRed.reservas[i].descuento=$scope.ProdctInfo[ped.objeto].precio*ped.descuento/100
                    break
                case 3:
                    $scope.PedidosRed.reservas[i].descuento=ped.descuento
                    break;
            }
        }
        console.log($scope.PedidosRed)
        crear.pedido($cookies.get('empresa'),$cookies.get('id'),$scope.PedidosRed.reservas,$scope.PedidosRed.contado,$scope.PedidosRed.Abono,$scope.PedidosRed.transporte,$scope.total,$scope.PedidosRed.cliente,$scope.PedidosRed.resposable,function(response){
            console.log(response)
        })
    }
}])

app.filter('faltantes',function(){
    return function(input){
        //console.log(input)
        if(input){
            ret=true
            if(input.aprovado){
                for (let i = 0; i < input.reservas.length; i++) {
                    if(!input.reservas[i].Aprovado){
                        ret=false
                        break
                    }
                }
                //console.log(ret)
                return ret
            }
            else return true
        }
        else return true
    }
})

app.filter('estado', function(){
    return function(x,ordenes){
        var respuesta=false
        if(ordenes){
            if(x.Aprovado){
                if(ordenes.congelado){
                    for (let i = 0; i < ordenes.congelado.prductos.length; i++) {
                        const ElmFrz = ordenes.congelado.prductos[i];
                        if(ElmFrz.producto==x.objeto) {
                            respuesta=true
                            return 'Retenido'
                            break
                        }
                    }
                    if(!respuesta){
                        if(ordenes.salida){
                            for (let i = 0; i < ordenes.salida.prductos.length; i++) {
                                const ElmOut = ordenes.salida.prductos[i];
                                if(ElmOut.producto==x.objeto) {
                                    switch(ordenes.salida.estado){
                                        case 0:
                                            return 'En cola'
                                            break
                                        case 1:
                                            return 'Alistando'
                                            break
                                        case 2:
                                            return 'Enviado'
                                            break
                                        default:
                                            return 'Desconocido'
                                            break
                                    }
                                    break
                                }
                            }
                        }
                    }
                }
                else{
                    if(ordenes.salida){
                        for (let i = 0; i < ordenes.salida.prductos.length; i++) {
                            const ElmOut = ordenes.salida.prductos[i];
                            if(ElmOut.producto==x.objeto) {
                                switch(ordenes.salida.estado){
                                    case 0:
                                        return 'En cola'
                                        break
                                    case 1:
                                        return 'Alistando'
                                        break
                                    case 2:
                                        return 'Enviado'
                                        break
                                    default:
                                        return 'Desconocido'
                                        break
                                }         break
                            }
                        }
                    }
                }
            }
            else return 'Rechazado'
        }
        else return 'Desconocido'
    }
})