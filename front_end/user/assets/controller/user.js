var app = angular.module("app", ['ngCookies',"leaflet-directive"]);
app.controller('user',['$scope','$http','$cookies','$window','$timeout','busquedas','links','leafletData',function($scope,$http,$cookies,$window,$timeout,busquedas,links){
    //init values
    console.log($cookies.getAll())
    $scope.user = $cookies.get('id');
    $scope.events= {};
    $scope.center={
        lat: 9.9356124,
        lng: -84.1833856,
        zoom: 12
    };
    $scope.markers=[];
    $scope.divMarkers=[];
    $scope.clnMarkers=[];

    //saltos de pagina
    $scope.menu=[]
    $scope.links = links.links

    $scope.goto = function(i,where) {
        links.Fgoto($window,i,where)
    }

    $scope.changemenu=function(x){
        if($scope.menu!=x)$scope.menu=x
    }

    $scope.chgCollCat = function(x){
        console.log($scope.catalogo[x])
        $scope.col=x
    }

    $scope.go_to =function(lat,lon,z){
        console.log(lat,lon,z);
        $scope.center={
            lat: Number(lat),
            lng: Number(lon),
            zoom: Number(z)
        };
    } 

    $scope.MakeMarkers=function(){
        $scope.markers=$scope.divMarkers.concat($scope.clnMarkers);
    }


    $scope.clientes=function(where,i,data){
        $http({
            method:'GET',
            url:'/clientes/agentes/get/'+$scope.user+'/'+where+'/'+i*100+'/100'
        }).
        then(function(response){
            console.log(response.data.datos);
            data=data.concat(response.data.datos)
            if(response.data.last>=100*(1+i)){
                $scope.clientes(where,i+1)
            } 
            else{
                $scope.mycln = data;
                //markers
                for (let i = 0; i < data.length; i++) {
                    $scope.clnMarkers.push({
                        lat:Number(data[i].lat),
                        lng:Number(data[i].lon),
                        message:String(data[i].nombre),
                        icon: {
                            iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                        }
                    })  
                }
                $scope.MakeMarkers()
            }
        })
    }

    $scope.lastPos= function(div,i,data){
         /*$http({
            method:'GET',
            url:'/divice/keys/'+div[i].id+'/lat,lon/2'
        }).
        then(function(response){
            console.log(response.data.data);
            if(response.data.data[response.data.data.length-1].lat != null) data.push({
                lat:Number(response.data.data[response.data.data.length-1].lat),
                lng:Number(response.data.data[response.data.data.length-1].lon),
                message:String(div[i].unidad),
                icon: {
                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
                }
            })
            if(i+1<div.length) lastPos(div,i+1,data);
            else {
                $scope.divMarkers=data;
                console.log($scope.markers);
                $scope.MakeMarkers()
            }
        })*/
        busquedas.consultaslast('lat,lon',div,10,i,new Array(0),null,function(datos,last){
            console.log(datos,last)
            if(datos[datos.length-1].lat != null) data.push({
                lat:Number(datos[datos.length-1].lat),
                lng:Number(datos[datos.length-1].lon),
                message:String(div[0].unidad),
                icon: {
                    iconUrl:'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
                }
            })
            if(1<div.length) $scope.lastPos(div,i+1,data);
            else {
                $scope.divMarkers=data;
                console.log($scope.markers);
                $scope.MakeMarkers()
            }
        }) 
    }

    busquedas.init(function(user,empresa){
        console.log(user,empresa)
        $scope.users= user;
        $scope.empresa= empresa;
    });

    $scope.keepTracking=function(){
        console.log($scope.myDiv);
        $scope.lastPos($scope.myDiv,0,new Array(0));
        $timeout(function(){
            $scope.keepTracking(); 
        },5000);
    }
    
    //inicio
    $scope.init = function(){
        $http({
            method:'GET',
            url:'/divice/of/'+String($cookies.get('id'))
        }).
        then(function(response){
            console.log(response.data);
            $scope.myDiv=response.data;
            $scope.lastPos($scope.myDiv,0,new Array(0));
            $scope.clientes('null',0, new Array(0));
            $scope.keepTracking();
        })
    }

    $scope.init();
    /*$scope.$on('leafletDirectiveMarker.click', function(event, args){
      // Access underlying marker on leaflet object as args.leafletObject.options
      var leafEvent = args.leafletEvent;
      console.log(leafEvent)
    });*/
}])