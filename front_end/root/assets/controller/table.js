app.controller('table',['$scope','$http','$cookies',function($scope,$http,$cookies){
    console.log('table');

    $scope.Fassig=false;
    $scope.Finstall=false;

    $scope.tracksTable = function(){
        $http({
            method:'GET',
            url:'/struc/all'
        }).
        then(function(response){
            console.log(response.data);
            info=response.data;
            $scope.trackers=info;
        })
    }

    $scope.new =function(){
        $scope.IdStr=null;
        $scope.nombre=null;
        $scope.header=null;
        $scope.Model=null;
        $scope.version=null;
        $scope.structure=null;
    }

    $scope.findStr = function(id){
        $http({
            method:'GET',
            url:'/struc/one/'+String(id)
        }).
        then(function(response){
            console.log(response.data);
            info=response.data;
            $scope.IdStr=id;
            $scope.nombre=info.nombre;
            $scope.header=info.header;
            $scope.tipo=info.tipo;
            $scope.Model=info.model_ID;
            $scope.version=info.protocol;
            $scope.structure=info.structure;
        })
    }

    $scope.esturcturEdit = function(id){
        STR=String($scope.structure);
        STR=STR.replace(/;/g,',');
        console.log(STR);
        StrArr=STR.split(',');
        
        var data={
            id:id,
            StrChg:{
                nombre: $scope.nombre,
                header: $scope.header,
                tipo: $scope.tipo,
                protocol : $scope.version,
                model_ID : $scope.Model,
                structure: StrArr,
            }
        };
        console.log(data)
        $http({
            method:'POST',
            url:'/struc/change',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.tracksTable();
        })
    }

    $scope.imeiTable=function(){
        var filter={};
        if($scope.head != undefined) filter['tipo']=$scope.head;
        if($scope.Fimei != undefined) filter['imei']=$scope.Fimei;
        if($scope.Fassig !="NaN") filter['Asign']=$scope.Fassig;
        if($scope.Finstall !="NaN") filter['install']=$scope.Finstall;
        console.log(filter);
        var data={
            'filter':filter
        };
        $http({
            method:'POST',
            url:'/get/trackes',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.imeis=response.data;
        })
    }

    $scope.CrearTracker=function(){
        var data={
            'imei':$scope.imeiNew,
            'tipo':$scope.TrackModel
        };
        console.log(data);
        $http({
            method:'POST',
            url:'/imei/crear/',
            data:data
        }).
        then(function(response){
            console.log(response.data);
            $scope.imeiTable();
        })
    }

    $scope.tracksTable();
    $scope.imeiTable();
    
}])