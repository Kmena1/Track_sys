var express = require('express');
var app = express();
var mongoose     = require('mongoose');
var configDB     = require('./Top_end/configs/database.js');

//Configuration mongo
// ==========================================================================
mongoose.connect(configDB.url);


var http = require('http');

var bodyParser = require('body-parser');

var path    = require("path");
var fs = require('fs');

var multer = require('multer');

var cors = require('cors');
app.use(cors());

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static(__dirname + '/public'));

app.get('/hola',function(req,res){
    console.log('hola');
    res.json('hola')
    
})


//routes
require('./Top_end/routes/email.js')(app);
require('./Top_end/routes/imeis.js')(app);
require('./Top_end/routes/users.js')(app);
require('./Top_end/routes/divice.js')(app);
require('./Top_end/routes/clientes.js')(app);
require('./Top_end/routes/inventario.js')(app);
require('./Top_end/routes/calendario.js')(app);
require('./Top_end/routes/mensajes.js')(app);
var fileAPI = require('./Top_end/routes/file.js');
fileAPI.load(app,multer,fs,__dirname);
var fileAPI2 = require('./Top_end/routes/upload.js');
fileAPI2.load(app,multer,fs,__dirname);

app.get('/qr/*',function(req,res){
    res.sendFile(path.join(__dirname+'/../front_end/empresa/assets/htmls/qrShow.html'))
})


app.get('/*',function(req,res){
    let url=req.url.split('/');
    //console.log(url);
    if(url[1]=='node_modules') res.sendFile(path.join(__dirname+'/../'+req.url));
    else res.sendFile(path.join(__dirname+'/../front_end/'+req.url));
});

/*var receptor = require('./low/scripts/HWCommunication/receptor.js');
receptor.TCPserver(app);*/

app.listen(2500);