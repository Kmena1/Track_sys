// Libraries
var mongoose = require('mongoose');
var app = require('express')();
var bodyParser = require('body-parser');
var ObjectId = require('mongodb').ObjectID;

var PassArte = require('./../../top/models/crear_empresa_model.js');
var imei = require('./../../top/models/imei_inventario_modelo.js');

// Connection to Mongo
mongoose.connect('mongodb://localhost/Passarte');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});
// Prepare the schema
var SuntecSchema = mongoose.Schema({
    header: String,
    protocol: String,
    model_ID: String,
	structure:[String]
});

// Get the object model
var Device = mongoose.model('SuntecTracks', SuntecSchema);



app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.get('/HWSchema/', function (req, res) {
    //res.sendFile(__dirname + "/index.html");
    res.sendFile(__dirname + "/test.html");
});

app.post('/HWSchema/editar',function(req,res){
	var info = req.body;
	var Imei ={
		header: info.header,
	    protocol: info.protocol,
	    model_ID: info.model_ID,
		structure:info.structure	
	};
	console.log(info.id);
	Device.findOne({'_id':ObjectId(info.id)},function(err,imei){
		if(err) console.log(err);
		else{
			imei.header= info.header;
		    imei.protocol= info.protocol;
		    imei.model_ID= info.model_ID;
			imei.structure=info.structure;
			imei.save(function(err){
				if(err)console.log(err);
			})
		}
	})
	res.json(null);
});

app.post('/HWSchema/post', function (req, res) {
    var GOT = req.body;
    // Registering
    console.log(GOT);
    try
    {
    	console.log(GOT.model_iD);
        var Sensor = new Device({
            header:GOT.header, 
            structure:GOT.structure, 
            protocol: GOT.protocol,
            model_ID: GOT.model_iD
        });
        Device.find({header:GOT.header, model_ID:GOT.model_iD}, function(err, sensor){
            // Send by JSON
            if(sensor.length == 0)
            {
                Sensor.save(function(err, sensor){
                    if(err) return console.error(err);
                    console.log("Device registered: " + GOT.header);
                });
            }
            else
            {
                console.log("Device already in DB: " + GOT.header);
            }
        });

    }
    catch(err)
    {
        console.log("Eduardo loves Monica")
    }
    res.json(null);
});

app.post('/HWSchema/post/eliminar', function (req, res) {
    var GOT = req.body;
    // Registering
    console.log('revicido',GOT.imei);
    Device.remove({'_id':ObjectId(GOT.imei)},function(err){
    	if(err)console.log(err);
    });
    res.json({'respuesta':'eliminado'});
 });

app.get('/HWSchema/get/:showID', function (req, res) {
    // Get the parameter
    var filter = req.params.showID;
    // Filtering
    if(filter == "all")
    {
        console.log("Printing all sensors");
        Device.find({}, function(err, sensor){
            // Debug in console
            for(var i in sensor)
                console.log(sensor[i]);
            // Send by JSON
            res.json(sensor);
        });
    }
    else
    {
        console.log("Printing: " + filter);
        // Debug in console        
        Device.find({header:filter}, function(err, sensor){
             // Send by JSON
            res.json(sensor[0]);
            //console.log(sensor[0]);
        });
       
    }
});
/*******************************************************************************************************************************
 * apartir de este punto comienzan las pruebas en el puerto 3001
 ******************************************************************************************************************************/
app.get('/position/bus/:imei',function(req,res){
		var positionBuffer=new Array(0);
		var imei=req.params.imei;
		PassArte.Business.findOne({'buses.imei':imei},function(err,bus){
			if(bus==null)res.json(null);
			else{
				for(i=0;i<bus.buses.length;i++){
					if(bus.buses[i].imei == imei){
						for(j=0;j<bus.buses[i].contents.length;j++){
							position={
								lat:bus.buses[i].contents[j].lat,
								lon:bus.buses[i].contents[j].lon, 
								hora: bus.buses[i].contents[j].date,
								time: bus.buses[i].contents[j].time	
								};
							position=JSON.stringify(position);
							position=JSON.parse(position);
							//if(j>0)positionBuffer+=",";
							//positionBuffer+=position;
							positionBuffer.push(position);
						}
						//console.log(positionBuffer);
						//const buf = new Buffer(positionBuffer);
						positionBuffer=JSON.stringify(positionBuffer);
						positionBuffer=JSON.parse(positionBuffer);
						res.json(positionBuffer);
						break;
					}
				}
			};
		});
			
	});
	
app.get('/position/fecha/:imei/:dia/:horai/:horaf',function(req,res){
		var positionBuffer=new Array(0);
		var imei=req.params.imei;
		PassArte.Business.findOne({'buses.imei':imei},function(err,bus){
			if(bus==null)res.json(null);
			else{
				for(i=0;i<bus.buses.length;i++){
					if(bus.buses[i].imei == imei){
						for(j=0;j<bus.buses[i].contents.length;j++){
							hora=bus.buses[i].contents[j].time.replace(/:/g,'');
							console.log(hora, bus.buses[i].contents[j].date);
							if(bus.buses[i].contents[j].date==Number(req.params.dia)
								&& Number(hora)>Number(req.params.horai)
								&& Number(hora)<Number(req.params.horaf)
								){
								position={
									lat:bus.buses[i].contents[j].lat,
									lon:bus.buses[i].contents[j].lon, 
									hora: bus.buses[i].contents[j].date,
									time: bus.buses[i].contents[j].time	
									};
								position=JSON.stringify(position);
								position=JSON.parse(position);
								positionBuffer.push(position);
								}
						}
						//console.log(positionBuffer);
						//const buf = new Buffer(positionBuffer);
						positionBuffer=JSON.stringify(positionBuffer);
						positionBuffer=JSON.parse(positionBuffer);
						res.json(positionBuffer);
						break;
					}
				}
			};
		});
	});

app.get('/position/distancia/:imei/:distancia/',function(req,res){
		var positionBuffer=new Array(0);
		var imei=req.params.imei;
		PassArte.Business.findOne({'buses.imei':imei},function(err,bus){
			if(bus==null)res.json(null);
			else{
				for(i=0;i<bus.buses.length;i++){
					if(bus.buses[i].imei == imei){
						for(j=0;j<bus.buses[i].contents.length;j++){
							if(j>0){
								a=Math.sqrt(
                    			Math.pow((bus.buses[i].contents[j].lat-lat1),2)+
                    			Math.pow((bus.buses[i].contents[j].lon-lon1),2))*111184.726;
							}
							else {
								a=0;
								lat1=bus.buses[i].contents[0].lat;
								lon1=bus.buses[i].contents[0].lon;
								b=0;
							}
							//console.log(a,req.params.distancia);
							if (a>Number(req.params.distancia)){
									console.log(a,b);
									position={
									lat:bus.buses[i].contents[j].lat,
									lon:bus.buses[i].contents[j].lon, 
									hora: bus.buses[i].contents[j].date,
									time: bus.buses[i].contents[j].time	
									};
									position=JSON.stringify(position);
									position=JSON.parse(position);
									positionBuffer.push(position);
									lat1=bus.buses[i].contents[j].lat;
									lon1=bus.buses[i].contents[j].lon;
									b++;
							}
						}
						//console.log(positionBuffer);
						//const buf = new Buffer(positionBuffer);
						positionBuffer=JSON.stringify(positionBuffer);
						positionBuffer=JSON.parse(positionBuffer);
						res.json(positionBuffer);
						break;
					}
				}
			};
		});
	});
	
app.get('/position/bus/:imei/last/:cant',function(req,res){
		var positionBuffer=new Array(0);
		var imei=req.params.imei;
		PassArte.Business.findOne({'buses.imei':imei},function(err,bus){
			if(bus==null)res.json(null);
			else{
				for(i=0;i<bus.buses.length;i++){
					if(bus.buses[i].imei == imei){
						cantid=bus.buses[i].contents.length;
						console.log(cantid);
						for(j=cantid-1;j>cantid-req.params.cant-1;j--){
							position={
								lat:bus.buses[i].contents[j].lat,
								lon:bus.buses[i].contents[j].lon, 
								dia: bus.buses[i].contents[j].date,
								hora: bus.buses[i].contents[j].time,
								bateria: bus.buses[i].contents[j].battery	
								};
							position=JSON.stringify(position);
							position=JSON.parse(position);
							//if(j>0)positionBuffer+=",";
							//positionBuffer+=position;
							positionBuffer.push(position);
							if(i==0)break;
						}
						//console.log(positionBuffer);
						//const buf = new Buffer(positionBuffer);
						positionBuffer=JSON.stringify(positionBuffer);
						positionBuffer=JSON.parse(positionBuffer);
						res.json(positionBuffer);
						break;
					}
				}
			};
		});
			
	});
app.get('/position/company/:id',function(req,res){
		var id=req.params.id;
		var busesBuffer =new Array(0);
		PassArte.Business.findById(id,function(err,bus){
			if(bus==null)res.json(null);
			else{
				for(i=0;i<bus.buses.length;i++){
					var positionBuffer=new Array(0);
					for(j=0;j<bus.buses[i].contents.length;j++){
						position={
							bus:bus.buses[i].imei,
							lat:bus.buses[i].contents[j].lat,
							lon:bus.buses[i].contents[j].lon, 
							fecha: bus.buses[i].contents[j].date,
							hora: bus.buses[i].contents[j].time
							};
						position=JSON.stringify(position);
						position=JSON.parse(position);
						//if(j>0)positionBuffer+=",";
						//positionBuffer+=position;
						positionBuffer.push(position);
					}
					//console.log(positionBuffer);
					//const buf = new Buffer(positionBuffer);
					positionBuffer=JSON.stringify(positionBuffer);
					positionBuffer=JSON.parse(positionBuffer);
					busesBuffer.push(positionBuffer);
				}
				busesBuffer=JSON.stringify(busesBuffer);
				busesBuffer=JSON.parse(busesBuffer);
				res.json(busesBuffer);
			};
		});
			
	})
	
	app.get('/position/company/:id/last',function(req,res){
		var positionBuffer=new Array(0);
		var id=req.params.id;
		PassArte.Business.findById(id,function(err,bus){
			if(bus==null)res.json(null);
			else{
				for(i=0;i<bus.buses.length;i++){
					console.log(i);
					position={
						bus:bus.buses[i].imei,
						lat:bus.buses[i].Last_one.lat,
						lon:bus.buses[i].Last_one.lon, 
						fecha: bus.buses[i].Last_one.date,
						hora: bus.buses[i].Last_one.time
						};
					position=JSON.stringify(position);
					position=JSON.parse(position);
					//if(j>0)positionBuffer+=",";
					//positionBuffer+=position;
					positionBuffer.push(position);
				}
				//console.log(positionBuffer);
				//const buf = new Buffer(positionBuffer);
				positionBuffer=JSON.stringify(positionBuffer);
				positionBuffer=JSON.parse(positionBuffer);
				res.json(positionBuffer);
			};
		});
			
	});
	
	app.get('/position/bus/:imei/last',function(req,res){
		var positionBuffer=new Array(0);
		var imei=req.params.imei;
		PassArte.Business.findOne({'buses.imei':imei},function(err,bus){
			if(bus==null)res.json(null);
			else{
				for(i=0;i<bus.buses.length;i++){
					if(bus.buses[i].imei == imei){
							position={
								lat:bus.buses[i].Last_one.lat,
								lon:bus.buses[i].Last_one.lon, 
								fecha: bus.buses[i].Last_one.date,
								hora: bus.buses[i].Last_one.time
							}
							position=JSON.stringify(position);
							position=JSON.parse(position);
							res.json(position);
						}
					}
				}
		});	
	});
	
	app.get('/position/archiv/:imei/',function(req,res){
		var positionBuffer=new Array(0);
		var imei=req.params.imei;
		PassArte.Business.findOne({'buses.imei':imei},function(err,bus){
			if(bus==null)res.json(null);
			else{
				for(i=0;i<bus.buses.length;i++){
					if(bus.buses[i].imei == imei){
							position={
								lat:bus.buses[i],
								cantidad:bus.buses[i].contents.length
							}
							position=JSON.stringify(position);
							position=JSON.parse(position);
							res.json(position);
						}
					}
				}
		});	
	});
	
/********************************************************************************************************************************
 * pruebas del imei
 ****************************************************************************************************************************/
app.get('/imei/disponibles',function(req,res){
		imei.tracker.findOne({'used':false},function(err,track){
			var Imei=track.imei;
			Imei=JSON.stringify(Imei);
			Imei=JSON.parse(Imei);
			res.json(Imei);
		});
	});

app.get('/imei/company/:name',function(req,res){
		nombre=req.params.name;
		imei.tracker.find({'empresa':nombre},function(err,empresa){
			var EmpresaBuff=new Array(0);
			for (i=0; i<empresa.length;i++){
				 var Empresa ={bus:empresa[i].bus,imei:empresa[i].imei}
				Empresa=JSON.stringify(Empresa);
				Empresa=JSON.parse(Empresa);
				EmpresaBuff.push(Empresa);
			}
			EmpresaBuff=JSON.stringify(EmpresaBuff);
			EmpresaBuff=JSON.parse(EmpresaBuff);
			res.json(EmpresaBuff);
		});
	});
app.get('/imei/working',function(req,res){
		imei.tracker.find({'used':true, 'working':true},function(err,track){
			var EmpresaBuff=new Array(0);
			for (i=0; i<track.length;i++){
				 var Empresa ={bus:track[i].bus,imei:track[i].imei,empresa:track[i].empresa}
				Empresa=JSON.stringify(Empresa);
				Empresa=JSON.parse(Empresa);
				EmpresaBuff.push(Empresa);
			}
			EmpresaBuff=JSON.stringify(EmpresaBuff);
			EmpresaBuff=JSON.parse(EmpresaBuff);
			res.json(EmpresaBuff);
		});
	});

app.get('/imei/all',function(req,res){
		imei.tracker.find({},function(err,track){
			var EmpresaBuff=new Array(0);
			for (i=0; i<track.length;i++){
				Empresa=JSON.stringify(track[i]);
				Empresa=JSON.parse(Empresa);
				EmpresaBuff.push(Empresa);
			}
			EmpresaBuff=JSON.stringify(EmpresaBuff);
			EmpresaBuff=JSON.parse(EmpresaBuff);
			res.json(EmpresaBuff);
		});
	});
	
/**********************************************************************************************************************
 * 
 *********************************************************************************************************************/
app.get('/datos/empresa/:id',function(req,res){
		PassArte.Business.findById(req.params.id,function(err,empresa){
			if (empresa==null) res.json(null);
			else {
				var empresaBuffer=new Array(0);
				Empresa={
					id: empresa._id,
					nombre:empresa.Company, 
					cedula:empresa.cedula, 
					phone:empresa.phone,
					email:empresa.email,
					fechai:empresa.fechai,
					Cant_buses:empresa.buses.length
					}
				Empresa=JSON.stringify(Empresa);
				Empresa=JSON.parse(Empresa);
				empresaBuffer.push(Empresa);
				var busesBuffer=new Array(0);
				for(i=0;i<empresa.buses.length;i++){
					bus={
						nombre:empresa.buses[i].nombre,
						unidad:empresa.buses[i].unidad,
						imei:empresa.buses[i].imei
					}
					console.log(bus);
					bus=JSON.stringify(bus);
					bus=JSON.parse(bus);
					busesBuffer.push(bus);
				}
				busesBuffer=JSON.stringify(busesBuffer);
				busesBuffer=JSON.parse(busesBuffer);
				empresaBuffer.push(busesBuffer);
				empresaBuffer=JSON.stringify(empresaBuffer);
				empresaBuffer=JSON.parse(empresaBuffer);
				res.json(empresaBuffer);
			}
		});
	});

app.get('/datos/empresa/',function(req,res){
		PassArte.Business.find({},function(err,empresa){
			if (empresa==null) res.json(null);
			else {
				var EmpresaBuff=new Array(0);
				for(j=0;j<empresa.length;j++){
					var empresaBuffer=new Array(0);
					Empresa={
						id: empresa[j]._id,
						nombre:empresa[j].Company, 
						cedula:empresa[j].cedula, 
						phone:empresa[j].phone,
						email:empresa[j].email,
						fechai:empresa[j].fechai,
						}
					Empresa=JSON.stringify(Empresa);
					Empresa=JSON.parse(Empresa);
					empresaBuffer.push(Empresa);
					var busesBuffer=new Array(0);
					for(i=0;i<empresa[j].buses.length;i++){
						bus={
							nombre:empresa[j].buses[i].nombre,
							unidad:empresa[j].buses[i].unidad,
							imei:empresa[j].buses[i].imei
						}
						console.log(bus);
						bus=JSON.stringify(bus);
						bus=JSON.parse(bus);
						busesBuffer.push(bus);
					}
					busesBuffer=JSON.stringify(busesBuffer);
					busesBuffer=JSON.parse(busesBuffer);
					empresaBuffer.push(busesBuffer);
					empresaBuffer=JSON.stringify(empresaBuffer);
					empresaBuffer=JSON.parse(empresaBuffer);
					EmpresaBuff.push(empresaBuffer);
				}
				EmpresaBuff=JSON.stringify(EmpresaBuff);
				EmpresaBuff=JSON.parse(EmpresaBuff);
				res.json(EmpresaBuff);
			}
		});
	});
	
	app.get('/datos/company/:id',function(req,res){
		PassArte.Business.findById(req.params.id,function(err,empresa){
			if (empresa==null) res.json(null);
			else {
				Empresa={
					id: empresa._id,
					nombre:empresa.Company, 
					cedula:empresa.cedula, 
					phone:empresa.phone,
					email:empresa.email,
					fechai:empresa.fechai,
					}
				Empresa=JSON.stringify(Empresa);
				Empresa=JSON.parse(Empresa);
				res.json(Empresa);
			}
		});
	});
	//devuelve todas las empresa
	app.get('/datos/company/',function(req,res){
		PassArte.Business.find({},function(err,empresa){
			if (empresa==null) res.json(null);
			else {
				var EmpresaBuff=new Array(0);
				for(j=0;j<empresa.length;j++){
					Empresa={
						id: empresa[j]._id,
						nombre:empresa[j].Company, 
						cedula:empresa[j].cedula, 
						phone:empresa[j].phone,
						email:empresa[j].email,
						fechai:empresa[j].fechai,
						Cant_buses:empresa[j].buses.length
						}
					Empresa=JSON.stringify(Empresa);
					Empresa=JSON.parse(Empresa);
					EmpresaBuff.push(Empresa);
				}
				EmpresaBuff=JSON.stringify(EmpresaBuff);
				EmpresaBuff=JSON.parse(EmpresaBuff);
				res.json(EmpresaBuff);
			}
		});
	});

app.listen(3001, function () {
    console.log('Example app listening on port 3001!');
});



