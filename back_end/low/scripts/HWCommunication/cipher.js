 /*
    SunTech jailbreak
    PassArte Lab

    By: ln666
    About: This script lets you get the different parts from the tracker data line, 
    associating the requested feature with the index position in the data line.
    This script uses the "SuntecTracks" collection which contains the feature's name
    and the index, depending on the tracker's model.

    Usage: 
        Params: features: the features to look for
                line: the line read from tracker
                callback: function which contains result
        Example:
                cipher(model,imei, "lat", function(result){...});
                Where "lat" is the feature
                

    Last Update: Ene/22/2017
    NOTE: It's tested
*/

var cmd;

module.exports = {
    cipher : function(model,id,version,command, callback)
    {
        if(model ==  "ST300")
        {
            switch(command.cmd)
            {
                // Preset
                case "Preset":
                    cmd = "ST300CMD;";
                    cmd += id;
                    cmd += ";";
                    cmd += version;
                    cmd += ";Preset\n\r";
                    break;
                // Writing outputs
                case "WriteOutput":
                    cmd = "ST300CMD;";
                    cmd += id;
                    cmd += ";";
                    cmd += version;
                    if(command.value == 1)
                        cmd += ";ENABLE";
                    else
                        cmd += ";DISABLE";
                    cmd += command.pin;
                    cmd += "\n\r";
                    break;
                // Request status
                case "GetStatus":
                    cmd = "ST300CMD;";
                    cmd += id;
                    cmd += ";";
                    cmd += version;
                    cmd += ";StatusReq\n\r";
                    break;
                // Get DID
                case "GetDID":
                    cmd = "ST300HGD;";
                    cmd += id;
                    cmd += ";";
                    cmd += version;
                    cmd += ";";
                    cmd += command.pos;
                    cmd += "\n\r";
                    break;
                // Remove DID
                case "RemoveDID":
                    cmd = "ST300HRD;";
                    cmd += id;
                    cmd += ";";
                    cmd += version;
                    cmd += ";";
                    cmd += command.pos;
                    cmd += "\n\r";
                    break;
                // Register DID 
                case "RegisterDID":
                    cmd = "ST300HAD;";
                    cmd += id;
                    cmd += ";";
                    cmd += version;
                    cmd += ";";
                    cmd += command.pos;
                    cmd += ";";
                    cmd += command.value;
                    cmd += "\n\r";
                    break;
                default:
                    cmd = null;
                    break;
            }
            callback(cmd);

        }
        else
            // Null if there is no command according to characteristics
            callback(null);
    }
};

/*
    COMMAND LIST:

    1. Preset
    2. WriteOutput, pin, value
    3. GetStatus
    4. SetDID, pos, value
    5. GetDID, pos
    6. RemoveDID, pos

    command = cmd, params...
*/