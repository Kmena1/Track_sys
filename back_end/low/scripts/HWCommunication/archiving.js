/*
    SunTech jailbreak
    PassArte Lab

    By: ln666
    About: This script will push the relevant data to the Database according to the settings put in
    each bus. It's important to say that redundant data will be ignored.

    Usage: 
        Please, give more chance. Meanwhile, feed me with sushi :D
                
    Requirements:
        
    Data:
        Input: content (obj to insert), lastReg (last obj inserted), settings (a list of rules)
    Last Update: Jan/4/2018

    NOTE: In development

    archiving_settings:
        [{
            rule: {type: String, required: true},
            property: {type: String, required: true},
            tolerance: {type: Number, required: true},
            module: Number,
            master: {type: Boolean, default: false},
            reference: {type: Number, default: null},
            operator: {type: String, default: "ge"}
        }]
*/

var da= require('./../../../Top_end/models/Alertas_model.js');
var dv= require('./../../../Top_end/models/Vehiculos_models.js');
var dh= require('./../../../Top_end/models/history.js')
var dp= require('./../../../Top_end/models/Posiciones_models.js')
var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;
var moment=require('moment');

module.exports = {
    postable: function (content, lastReg, settings, id)
    {
        console.log('entrada al arch',lastReg,content);
        
        // Flag

        var Arch_save =true;
        var insert = false;
        var insert_master = true;
	
        if(settings == undefined || settings == null || settings.length == 0 || lastReg==null)

        {
            Arch_save =true;
            insert = true;
            insert_master = true;
        }

        else
        // Enabling the flag - Basically is an OR Logic
            for(var i = 0; i < settings.length; i++)
            {
                console.log('rule',i+1,settings[i].rule);
                //si no hay rules aparte de las alertas
                if(settings[i].rule != 'alerta') Arch_save=false
                if(settings[i].rule=='Almacenar'){
                    //almacena el los datos recividos en otra base de datos
                    var newValue = index(content,'date');
                    var lastValue = index(lastReg,'date');
                    console.log('almacenar',Number(newValue)==Number(lastValue))
                    if(Number(newValue)!=Number(lastValue)) SaveMake(id,newValue)
                }
                else{
                    if(settings[i].rule=='time'){
                        /*var lastdate= index(lastReg,"date");
                        var lasttime= index(lastReg,"time");
                        var newdate= index(content,"date");
                        var newtime= index(content,"time");
                        //creacion de los tiempos en el formato YYYYMMDD HH:mm:ss
                        lastT=String(lastdate)+' '+String(lasttime);
                        newT=String(newdate)+' '+String(newtime);
                        console.log('tiempos: ',lastT,newT);
                        //creacion de las fechas 1(last) y 2(new)
                        var fecha1 = moment(lastT, "YYYYMMDD HH:ss:mm");
                        var fecha2 = moment(newT, "YYYYMMDD HH:ss:mm");
                        //se adquiere la diferencia que pide el modulo
                        /**
                        Y=years
                        M=meses
                        D=dias
                        h=horas
                        m=minutos
                        s=segundos
                        var diff = fecha2.diff(fecha1, settings[i].property);
                        console.log(diff)
                        //comparacion
                        var result=compare(settings[i].operator,settings[i].tolerance,diff);
                        if(result)
                            insert = true;
                        if(settings[i].master && !result)
                            insert_master = false;
                        */
                    }
                    else{
                        if(settings[i].rule=='radio'){
                            //latidut y longitud del viejo
                            lat1=Number(index(lastReg,'lat'));
                            lon1=Number(index(lastReg,'lon'));
                            //latidut y longitud del nuevo
                            lat2=Number(index(content,'lat'));
                            lon2=Number(index(content,'lon'));
                            console.log(lat1,lon1,lat2,lon2);
                            //radio[m]= r*pi*(delta^2(lat)+delta^2(long))^1/2 /180
                            radio=110950.58*Math.sqrt((lat1-lat2)*(lat1-lat2)+(lon1-lon2)*(lon1-lon2));
                            console.log('radio',radio);
                            //crea la comparacion
                            var result=compare(settings[i].operator,settings[i].tolerance,radio);
                            if(result)
                                insert = true;
                            if(settings[i].master && !result)
                                insert_master = false;

                        }
                        else{
                            //caso especial donde se define una alerta no un filtro
                            if(settings[i].rule=='alerta'){
                                //propietys:
                                /**
                                * geocerca: se define un punto en la referencia, siendo ref1 la latitud y ref2 la longitud
                                con esto se calula el radio y se define luego la alerta dependiendo del operador y la tolerancia siendo la geocerca
                                *Static: si los ultimos puntos de latitud y longitud no se diferencian mas de la referencia en un tiempo mayor
                                que la tolerancia
                                */
                                console.log('Alerta sets:',settings[i])
                                switch(settings[i].property){
                                    case "geocerca":
                                        var lat1 = index(content,'lat');
                                        var lon1 = index(content,'lon');
                                        var lat2 = settings[i].reference.ref1;
                                        var lon2 = settings[i].reference.ref2;
                                        //radio[m]= r*pi*(delta^2(lat)+delta^2(long))^1/2 /180
                                        radio=110950.58*Math.sqrt((lat1-lat2)*(lat1-lat2)+(lon1-lon2)*(lon1-lon2));
                                        console.log('radio:',radio)
                                        var result=compare(settings[i].operator,settings[i].tolerance,radio);
                                        if(result){ 
                                            var newdate= index(content,"date");
                                            var newtime= index(content,"time");
                                            //creacion de los tiempos en el formato YYYYMMDD HH:mm:ss
                                            newT=String(newdate)+' '+String(newtime);
                                            var fecha = moment(newT, "YYYYMMDD HH:mm:ss");
                                            MakeAlert(id,{
                                                tipo: settings[i].property,
                                                lat:  lat1,
                                                lon:  lon1,
                                                time:  fecha
                                            })
                                        }
                                    break;
                                    case "Static":
                                        //obtiene lo tiemposvar lastdate= index(lastReg,"date");
                                        var lasttime= index(lastReg,"time");
                                        var newdate= index(content,"date");
                                        var newtime= index(content,"time");
                                        //creacion de los tiempos en el formato YYYYMMDD HH:mm:ss
                                        lastT=String(lastdate)+' '+String(lasttime);
                                        newT=String(newdate)+' '+String(newtime);
                                        console.log('tiempos: ',lastT,newT);
                                        //creacion de las fechas 1(last) y 2(new)
                                        var fecha1 = moment(lastT, "YYYYMMDD HH:mm:ss");
                                        var fecha2 = moment(newT, "YYYYMMDD HH:mm:ss");
                                        //obtiene las lat y lon
                                        var lat1 = index(content,'lat');
                                        var lat2 = index(lastReg,'lat');
                                        var lon1 = index(content,'lon');
                                        var lon2 = index(lastReg,'lon');
                                        //si se cumple la condicion se solicita la alerta
                                        if(lat1-lat2>settings[i].reference.ref1 && lon1-lon2>settings[i].reference.ref && fecha2.diff(fecha1,'m')>settings[i].tolerance){
                                            MakeAlert(id,{
                                                tipo: settings[i].property,
                                                lat:  lat1,
                                                lon:  lon1,
                                                time:  fecha2
                                            })
                                        }
                                    break;
                                }
                            }
                            else{
                                // Get both components
                                var newValue = index(content,settings[i].property);
                                var lastValue = index(lastReg,settings[i].property);
                                var referenceValue = settings[i].reference.ref1;
                                if(isNaN(newValue) || isNaN(lastValue))
                                {
                                    console.log("A component gotten by Archiving is not a number " + settings[i].property,newValue,lastValue);
                                    if(lastValue==null) {
                                        insert = true;
                                        if(settings[i].master && !result) insert_master = false;
                                    }
                                }
                                else
                                {
                                    newValue = Number(newValue);
                                    lastValue = Number(lastValue);
                                    console.log(newValue,lastValue)
                                    // Switching the case
                                    switch(settings[i].rule)
                                    {
                                        case "moduled_tolerance":
                                            // Compute the module
                                            newValue = newValue % (Math.pow(10,settings[i].module));
                                            lastValue = lastValue % (Math.pow(10,settings[i].module));
                                            console.log("N:"+newValue + " L:"+lastValue);
                                            // Compute the error
                                            if(referenceValue == null)
                                                var error = Math.abs(newValue-lastValue)/lastValue;
                                            else
                                                var error = Math.abs(newValue-referenceValue)/referenceValue;
                                            console.log("E:" + error*100);
                                            // Verify if should be posted
                                            var result=compare(settings[i].operator)//,settings[i].tolerance,error);
                                            if(result)
                                                insert = true;
                                            if(settings[i].master && !result)
                                                insert_master = false;
                                            break;
                                        case "simple_tolerance":
                                            // Compute the error
                                            if(referenceValue == null)
                                                if(lastValue!=0)var error = Math.abs(newValue-lastValue)/lastValue;
                                                else error= Math.abs(newValue);
                                            else
                                                var error = Math.abs(newValue-referenceValue)/referenceValue;
                                            console.log("E:" + error*100 , settings[i].operator,settings[i].tolerance,error);
                                            // Verify if should be posted
                                            var result=compare(settings[i].operator,settings[i].tolerance,error);
                                            if(result)
                                                insert = true;
                                            if(settings[i].master && !result)
                                                insert_master = false;
                                        case "fixed_tolerance":
                                            // Compute the error
                                            if(referenceValue == null)
                                                var error = Math.abs(newValue-lastValue);
                                            else
                                                var error = Math.abs(newValue-referenceValue);
                                            // Verify if should be posted
                                            var result=compare(settings[i].operator,settings[i].tolerance,error);
                                            if(result)
                                                insert = true;
                                            if(settings[i].master && !result)
                                                insert_master = false;
                                            break;
                                        
                                        default:
                                            insert = true;
                                            break;
                                    }  
                                }
                            }
                        }
                    }
                }
            }
            // Deciding whether posting or not.
        //console.log(insert);
        return (Arch_save || insert) && insert_master;
    }
}

function MakeAlert(id,alert){
    //busca el dispositivo al cual isertarle la alerta
    dv.Vehiculos.findById(ObjectId(id),function(err,device){
        if(err) console.log(err);
        else{
            //creacion de la nueva alerta apuntando a la anterior
            var alerta =new da.Alert(alert);
            alerta['before']=device.Alertas_last;
            //guarda la alerta
            alerta.save(function(err){
                if(err) console.log(err);
                else{      
                    //si no hay alertas se almacena de automaticamente
                    if(device.Alertas==null){
                        device.Alertas=alerta._id;
                        device.Alertas_last=alerta._id;
                        device.save(function(err){
                            if(err) console.log(err);
                            else{
                                
                            }
                        })
                    }
                    //sino se busca la ultima alerta para compararla por tiempo para evitar redundancia
                    else{
                        //busqueda de la ultima alerta
                        da.Alert.findById(ObjectId(device.Alertas_last),function(err,lastAlert){
                            if(err) console.log(err);
                            else{
                                console.log(lastAlert,alerta);
                                //si el tiempo entre las alertas es mayor a 10 minutos
                                if(moment(new Date(lastAlert.time)).diff(moment(new Date(alerta.time)), 'm')>10){
                                    //guarda la alerta y actualiza la ultima alerta del dispositivo
                                    lastAlert.next=alerta._id
                                    device.Alertas_last=alert._id;
                                    lastAlert.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            device.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    
                                                }
                                            })
                                        }
                                    })
                                }
                                //sino remueve la alerta
                                else{
                                    alerta.remove(function(err){
                                        if(err) console.log(err);
                                        else console.log('alerta removida')
                                    })
                                }
                            }
                        })
                    }
                }
            })
        }
    })
}

function lookforHistories(id,last,date,callback){
    dp.Position.findById(ObjectId(id),function(err,pos){
        if(err) console.log(err);
        else{
            //si las fechas son diferentes se retorna los extremos
            if(date!=pos.date){
                console.log('since '+pos._id+' to '+last);
                callback(pos._id,last)
            }
            else{
                //si el anterior es null se crea el ultimo historial y se devuelve
                if(pos.before==null){
                    console.log('since '+pos._id+' to '+last);
                    callback(pos._id,last)
                }
                //sino busca mas atras
                else histories(pos.before,last,date,callback)
            }
        }
    })
}

function SaveMake(id,fecha){
    dv.Vehiculos.findById(ObjectId(id),function(err,device){
        if(err) console.log(err);
        else{
            lookforHistories(device.posiciones_last,device.posiciones_last,fecha,function(inicio,final){
                var history= new dh.history({
                    fechas: fecha ,
                    inicio: inicio,
                    final: final,
                    before: device.historycs,
                })
                history.save(function(err){
                    if(err) console.log(err);
                    else{
                        device.historycs=history._id;
                        device.save(function(err){
                            if(err) console.log(err);
                            else{
                                
                            }
                        })
                    }
                })
            })
        }
    })
}

function index(obj,is, value) {
    if (typeof is == 'string')
        return index(obj,is.split('.'), value);
    else if (is.length==1 && value!==undefined)
        return obj[is[0]] = value;
    else if (is.length==0)
        return obj;
    else
        return index(obj[is[0]],is.slice(1), value);
}

function compare(operator,tolerance,value){
    switch(operator)
    {
        case 'ge':
            result = value >= tolerance;
            break;
        case 'le':
            result = value <= tolerance;
            break;
        case 'l':
            result = value < tolerance;
            break;
        case 'g':
            result = value >= tolerance;
            break;
        case 'e':
            result = value == tolerance;
            break;
    }
    //console.log(result)
    return result;
}

/*
    Rules:
        No rule: no element in the settings object
        Rule: is present in the settings object
            1. moduled_tolerance
                Basically, a large number has to be shorted and a tolerance is applied
                For instace:
                    odo: 123456789
                    -> Mod: 3 (the last three numbers)
                    -> Tolerance: 5 (five percent)
                    So after applying module:
                    odo: 789 because, odo % 10^3
                    And then, with the tolerance:
                    abs(odo1 - odo2)/odo1 >? 5*0.01
            2. simple_tolerance
                In this case, you need only the past and the new value:
                    abs(value2-value1)/value1 >? tolerance*0.01
                    Where tolerance is given by settings and value2 and value1 are given by the
                    new and the last value.
            3. fixed_difference
                The last case is simplier than the other ones. A difference is given and you only have
                to compare the difference between the values with the difference given by settings
                For example:
                    value2: new one
                    value1: last one
                    Then,
                        value2-value1 >? difference
            4.  

    Settings object:
        {
            rule: "string" (shown before)
            property: "string" (feature which is affected)
            tolerance: "number" in percentage | fixed difference
            module: the number of digits that are taken into account (if applicable)
        }
*/
