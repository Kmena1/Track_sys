//PID start 14 cnt 13
//respPID start 6 cnt 5
// Libraries
//var mongoose = require('mongoose');
var mongoose = require('mongoose');
var OBD = require('./../../../top/models/OBD_model.js');
var request = require('request');
var hexToBinary = require('hex-to-binary');

module.exports = { 
	Deco: function (line,start,cnt,imei){
		var Buffdatos = new Array(0);
		//console.log('entro');
		//console.log('linea: ',line, imei);
		OBD.OBD_models.findOne({'tracker':Number(imei)},function(err,track){
			if(track != null){
				console.log('resumen',track.resumen.cod.length);
				console.log(line.length, line[line.length-1])
				if(line[line.length-1]!="Not Ready"){
					//decodificacion de los PIDs
					for (var i=start; i < line.length; i++) {
						console.log('linea:',line[i]);
						 var respuesta = String(line[i]).split('|');
						if(respuesta[1] !='n/a'){
								/*******************
								* preparacion de repuestas que devuelven mas de un valor
								*/
									if(respuesta[0]=='14' || respuesta[0]=='15' || respuesta[0]=='16' || respuesta[0]=='17' || respuesta[0]=='18'
									|| respuesta[0]=='19' || respuesta[0]=='1A' || respuesta[0]=='1B'|| respuesta[0]=='24' || respuesta[0]=='25' 
									|| respuesta[0]=='26' || respuesta[0]=='27' || respuesta[0]=='28'|| respuesta[0]=='29' || respuesta[0]=='2A' 
									|| respuesta[0]=='2B' || respuesta[0]=='34' || respuesta[0]=='35' || respuesta[0]=='36' || respuesta[0]=='37'
									|| respuesta[0]=='38'|| respuesta[0]=='39' || respuesta[0]=='3A' || respuesta[0]=='3B'){
										respuesta[1]=String(String(respuesta[1])+','+String(line[i+1]));
										i=i+1;
									}
									if(respuesta[0]=='4f' ){
										respuesta[1]=String(String(respuesta[1])+','+String(linea[i+1])+','+String(linea[i+2])+','+String(linea[i+3]));
										i=i+3;
									}
								/**************************************************************************************/
								/*	
								for (var j=0; j < track.resumen.cod.length; j++) {
										if(track.resumen.cod[j].nombre==respuesta[0]){
											DECODIFICADOR(respuesta[0],respuesta[1],track.resumen.cod[j].valor,track.datos.length,imei)
											break;
										}
										else{
											if(j == track.resumen.cod.length-1){
												DECODIFICADOR(respuesta[0],respuesta[1],null,track.datos.length,imei)
											}
										}
									};*/
								//cods con mas de 1 respuestas
								//gases
								if(respuesta.length >1){
									var cod = respuesta[0];
									var valor = respuesta[1];
									 if(respuesta[0]=='14' || respuesta[0]=='15' || respuesta[0]=='16' || respuesta[0]=='17' || respuesta[0]=='18'
									|| respuesta[0]=='19' || respuesta[0]=='1A' || respuesta[0]=='1B'|| respuesta[0]=='24' || respuesta[0]=='25' 
									|| respuesta[0]=='26' || respuesta[0]=='27' || respuesta[0]=='28'|| respuesta[0]=='29' || respuesta[0]=='2A' 
									|| respuesta[0]=='2B' || respuesta[0]=='34' || respuesta[0]=='35' || respuesta[0]=='36' || respuesta[0]=='37'
									|| respuesta[0]=='38'|| respuesta[0]=='39' || respuesta[0]=='3A' || respuesta[0]=='3B'){
										console.log(respuesta[1]);
									  	var datos = String(respuesta[1]).split(',');
										ajuste = datos[1];
										for (var j=0; j < track.resumen.cod.length; j++) {
										  if( track.resumen.cod[j].nombre == cod){
										  	 if(track.resumen.cod[j].valor!=ajuste){
													Buffdatos.push({'valor':ajuste,'cod':cod});
												  	track.resumen.cod[j].valor = ajuste;
											  	}
											 break;
											}
											else{ 
												if(j==track.resumen.cod.length-1){
													Buffdatos.push({'valor':ajuste,'cod':cod});
												  	track.resumen.cod.push({'valor':ajuste,'nombre':cod});
												}
											}
										}
									}
									//datos varios
									if (respuesta[0]=='4f') {
										var valor = respuesta[1].split(',');
										for (var j=0; j < track.resumen.cod.length; j++) {
										  if( track.resumen.cod[j].nombre == cod){
										  	var anterior = track.resumen.cod[j].valor;
										  	var anterior =anterior.split(',');
										  	if (valor[0] != anterior[0] || valor[1] != anterior[1] || valor[2] != anterior[2] || valor[3] != anterior[3]) {
										  		Buffdatos.push({'valor':String(valor[0]+','+valor[1]+','+valor[2]+','+valor[3]),'cod':cod});
										  		track.resumen.cod[j].valor = String(valor[0]+','+valor[1]+','+valor[2]+','+valor[3]);
										  	}
										  }
										else{
										  	if(j == track.resumen.cod.length -1){
												Buffdatos.push({'valor':String(valor[0]+','+valor[1]+','+valor[2]+','+valor[3]),'cod':cod});
										  		track.resumen.cod.push({'valor':String(valor[0]+','+valor[1]+','+valor[2]+','+valor[3]),'nombre':cod});
											}
										}	  
										};
										if(track.resumen.cod.length== 0){
											Buffdatos.push({'valor':String(valor[0]+','+valor[1]+','+valor[2]+','+valor[3]),'cod':cod});
										  	track.resumen.cod.push({'valor':String(valor[0]+','+valor[1]+','+valor[2]+','+valor[3]),'nombre':cod});
										}
									};
									//cods con decodificacion
									//cod MIL
									if(respuesta[0]=='01'){
										var databin = hexToBinary(respuesta[1]);
										var info = '';
										if (databin[0]=='1'){info += 'luz encendida/'};
										if (Number(databin.substring(1,7))>0){info=info+'cantidad de alertas:'+databin.substring(1,7)+'/'}
										if (Number(databin.substring(13,15))>0){
											info+='pruebas disponibles: ';
											if(databin[13]=='1'){info+='componentes;'};
											if(databin[14]=='1'){info+='sistema;'};
											if(databin[15]=='1'){info+='fallos;'};
										}
										for (var j=0; j < track.resumen.cod.length; j++) {
										  if( track.resumen.cod[j].nombre == cod){
										  		if(track.resumen.cod[j].valor!=info){
											  		Buffdatos.push({'valor':info,'cod':cod});
											  		track.resumen.cod[j].valor = info;
											  	};
											  	break;
										   }
										  else{
										  	if(i == track.resumen.cod.length -1){
										  		Buffdatos.push({'valor':info,'cod':cod});
										  		track.resumen.cod.push({'valor':info,'nombre':cod});
										  	}
										  }
										};
										if(track.resumen.cod.length== 0){
											Buffdatos.push({'valor':info,'cod':cod});
										  	track.resumen.cod.push({'valor':info,'nombre':cod});
										}
										
									}
									//estado del motor
									if(respuesta[0]=='03'){
										switch(Number(valor)){
											case 1:
												estado='bucle abierto por temperatura';
												break;
											case 2:
												estado='bucle cerrado';
												break;
											case 4:
												estado='bucle abierto carga del motor';
												break;
											case 8:
												estado='bucle abierto por falla';
												break;
											case 16:
												estado='bucle cerrado por sensor de oxigeno';
												break;
											default:
												estado='cod no identificado';
												break;
										}
										for (var j=0; j < track.resumen.cod.length; j++) {
										  	if( track.resumen.cod[j].nombre == cod){
												if(track.resumen.cod[j].valor!=estado){
												  	Buffdatos.push({'valor':estado,'cod':cod});
												  	track.resumen.cod[j].valor = estado;
												};
												break;
											}
											else{
												if(j == track.resumen.cod.length -1){
											  		Buffdatos.push({'valor':estado,'cod':cod});
											  		track.resumen.cod.push({'valor':estado,'nombre':cod});
											  	}
											}
										};
										if(track.resumen.cod.length == 0){
											Buffdatos.push({'valor':info,'cod':cod});
										  	track.resumen.cod.push({'valor':info,'nombre':cod});
										}
									}
									//tipo de gasolina
									if(respuesta[0]=='51'){
										switch(respuesta[1]){
											case 1:
												var tipo = 'gasolina';
												break;
											case 2:
												var tipo = 'methanol';
												break;
											case 3:
												var tipo = 'ethanol';
												break;
											case 4:
												var tipo = 'disel';
												break;
											case 5:
												var tipo = 'LPG';
												break;
											case 6:
												var tipo = 'CNG';
												break;
											case 7:
												var tipo = 'Propanol';
												break;
											case 8:
												var tipo = 'Electrico';
												break;
											case 9:
												var tipo = 'Bifuel running gasolina';
												break;
											case 10:
												var tipo = 'Bifuel running methanol';
												break;
											case 11:
												var tipo = 'Bifuel running ethanol';
												break;
											case 12:
												var tipo = 'Bifuel running LPG';
												break;
											case 13:
												var tipo = 'Bifuel running CNG';
												break;
											case 14:
												var tipo = 'Bifuel running Propanol';
												break;
											case 15:
												var tipo = 'Bifuel running Electrico';
												break;
											case 16:
												var tipo = 'Bifuel running Electrico y combustion interna';
												break;
											case 17:
												var tipo = 'hidrido de gasolina';
												break;
											case 18:
												var tipo = 'hidrido methanol';
												break;
											case 19:
												var tipo = 'hidrido ethanol';
												break;
											case 20:
												var tipo = 'hidrido electrico';
												break;
											case 21:
												var tipo = 'hidrido electrico y combustion interna';
												break;
											case 22:
												var tipo = 'hidrido regenerativo';
												break;
											case 23:
												var tipo = 'Bifuel running disel';
												break;		
										}
										console.log(tipo);
										for (var j=0; j < track.resumen.cod.length; j++) {
										  if( track.resumen.cod[j].nombre == cod){
										  	if(track.resumen.cod[j].valor != tipo){
											  	Buffdatos.push({'valor':tipo,'cod':cod});
											  	track.resumen.cod[j].valor = tipo;
											};
											break;
										  }
										  else{
										  	if(j == track.resumen.cod.length -1){
										  		Buffdatos.push({'valor':tipo,'cod':cod});
										  		track.resumen.cod.push({'valor':tipo,'nombre':cod});
										  	}
										  }
										};
										if(track.resumen.cod.length == 0){
											Buffdatos.push({'valor':tipo,'cod':cod});
										  	track.resumen.cod.push({'valor':tipo,'nombre':cod});
										}
									}
									//cods con solo guardado
									if(respuesta[0]!='14' && respuesta[0]!='15' && respuesta[0]!='16' && respuesta[0]!='17' && respuesta[0]!='18'
									&& respuesta[0]!='19' && respuesta[0]!='1A' && respuesta[0]!='1B'&& respuesta[0]!='24' && respuesta[0]!='25' 
									&& respuesta[0]!='26' && respuesta[0]!='27' && respuesta[0]!='28'&& respuesta[0]!='29' && respuesta[0]!='2A' 
									&& respuesta[0]!='2B' && respuesta[0]!='34' && respuesta[0]!='35' && respuesta[0]!='36' && respuesta[0]!='37'
									&& respuesta[0]!='38'&& respuesta[0]!='39' && respuesta[0]!='3A' && respuesta[0]!='3B' && respuesta[0]!='4f'
									&& respuesta[0]!='01' &&respuesta[0]!='03' && respuesta[0]!='51'){
										for (var j=0; j < track.resumen.cod.length; j++) {
										  if( track.resumen.cod[j].nombre == cod){
										  	 if(track.resumen.cod[j].valor!=valor){
										  	 		console.log('repetido');
													Buffdatos.push({'valor':valor,'cod':cod});
												  	track.resumen.cod[j].valor = valor;
											  	}
											  break;
											}
											else{ 
												if(j==track.resumen.cod.length-1){
													Buffdatos.push({'valor':valor,'cod':cod});
												  	track.resumen.cod.push({'valor':valor,'nombre':cod});
												}
											}
										}
										if(track.resumen.cod.length == 0){
											Buffdatos.push({'valor':valor,'cod':cod});
										  	track.resumen.cod.push({'valor':valor,'nombre':cod});
										}
									} 
								}
							}
								
						};
				}
				console.log(Buffdatos);
				track.datos.push({
					fecha:new Date(),
					Numero:track.datos.length,
					datos:Buffdatos
				});
				track.save(function(err){
					if(err) console.log(err)
					else{
					}
				})
			}
			else{
				Obd= new OBD.OBD_models({'tracker':Number(imei)});
				Obd.save(function(err){
					if(err) console.log(err);
				});
				console.log('nuevo');
			}
		})
	}
};
/*
function DECODIFICADOR(cod, valor,anterior,record,track){
	OBD.OBD_models.findOne({'tracker':Number(imei)},function(err,tracker){
		if(tracker == null) console.log('vacio');
		switch(cod){
			//cods de emergencia
			case '01':
				var databin = hexToBinary(valor);
				var info = '';
				if (databin[0]=='1'){info += 'luz encendida/'};
				if (Number(a.substring(1,7))>0){info=info+'cantidad de alertas:'+a.substring(1,7)+'/'}
				if (Number(a.substring(13,15))>0){
					info+='pruebas disponibles: ';
					if(databin[13]=='1'){info+='componentes;'};
					if(databin[14]=='1'){info+='sistema;'};
					if(databin[15]=='1'){info+='fallos;'};
				}
				if(info != anterior) {
					tracker.datos[record].datos.push.push({'valor':info,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = info;
					   }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :info
  							})
					  	}
					  }
					};
				}
				break;
			case '02':
				break;
			//estado del sistema del motor
			case '03':
				switch(Number(valor)){
					case 1:
						estado='bucle abierto por temperatura';
						break;
					case 2:
						estado='bucle cerrado';
						break;
					case 4:
						estado='bucle abierto carga del motor';
						break;
					case 8:
						estado='bucle abierto por falla';
						break;
					case 16:
						estado='bucle cerrado por sensor de oxigeno';
						break;
					default:
						estado='cod no identificado';
						break;
				}
				if(estado != anterior) {
					tracker.datos[record].datos.push.push({'valor':estado,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = estado;
					   }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :estado
  							})
					  	}
					  }
					};
					tracker.save();
				}
			
				break;
			//carga del motor
			case '04':
				if(valor != anterior) {
					tracker.datos[record].datos.push.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
				tracker.save();
				break;
			//temperatura del liquido de enfriamiento del motor
			case '05':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//ajuste a corto plazo del combustible banco 1(combustible rico o pobre)
			case '06':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
				
					tracker.save();
				}
				break;
			//ajuste a largo plazo del combustible banco 1(combustible rico o pobre)
			case '07':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//ajuste a corto plazo del combustible banco 2(combustible rico o pobre)
			case '08':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//ajuste a largo plazo del combustible banco 2(combustible rico o pobre)
			case '09':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//presion del combustible
			case '0A':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//presion del colector
			case '0B':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//RPM
			case '0C':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//velocidad
			case '0D':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//Avance de tiempo
			case '0E':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//Temperatura del aire del colector de admisión
			case '0F':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//Velocidad del flujo del aire MAF
			case '10':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			// 	Posición del acelerador
			case '11':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//Aire secundario controlado
			case '12':
				break;
			//presencia de sensores de oxigeno
			case '13':
				break;
			case '14':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '15':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '16':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '17':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '18':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '19':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '1A':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '1B':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			//formato del OBD
			case '1C':
				
				break;
			//sensores de oxigeno en el banco 2
			case '1D':
				break;
			//entradas auxiliares
			case '1E':
				break;
			//tiempo en macha del motor
			case '1F':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				
				break;
			//distancia recorrida con luz indicadora 
			case '21':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			case '22':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				
				break;
			//presion del tren de combustible
			case '23':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				
				break;
			//sensores (relacion combustible aire)
			case '24':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '25':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '26':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '27':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '28':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '29':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '2A':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '2B':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			//EGR
			case '2C':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				
				break;
			//Falla EGR
			case '2D':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				
				break;
			//purga evaporativa
			case '2E':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				
				break;
			//porcetaje de gasolina
			case '2F':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//Cantidad de calentamientos desde que se borraron los fallas
			case '30':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//distancia recorrida desde que se borraron los datos
			case '31':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//presion de vapor
			case '32':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//presion barometrica absoluta
			case '33':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//sensores de gases (relacion actual aire combustible)
			case '34':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '35':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '36':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '37':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '38':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '39':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '3A':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			case '3B':
				var datos = valor.slipt(',');
				ajuste = datos[1];
				if(ajuste != anterior) {
					tracker.datos[record].datos.push({'valor':ajuste,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = ajuste;
					  }
					};
					tracker.save();
				}
				break;
			//temperatura del catalizador
			case '3C':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
				tracker.save();
				};
				break;
			case '3D':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
				tracker.save();
				};
				break;
			case '3E':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
				tracker.save();
				};
				break;
			case '3F':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//estado de los monitores
			case '41':
				break;
			//voltaje del modulo de control
			case '42':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//voltaje de carga absoluta
			case '43':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					  
					tracker.save();
					};
				break;
			//relacion combustible aire
			case '44':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//posicion relativa del acelerador
			case '45':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//temperatura ambiental
			case '46':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//posicion absoluta del acelerador B
			case '47':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//posicion absoluta del acelerador C
			case '48':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//posicion del pedal D
			case '49':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//posicion del pedal E
			case '4A':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//posicion del pedal F
			case '4B':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//actual comando del acelerador
			case '4C':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//Tiempo con Mil
			case '4D':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//tiempo desde que se borraron los cods
			case '4E':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
				tracker.save();
				};		
				break;
			//Equivalencia entre 
			case '4F':
				var valor = valor.split(',');
				var anterior =anterior.split(',');
				if(valor[0] != anterior[0] || valor[1] != anterior[1] || valor[2] != anterior[2] || valor[3] != anterior[3]) {
					tracker.datos[record].datos.push({'valor':String(valor[0]+','+valor[1]+','+valor[2]+','+valor[3]),'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = String(valor[0]+','+valor[1]+','+valor[2]+','+valor[3]);
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :String(valor[0]+','+valor[1]+','+valor[2]+','+valor[3])
  							})
					  	}
					  }
					};
				tracker.save();
				}
				break;
			//flujo de aire
			case '50':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//tipo de combustible
			case '51':
				switch(Number(valor)){
					case 1:
						tipo = 'gasolina';
						break;
					case 2:
						tipo = 'methanol';
						break;
					case 3:
						tipo = 'ethanol';
						break;
					case 4:
						tipo = 'disel';
						break;
					case 5:
						tipo = 'LPG';
						break;
					case 6:
						tipo = 'CNG';
						break;
					case 7:
						tipo = 'Propanol';
						break;
					case 8:
						tipo = 'Electrico';
						break;
					case 9:
						tipo = 'Bifuel running gasolina';
						break;
					case 10:
						tipo = 'Bifuel running methanol';
						break;
					case 11:
						tipo = 'Bifuel running ethanol';
						break;
					case 12:
						tipo = 'Bifuel running LPG';
						break;
					case 13:
						tipo = 'Bifuel running CNG';
						break;
					case 14:
						tipo = 'Bifuel running Propanol';
						break;
					case 15:
						tipo = 'Bifuel running Electrico';
						break;
					case 16:
						tipo = 'Bifuel running Electrico y combustion interna';
						break;
					case 17:
						tipo = 'hidrido de gasolina';
						break;
					case 18:
						tipo = 'hidrido methanol';
						break;
					case 19:
						tipo = 'hidrido ethanol';
						break;
					case 20:
						tipo = 'hidrido electrico';
						break;
					case 21:
						tipo = 'hidrido electrico y combustion interna';
						break;
					case 22:
						tipo = 'hidrido regenerativo';
						break;
					case 23:
						tipo = 'Bifuel running disel';
						break;		
				}
				if(tipo != anterior) {
					tracker.datos[record].datos.push({'valor':tipo,'cod':cod});
					for (var i=0; i < tracker.resumen.cod.length; i++) {
					  if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = tipo;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :tipo
  							})
					  	}
					  }
					};
				tracker.save();
				}
				break;
			//procentaje de ethanol
			case '52':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//presion absoluta de evaporacion
			case '53':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//presion de evaporacion
			case '54':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//ajuste del sensor de oxigeno
			case '55':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			case '56':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			case '57':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			case '58':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//presion absulta del tran de combustible
			case '59':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//posicion del pedal
			case '5A':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//tiempo de vida de baterias hibridas
			case '5B':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//temperatura del aceite del motor
			case '5C':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//sincronizacion de inyeccion
			case '5D':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//velocidad de velocidad de combustible
			case '5E':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//requisitos de diseño
			case '5F':
				break;
			//torque solictado por el conductor
			case '61':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//torque actual del motor
			case '62':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					};
					tracker.save();
				break;
			//torque de referencia
			case '63':
				if(valor != anterior) {
					tracker.datos[record].datos.push({'valor':valor,'cod':cod});
					if( tracker.resumen.cod[i].nombre == cod){
					  	tracker.resumen.cod[i].valor = valor;
					  }
					  else{
					  	if(i == tracker.resumen.cod.length -1){
					  		tracker.resumen.cod.push({
					  			'nombre':cod,
  								'valor' :valor
  							})
					  	}
					  }
					tracker.save();
				break;
		}
	}
	})
}*/