/*
    SunTech jailbreak
    PassArte Lab

    By: ln666
    About: This script will get the data from trackers, will execute the decipher script and the
    archiving script. It is connected to the Top-Backend through the Mongo Database where the 
    features are got and then, extracted from the tracker's data.

    Usage: 
        Please, give more chance. Meanwhile, feed me with sushi :D
        Important: The port and ip must be set in order to be legible through Internet
                
    Requirements:
        npm install net
        database.js (exported)
        decipher.js (exported)
        archiving.js (exported)

    Last Update: Dec/23/2017f
    NOTE: Beta

    para probarlo uasr python con la libreria sockect
    con el script:

import socket
TCP_IP = '34.237.241.0'
TCP_PORT = 200
BUFFER_SIZE = 1024
MESSAGE = "ST300STT;205910424;04;907;2019003;17:12:47;7d746;+18.445874;-098.126892;000.000;000.00;0;0;0;12.04;000000;1;0008;000005;0.0;1;0;00000000000000;0;000.000;0;000.000"
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.send(MESSAGE)
s.close()

    para instalar socket usar pip con la instruccion pip install python-handler-socket
*/

// Required libraries
var net = require('net');
var decipher = require('./decipher.js');
var PassArte = require('./../../../Top_end/models/Vehiculos_models.js');
var Positions = require('./../../../Top_end/models/Posiciones_models.js');
var da= require('./../../../Top_end/models/Archive.js');
var di= require('./../../../Top_end/models/Imeis_models.js');
var dh= require('./../../../Top_end/models/history.js');
var ObjectId = require('mongodb').ObjectID;
var Archiving = require('./archiving.js');
var Historics = require('./Historics.js');
var sender = require('./sender.js');

// Events
var events = require('events');
var eventCMDResponse = new events.EventEmitter();

var port = 100;
var ip = '34.237.241.0';

var ut=function(array){
this.array = array;
}

var tramasCnt = 0;
var datosBuffer = [];
var waiting_cmd_response = [];
ut.TCPserver = function(app){
    var client = net.createServer(function(trackerServer) {
        //trackerServer.write('Tracker Server Up\r\n'); QUIT

        // Handle incoming messages from clients.
        trackerServer.on('data', (data) => { 
        try{
            console.log('recibido',data.toString().replace(/[\n\r]*$/, ''));
            // Get the string
            //var buf = data.toString().replace(/[\n\r]*$/, '');
            // Explode the string using \t
            //var m = buf.split(";");
            tramasCnt++;
            console.log("No de Trama: "+tramasCnt);
            //console.log("Trama:" + m);
            // Start Sender
            sender(app, trackerServer, eventCMDResponse, decipher, function(imei){
                waiting_cmd_response.push(imei);
                console.log(waiting_cmd_response)
            });
            // Get the number

            datosBuffer.push(data);
            
        }
        catch(err)
        {
            console.log("Corrupted Package",err);
        }
    });

    });
    client.listen(port);
};

function deleteZero(){
    console.log('borrando elemento 0')
    //elimina el elemento cero del array y vuelve a correr la funcion
    datosBuffer.splice(0,1);
    saveHandler()
}


//asistente de guardado de datos, guarda los uno a 
function saveHandler(){
    if(datosBuffer.length>0){
        var buf = datosBuffer[0].toString().replace(/[\n\r]*$/, '');
        // Explode the string using \t
        var m = buf.split(";");
        decipher.decipher("imei", buf, null, function(imei, report)
            {
                // Verify if the imei is awaiting commands
                if(imei != null)
                {
                    var imei_command = waiting_cmd_response.indexOf(imei);
                    // If imei is not present, do the habitual routine
                    if(imei_command == -1)
                    {
                        // Get features
                        console.log("Imei: " , imei);
                        di.imeis.findOne({imei:Number(imei)},function(err,imei_ser){
                            if(err) {
                                console.log(err);
                                deleteZero()
                            }
                            else{
                                //console.log('imei del dispositivo:',imei_ser);
                                if(imei_ser==null) {
                                    console.log('no hay dispositivo con el id: ', imei)
                                    deleteZero()    
                                }
                                else{
                                    if(imei_ser.Asign==false) {
                                        console.log('no hay dispositivo con el id: ',imei);
                                        deleteZero()
                                    }
                                    else{
                                        var query = PassArte.Vehiculos.findById(ObjectId(imei_ser.asign_to));
                                        query.select('_id posiciones_last Archiving imei placa Alertas Alertas_last historycs');
                                        //query.select('buses Company');
                                        query.exec(function(err, bus){
                                            console.log('dispositivo:',bus);
                                            // Features array
                                            if(bus == null){
                                                console.log("Error: No hay bus registrado con el imei "+imei);
                                                deleteZero()
                                            }
                                            else
                                            {
                                                console.log("bus registrado con el imei" + imei);
                                                var id = bus._id;
                                                var features = "";
                                                var lastTrack = null;
                                                var settings = null;
                    
                                                //cambiar ya se tienes las los buses separados
                    
                                                Positions.Position.findById(ObjectId(bus.posiciones_last),function(err,position){
                                                    if(err) {
                                                        console.log(err);
                                                        deleteZero()
                                                    }
                                                    else{
                                                        Positions.Position.findById(ObjectId(position.before),function(err,lastpos){
                                                            if(err) {
                                                                console.log(err);
                                                                deleteZero()
                                                            }
                                                            else{
                                                                di.imeis.findOne({'imei':imei},function(err,Aimei){
                                                                    if(err) {
                                                                        deleteZero()
                                                                        console.log(err)
                                                                    }
                                                                    else{
                                                                        console.log('datos',lastpos,Aimei);
                                                                        features=Aimei.structure;
                                                                        lastTrack=lastpos;
                                                                        placa=bus.placa;
                                                                        settings=bus.Archiving;
                                                                        decipher.decipher(features, buf, placa, function(results)
                                                                        {
                                                                            // Results contain the results of the features requested
                                                                            // New obj
                                                                            // DEBUG console.log(results);
                                                                                    
                                                                            //console.log(bus);
                                                                            var newTrack = {};
                                                                            for(var i = 1; i < features.length; i++ )
                                                                                objBuilder(newTrack, features[i], results[i]);
                                                                            console.log("nueva trama", newTrack);
                                                                            //Achriving, el setting se crea buscando todos los ids que estan en el arreglo bus.Archiving
                                                                            setts(bus.Archiving,new Array(0),newTrack, lastTrack,bus,0)
                                                                            deleteZero()
                                                                        })
                            
                                                                    }
                                                                })
                                                                
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                }
                            }
                        })
                    
                    }
                    // If the imei is waiting
                    else
                    {
                        console.log("Command got");
                        // Send data
                        eventCMDResponse.emit('response',imei,buf);
                        // Remove
                        eventCMDResponse.removeAllListeners();
                        // Quit from stack
                        waiting_cmd_response.splice(imei_command, 1);
                        deleteZero()
                    }
                }
                else{
                    deleteZero()
                }
            });
    }
    else{
        console.log('esperando datos')
        setTimeout(saveHandler, 3000);
    }
}
saveHandler()

function objBuilder(obj,is, value) {
    if (typeof is == 'string')
        return objBuilder(obj,is.split('.'), value);
    else if (is.length==1 && value!==undefined)
        return obj[is[0]] = value;
    else if (is.length==0)
        return obj;
    else
        return objBuilder(obj[is[0]],is.slice(1), value);
};

function setts(array,settings,newTrack, lastTrack,bus,i){
    //console.log('preentrada al arch',newTrack, lastTrack)
    //creacion de un array de los criterios de archiving
    if(array.length>0 && array.length>i){
        //se busca cada criterio de archiving
        da.Archive.findById(ObjectId(array[i]),function(err,arch){
            if(err) console.log(err);
            else{
                //se almacenacenan en una array
                settings.push(arch);
                //console.log('arch setts:',settings)
                //se vuelve a correr aumentando el index
                setts(array,settings,newTrack, lastTrack,bus,i+1);
            }
        })
    }
    else{
        console.log('alertas:',bus.Alertas_last);
        //una vez que se crea el array de archiving se ve si es valido para guardarse
        if(Archiving.postable(newTrack, lastTrack, settings,bus._id)){
            console.log('Arch pass',newTrack)
            posiciones = new Positions.Position();
            posiciones.save(function(err){
                if(err) console.log(err)
                else{
                    PassArte.Vehiculos.findById(ObjectId(bus._id),function(err,bus){
                        if(err) console.log(err)
                        else{
                            Positions.Position.findById(ObjectId(bus.posiciones_last),function(err,lastpos){
                                if(err) console.log(err)
                                else{
                                    before=lastpos.before;
                                    bus.posiciones_last=posiciones._id;
                                    bus.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            lastpos=new Positions.Position(newTrack);
                                            lastpos.next=posiciones._id;
                                            lastpos.before=before;
                                            lastpos.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    posiciones.before=lastpos._id;
                                                    posiciones.save(function(err){
                                                        if(err) console.log(err);
                                                        else{
                                                            //history(newTrack.date, bus.historycs,posiciones._id,bus._id);
                                                            console.log("done");
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
        else{ console.log('rejected by Archiving')}
    }
}

function history(date, histori,id,bus){
    if(histori == null){
        var hist = new dh.history({
            'fechas':{
                [date]:id
            }
        });
        hist.save(function(err){
            if(err) console.log(err);
            else{
                dv.Vehiculos.findById(ObjectId(bus),function(err,bus){
                    if(err) console.log(err);
                    else{
                        bus.historycs=hist._id;
                        bus.save(function(err){
                            if(err) console.log(err);
                            else{
                                return bus;
                            }
                        })
                    }
                })    
            }
        })
    }
    else{
        dh.history.findById(ObjectId(histori),function(err,hst){
            if(err) console.log(err);
            else{
                if(hst.fechas[date]==undefined){
                    var data = hst.fechas;
                    data[date] = id;
                    hst.fechas = data;
                    console.log(hst,data);
                    hst.remove(function(err){
                        if(err) console.log(err);
                        else{
                            var hist = new dh.history({
                                'fechas':data
                            });
                            hist.save(function(err){
                                if(err) console.log(err);
                                else{
                                    console.log(hist,data);
                                    dv.Vehiculos.findById(ObjectId(bus),function(err,bus){
                                        if(err) console.log(err);
                                        else{
                                            bus.historycs=hist._id;
                                            bus.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    return bus;
                                                }
                                            })
                                        }
                                    })    
                                }
                            })
                        }
                    })
                }
            }
        })
    }
}

module.exports = exports=ut;

// 
/*
    Flow:
    1. Get the string
    2. Explode the string
    3. Get the number
    4. Get the features
    5. Get results
    6. Save into the DB
*/
