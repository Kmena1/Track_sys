/*
    SunTech jailbreak
    PassArte Lab

    By: ln666
    About: This script lets you get the different parts from the tracker data line, 
    associating the requested feature with the index position in the data line.
    This script uses the "SuntecTracks" collection which contains the feature's name
    and the index, depending on the tracker's model.

    Usage: 
        Params: features: the features to look for
                line: the line read from tracker
                callback: function which contains result
        Example:
                descipher("lat", line, function(result){...});
                Where "lat" is the feature, line is the string from tracker and result is where
                the data is collected.
                

    Last Update: Dec/18/2017
    NOTE: It's tested
*/

// Libraries
//var mongoose = require('mongoose');
var mongoose = require('mongoose');
var PassArte = require('./../../../Top_end/models/Empresas_models.js');
var di = require('./../../../Top_end/models/Imeis_models.js')
var request = require('request');
//var OBD = require('./OBD_deco.js');
// Connection to Mongo
/* mongoose.connect('mongodb://localhost/devices');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
}); */

// Decipher function - features can be a string or string array
// It returns an array associated with the features in a dictionary
module.exports = { decipher: function (features, line,placa, callback)
{
    // Explode all the line
    var linea = line.split(";");
    if(linea.length == 1){
    	line = line.split(",");
    }
    else{
    	line=linea;
    }
    // Get the object
    getLineModel(line,placa, function(model){
        //console.log('model',model);
        if(model == null)
        {
            callback(null);
            return;
        }
    
        // If there is a single feature
        if((typeof features) == "string" )
        {
        	//console.log(features);
            var featureIndex = model.indexOf(features);
            if(featureIndex != -1)
                callback(line[featureIndex]);
            else
                callback(null);
        }
        // If there is an array
        else if((typeof features) == "object")
        {
            var result = [];
            for(var i = 0; i < features.length; i++)
            {
                var featureIndex = model.indexOf(features[i]);
                if(featureIndex != -1)
                    result.push(line[featureIndex]);
                else
                    result.push(null);
            }
            callback(result);
        }
        // Error
        else
        {
            callback(null);
        }
    });
}
}

function getLineModel(line, placa, callback)
{
    // Getting report type
    var header = line[0];
    if(header=='ST910') {
        buscar={
            tipo:header,
        }
        tipo = line[1];
    }
    else{
        buscar={
            tipo:header=header.substring(0,header.length - 3),
            model_ID: line[2]
        }
        tipo= header.substring(header.length - 3,header.length-1)
    } 
    var col3 = line[2];
    //console.log('datos_buscar:',buscar);
    // Getting possible results
    if(tipo == 'Location' || tipo == 'STT'){
        di.Structure.findOne(buscar, function(err, dev){
            if(err) console.log(err)
            else{
                if(dev==null) console.log('No hay estructura con el header', header)
                else callback(dev.structure)
            // Verifying
            }
            
        })
    }
    else callback(null)
}


/* debug();
function debug()
{
    var line = "ST300STT;90;01;version;date;time;cell;344.3;44.3;speed;course;satt;fix;dist;power;io;mode;mess;drive;volt;msgtype;rpm;io;reg;sp-rp;sp-time;saverg";
    decipher(["lat", "lon", "speed"],line, function(result){
        console.log(result);
    });
    
} */


