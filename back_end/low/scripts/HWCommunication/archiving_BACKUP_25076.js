/*
    SunTech jailbreak
    PassArte Lab

    By: ln666
    About: This script will push the relevant data to the Database according to the settings put in
    each bus. It's important to say that redundant data will be ignored.

    Usage: 
        Please, give more chance. Meanwhile, feed me with sushi :D
                
    Requirements:
        
    Data:
        Input: content (obj to insert), lastReg (last obj inserted), settings (a list of rules)
    Last Update: Jan/4/2018

    NOTE: In development

    archiving_settings:
        [{
            rule: {type: String, required: true},
            property: {type: String, required: true},
            tolerance: {type: Number, required: true},
            module: Number,
            master: {type: Boolean, default: false},
            reference: {type: Number, default: null},
            operator: {type: String, default: "ge"}
        }]
*/

module.exports = {
    postable: function (content, lastReg, settings)
    {
        // Flag
        var insert = false;
        var insert_master = true;
	
	if(settings == undefined || settings == null || settings.length == 0)
<<<<<<< HEAD
		{insert =true;
		console.log(settings);}
=======
	{
        insert = true;
        insert_master = true;
    }

>>>>>>> e56a2dc657c192ff9b04a420cf665e2188b0028d
	else
	// Enabling the flag - Basically is an OR Logic
        for(var i = 0; i < settings.length; i++)
        {
            // Get both components
            var newValue = index(content,settings[i].property);
            var lastValue = index(lastReg,settings[i].property);
            var referenceValue = settings[i].reference;
            if(isNaN(newValue) || isNaN(lastValue))
            {
                console.log("A component gotten by Archiving is not a number");
            }
            else
            {
                newValue = Number(newValue);
                lastValue = Number(lastValue);
            // Switching the case
                switch(settings[i].rule)
                {
                    case "moduled_tolerance":
                        // Compute the module
                        newValue = newValue % (Math.pow(10,settings[i].module));
                        lastValue = lastValue % (Math.pow(10,settings[i].module));
                        console.log("N:"+newValue + " L:"+lastValue);
                        // Compute the error
                        if(referenceValue == null)
                            var error = Math.abs(newValue-lastValue)/lastValue;
                        else
                            var error = Math.abs(newValue-referenceValue)/referenceValue;
                        console.log("E:" + error*100);
                        // Verify if should be posted
                        var result;
                        switch(settings[i].operator)
                        {
                            case 'ge':
                                result = error*100 >= settings[i].tolerance;
                                break;
                            case 'le':
                                result = error*100 <= settings[i].tolerance;
                                break;
                            case 'l':
                                result = error*100 < settings[i].tolerance;
                                break;
                            case 'g':
                                result = error*100 >= settings[i].tolerance;
                                break;
                            case 'e':
                                result = error*100 == settings[i].tolerance;
                                break;
                            case 'ne':
                                result = error*100 != settings[i].tolerance;
                                break;
                        }
                        if(result)
                            insert = true;
                        if(settings[i].master && !result)
                            insert_master = false;
                        break;
                    case "simple_tolerance":
                        // Compute the error
                        if(referenceValue == null)
                            var error = Math.abs(newValue-lastValue)/lastValue;
                        else
                            var error = Math.abs(newValue-referenceValue)/referenceValue;
                        console.log("E:" + error*100);
                        // Verify if should be posted
                        var result;
                        switch(settings[i].operator)
                        {
                            case 'ge':
                                result = error*100 >= settings[i].tolerance;
                                break;
                            case 'le':
                                result = error*100 <= settings[i].tolerance;
                                break;
                            case 'l':
                                result = error*100 < settings[i].tolerance;
                                break;
                            case 'g':
                                result = error*100 >= settings[i].tolerance;
                                break;
                            case 'e':
                                result = error*100 == settings[i].tolerance;
                                break;
                            case 'ne':
                                result = error*100 != settings[i].tolerance;
                                break;
                        }
                        if(result)
                            insert = true;
                        if(settings[i].master && !result)
                            insert_master = false;
                    case "fixed_tolerance":
                        // Compute the error
                        if(referenceValue == null)
                            var error = Math.abs(newValue-lastValue);
                        else
                            var error = Math.abs(newValue-referenceValue);
                        // Verify if should be posted
                        var result;
                        switch(settings[i].operator)
                        {
                            case 'ge':
                                result = error >= settings[i].tolerance;
                                break;
                            case 'le':
                                result = error <= settings[i].tolerance;
                                break;
                            case 'l':
                                result = error < settings[i].tolerance;
                                break;
                            case 'g':
                                result = error >= settings[i].tolerance;
                                break;
                            case 'e':
                                result = error == settings[i].tolerance;
                                break;
                        }
                        if(result)
                            insert = true;
                        if(settings[i].master && !result)
                            insert_master = false;
                        break;
                    default:
                        insert = true;
                        break;
                }
            }
        }
        // Deciding whether posting or not.
	//console.log(insert);
        return insert && insert_master;
    }
}

function index(obj,is, value) {
    if (typeof is == 'string')
        return index(obj,is.split('.'), value);
    else if (is.length==1 && value!==undefined)
        return obj[is[0]] = value;
    else if (is.length==0)
        return obj;
    else
        return index(obj[is[0]],is.slice(1), value);
}

/*
    Rules:
        No rule: no element in the settings object
        Rule: is present in the settings object
            1. moduled_tolerance
                Basically, a large number has to be shorted and a tolerance is applied
                For instace:
                    odo: 123456789
                    -> Mod: 3 (the last three numbers)
                    -> Tolerance: 5 (five percent)
                    So after applying module:
                    odo: 789 because, odo % 10^3
                    And then, with the tolerance:
                    abs(odo1 - odo2)/odo1 >? 5*0.01
            2. simple_tolerance
                In this case, you need only the past and the new value:
                    abs(value2-value1)/value1 >? tolerance*0.01
                    Where tolerance is given by settings and value2 and value1 are given by the
                    new and the last value.
            3. fixed_difference
                The last case is simplier than the other ones. A difference is given and you only have
                to compare the difference between the values with the difference given by settings
                For example:
                    value2: new one
                    value1: last one
                    Then,
                        value2-value1 >? difference
            4.  

    Settings object:
        {
            rule: "string" (shown before)
            property: "string" (feature which is affected)
            tolerance: "number" in percentage | fixed difference
            module: the number of digits that are taken into account (if applicable)
        }
*/
