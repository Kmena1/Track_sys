module.exports = {
	Resumen: function(imei, lugar){
		var PassArte = require('./../../../top/models/crear_empresa_model.js');
		var resumen = require('./../../../top/models/posiciones_resumidas_models.js');
		var Positions = require('./../../../top/models/posiciones_resumidas_models.js');
		
		PassArte.Business.findOne({'buses.imei': Number(imei)},function(err,empresa){
			var gasolina = 0;
			var velocidad =0;
			var odometro = 0;
			var tiempo=0;
			var distancia =0;
			for(i=0;i<empresa.buses.length;i++){
				if(empresa.buses[i].imei==imei){
					console.log(empresa.buses[i].imei);
					var resumido = new Array(0);
					var contenido = empresa.buses[i].contents;
					console.log('tamaño antiguo '+empresa.buses[i].contents.length);
					if(contenido.length>1){
						resumido.push(contenido[1]);
						last = 1;
						for (j=1;j<contenido.length;j++){
							gasolina = (gasolina*(j-1)+(contenido[j].tf/contenido.speed))/j;
							velocidad= (velocidad*(j-1)+contenido[j].speed)/j;
							odometro = (odometro*(j-1)+contenido[j].to)/j;
							tiempo = contenido[j].drive;
							distancia = contenido[j].dist;
							d = Math.sqrt(
						        Math.pow((Number(contenido[j].lat)-Number(contenido[last].lat)),2)+
						        Math.pow((Number(contenido[j].lon)-Number(contenido[last].lon)),2)
						    )*111184.726;
						    s = Math.sqrt(
						        Math.pow((Number(contenido[j].speed)-Number(contenido[last].speed)),2)
						    );
						    f = Math.sqrt(
						        Math.pow((Number(contenido[j].tf)-Number(contenido[last].tf)),2)
						    );
						    /*h= Math.sqrt(
						        Math.pow((Number(contenido[i].drive)-Number(contenido[i-1].drive)),2)
						    );*/
						    if(d>50 || s>0.1 || f>1){
						    	resumido.push(contenido[j]);
						    	last=j;
						    }
						};
						console.log("tamaño del resumen:"+resumido.length)
						Positions.ruta.findOne({'imei':imei},function(err,posicion){
							console.log(posicion.imei);
							var ruta={
								'numero':posicion.ruta.length+1,
								'datos':resumido
							}
							var Check={
								'lugar': lugar,
								'datein':resumido[0].date,
						  		'timein':resumido[0].time,
						  		'dateout':resumido[resumido.length-1].date,
						  		'timeout':resumido[resumido.length-1].time
							}
							console.log(Check);
							posicion.ruta.push(ruta);
							posicion.Check.push(Check);
							posicion.save(function(err){
								if(err)console.log(err);
							});
						});
						empresa.buses[i].contents=[];
						empresa.buses[i].Last_one=resumido[resumido.length-1];
						empresa.save();
					}
				}
			}
			resumen.ruta.findOneAndUpdate({'imei':Number(imei)},{$set:{'Gasto_gas':gasolina , 'Velocidad_promedio':velocidad, 
				'Tiempo_conduccion': tiempo , 'Distancia_conduccion' : distancia , 'Odometro_promedio': odometro}})
		});
		
	}
}
