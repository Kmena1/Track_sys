/*
    SunTech jailbreak
    PassArte Lab

    By: ln666
    About: This script will push the relevant data to the Database according to the settings put in
    each bus. It's important to say that redundant data will be ignored.

    Usage: 
        Please, give more chance. Meanwhile, feed me with sushi :D
                
    Requirements:
        
    Data:
        Input: content (obj to insert), lastReg (last obj inserted), settings (a list of rules)
    Last Update: Dec/19/2017
    NOTE: In development
*/

module.exports = {
    postable: function (content, lastReg, settings)
    {
        // Flag
        var insert = false;
	
	if(settings == undefined || settings == null || settings.length == 0)
		{insert =true;
		console.log(settings);}
	else
	// Enabling the flag - Basically is an OR Logic
        for(var i = 0; i < settings.length; i++)
        {
            // Get both components
            var newValue = index(content,settings[i].property);
            var lastValue = index(lastReg,settings[i].property);
            if(isNaN(newValue) || isNaN(lastValue))
            {
                console.log("A component gotten by Archiving is not a number");
            }
            else
            {
                newValue = Number(newValue);
                lastValue = Number(lastValue);
            // Switching the case
                switch(settings[i].rule)
                {
                    case "moduled_tolerance":
                        // Compute the module
                        newValue = newValue % (Math.pow(10,settings[i].module));
                        lastValue = lastValue % (Math.pow(10,settings[i].module));
                        console.log("N:"+newValue + " L:"+lastValue);
                        // Compute the error
                        var error = Math.abs(newValue-lastValue)/lastValue;
                        console.log("E:" + error*100);
                        // Verify if should be posted
                        if(error*100 >= settings[i].tolerance)
                            insert = true;
                        break;
                    case "simple_tolerance":
                        // Compute the error
                        var error = Math.abs(newValue-lastValue)/lastValue;
                        // Verify if should be posted
                        if(error*100 >= settings[i].tolerance)
                            insert = true;
                        break;
                    case "fixed_tolerance":
                        // Compute the error
                        var error = Math.abs(newValue-lastValue);
                        // Verify if should be posted
                        if(error >= settings[i].tolerance)
                            insert = true;
                        break;
                    default:
                        insert = true;
                        break;
                }
            }
        }
        // Deciding whether posting or not.
	//console.log(insert);
        return insert;
    }
}

function index(obj,is, value) {
    if (typeof is == 'string')
        return index(obj,is.split('.'), value);
    else if (is.length==1 && value!==undefined)
        return obj[is[0]] = value;
    else if (is.length==0)
        return obj;
    else
        return index(obj[is[0]],is.slice(1), value);
}

/*
    Rules:
        No rule: no element in the settings object
        Rule: is present in the settings object
            1. moduled_tolerance
                Basically, a large number has to be shorted and a tolerance is applied
                For instace:
                    odo: 123456789
                    -> Mod: 3 (the last three numbers)
                    -> Tolerance: 5 (five percent)
                    So after applying module:
                    odo: 789 because, odo % 10^3
                    And then, with the tolerance:
                    abs(odo1 - odo2)/odo1 >? 5*0.01
            2. simple_tolerance
                In this case, you need only the past and the new value:
                    abs(value2-value1)/value1 >? tolerance*0.01
                    Where tolerance is given by settings and value2 and value1 are given by the
                    new and the last value.
            3. fixed_difference
                The last case is simplier than the other ones. A difference is given and you only have
                to compare the difference between the values with the difference given by settings
                For example:
                    value2: new one
                    value1: last one
                    Then,
                        value2-value1 >? difference

    Settings object:
        {
            rule: "string" (shown before)
            property: "string" (feature which is affected)
            tolerance: "number" in percentage | fixed difference
            module: the number of digits that are taken into account (if applicable)
        }
*/
