
/*
    SunTech jailbreak
    PassArte Lab

    By: ln666
    About: This script will allow you to send commands using TCP for broadcasting or 
    sending to single or multiple trackers.

    Usage: 
        Please, give more chance. Meanwhile, feed me with sushi :D
        Important: The port and ip must be set in order to be legible through Internet
                
    Requirements:
        npm install net
        npm install express
        database.js (exported)
    
    Params:
        id = imei
        model = model of tracker
        cmd = command to send
        mode = track | cmd
        version


    Missing:
        - Add listener

    Last Update: Jan/22nd/2018
    NOTE: Beta
*/

// Require the cipher
var cipher = require('./cipher.js').cipher;
var path = require('path');

module.exports = function(app, client, eventOnResult, decipher, callback){
	console.log('sender: listo');
	var bodyParser = require('body-parser');
	
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json())
    
    //sender : function(app, tcp){
        app.post('/HWCommunication/send/cmd', function (req, res) {
            // Get the parameters
            var got = req.body;
            console.log(got);
            if(got.cmd != undefined && got.id != undefined && got.model != undefined && got.version != undefined)
            {
                var cmd = got.cmd; 
                var id = got.id;
                var model = got.model;
                var mode = got.mode;
                var version = got.version;
                // Parse as JSON
                try {
                    // If an object or array
                    id = JSON.parse(id);
                } catch (error) {
                    // If it's a string
                    true;
                }
        
                // Determining if it is an string or an array
                if(typeof(id) == "object")
                {

                    console.log("# CIPHER ERROR: Arrays are not supported");
                }
                // If it's just a single tracker
                else if(typeof(id) == "number" || typeof(id) == "string")
                {
                    // Single case - See if the command is standard
                    if(mode == "track")
                    {
                        callback(id);
                        client.write(cmd.cmd);
                    }
                    else if(mode == "cmd")
                    {
                        console.log("# CIPHER ERROR: standarisation is not completed");
                        cipher(model, id, version, cmd,function(result){
                            if(result == null)
                            {
                                // It is not standard
                                console.log("# CIPHER ERROR: command not standard");
                            }
                            else
                            {
                                if(result != null)
                                {
                                    // Actions
                                    eventOnResult.on('response',function(imei, line){
                                        decipher.decipher(['header','res','imei','protocol','data'],line, null,function(data,report){
                                            // Send Result
                                            res.json(data);
                                        });
                                    });
                                    // Register command
                                    callback(id);
                                    // Send command
                                    client.write(result);
                                }
                                
                            }
                        }); 
                    }
                    else
                        console.log("# CIPHER ERROR: unknown command mode");
                }  
                else
                {
                    // Error
                    console.log("# CIPHER ERROR: typeof command not valid...");
                }
            }
            else
            {
                // Error
                console.log("# CIPHER ERROR: Arguments are missing...");
            }
            
        });

        app.post('/HWCommunication/send/shell', function (req, res) {
            // Get the parameters
            var got = req.body;
            console.log(got)
            if(got.cmd != undefined)
            {
                var cmd = got.cmd;
            	console.log(cmd);
                // Execute 
                try {
                    client.write(cmd);
                    callback(got.id)
                } catch (error) {
                    console.log("# CIPHER ERROR: Execution Error...");
                }
            }
            else
            {
                // Error
                console.log("# CIPHER ERROR: Arguments are missing...");
            }
            
        });
        
    //}
};