// DB Linking
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/devices');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});


var BusinessSchema = mongoose.Schema({
    user: String,
    content:[{
        cfilename: String,
        ctype: String,
        creationTime: Date,
        expirationTime: Date,
        deletionTime: Date,
        cenabled: Boolean
    }],
    buses:[{
        structure: [String],
        imei: Number,
        contents: 
        [{
            imei		  : { type: String, required : true  },
            lat		  : { type: String, required : true  },
            lon		  : { type: String, required : true  },
            to		  : { type: String, required : true  },
            tf		  : { type: String, required : true  },
            ADC		  : { type: String, required : true  },
            drive	          : { type: String, required : true  },
            t1		  : { type: String, required : true  },
            t2		  : { type: String, required : true  },
            t3		  : { type: String, required : true  },
            DID		  : { type: String, required : true  },
            DIDR		  : { type: String, required : true  },
            EmgerId	  : { type: String, required : false  },
            speed		  : { type: String, required : true  },
            vs		  : { type: String, required : true  }
        }],
        last_content:
        {
            imei		  : { type: String, required : true  },
            lat		  : { type: String, required : true  },
            lon		  : { type: String, required : true  },
            to		  : { type: String, required : true  },
            tf		  : { type: String, required : true  },
            ADC		  : { type: String, required : true  },
            drive	          : { type: String, required : true  },
            t1		  : { type: String, required : true  },
            t2		  : { type: String, required : true  },
            t3		  : { type: String, required : true  },
            DID		  : { type: String, required : true  },
            DIDR		  : { type: String, required : true  },
            EmgerId	  : { type: String, required : false  },
            speed		  : { type: String, required : true  },
            vs		  : { type: String, required : true  }
        },
        archiving_settings:
        [{
            rule: {type: String, required: true},
            property: {type: String, required: true},
            tolerance: {type: Number, required: true},
            module: Number
        }]
    }]
});

module.exports = {
    Business : mongoose.model('devices', BusinessSchema),
    mongoose : mongoose
};
 