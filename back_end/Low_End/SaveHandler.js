//bibliotecas
var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;

//schemas
var di= require('./../Top_end/models/Imeis_models.js');
var dp= require('./../Top_end/models/Posiciones_models.js');
var dv= require('./../Top_end/models/Vehiculos_models.js');
var dh= require('./../Top_end/models/history.js');
var dobd= require('./../Top_end/models/OBD.js');

//bibliotecas
var arch=require('./Arch.js')
var alerta=require('./Alertas.js')
var OBD=require('./OBD.js')

//MESSAGE = "ST300ALT;205910424;04;010;20181227;17:11:47;7d746;+19.445874;-099.126892;000.000;000.00;4;1;1;12.46;110100;66;000266;0.0;1;0;012497F1160000;1;000.054"

//obtiene la trama con el header
function getTrama(search,callback) {
    console.log('buscando: ',search)
    di.Structure.findOne(search, function(err, str){
        if(err) callback(2,null)
        else{
            if(str==null){
                callback(1,null)
            }
            else{
                callback(0,str.structure)
            }
        }
    })
}

//convierte la trama que entra a JSON con la estructura
function JSON_parse(trama,structure,callback){
    TramaParse={}
    for (let i = 0; i < trama.length; i++) {
        TramaParse[structure[i]]=trama[i]
    }
    callback(TramaParse)
}

//busca la ultima posicion del dispositivo con el imei
function FindLast(imei,ip,callback){
    di.imeis.findOne({'imei':imei}, function(err, imei){
        //error en la busqueda
        if(err) callback(1,null,null)
        else{
            //no hay registrado
            if(imei==null) callback(2,null,null)
            else{
                //el imei no ha sido asignado
                if(imei.Asign==false)callback(3,null,null)
                else{
                    //se actualiza el ip
                    imei.ip = new Array(ip);
                    imei.save()
                    //si busca el dispositivo
                    dv.Vehiculos.findById(ObjectId(imei.asign_to),function(err,dsp){
                        //error en la busqueda 
                        if(err) callback(1,null,null)
                        else{
                            //el dispositivo no tiene tramas asignadas
                            if(dsp.posiciones_last==null)callback(4,null,dsp._id)
                            else{
                                //busqueda de los datos
                                dp.Position.findById(ObjectId(dsp.posiciones_last),function(err,psc){
                                    //error en la busqueda
                                    if(err) callback(1,null,null)
                                    //devuelve el ultimo dato
                                    else{
                                        callback(0,psc,dsp._id)
                                    }
                                })
                            }
                        }
                    })
                }
            }
        }
    })
}

//guardar
function SaveFuntion(dispositivo,data,callback){
    var newData=dp.Position(data)
    newData.save(function(err){
        console.log('guardado')
        //error de gaurdado
        if(err) callback(2)
        else{
            dv.Vehiculos.findById(ObjectId(dispositivo),function(err,dsp){
                //error de busqueda
                if(err) callback(1)
                else{
                    //si es el primero
                    if(dsp.posiciones_last==null){
                        //se reasignan los ids
                        dsp.posiciones=newData._id
                        dsp.posiciones_last=newData._id;
                        dsp.save(function(err){
                            //error de gaurdado
                            if(err) callback(2)
                            else{
                                callback(0)
                            }
                        })
                    }
                    //si hay mas se anexa al ultimo
                    else{
                        dp.Position.findById(ObjectId(dsp.posiciones_last),function(err,psc){
                            //error de busqueda
                            if(err) callback(1)
                            else{
                                //se reasignan los ids
                                psc.next=newData._id
                                dsp.posiciones_last=newData._id;
                                newData.before=psc._id;
                                psc.save(function(err){
                                    //error de gaurdado
                                    if(err) callback(2)
                                    else{
                                        dsp.save(function(err){
                                            //error de gaurdado
                                            if(err) callback(2)
                                            else{
                                                newData.save(function(err){
                                                    //error de gaurdado
                                                    if(err) callback(2)
                                                    else{
                                                        callback(0)
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
    })
}

//hacer historicos
MakeHystorics = function(dps,fecha,id,callback){
    console.log(dps.historycs)
    dv.Vehiculos.findById(ObjectId(dps._id),function(err,vhc){
        if(err) {
            console.log(err);
            callback(1)
        }
        else{
            if(vhc.historycs!=null){
                dh.history.findById(ObjectId(vhc.historycs),function(err,hst){
                    if(err) console.log(err);
                    else{
                        if(Number(fecha)>Number(hst.fechas)){
                            var historial = new dh.history({
                                fechas: fecha,
                                inicio: hst.final,
                                final: id,
                                before: vhc.historycs
                            })
                            historial.save(function(err){
                                if(err) {
                                    console.log(err);
                                    callback(1)
                                }
                                else{
                                    vhc.historycs=historial._id
                                    vhc.save(function(err){
                                        if(err) {
                                            console.log(err);
                                            callback(1)
                                        }
                                        else{
                                            callback(0)
                                        }
                                    })
                                }
                            })
                        }
                        else callback(1)
                    }
                })
            }
            else{
                var historial = new dh.history({
                    fechas: fecha,
                    inicio: vhc.posiciones,
                    final: vhc.posiciones_last,
                    before: vhc.historycs
                })
                historial.save(function(err){
                    if(err) {
                        console.log(err);
                        callback(1)
                    }
                    else{
                        console.log(historial)
                        vhc.historycs=historial._id
                        vhc.save(function(err){
                            if(err) {
                                console.log(err);
                                callback(1)
                            }
                            else{
                                callback(0)
                            }
                        })
                    }
                })
            }
        }
    })
}

//guardar trama entrante
function SaveTrama(search,m,ip,i,callback){
    getTrama(search,function(err,structure){
        //si existe el header
        if(err==0){
            //transforma de array a JSON
            JSON_parse(m,structure,function(Jtrama){
                FindLast(Jtrama['imei'],ip,function(err,last,dsp){
                    switch (err) {
                        //no hay error
                        case 0:
                            //console.log(arch.arch(last,Jtrama,0))
                            //entra al archiving
                            if(arch.arch(last,Jtrama,0)){
                                SaveFuntion(dsp,Jtrama,function(err){
                                    switch (err) {
                                        //no hay error
                                        case 0:
                                            console.log('guardado');
                                            //historics
                                            //console.log(Jtrama,last)
                                            if(last['date']){
                                                if(Jtrama['date']!=last['date']) MakeHystorics(dsp,last['date'],last._id,function(err){
                                                    callback(i,0)
                                                })
                                                else callback(i,0)
                                            }
                                            else callback(i,0)
                                            break;
                                        //error de busqueda
                                        case 1:
                                            console.log('error de busqueda')
                                            callback(i,1)
                                            break;
                                        //imei no registrado
                                        case 2:
                                            console.log('error de guardado');
                                            callback(i,1)
                                            break;
                                    }
                                })
                            }
                            else{
                                console.log('rechazados por el archiving')
                                callback(i,1)
                            }
                            break;
                            
                        //error de busqueda
                        case 1:
                            console.log('error de busqueda')
                            callback(i,1)
                            break;
                        //imei no registrado
                        case 2:
                            console.log('error: imei '+Jtrama['imei']+' no registrado');
                            callback(i,1)
                            break;
                        //imei no asignado
                        case 3:
                            console.log('error: imei '+Jtrama['imei']+' no asignado');
                            callback(i,1)
                            break;
                        //primera trama del dispositivo
                        case 4:
                            SaveFuntion(dsp,Jtrama,function(err){
                                switch (err) {
                                    //no hay error
                                    case 0:
                                        callback(i,0)
                                        break;
                                    //error de busqueda
                                    case 1:
                                        console.log('error de busqueda')
                                        callback(i,1)
                                        break;
                                    //imei no registrado
                                    case 2:
                                        console.log('error de guardado');
                                        callback(i,1)
                                        break;
                                }
                            })
                            break;
                        default:
                            callback(i,1)
                            break;
                    }
                })
            })
        }
        else{
            console.log('no existe la estructura que desea buscar')
            callback(i,2)
        }
    })
}


//funcion alerta: obtine la trama y parsea la entrada ademas de buscar el dispositivo
function Function_alerta(search,m,ip,i,callback){
    getTrama(search,function(err,structure){
        console.log(structure)
        //si existe el header
        if(err==0){
            //transforma de array a JSON
            JSON_parse(m,structure,function(Jtrama){
                di.imeis.findOne({'imei':Jtrama['imei']}, function(err, imei){
                    //error en la busqueda
                    if(err) callback(i,1)
                    else{
                        //no hay registrado
                        if(imei==null) callback(i,2)
                        else{
                            //el imei no ha sido asignado
                            if(imei.Asign==false)callback(i,3)
                            else{
                                //se actualiza el ip
                                imei.ip = new Array(ip);
                                imei.save()
                                //si busca el dispositivo
                                dv.Vehiculos.findById(ObjectId(imei.asign_to),function(err,dsp){
                                    //error en la busqueda 
                                    if(err) callback(i,1)
                                    else{
                                        //console.log(Jtrama,dsp)
                                        switch(alerta.Alerta(Jtrama,dsp)){
                                            case 0:
                                                callback(i,0)
                                                break
                                            case 1:
                                                callback(i,1)
                                                break
                                            case 2:
                                                callback(i,2)
                                                break
                                            default:
                                                console.log('error de codigo de guardado')
                                                callback(i,3)

                                        }
                                    }
                                })
                            }
                        }
                    }
                })
            })
        }
    })
}

//funcion emergencia: obtine la trama y parsea la entrada ademas de buscar el dispositivo
function Function_Emergency(search,m,ip,i,callback){
    getTrama(search,function(err,structure){
        console.log(structure)
        //si existe el header
        if(err==0){
            //transforma de array a JSON
            JSON_parse(m,structure,function(Jtrama){
                di.imeis.findOne({'imei':Jtrama['imei']}, function(err, imei){
                    //error en la busqueda
                    if(err) callback(i,1)
                    else{
                        //no hay registrado
                        if(imei==null) callback(i,2)
                        else{
                            //el imei no ha sido asignado
                            if(imei.Asign==false)callback(i,3)
                            else{
                                //se actualiza el ip
                                imei.ip = new Array(ip);
                                imei.save()
                                //si busca el dispositivo
                                dv.Vehiculos.findById(ObjectId(imei.asign_to),function(err,dsp){
                                    //error en la busqueda 
                                    if(err) callback(i,1)
                                    else{
                                        //console.log(Jtrama,dsp)
                                        switch(alerta.Emergency(Jtrama,dsp)){
                                            case 0:
                                                callback(i,0)
                                                break
                                            case 1:
                                                callback(i,1)
                                                break
                                            case 2:
                                                callback(i,2)
                                                break
                                            default:
                                                console.log('error de codigo de guardado')
                                                callback(i,3)

                                        }
                                    }
                                })
                            }
                        }
                    }
                })
            })
        }
    })
}

//funcion para control del guardado de OBD
function Function_PID(m,ip,i,start,callback){
    OBD.Deco(m,start,m[start-1],function(err,datos){
        if(err==0){
            datos['lat']=m[6]
            datos['lon']=m[7]
            datos['date']=m[4]
            datos['time']=m[5]
            di.imeis.findOne({'imei':m[1]}, function(err, imei){
                //error en la busqueda
                if(err) callback(i,1)
                else{
                    //no hay registrado
                    if(imei==null) callback(i,2)
                    else{
                        //el imei no ha sido asignado
                        if(imei.Asign==false)callback(i,3)
                        else{
                            //se actualiza el ip
                            imei.ip = new Array(ip);
                            imei.save()
                            //si busca el dispositivo
                            dv.Vehiculos.findById(ObjectId(imei.asign_to),function(err,dsp){
                                //primer OBD
                                if(dsp.OBD_Pos==null){
                                    var OBD=new dobd.OBD(datos)
                                    OBD.save(function(err){
                                        if(err) callback(i,4)
                                        else{
                                            dsp.OBD_Pos=OBD._id;
                                            dsp.save(function(err){
                                                if(err) callback(i,null,null);
                                                else{
                                                    callback(i,0)
                                                }
                                            })
                                        }
                                    })
                                }
                                else{
                                    dobd.OBD.findById(ObjectId(dsp.OBD_Pos),function(err,obd){
                                        if(err) callback(i,1)
                                        else{
                                            datos['before']=obd._id;
                                            var OBD=new dobd.OBD(datos)
                                            OBD.save(function(err){
                                                if(err) callback(i,4);
                                                else{
                                                    dsp.OBD_Pos=OBD._id;
                                                    dsp.save(function(err){
                                                        if(err) callback(i,4);
                                                        else{
                                                            callback(i,0)
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    }
                }
            })
        }
        else{
            callback(i,2)
        }
    })
}

//init
function init(i,trama,ip,callback){
    //console.log(trama.buffer)
    //trasforma la trama a array
    if(trama.buffer.length>0){
        var buf = trama.buffer.toString().replace(/[\n\r]*$/, '');
        console.log(buf)
        var m = buf.split(";");
        //crea el JSON de busqueda
        /********************
            ST300STT: header[0], model[2]
            ST300CMD: header[0], imei[2], type[4]
            ST910: header[0], type[1]
         */
         var search ={};
         var tipo = ''
         switch (m[0]) {
             case 'ST300STT':
                tipo='estado'
                search['header']=m[0];
                search['model_ID']=m[2]
                break;
             case 'ST300ALT':
                tipo='Alerta'
                search['header']=m[0];
                search['model_ID']=m[2]
                break;
             case 'ST300EMG':
                tipo='emergencia'
                search['header']=m[0];
                search['model_ID']=m[2]
                break;
             case 'ST500STT':
                tipo='estado'
                search['header']=m[0];
                search['model_ID']=m[2]
                break;
             case 'ST500ALT':
                tipo='Alerta'
                search['header']=m[0];
                search['model_ID']=m[2]
                break;
             case 'ST500PID':
                tipo='PID'
                search['header']=m[0];
                search['model_ID']=m[2];
                start=14
                break;
             case 'ST500EMG':
                tipo='emergencia'
                search['header']=m[0];
                search['model_ID']=m[2]
                break;
            case 'ST300CMD':
                tipo='respuesta'
                break;
            case 'ST500CMD':
                switch (m[4]){
                    case 'ReqOBDinfPID':
                        tipo='PID'
                        search['header']=m[0];
                        search['model_ID']=m[2]
                        start=6
                        break;
                    default:
                        tipo='respuesta'
                        break
                }
                break
            case 'ST910':
                tipo='estado'
                search['header']='ST910';
                search['tipo']=m[1]
                break;
             default:
                tipo='estado'
                search['header']=m[0];
                search['model_ID']=m[2]
                break;
         }
         //funcion a realizar segun la trama entrante
        switch (tipo) {
            case 'estado':
                SaveTrama(search,m,ip,i,callback)
                break;
            case 'respuesta':
                //se tiene que crear el script para manejar las respuestas, por el momento solo la imprime
                console.log('respuesta a ',m[2],'. Tipo: ', m[4])
                var response=''
                for(let j=5; j<m.length;j++){
                    response=response+m[j]
                }
                if(response)console.log(response)
                callback(i,0)
                break
            case 'Alerta':
                Function_alerta(search,m,ip,i,callback)
                break
            case 'emergencia':
                Function_Emergency(search,m,ip,i,callback)
                break
            case 'PID':
                Function_PID(m,ip,i,start,callback)
                break
            default:
                SaveTrama(search,m,ip,i,callback)
                break;
        }
        
    }

}

exports.init=init
exports.historycs=MakeHystorics