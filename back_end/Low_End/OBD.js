// Libraries
//var mongoose = require('mongoose');
var mongoose = require('mongoose');
//var OBD = require('./../../../top/models/OBD_model.js');

var request = require('request');
var hexToBinary = require('hex-to-binary');

/**
    error 1: not ready
    errot 2: no se guardo
 */

//PIDS :https://es.wikipedia.org/wiki/OBD-II_PID
/**
    divisiones
    Gas
    engine
    sensors
    temperatura
    air
    alertas
 */
var PIDS_Names={
    '02':'fallasDTC',
    '04':'engineFail',
    '05':'breakliquidT',
    //Ajuste de combustible
    '06':'ALT1',
    '07':'ALS1',
    '08':'ALT2',
    '09':'ALS2',
    '0A':'PresionFuel',
    '0B':'PresionAdmision',
    '0C':'RPM',
    '0D':'Speed',
    '0E':'timeAvc',
    '0F':'colectorT',
    '10':'MAF',
    '11':'PosAcel',
    '12':'AirSecSTT',
    '13':'SenOxi',
    '14':'Sox1',
    '15':'Sox2',
    '16':'Sox3',
    '17':'Sox4',
    '18':'Sox5',
    '19':'Sox6',
    '1A':'Sox7',
    '1B':'Sox8',
    '1C':'OBD',
    '1D':'SenOxi4',
    '1E':'IO',
    '1F':'enginetime',
    '21':'DistMIL',
    '22':'fuelPresion',
    '23':'fuelMesPresion',
    '24':'Sox1CA',
    '25':'Sox2CA',
    '26':'Sox3CA',
    '27':'Sox4CA',
    '28':'Sox5CA',
    '29':'Sox6CA',
    '2A':'Sox7CA',
    '2B':'Sox8CA',
    '2C':'ERG',
    '2D':'ERGfail',
    '2E':'Purga',
    '2F':'FuelLevel',
    '30':'heatErase',
    '31':'DistErase',
    '32':'EvapPresio',
    '33':'Presion',
    '34':'Sox1Act',
    '35':'Sox2Act',
    '36':'Sox3Act',
    '37':'Sox4Act',
    '38':'Sox5Act',
    '39':'Sox6Act',
    '3A':'Sox7Act',
    '3B':'Sox8Act',
    '3C':'CatB1S1T',
    '3D':'CatB1S2T',
    '3E':'CatB2S1T',
    '3F':'CatB2S2T',
    '41':'monitors',
    '42':'ControlVolt',
    '43':'Charg',
    '44':'fuelAir',
    '45':'PosAce',
    '46':'tempeture',
    '47':'PosAceB',
    '48':'PosAceC',
    '49':'PedalD',
    '4A':'PedalE',
    '4B':'PedalF',
    '4C':'comadAcc',
    '4D':'MilT',
    '4E':'TimeErase',
    '50':'AirFlux',
    '52':'Ethanol',
    '53':'EvapPresioABS',
    '54':'EvapPresio',
    '55':'AjsSenOxiSB1B3',
    '56':'AjsSenOxiLB1B3',
    '57':'AjsSenOxiSB2B4',
    '58':'AjsSenOxiLB2B4',
    '59':'fuelPresion',
    '5A':'pedalPos',
    '5B':'BattLife',
    '5C':'OilT',
    '5D':'syncIny',
    '5E':'FuelSpeed',
    '5F':'EmisionReq',
    '61':'torqueSol',
    '62':'torqueAct',
    '63':'torqueRef',
    '64':'EnginePorc',
    '65':'IOAux',
    '66':'SairFlux',
    '67':'EngCoolT',
    '68':'AirInT',
    '69':'ERG_ComFail',
    '6A':'DiselIn',
    '6B':'EscGasT',
    '6C':'DiselControl',
    '6D':'fuelPresion',
    '6E':'InyPresion',
    '6F':'TurboPresion',
    '70':'risePresion',
    '71':'VGT',
    '72':'GateWaste',
    '73':'EspPresion',
    '74':'TurboRPM',
    '75':'TurboT',
    '76':'TurboT',
    '77':'CACT',
    '78':'EGT1',
    '79':'EGT2',
    '7A':'DPF',
    '7B':'DPF',
    '7C':'DPFT',
    '7D':'ContNox',
    '7E':'ContPM',
    '7F':'engineT',
    '81':'AECD',
    '82':'AECD',
    '83':'SNox',
    '84':'colectorT',
    '85':'SistNox',
    '85':'PM',
    '86':'ColectorPresion'
}


function Deco(line,start,cnt,callback){
    var res={}
    console.log(line)
    //si los PID's estan listos
    if(line[line.length-1]!="Not Ready"){
        //se recorre desde el start todos los PID's
        for (var i=start; i < line.length; i++) {
            //los PIDS se separan por '|', por lo que se transforman a un arrelgo donde 0 es el PID y el 1 es el valor
            var respuesta = String(line[i]).split('|');
            if(respuesta.length >1){
                //si el valor no es NA se analiza
                if(respuesta[1] !='n/a'){
					var cod = respuesta[0];
					var valor = respuesta[1];
                    //gases, se separan por una coma por el voltaje y el ajuste
                    if(respuesta[0]=='14' || respuesta[0]=='15' || respuesta[0]=='16' || respuesta[0]=='17' || respuesta[0]=='18'
					    || respuesta[0]=='19' || respuesta[0]=='1A' || respuesta[0]=='1B'|| respuesta[0]=='24' || respuesta[0]=='25' 
					    || respuesta[0]=='26' || respuesta[0]=='27' || respuesta[0]=='28'|| respuesta[0]=='29' || respuesta[0]=='2A' 
					    || respuesta[0]=='2B' || respuesta[0]=='34' || respuesta[0]=='35' || respuesta[0]=='36' || respuesta[0]=='37'
					    || respuesta[0]=='38'|| respuesta[0]=='39' || respuesta[0]=='3A' || respuesta[0]=='3B'){
                        var datos = String(respuesta[1]).split(',');
                        res[PIDS_Names[respuesta[0]]]={
                            'voltaje':datos[0],
                            'ajuste':datos[1]
                        }
                    }
                    //datos varios: aire, voltaje del sensor de oxígenos, corriente del sensor de oxígenos y presión absoluta del colector de entrada
                    if (respuesta[0]=='4f') {
						var valor = respuesta[1].split(',');
                        res['Data_4f']={
                            'aire':datos[0],
                            'voltaje':datos[1],
                            'corriente':datos[2],
                            'presión':datos[3]
                        }
                    }
                    //cod MIL
					if(respuesta[0]=='01'){
						var databin = hexToBinary(respuesta[1]);
						var info = '';
						if (databin[0]=='1'){info += 'luz encendida/'};
						if (Number(databin.substring(1,7))>0){info=info+'cantidad de alertas:'+databin.substring(1,7)+'/'}
						if (Number(databin.substring(13,15))>0){
							info+='pruebas disponibles: ';
							if(databin[13]=='1'){info+='componentes;'};
							if(databin[14]=='1'){info+='sistema;'};
							if(databin[15]=='1'){info+='fallos;'};
						}
                        res['MIL']=info;
                    }
                    ////estado del motor
					if(respuesta[0]=='03'){
						switch(Number(valor)){
							case 1:
								estado='bucle abierto por temperatura';
								break;
							case 2:
								estado='bucle cerrado';
								break;
							case 4:
								estado='bucle abierto carga del motor';
								break;
							case 8:
								estado='bucle abierto por falla';
								break;
							case 16:
								estado='bucle cerrado por sensor de oxigeno';
								break;
							default:
								estado='cod no identificado';
								break;
						}
                        res['motor_stt']=estado
                    }
                    ////tipo de gasolina
					if(respuesta[0]=='51'){
						switch(respuesta[1]){
							case 1:
								var tipo = 'gasolina';
								break;
							case 2:
								var tipo = 'methanol';
								break;
							case 3:
								var tipo = 'ethanol';
								break;
							case 4:
								var tipo = 'disel';
								break;
							case 5:
								var tipo = 'LPG';
								break;
							case 6:
								var tipo = 'CNG';
								break;
							case 7:
								var tipo = 'Propanol';
								break;
							case 8:
								var tipo = 'Electrico';
								break;
							case 9:
								var tipo = 'Bifuel running gasolina';
								break;
							case 10:
								var tipo = 'Bifuel running methanol';
								break;
							case 11:
								var tipo = 'Bifuel running ethanol';
								break;
							case 12:
								var tipo = 'Bifuel running LPG';
								break;
							case 13:
								var tipo = 'Bifuel running CNG';
								break;
							case 14:
								var tipo = 'Bifuel running Propanol';
								break;
							case 15:
								var tipo = 'Bifuel running Electrico';
								break;
							case 16:
								var tipo = 'Bifuel running Electrico y combustion interna';
								break;
							case 17:
								var tipo = 'hidrido de gasolina';
								break;
							case 18:
								var tipo = 'hidrido methanol';
								break;
							case 19:
								var tipo = 'hidrido ethanol';
								break;
							case 20:
								var tipo = 'hidrido electrico';
								break;
							case 21:
								var tipo = 'hidrido electrico y combustion interna';
								break;
							case 22:
								var tipo = 'hidrido regenerativo';
								break;
							case 23:
								var tipo = 'Bifuel running disel';
								break;		
						}
                        res['gas_type']=tipo
                    }
                    //cods con solo guardado
					if(respuesta[0]!='14' && respuesta[0]!='15' && respuesta[0]!='16' && respuesta[0]!='17' && respuesta[0]!='18'
					&& respuesta[0]!='19' && respuesta[0]!='1A' && respuesta[0]!='1B'&& respuesta[0]!='24' && respuesta[0]!='25' 
					&& respuesta[0]!='26' && respuesta[0]!='27' && respuesta[0]!='28'&& respuesta[0]!='29' && respuesta[0]!='2A' 
					&& respuesta[0]!='2B' && respuesta[0]!='34' && respuesta[0]!='35' && respuesta[0]!='36' && respuesta[0]!='37'
					&& respuesta[0]!='38'&& respuesta[0]!='39' && respuesta[0]!='3A' && respuesta[0]!='3B' && respuesta[0]!='4f'
					&& respuesta[0]!='01' &&respuesta[0]!='03' && respuesta[0]!='51'){
                        res[PIDS_Names[respuesta[0]]]=respuesta[1]
                    }
                }
            }
        }
        console.log(res);
        //si res esta vacio
        if(Object.keys(res).length==0)callback(2,res)
        else callback(0,res)
    }
    //else
    else{
        callback(1,null)
    }
}

exports.Deco=Deco