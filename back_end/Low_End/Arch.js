var ArchSetts=[
    {
        'tipo':'radio',
        'tolerancia':100,
        'modal':'ge',
    },
    {
        'tipo':'compare',
        'tag':'speed',
        'tolerancia':10,
        'modal':'ge'
    },
    {
        'tipo':'compare',
        'tag':'io',
        'tolerancia':0,
        'modal':'ne'
    }
]

function arch(last,actual,i){
    save=false
    var criteria=ArchSetts[i]
    switch (criteria.tipo) {
        case 'radio':
            //latidut y longitud del viejo
            lat1=last.lat;
            lon1=last.lon;
            //latidut y longitud del nuevo
            lat2=actual.lat;
            lon2=actual.lon;
            console.log(lat1,lon1,lat2,lon2);
            //radio[m]= r*pi*(delta^2(lat)+delta^2(long))^1/2 /180
            radio=110950.58*Math.sqrt((lat1-lat2)*(lat1-lat2)+(lon1-lon2)*(lon1-lon2));
            save=compare(criteria.modal,criteria.tolerancia,radio)
            break;
        case 'compare':
            //anterior
            anterior=Number(last[criteria.tag])
            //actual
            nuevo=Number(actual[criteria.tag])
            //console.log(last[criteria.tag],actual[criteria.tag],criteria.tag)
            if(last[criteria.tag]==null){
                if(isNaN(nuevo)) save=false
                else save=true;
            }
            else{
                //diferencia
                value=Math.abs(nuevo-anterior)
                //comparacion
                save=compare(criteria.modal,criteria.tolerancia,value)
            }
        default:
            break;
    }
    //si se cumplio el criterio o se analizaron todos los criterios se devualve el resusltado
    if(save) return save
    else{
        if(ArchSetts.length>i+1) return arch(last,actual,i+1)
        else return save
    }
}

function compare(operator,tolerance,value){
    var result = false;
    switch(operator)
    {
        case 'ge':
            result = value >= tolerance;
            break;
        case 'le':
            result = value <= tolerance;
            break;
        case 'l':
            result = value < tolerance;
            break;
        case 'g':
            result = value >= tolerance;
            break;
        case 'e':
            result = value == tolerance;
            break;
        case 'ne':
            result = value != tolerance;
            break;
    }
    //console.log(result)
    return result;
}

exports.arch=arch