//bibliotecas
var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;

//schemas
var di= require('./../Top_end/models/Imeis_models.js');
var dp= require('./../Top_end/models/Posiciones_models.js');
var dv= require('./../Top_end/models/Vehiculos_models.js');
var da= require('./../Top_end/models/Alertas_model.js');

//funciones

/*categorias de alertas:
    ST300:{
        GPS: [3,4],8
        GeoFence: 5,6
        speed: 1,2,
        sleep: 9,10
        battey : 13,14
        Shocked/collition: 15/16
        ruta definida: 18,19
        Engine = 22-28
        ignition: 33-34
        conected to: 41,41 44,45
        Patron de manejo: 46,47,48,49 86
        jamming: 50
        i-button: 59-60
        Stop-time: 61-62 68-69
        Dead center: 63
        RPM: 64-65
        cal-odometro=66-67
        fuel: 73 80-84
        temperatura: 75-76-77
        camara: 87
        sim:88
    }
*/
//MakeAlerta('',trama[''],trama['date']+trama['time'],trama['lat'],trama['lon'],null)
//crea las alertas y retorna el id de las alertas
function MakeAlerta(Descripcion,value,fecha,lat,lon,user,callback){
    var Alerta = new da.Alertas({
        fecha:fecha,
        value:value,
        Descripcion:Descripcion,
        lat:lat,
        lon:lon,
        user:user,
    })
    console.log(Alerta);
    Alerta.save(function(err){
        if(err) console.log(err);
        else{
            callback(Alerta._id)
        }
    })
}

//busca dispositivo
function Alerta(trama,dispositivo){
    console.log(trama['header'],Number(trama['AlertId']))
    if(trama['header']=='ST300ALT'){
        dv.Vehiculos.findById(ObjectId(dispositivo._id),function(err,vhc){
            if(err) console.log(err);
            else{
                switch (Number(trama['AlertId'])) {
                    //overspeed
                    case 1:
                        MakeAlerta('Alerta por Exceso de velocidad',trama['speed'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'], function(id){
                            vhc.Alt.overspeed.push(id)
                            //console.log(vhc)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    case 2:
                        MakeAlerta('Final de alerta por exceso de velocidad',trama['speed'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.overspeedFinish.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    case 3:
                        vhc.gps=false;
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break;
                    case 4:
                        vhc.gps=true;      
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })                  
                        break;
                    //geofence
                    case 5:
                        MakeAlerta('Salida de la Geo cerca','',trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.geofenceout.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    case 6:
                        MakeAlerta('entrada de la Geo cerca','',trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.geofencein.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //sleep mode
                    case 9:
                        vhc.sleep=true;
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break;
                    case 10:
                        vhc.sleep=false;
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break   
                    //backup battery error
                    case 13:
                        MakeAlerta('erorr de la bateria',trama['volt'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.batteryError.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //battery down
                    case 14:
                        MakeAlerta('Bateria baja',trama['volt'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.batteryDown.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //shocked
                    case 15:
                        MakeAlerta('Shocked',trama['HB'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.schocked.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //collition
                    case 16:
                        MakeAlerta('colicion',trama['HB'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.schocked.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //ruta definida
                    case 18:
                        MakeAlerta('Salida de la ruta definida',trama['fix'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'], function(id){
                            vhc.Alt.rutaDefinidaout.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    case 19:
                        MakeAlerta('entrada de la ruta definida',trama['fix'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'], function(id){
                            vhc.Alt.rutaDefinidain.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //engine
                    //Exceed speed
                    case 22:
                        MakeAlerta('Motor: Execeso de velocidad',trama['speed'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Engine.Excced_Speed.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //speed violation
                    case 23:
                        MakeAlerta('Motor: Violacion de velocidad',trama['speed'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Engine.Violation_Speed.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //coolant violation
                    case 24:
                        MakeAlerta('Motor: Violacion de coolant','',trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Engine.Coolant.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //oil presure
                    case 25:
                        MakeAlerta('Motor: presion de aceite','',trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Engine.oil.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;    
                    //RPM violation
                    case 26:
                        MakeAlerta('Motor: Violacion de RPM',trama['rpm'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Engine.RPM.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //hard break
                    case 27:
                        MakeAlerta('Motor: Frenado brusco',trama['HB'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'], function(id){
                            vhc.Alt.Engine.Break.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //DTC
                    case 28:
                        MakeAlerta('Motor: Error de codigo en el DTC',null,trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Engine.DTC.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //service
                    case 78:
                        MakeAlerta('Motor: nivel de servico bajo',null,trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Engine.Service .push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //ignite
                    case 34:
                        vhc.ignite=false;
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break;
                    case 33:
                        vhc.ignite=true;
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break  
                    //main power
                    case 41:
                        vhc.battery=false;
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break;
                    case 40:
                        vhc.battery=true;
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break  
                    //Back-up battery
                    case 45:
                        vhc.back_up_Battery=false;
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break;
                    case 44:
                        vhc.back_up_Battery=true;
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break  
                    //DPA
                    //fast acelation
                    case 46:
                        MakeAlerta('DPA: Aceleracion',trama['AC'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.DPA.fast_acelation.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //fast acelation
                    case 47:
                        MakeAlerta('DPA: Aceleracion',trama['AC'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.DPA.fast_acelation2.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //sharp turn
                    case 48:
                        MakeAlerta('DPA: Curva violenta',trama['sharp'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.DPA.sharp_turn.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //overspeed
                    case 49:
                        MakeAlerta('DPA: Sobrevelocidad',trama['speed'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.DPA.overspeed.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //stop
                    //on-Ndrive
                    case 61:
                        MakeAlerta('Tiempo en parqueo sobrepasado',trama['drive'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.stop.on_Ndrive.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //Stoped
                    case 62:
                        MakeAlerta('Tiempo detenido sobrepasado',trama['drive'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.stop.Stoped.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //stop-ignitOn
                    case 68:
                        MakeAlerta('Tiempo detenido a la ignicion sobrepasado',trama['drive'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.stop.stop_ignitOn.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //moving
                    case 69:
                        MakeAlerta('Conduccion despues de largo tiempo detenido',trama['drive'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.stop.stop_ignitOn.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //RPMCal
                    case 65:
                        vhc.RPM=new Date()
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break;
                    //ODMcal
                    case 66:
                        vhc.odometro=new Date()
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break;
                    //ODMcal
                    case 67:
                        vhc.odometro=new Date()
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break;
                    //ODMcal
                    case 67:
                        vhc.DPACal=new Date()
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break;
                    //gas decress
                    case 73:
                        MakeAlerta('Alerta por perdida rapida de combustible',trama['tf'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.gas.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //gas incress
                    case 73:
                        MakeAlerta('Alerta por perdida rapida de combustible',trama['tf'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.gas.push(id)
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                        })
                        break;
                    //temperatura high
                    case 75:
                        vhc.temperatura=2
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break
                    //temperatura low
                    case 76:
                        vhc.temperatura=0
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break
                    //temperatura normal
                    case 77:
                        vhc.temperatura=1
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break
                    //sensor deconectado
                    case 81:
                        vhc.Sgas1=false
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break
                    case 82:
                        vhc.Sgas2=false
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break
                    //sensor conectado
                    case 83:
                        vhc.Sgas1=true
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break
                    case 84:
                        vhc.Sgas2=true
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break
                    case 88:
                        vhc.sim=trama['date']
                        vhc.save(function(err){
                            if(err) {
                                console.log(err)
                                return 2
                            }
                            else{
                                console.log('alerta guardada')
                                return 0
                            }
                        })
                        break
                    default:
                        console.log('no hay registro de accion para el codigo'+trama['AlertId'])
                        return 0
                        break;
                }
            }
        })
    }
    else {
        if(trama['header']=='ST500ALT'){
            dv.Vehiculos.findById(ObjectId(dispositivo._id),function(err,vhc){
                if(err) console.log(err);
                else{
                    switch (Number(trama['AlertId'])) {
                        //overspeed
                        case 1:
                            MakeAlerta('Alerta por Exceso de velocidad',trama['speed'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'], function(id){
                                vhc.Alt.overspeed.push(id)
                                //console.log(vhc)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        case 2:
                            MakeAlerta('Final de alerta por exceso de velocidad',trama['speed'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.overspeedFinish.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //geofence
                        case 5:
                            MakeAlerta('Salida de la Geo cerca','',trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.geofenceout.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        case 6:
                            MakeAlerta('entrada de la Geo cerca','',trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.geofencein.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //sleep mode
                        case 9:
                            vhc.sleep=true;
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break;
                        case 10:
                            vhc.sleep=false;
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break   
                        //backup battery error
                        case 13:
                            MakeAlerta('erorr de la bateria',trama['volt'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.batteryError.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //battery down
                        case 14:
                            MakeAlerta('Bateria baja',trama['volt'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.batteryDown.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //shocked
                        case 15:
                            MakeAlerta('Shocked',trama['HB'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.schocked.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //collition
                        case 16:
                            MakeAlerta('colicion',trama['HB'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.schocked.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //ignite
                        case 34:
                            vhc.ignite=false;
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break;
                        case 33:
                            vhc.ignite=true;
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break  
                        //main power
                        case 41:
                            vhc.battery=false;
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break;
                        case 40:
                            vhc.battery=true;
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break  
                        //Back-up battery
                        case 45:
                            vhc.back_up_Battery=false;
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break;
                        case 44:
                            vhc.back_up_Battery=true;
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break  
                        //DPA
                        //fast acelation
                        case 46:
                            MakeAlerta('DPA: Aceleracion',trama['AC'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.DPA.fast_acelation.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //fast acelation
                        case 47:
                            MakeAlerta('DPA: Aceleracion',trama['AC'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.DPA.fast_acelation2.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //OBD
                        //conected
                        case 80:
                            vhc.OBD=false
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break;
                        case 81:
                            vhc.OBD=true
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break;
                        //DTC
                        case 83:
                            vhc.DTC=false
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break;
                        case 84:
                            vhc.DTC=true
                            vhc.save(function(err){
                                if(err) {
                                    console.log(err)
                                    return 2
                                }
                                else{
                                    console.log('alerta guardada')
                                    return 0
                                }
                            })
                            break;
                        //RPM
                        //Conducion iniciada con alto RPM SDHR
                        case 85:
                            MakeAlerta('Conducion iniciada con alto RPM ',trama['AC'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.RPM.SDHR.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //conduccion finalizada con alto RPM FDHR
                        case 86:
                            MakeAlerta('Conducion iniciada con alto RPM ',trama['AC'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.RPM.FDHR.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //alta velocidad bajo RPM HSLR
                        case 87:
                            MakeAlerta('Conducion iniciada con alto RPM ',trama['AC'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.RPM.HSLR.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        //baja velocidad alta RPM LSHR
                        case 87:
                            MakeAlerta('Conducion iniciada con alto RPM ',trama['AC'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.RPM.LSHR.push(id)
                                vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                            })
                            break;
                        default:
                            console.log('no hay registro de accion para el codigo'+trama['AlertId'])
                            return 1
                            break;
                    }
                }
            })
        }
        else return 1
    }
}

function Emergency(trama,dispositivo){
    if(trama['header']=='ST300'){
        dv.Vehiculos.findById(ObjectId(dispositivo._id),function(err,vhc){
            if(err) console.log(err);
            else{
                switch (Number(trama['EmergId'])){
                    //panic
                    case 1:
                        MakeAlerta('Emergencia de Boton de panico',trama['io'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Emergency.panic.push(id)
                            vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                        })
                        break;
                    //parking_lock
                    case 2:
                        MakeAlerta('Emergencia de Boton de panico',trama['io'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Emergency.parking_lock.push(id)
                            vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                        })
                        break;
                    //remove_power
                    case 3:
                        MakeAlerta('Emergencia de Boton de panico',trama['io'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Emergency.remove_power.push(id)
                            vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                        })
                        break;
                    //anti_theft
                    case 5:
                        MakeAlerta('Emergencia de Boton de panico',trama['io'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Emergency.anti_theft.push(id)
                            vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                        })
                        break;
                    //antitheft_door
                    case 6:
                        MakeAlerta('Emergencia de Boton de panico',trama['io'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Emergency.antitheft_door.push(id)
                            vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                        })
                        break;
                    //motion
                    case 7:
                        MakeAlerta('Emergencia de Boton de panico',trama['io'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Emergency.motion.push(id)
                            vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                        })
                        break;
                    //antitheft_shock
                    case 8:
                        MakeAlerta('Emergencia de Boton de panico',trama['io'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                            vhc.Alt.Emergency.antitheft_shock.push(id)
                            vhc.save(function(err){
                                    if(err) {
                                        console.log(err)
                                        return 2
                                    }
                                    else{
                                        console.log('alerta guardada')
                                        return 0
                                    }
                                })
                        })
                        break;
                    default:
                        console.log('no hay accion para el codigo de la emergencia')
                    break
                }
            }
        })
    }
    else {
        if(trama['header']=='ST300'){
            dv.Vehiculos.findById(ObjectId(dispositivo._id),function(err,vhc){
                if(err) console.log(err);
                else{
                    switch (Number(trama['EmergId'])){
                        //parking_lock
                        case 2:
                            MakeAlerta('Emergencia de Boton de panico',trama['io'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.Emergency.parking_lock.push(id)
                                vhc.save(function(err){
                                        if(err) {
                                            console.log(err)
                                            return 2
                                        }
                                        else{
                                            console.log('alerta guardada')
                                            return 0
                                        }
                                    })
                            })
                            break;
                        //remove_power
                        case 3:
                            MakeAlerta('Emergencia de Boton de panico',trama['io'],trama['date']+trama['time'],trama['lat'],trama['lon'],trama['DID'],function(id){
                                vhc.Alt.Emergency.remove_power.push(id)
                                vhc.save(function(err){
                                        if(err) {
                                            console.log(err)
                                            return 2
                                        }
                                        else{
                                            console.log('alerta guardada')
                                            return 0
                                        }
                                    })
                            })
                            break;
                            //anti_theft
                        default:
                            console.log('no hay accion para el codigo de la emergencia')
                            break
                    }
                }
            })
        }
        else return 1
    }
}

exports.Alerta=Alerta
exports.Emergency=Emergency