/**

 */

// Required libraries
var express = require('express');
var app = express();
var mongoose     = require('mongoose');
var configDB     = require('./../Top_end/configs/database.js');

var net = require('net');
var ObjectId = require('mongodb').ObjectID;

//mongo schema
var ds= require('./../Top_end/models/Low_End_Modals.js');
var di= require('./../Top_end/models/Imeis_models.js');
var dh= require('./../Top_end/models/history.js');
var dp= require('./../Top_end/models/Posiciones_models.js');
var dv= require('./../Top_end/models/Vehiculos_models.js');


//Scripts
var SFunctionHandler=require('./SaveHandler.js');

//variable
var max=100;
var port =200;
var SH_ID={};
var SH_L=[];
var clientsBuff=[]
var buffer=[]
var IpBuff=[]
var running=false;
var creando=false;

/********************************Borra los esquemas******************** */
function deleteSH(i,callback){
    ds.Save.findById(ObjectId(SH_ID[String(i)]),function(err,SH){
        if(err) console.log(err);
        else{
            SH.remove(function(err){
                if(err) console.log(err)
                else {
                    if(SH_L.length>i+1)deleteSH(i+1,callback)
                    else callback()
                }
            })
        }
    })
}

/*********************************reset******************************* */
function reset(){
    var n=0;
    for (let i = 0; i < SH_L.length; i++) {
        n = SH_L[i] + n;
    }
    //si no hay mas datos
    if(n==0) {
        //si el buffer esta vacio
        if(buffer.length==0){
            running=false;
            deleteSH(0,function(){
                SH_ID={};
                SH_L=[];
            })
        }
        //si el buffer contiene datos
        else{
            running=false;
        }
    }
}

/*********************************Borrar *******************************/
function borrar(i){
    console.log(SH_ID)
    //busca el SaveHandler
    ds.Save.findById(ObjectId(SH_ID[String(i)]),function(err,SH){
        if(err) {
            console.log(err);

        }
        else{
            //console.log(SH)
            SH.data.splice(0,1);
            SH.ips.splice(0,1);
            SH.save(function(err){
                if(err) borrar(i);
                else{
                    SH_L[i]=SH.data.length;
                    console.log('borrando ',i,',actual size:',SH.data.length)
                    if(SH.data.length>0) SFunctionHandler.init(i)
                    else reset()
                }
            })
        }
    })
}


/**********************************Save Handler**************************/
function SaveHandler(i){
    console.log('Save schema: ',i);
    //busca la primera trama
    ds.Save.findById(ObjectId(SH_ID[String(i)]),function(err,SH){
        if(err) console.log(err);
        else{
            var data = SH.data[0];
            var ip = SH.ips[0]
            SFunctionHandler.init(i,data,ip,function(j,err){
                borrar(j)
            })
        }
    })
}

/**********************************Buffer push**************************/
function Buffer_push(){
    ds.Save.findById(ObjectId(SH_ID['0']),function(err,SH){
        if(err) console.log(err);
        else{
            SH.data=SH.data.concat(buffer);
            SH.ips =SH.ips.concat(IpBuff);
            SH.save(function(err){
                //si hay un error en el guardardo se intenta de nuevo
                if(err) Buffer_push()
                else{
                    //se actualiza el contador del buffer y se reinicia el buffer
                    SH_L[0]=SH_L[0]+buffer.length
                    buffer=[]
                    IpBuff=[]
                    if(!running) SaveHandler(0)
                }
            })
        }
    })
}

/********************************FindMenor****************************/
function findMenor(callback){
    j=0;
    for (let i = 1; i < SH_L.length; i++) {
        if(SH_ID[i]<SH_ID[i-1]){
            j=i;
        }
    }
    callback(j)
}

/******************************New SaveHandler***************** */
function New_SH(i,data,ip) {
    console.log('creando nuevo save')
    //crea un nuevo SaveHandler()
    var SH=new ds.Save({
        i:i,
        data:new Array(data),
        ips:new Array(ip.remoteAddress+":"+ip.remotePort)
    })
    SH.save(function(err){
        if(err) console.log(err);
        else{
            //se agregan los datos del esquema creado
            SH_ID[String(i)]=SH._id;
            SH_L.push(1);
            //se inicia el SaveHandler;
            SaveHandler(i);
            //se indica que esta corriendo el sistema y se termino de crear es esquema
            running=true;
            creando=false;
            //si hay datos en el buffer
            if(buffer.length>0) Buffer_push()
        }
    })
}

/****************************Add_data************************** */
function Add_data(i,data,ip){
    ds.Save.findById(ObjectId(String(i)),function(err,SH){
        if(err) console.log(err);
        else{
            SH.data.push(data);
            SH.ips.push(ip)
            SH.save(function(err){
                //si hay un error en el guardardo se intenta de nuevo
                if(err) Add_data(i,data)
                else{
                    //se actualiza el contador del buffer
                    SH_L[0]=SH_L[0]+1
                }
            })
        }
    })
}

/***********************************receptor*****************************/
net.createServer(function(trackerServer) {
     // Identify this client
    trackerServer.name = trackerServer.remoteAddress + ":" + trackerServer.remotePort 

    // Put this new client in the list
    clientsBuff.push(trackerServer);

    // Handle incoming messages from clients.
    trackerServer.on('data', (data) => { 
        try {
            console.log('new data from:',trackerServer.remoteAddress+":"+trackerServer.remotePort )
            //trackerServer.write("recivido")
            //si no esta corriendo
            if(!running){
                //si no se esta creando un nuevo SaveHandler
                if(!creando){
                    creando=true;
                    //crea un nuevo SaveHandler()
                    New_SH(0,data,trackerServer)
                }
                //si se est creando un nuevo Save se almacenan los datos que entran en el buffer
                else{
                    buffer.push(data)
                    IpBuff.push(trackerServer)
                }
            }
            //si esta corriendo
            else{
                //busca el Schema con menos datos
                findMenor(function(j){
                    //si se supero el maximo de datos
                    if(SH_L[j]>max){
                        //si no se esta creando un nuevo saveHandler
                        if(!creando){
                            creando=true;
                            //crea un nuevo SaveHandler()
                            New_SH(0,data,trackerServer)
                        }
                        //si se esta creando se agrega sin importar el max
                        else{
                            Add_data(j,data,trackerServer)
                        }
                    }
                    else{
                        Add_data(j,data,trackerServer)
                    }
                })
            }
        } catch (error) {
            console.log(error)
        }
    })

    // Remove the client from the list when it leaves
    trackerServer.on('end', function () {
        clientsBuff.splice(clientsBuff.indexOf(trackerServer), 1);
    });

    function sender(){
        console.log('send')
    }
}).listen(port);

//apps
app.get('/write/:imei/:message',function(req,res){
    /*var info=req.params;
    try {
        if(clientsBuff.length>Number(info.number)){
            puerto=clientsBuff[Number(info.number)]
            puerto.write(info.message)
            res.json(info.message)
        }
    } catch (error) {
        console.log(error)
        res.json(error)
    }*/
    var info=req.params;
    var search={'imei':Number(info.imei)}
    console.log(search)
    di.imeis.findOne(search,function(err,imei){
        //console.log(imei)
        //error en la busqueda
        if(err) res.json({
            'err':1,
            'msg':info.message
        })
        else{
            //no hay registrado
            if(imei==null) res.json({
                'err':2,
                'msg':info.message
            })
            else{
                //se obtiene el ip
                var dir = imei.ip[0];
                if(dir){
                    var puerto=null;
                    //se busca el ip en clientsBuff
                    for (let i = 0; i < clientsBuff.length; i++) {
                        clientport=clientsBuff[i].remoteAddress + ":" + clientsBuff[i].remotePort
                        console.log('puerto '+i+":"+clientport,puerto)
                        if(dir==clientport){
                            puerto=clientsBuff[i]
                            break;
                        }
                    }
                    if(puerto==null) res.json({
                        'err':3,
                        'msg':info.message
                    })
                    else{
                        puerto.write(info.message)
                        res.json({
                            'err':0,
                            'msg':info.message
                        })
                    }
                } 
            }
        }
    })
    
})

app.get('/all/historics/',function(req,res){
    dh.history.find({},function(err,hst){
        if(err) res.json(err)
        else res.json(hst)
    })
}) 

app.get('/change/hist/before/:hst/:before',function(req,res){
    var info=req.params;
    dh.history.findById(ObjectId(info.hst),function(err,hyst){
        if(err) console.log(err);
        else{
            hyst.before=info.before;
            hyst.save(function(err){
                if(err) console.log(err);
                else{
                    res.json(hyst)
                }
            })
        }
    })
})

app.get('/redo/hst/:div',function(req,res){
    var info=req.params;
    dv.Vehiculos.findById(ObjectId(info.div),function(err,vhc){
        if(err) console.log(err);
        else{
            if(vhc.historycs) res.json('dispositivo con historial')
            else{
                console.log(vhc)
                RedoHist(vhc.posiciones,vhc._id,null,function(){
                    res.json('listo')
                })
            }
        }
    })
})

function RedoHist(id,vhc,date,callback) {
    dp.Position.findById(ObjectId(id),function(err,psc){
        if(err) console.log(err);
        else{
            //si no es la primera vez que entra a la funcion
            if(date){
                //si la fecha de la posicion es mayor
                if(Number(psc.date)>Number(date)){
                    dv.Vehiculos.findById(ObjectId(vhc),function(err,dps){
                        if(err) console.log(err);
                        else{
                            //si no es el primer historial
                            if(dps.historycs){
                                SFunctionHandler.historycs(dps,Number(psc.date),psc._id,function(err){
                                    if(psc.next) RedoHist(psc.next,vhc,Number(psc.date),callback)
                                    else callback()
                                })
                            }
                            //sino se crea el primer historial
                            else{
                                var historial = new dh.history({
                                    fechas: date,
                                    inicio: dps.posiciones,
                                    final:psc._id,
                                })
                                historial.save(function(err){
                                    if(err) {
                                        callback(1)
                                    }
                                    else{
                                        console.log(historial)
                                        dps.historycs=historial._id
                                        dps.save(function(err){
                                            if(err) {
                                                console.log(err);
                                                callback(1)
                                            }
                                            else{
                                                if(psc.next) RedoHist(psc.next,vhc,Number(psc.date),callback)
                                                else callback()
                                            }
                                        })
                                    }
                                })
                            }
                        }
                    })
                }
                else{
                    if(psc.next) RedoHist(psc.next,vhc,Number(psc.date),callback)
                    else callback()
                }
            }
            else{
                RedoHist(id,vhc,Number(psc.date),callback)
            }
        }
    })
}

app.listen(2100)