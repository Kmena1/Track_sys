var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;

 //models
//El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
var dc =require('./../../Top_end/models/Clientes_models.js');
var da = require('./../models/Archive.js');
var dv = require('./../models/Vehiculos_models.js');

//funciones
function addClient(index, datos, res) {
    //se busca el index
    dc.myclient.findById(ObjectId(index),function(err,indexC){
        if(err) console.log(err);
        else{
            datos=JSON.parse(datos)
            // se busca si existe un elemento igual en la base de dato sino se crea
            dc.clientes.findOne(datos, function(err, cln){
                if(err) console.log(err)
                else{
                    //sino exite coincidencia se crea uno nuevo y se indexa
                    if(cln==null){
                        var cliente = new dc.clientes(datos);
                        cliente.save(function(err){
                            if(err) console.log(err);
                            else{
                                //se asigna a mis clientes
                                if(indexC.id.length==0) 
                                    indexC.id.push({
                                        'region':cliente.region,
                                        'clientes':new Array(1).fill(cliente._id)
                                    }) 
                                else {for (let i = 0; i < indexC.id.length; i++) {
                                    if(indexC.id[i].region==cliente.region){
                                        console.log(indexC.id[i].clientes,cliente._id,indexC.id[i].clientes.indexOf(cliente._id))
                                        if(indexC.id[i].clientes.indexOf(cliente._id)==-1){
                                            indexC.id[i].clientes.push(cliente._id)
                                            i=indexC.length+1;
                                        }
                                    }   
                                    else {
                                        if(i==indexC.id.length-1) {
                                            indexC.id.push({
                                                'region':cliente.region,
                                                'clientes':new Array(1).fill(cliente._id)
                                            }) 
                                            i=indexC.length+1;
                                        }
                                    }       
                                }}
                                //indexC.id.push(cliente._id);
                                indexC.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        res.json(cliente);
                                    }
                                })
                            }
                        })
                    }
                    //si existe solo se ingresa al index
                    else{
                        if(indexC.id.length==0) indexC.id.push({
                                'region':cln.region,
                                'clientes':new Array(1).fill(cln._id)
                            }) 
                        else {
                            for (let i = 0; i < indexC.id.length; i++) {
                            if(indexC.id[i].region==cln.region) {
                                console.log(indexC.id[i].clientes,cln._id,indexC.id[i].clientes.indexOf(cln._id))
                                if(indexC.id[i].clientes.indexOf(cln._id)==-1){
                                    indexC.id[i].clientes.push(cln._id)
                                    i=indexC.length+1;
                                }
                            }
                            else {
                                if(i==indexC.id.length-1) {
                                    indexC.id.push({
                                        'region':cln.region,
                                        'clientes':new Array(1).fill(cln._id)
                                    }) 
                                    i=indexC.length+1;
                                }
                            }
                        }}
                        //indexC.id.push(cliente._id);
                        indexC.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json(cln);
                            }
                        })
                    }
                }
            })
        }
    })
}

function aceptarSolicitud(solitud,index,callback){
    dc.myclient.findById(ObjectId(index),function(err,indexC){
        if(err) console.log(err);
        else{
            for (let i = 0; i < indexC.id.length; i++) {
                if(indexC.id[i].region==cliente.region){
                    console.log(indexC.id[i].clientes,cliente._id,indexC.id[i].clientes.indexOf(cliente._id))
                    if(indexC.id[i].clientes.indexOf(cliente._id)==-1){
                        indexC.id[i].clientes.push(solitud)
                        i=indexC.length+1;
                    }
                }   
                else {
                    if(i==indexC.id.length-1) {
                        indexC.id.push({
                            'region':cliente.region,
                            'clientes':new Array(1).fill(solitud)
                        }) 
                        i=indexC.length+1;
                    }
                }
            }
        }
    })
} 

function addAgent(agente,cliente,divices,state,res){
    //busca el index del agente
    dc.myclient.findById(ObjectId(agente),function(err,indexC){
        if(err) console.log(err);
        else{
            console.log('index', indexC)
            //si el index esta vacio
            if(indexC.id.length ==0){
                //crea el index del usuario
                indexC.id.push({
                    'region':cliente.region,
                    'clientes':new Array(1).fill(cliente._id)
                })
                indexC.save(function(err){
                        if(err) console.log(err);
                        else{
                            //actualizacion del agente asignado a una empresa
                            dc.clientes.findById(ObjectId(cliente._id),function(err,cln){
                                if(err) console.log(err);
                                else{
                                    console.log(cln);
                                    //se le asigna el id del agente al cliente
                                    cln.agentes.push(indexC.empresa);
                                    cln.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            //se le asigna el criterio de archiving al cliente
                                            arch(divices,cliente,state);
                                            res.json({
                                                'err':0,
                                                'data':agente
                                            })
                                        }
                                    }) 
                                }
                            })
                        }
                })
            }
            else {
                //si hay datos en el index busca la region,
                for (let i = 0; i < indexC.id.length; i++) {
                    //si se encuentra la region
                    if(cliente.region == indexC.id[i].region){
                        //agregar agente
                        //console.log('cliente',cliente,'index',indexC, 'state', state)
                        //si se desea agragar
                        if(state){
                            console.log(indexC.id[i].clientes,cliente._id,indexC.id[i].clientes.indexOf(cliente._id))
                            //se verifica que el cliente ya no este ene el index
                            if(indexC.id[i].clientes.indexOf(cliente._id)==-1){
                                //se asigna el cliente dentro de la region
                                indexC.id[i].clientes.push(cliente._id)
                                //se sale del for
                                i=indexC.id.length+1;
                                indexC.save(function(err){
                                    //if(err) console.log(err);
                                    //else{
                                    dc.clientes.findById(ObjectId(cliente._id),function(err,cln){
                                        if(err) console.log(err);
                                        else{
                                            //se asigna el agente dentro del cliente
                                            cln.agentes.push(indexC.empresa);
                                            cln.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    //se asigna el criterio de archiving
                                                    arch(divices,cliente,state);
                                                    res.json({
                                                        'err':0,
                                                        'data':agente
                                                    })
                                                }
                                            }) 
                                        }
                                    })
                                    //}
                                })
                            }
                        }
                        //eliminar el agente
                        else{
                            //ellimina el cliente del index
                            indexC.id[i].clientes.splice(indexC.id[i].clientes.indexOf(cliente._id),1);
                            indexC.save(function(err){
                                if(err) console.log(err);
                                else{
                                    i=indexC.id.length+1;
                                    //actualizacion del cliente
                                    dc.clientes.findById(ObjectId(cliente._id),function(err,cln){
                                        if(err) console.log(err);
                                        else{
                                            //se elimina el agentee del cliente
                                            cln.agentes.splice(cln.agentes.indexOf(indexC.empresa),1);
                                            cln.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    //criterio de archiving
                                                    arch(divices,cliente,state);
                                                    res.json({
                                                        'err':0,
                                                        'data':agente
                                                    })
                                                }
                                            }) 
                                        }
                                    })
                                }
                            })
                        }
                    }
                    //si no exite la region debe crearse
                    else if(i == indexC.id.length-1 && state){
                        //crea la nueva region con el cliente
                        indexC.id.push({
                            'region':cliente.region,
                            'clientes':new Array(1).fill(cliente._id)
                        })
                        i=indexC.id.length+1;
                        indexC.save(function(err){
                            if(err) console.log(err);
                            else{
                                //actualizacion del cliente
                                dc.clientes.findById(ObjectId(cliente._id),function(err,cln){
                                    if(err) console.log(err);
                                    else{
                                        //se asigna el agente al cliente
                                        cln.agentes.push(indexC.empresa);
                                        cln.save(function(err){
                                            if(err) console.log(err);
                                            else{
                                                //criterio de archving
                                                arch(divices,cliente,state);
                                                res.json({
                                                    'err':0,
                                                    'data':agente
                                                })
                                            }
                                        }) 
                                    }
                                })
                            }
                        })
                    }
                }
            }
        }
    })
}

function arch(divices,cliente,state){
    //console.log(divices,cliente,state);
    //si el cliente no tiene el criterio de archiving se crea
    if(cliente.archving == null){
        var Arch=new da.Archive({
            rule:'alerta',
            property:"geocerca",
            reference:{
                ref1:cliente.lat,
                ref2:cliente.lon,
            },
            operator:"le",
            tolerance:100,
        })
        Arch.save(function(err){
            if(err) console.log(err);
            else{
                dc.clientes.findById(ObjectId(cliente._id),function(err,cln){
                    if(err) console.log(err);
                    else{
                        cln.archving=Arch._id;
                        cln.save(function(err){
                            if(err) console.log(err);
                            else{
                                arch(divices,cliente,state);   
                            }
                        })
                    }
                })   
            }
        })
    }
    else{
        dv.Vehiculos.findById(ObjectId(divices),function(err,dvc){
            if(err) console.log(err);
            else{
                if(state){
                    dvc.Archiving.push(cliente.archving);
                    dvc.save(function(err){
                        if(err) console.log(err);
                        else{
                            if(dvc.next != null) arch(dvc.next,cliente,state)
                        }
                    })
                }
                else{
                    dvc.Archiving.splice(dvc.Archiving.indexOf(cliente.archving));
                    dvc.save(function(err){
                        if(err) console.log(err);
                        else{
                            if(dvc.next != null) arch(dvc.next,cliente,state)
                        }
                    })
                }
            }
        })
    }
}
exports.addClient=addClient;
exports.agente=addAgent; 
exports.aceptarSolicitud=aceptarSolicitud