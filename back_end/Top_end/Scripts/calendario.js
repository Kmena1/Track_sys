//bibliotecas necesarias
var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;
//models
var dc = require('./../models/Calendario.js');

function asignar_actividad(hor,tipo,descripcion,inicio,final,res){
    console.log(hor,tipo,descripcion)
    dc.Horarios.findById(ObjectId(hor),function(err,hrr){
        if(err) console.log(err);
        else{
            dc.tareas.findOne({'descripcion':descripcion}, function(err, tarea){
                if(err) console.log(err)
                else{
                    if(tarea==null) {
                        var tar = new dc.tareas({
                            'descripcion':descripcion,
                            'inicio':String(new Date(inicio).getHours())+':'+String(new Date(inicio).getMinutes()),
                            'final':String(new Date(final).getHours())+':'+String(new Date(final).getMinutes()),
                        })
                        tar.save(function(err){
                            if(err) console.log(err);
                            else{
                                if(tipo==1)hrr.horario.push(tar._id)
                                else hrr.tareas.push(tar._id)
                                hrr.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        res.json(hrr)
                                    }
                                })
                            }
                        })
                    }
                    else{
                        if(tipo==1)hrr.horario.push(tarea._id)
                        else hrr.tareas.push(tarea._id)
                        hrr.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json(hrr)
                            }
                        })
                    }
                }
            })
        }
    })
}

function diaF(id,dia,tipo,descripcion,inicio,final,res){
    dc.Horarios.findById(ObjectId(id),function(err,hor){
        if(err) console.log(err);
        else{
            if(hor.dia==Number(dia)) asignar_actividad(hor._id,tipo,descripcion,inicio,final,res)
            else{
                if(hor.next==null){
                    var hrr= new dc.Horarios({
                        'dia':Number(dia),
                        'horario':[],
                        'tareas':[],
                    })
                    hrr.save(function(err){
                        if(err) console.log(err);
                        else{
                            hor.next=hrr._id;
                            hor.save(function(err){
                                if(err) console.log(err);
                                else{
                                    asignar_actividad(hrr._id,tipo,descripcion,inicio,final,res)
                                }
                            })
                        }
                    })
                }
                else diaF(hor.next,dia,tipo,descripcion,inicio,final,res)
            }
        }
    })
}

function crear_actividad(cal,mes,dia,tipo,descripcion,inicio,final,res) {
    console.log(cal,mes,dia,tipo,descripcion)
    dc.Meses.findById(ObjectId(cal),function(err,clnd){
        if(err) console.log(err);
        else{
            console.log(clnd,String(mes))
            if(clnd[String(mes)]==null){
                var hor= new dc.Horarios({
                    'dia':Number(dia),
                    'horario':[],
                    'tareas':[],
                })
                console.log(hor);
                hor.save(function(err){
                    if(err) console.log(err);
                    else{
                        clnd[String(mes)]=hor._id;
                        clnd.save(function(err){
                            if(err) console.log(err);
                            else{
                                asignar_actividad(hor._id,tipo,descripcion,inicio,final,res)
                            }
                        })
                    }
                })
            }
            else diaF(clnd[String(mes)],dia,tipo,descripcion,inicio,final,res)
        }
    })
}

exports.crear_actividad = crear_actividad;