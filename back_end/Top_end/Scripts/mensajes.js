var mongoose     = require('mongoose');
var path    = require("path");
var http = require('http');
var bodyParser = require('body-parser');
var ObjectId = require('mongodb').ObjectID;

//models
//El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
var du=require('./../models/Usuarios.js');
var dm=require('./../models/Mensajes.js')

function nuevoreciver(recv,mensajer,res){
    du.Usuarios.findById(ObjectId(recv),function(err,reciver){
        if(err) console.log(err);
        else{
            if(reciver.Mensajes_nuevos==null){
                reciver.Mensajes_nuevos=mensajer._id;
                reciver.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(reciver);
                    }
                })
            }
            else{
                dm.Mensajes.findById(ObjectId(reciver.Mensajes_nuevos),function(err,mns){
                    if(err) console.log(err);
                    else{
                        dm.Mensajes.findById(ObjectId(mensajer._id),function(err,mnsN){
                            if(err) console.log(err);
                            else{
                                mns.next=mnsN._id;
                                mnsN.before=mns._is;
                                reciver.Mensajes_nuevos=mensajer._id;
                                mns.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        mnsN.save(function(err){
                                            if(err) console.log(err);
                                            else{
                                               reciver.save(function(err){
                                                   if(err) console.log(err);
                                                   else{
                                                       res.json(reciver);
                                                   }
                                               }) 
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        }
    })
}

function nuevosender(send,recv,mensajes,mensajer,res) {
    du.Usuarios.findById(ObjectId(send),function(err,sender){
        if(err) console.log(err);
        else{
            console.log(sender.Mensajes_enviados)
            if(sender.Mensajes_enviados==null){
                sender.Mensajes_enviados=mensajes._id;
                sender.save(function(err){
                    if(err) console.log(err);
                    else{
                        nuevoreciver(recv,mensajer,res)
                    }
                })
            }
            else{
                dm.Mensajes.findById(ObjectId(sender.Mensajes_enviados),function(err,mns){
                    if(err) console.log(err);
                    else{
                        dm.Mensajes.findById(ObjectId(mensajes._id),function(err,mnsN){
                            if(err) console.log(err);
                            else{
                                mns.next=mnsN._id;
                                mnsN.before=mns._is;
                                sender.Mensajes_enviados=mensajes._id;
                                mns.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        mnsN.save(function(err){
                                            if(err) console.log(err);
                                            else{
                                               sender.save(function(err){
                                                   if(err) console.log(err);
                                                   else{
                                                       nuevoreciver(recv,mensajer,res)
                                                   }
                                               }) 
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        }
    })
}

function agregar_leidos(usuario,mensaje,callback){
    du.Usuarios.findById(ObjectId(usuario),function(err,usr){
        if(err) console.log(err);
        else{
            if(usr.Mensajes_leidos==null){
                usr.Mensajes_leidos=mensaje;
                usr.save(function(err){
                    if(err) console.log(err);
                    else{
                        callback(usr);
                    }
                })
            }
            else{
                dm.Mensajes.findById(ObjectId(usr.Mensajes_leidos),function(err,mns){
                    if(err) console.log(err);
                    else{
                        dm.Mensajes.findById(ObjectId(mensaje),function(err,mnsN){
                            if(err) console.log(err);
                            else{
                                mns.next=mnsN._id;
                                mnsN.before=mns._is;
                                usr.Mensajes_leidos=mensaje;
                                mns.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        mnsN.save(function(err){
                                            if(err) console.log(err);
                                            else{
                                               usr.save(function(err){
                                                   if(err) console.log(err);
                                                   else{
                                                       callback(usr);
                                                   }
                                               }) 
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        }
    })
}

function leido(mensaje,usuario,res){
    dm.Mensajes.findById(ObjectId(mensaje),function(err,mns){
        if(err) console.log(err);
        else{
            //si el primer mensaje el tag de befor debe estar en null
            if(mns.before==null){
                du.Usuarios.findById(ObjectId(usuario),function(err,usr){
                    if(err) console.log(err);
                    else{
                        usr.Mensajes_nuevos=mns.next;
                        usr.save(function(err){
                            if(err) console.log(err);
                            else{
                                //si tiene next se debe cambiar la referencia del before
                                if(mns.next!=null){
                                    dm.Mensajes.findById(ObjectId(mns.next),function(err,mnsN){
                                        if(err) console.log(err);
                                        else{
                                            mnsN.before=mns.before;
                                            mnsN.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    agregar_leidos(usuario,mensaje,callback(function(response){
                                                        mns.remove(function(err){
                                                            if(err)console.log(err);
                                                            else res.json(response)
                                                        })
                                                    }))
                                                }
                                            })
                                        }
                                    })
                                }    
                                //sino se procede a agregarlo a leidos
                                agregar_leidos(usuario,mensaje,callback(function(response){
                                    mns.remove(function(err){
                                        if(err)console.log(err);
                                        else res.json(response)
                                    })
                                }))
                            }
                        })
                    }
                })
            }
            //si tiene next se debe cambiar la referencia del before
            if(mns.next!=null){
                dm.Mensajes.findById(ObjectId(mns.next),function(err,mnsN){
                    if(err) console.log(err);
                    else{
                        mnsN.before=mns.before;
                        mnsN.save(function(err){
                            if(err) console.log(err);
                            else{
                                agregar_leidos(usuario,mensaje,callback(function(response){
                                    mns.remove(function(err){
                                        if(err)console.log(err);
                                        else res.json(response)
                                    })
                                }))
                            }
                        })
                    }
                })
            }    
            //sino se procede a agregarlo a leidos
            agregar_leidos(usuario,mensaje,callback(function(response){
                mns.remove(function(err){
                    if(err)console.log(err);
                    else res.json(response)
                })
            }))
        }
    })
}

exports.nuevo=nuevosender;
exports.leido=leido;