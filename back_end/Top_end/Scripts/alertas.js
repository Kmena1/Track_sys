var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;

 //models
//El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
var da = require('./../models/Alertas_model.js');
var de = require('./../models/Empresas_models.js');

//funciones
function crear_alerta(alerta,before,callback) {
    var alerta = new da.Alertas({
        fecha:new Date(),
        Descripcion:alerta.Descripcion,
        Page: alerta.Page,
        user: alerta.user,
        before:before,
    })
    alerta.save(function(err){
        if(err) console.log(err);
        else{
            if(before!=null){
                da.Alertas.findById(ObjectId(before),function(err,alt){
                    if(err) console.log(err);
                    else{
                        alt.next=alert._id
                        alt.save(function(err){
                            if(err) console.log(err);
                            else{
                                callback(alerta._id)
                            }
                        })
                    }
                })
            }
            else{
                callback(alerta._id)
            }
        }
    })
}

function Handler_arlerta(empresa,alerta,which,callback){
    de.Empresas.findById(ObjectId(empresa),function(err,emp){
        if(err) console.log(err);
        else{
            var alt=emp.Alertas[which]
            if(alt==undefined) callback({
                'err':1,
                'data':null
            })
            else{
                crear_alerta(alerta,alt.inicial,function(id){
                    emp.Alertas[which].inicial=id;
                    emp.Alertas[which].activa=true;
                    emp.save(function(err){
                        if(err) console.log(err);
                        else{
                            callback({
                                'err':0,
                                'data':emp.Alertas[which]
                            })
                        }
                    })
                })
            }
        }
    })
}

function eliminar(alerta,empresa,which,callback){
    da.Alertas.findById(ObjectId(alerta),function(err,alt){
        if(err) console.log(err);
        else{
            //primera alerta
            if(alt.before==null){
                de.Empresas.findById(ObjectId(empresa),function(err,emp){
                    if(err) console.log(err);
                    else{
                        if(emp.Alertas[which]!=undefined){
                            //si es la unica alerta
                            if(alt.next=null){
                                emp.Alertas[which].inicial=null;
                                emp.Alertas[which].activa=false;
                            }
                            else{
                                emp.Alertas[which].inicial=alt.next;
                            }
                            emp.save(function(err){
                                if(err) console.log(err);
                                else{
                                    alt.remove(function(err){
                                        if(err)console.log(err)
                                        else callback(emp.Alertas[which])
                                    })
                                }
                            })
                            
                        }
                    }
                })
            }
            else{
                da.Alertas.findById(ObjectId(alt.before),function(err,altb){
                    if(err) console.log(err);
                    else{
                        if(emp.Alertas[which]!=undefined){
                            //no hay mas alertas
                            if(alt.next=null){
                                altb.next=null
                                altb.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        alt.remove(function(err){
                                            if(err)console.log(err)
                                            else callback(emp.Alertas[which])
                                        })
                                    }
                                })
                            }
                            //se busca la siguiente alerta y se indexa
                            da.Alertas.findById(ObjectId(alt.next),function(err,altn){
                                if(err) console.log(err);
                                else{
                                    altb.next=altn._id;
                                    altn.before=altb._id;
                                    altn.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            altb.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    alt.remove(function(err){
                                                        if(err)console.log(err)
                                                        else callback(emp.Alertas[which])
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    }
                })
            }            
        }
    })
}

//exports
exports.crear=crear_alerta;
exports.handler=Handler_arlerta;
exports.eliminar=eliminar;

