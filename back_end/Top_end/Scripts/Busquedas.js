var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;

 //models
//El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
var du= require('./../models/Usuarios.js');
var dp= require('./../models/Posiciones_models.js');
var dv = require('./../models/Vehiculos_models.js');
var da = require('./../models/Alertas_model.js');
var darc=require('./../models/Archive.js');
var dc =require('./../models/Clientes_models.js');
var dcal=require('./../models/Calendario.js');
var di =require('./../models/inventario.js');
var dm=require('./../models/Mensajes.js')
var dh=require('./../models/history.js')
var dor=require('./../models/ordenes.js')
var dl= require('./../models/Log.js');

/** Generalidades
 * al usar indexacion las busquedas se deben realizar con funciones que se llamen a si mismas hasta el final
al mantener funciones sincronicas que no esperan callbacks o respuestas al final de una funcion esta llama a la sigueinte
haciendo parecer como si solo se usara una funcion
 */

//busqueda de usuarios, con el fin de encontrar todos los usuarios de una empresa con un estado en particular
//ocupa un arreglo, de preferencia vacio; el estaus que se quiere buscar, si es null se asume que se quiere todos
//el primer id que se quiere buscar, los datos extras a mandar y la funcion res del restfulApi
function find_users(resp,status,id,extra,res){
    //se busca el usario
    du.Usuarios.findById(ObjectId(id),function(err,user){
        //si hay un error se devuelve solo el extra y lo que se lleve acumulado en el arreglo de usuarios
        if(err) res.json({
            'err':1,
            'datos':resp,
            'extra':extra
        })
        else{
            //si el estatus a buscar es nulo automaticamente se guardan los usuarios
            if(status==null) {
                resp.push({
                    'id':user._id,
                    'nombre':user.Nombre,
                });
            }
            else{
                // en caso de que el id no sea nulo se verifica el estado de este
                if(user.root==status || user.root==4) resp.push({
                    'id':user._id,
                    'nombre':user.Nombre,
                    'cod':user.cod,
                    'dispositivos':user.dispositivos_last,
                    'user':user.user,
                    'clientes':user.clientes,
                    'nivel':user.root,
                });
            }
            //se verifica si existen otros usarios
            if(user.next==null){
                //si no existen otros usaurios se devuelve la informacion con el key de error en nulo
                res.json({
                    'err':0,
                    'datos':resp,
                    'extra':extra,
                }) 
            }
            //si hay otros usarios se vuelve a llamar a la funcion con el arreglo de usuarios y el siguiente usuario
            else find_users(resp,status,user.next,extra,res);
        }
    })
};

//buscar todos los historiales
function find_allhist(id,send,res){
    dh.history.findById(ObjectId(id),function(err,hst){
        if(err) console.log(err);
        else{
            send.push(hst);
            if(hst.before == null) res.json({
                l:send.length,
                data:send
            })
            else find_allhist(hst.before,send,res)
        }
    })
}


//buscar todos los registros
function findlogs(id,send,res){
    dl.log.findById(ObjectId(id),function(err,lgs){
        if(err) console.log(err);
        else{
            send.push(lgs);
            if(lgs.before == null) res.json({
                l:send.length,
                data:send
            })
            else findlogs(lgs.before,send,res)
        }
    })
}

//busca un dato en el historial
function find_historial(hist,keys,send,res){
    dh.history.findById(ObjectId(hist),function(err,hst){
        if(err) console.log(err);
        else{
            buscar_keys(keys,hst.final,0,5000,hst.inicio,[],res)
        }
    })
}

//esta funcion permite los elementos de un esquema especificados en el arrglo de keys
function buscar_keys(keys,id,index,max,until,salida,res){
    //console.log('id entrante:',id);
    if(until == null || id != until){
        dp.Position.findById(ObjectId(id),function(err,posiciones){
            if(err) console.log(err);
            else{
                //console.log(posiciones)
                //conseguir los datos
                if(posiciones ==null)res.json({
                    'err':1,
                    'last':null,
                    'data':salida,
                })
                //sin la posicion no es nula procede a armar los datos
                else{
                    //se rrecorre el arreglo de keys buscandose en el arreglo, si esta se guarda sino se toma como null
                    var dato ={};
                    for (var i = 0; i < keys.length; i++) {
                        //console.log(dato[keys[i]],keys)
                        if(posiciones[keys[i]] != undefined ){dato[keys[i]]=posiciones[keys[i]]} 
                        else {dato[keys[i]]=null}; 
                    }
                    salida.push(dato);
                }
                //se verifica si se cumplio el maximo o ya no hay mas datos
                if((index+2>max && max != 'all') || posiciones.before==null){
                    console.log(index)
                    if(posiciones.before==null)res.json({
                        'err':0,
                        'last':posiciones.before,
                        'data':salida,
                    })
                    else res.json({
                        'err':2,
                        'last':posiciones.before,
                        'data':salida,
                    })
                }
                //si no se ha alcazado el maximo se reitera aumentando el index
                else buscar_keys(keys,posiciones.before,index+1,max,until,salida,res)
            }
        })
    }
    else{
        res.json({
            'err':0,
            'itr':index,
            'data':salida,
        })
    }
}

//buscar keys en un fecha
function buscar_Datekeys(keys,id,index,max,until,dateSch,salida,res){
    if(until == null || id != until){
        dp.Position.findById(ObjectId(id),function(err,posiciones){
            console.log(posiciones,index)
            if(err) console.log(err);
            else{
                //conseguir los datos
                if(posiciones ==null)res.json({
                    'err':1,
                    'itr':index,
                    'data':salida,
                })
                //sin la posicion no es nula procede a armar los datos
                else{
                    //se rrecorre el arreglo de keys buscandose en el arreglo, si esta se guarda sino se toma como null
                    var dato ={};
                    for (var i = 0; i < keys.length; i++) {
                        if(posiciones[keys[i]] != undefined ){dato[keys[i]]=posiciones[keys[i]]} 
                        else {dato[keys[i]]=null}; 
                    }
                    salida.push(dato);
                }
                //se verifica si se cumplio el maximo o ya no hay mas datos
                if(index>max || posiciones.next==null || posiciones.date != dateSch){
                    if(posiciones.next==null)res.json({
                        'err':0,
                        'itr':index,
                        'data':salida,
                    })
                    else res.json({
                        'err':2,
                        'itr':index,
                        'data':salida,
                    })
                }
                //si no se ha alcazado el maximo se reitera aumentando el index
                else buscar_keys(keys,posiciones.next,index+1,max,until,dateSch,salida,res)
            }
        })
    }
    else{
        res.json({
            'err':0,
            'itr':index,
            'data':salida,
        })
    }
}

//busca los dispositivos asociados a un usuario
function buscar_divices(id,send,res){
    dv.Vehiculos.findById(ObjectId(id),function(err,disp){
        if(err) console.log(err);
        else{
            send.push({
                'id':disp._id,
                'imei':disp.imei,
                'unidad':disp.unidad,
                'nombre':disp.nombre,
                'posiciones_last':disp.posiciones_last,
                'Alertas_last':disp.Alertas_last,
                'Archiving':disp.Archiving
            })
            if(disp.next!=null) buscar_divices(disp.next,send,res)
            else res.json(send)
        }
    })
}

//busca alertas desde el id ingresado al final o hasta llegar al inicio
function buscar_alertas(id,i,max,send,res){
    da.Alert.findById(ObjectId(id),function(err,alt){
        if(err) console.log(err);
        else{
            send.push(alt);
            if(alt.before==null)res.json({
                'err':0,
                'final':true,
                'data':send
            })
            else{
                if(max-1==i)res.json({
                    'err':0,
                    'final':false,
                    'data':send
                })
                else buscar_alertas(alt.before,i+1,max,send,res)
            }   
        }
    })
}

function buscar_alertas_array(alertas,i,send,res) {
    da.Alertas.findById(ObjectId(alertas[i]),function(err,alt){
        if(err) console.log(err);
        else{
            send.push(alt);
            //condicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            //si verificar si el siguiente no es null
            if(i+1<alertas.length) buscar_alertas_array(alertas,i+1,send,res)
            else{
                //si se completo la busqueda
                res.json({
                    'err':2,
                    'data':send,
                    'last':i
                })
            }
        }
    })
}

function LatLonLast(users,i,send,res,last,divs,user){
    dv.Vehiculos.findById(ObjectId(divs),function(err,div){
        if(err) console.log(err);
        else{
            if(div.posiciones_last==null){
                if(div.next==null) {
                    send.push({
                        id:user,
                        pos:last
                    });
                    LastPos(users,i+1,send,res)
                }
                else LatLonLast(users,i,send,res,last,div.next,user)
            }
            else{
                dp.Position.findById(ObjectId(div.posiciones_last),function(err,pos){
                    if(err) console.log(err);
                    else{
                        if(last==null) {
                            last=[pos.lat,pos.lon,pos.date,pos.time];
                            send.push({
                                id:user,
                                pos:last
                            });
                            LastPos(users,i+1,send,res)
                        }
                    }
                })
            }
        }
    })
}

//busca la ultima posicion de cada usuario
function LastPos(users,i,send,res){
    if(i==users.length) res.json(send)
    else{
        du.Usuarios.findById(ObjectId(users[i].id),function(err,user){
            if(err) console.log(err);
            else{
                if(user.dispositivos==null)send.push({
                    id:user._id,
                    pos:null,
                })
                else{
                    LatLonLast(users,i,send,res,null,user.dispositivos,user._id)
                }
            }
        })
    }
}

function buscar_clientes(clientes,i,max,send,res) {
    //console.log(clientes,i,max,send);
    if(clientes.length>i && i<max){
        dc.clientes.findById(ObjectId(clientes[i]),function(err,cln){
            if(err) res.json({
                'err':2,
                'last':i,
                'max':clientes.length,
                'datos':send
            })
            else{
                send.push(cln);
                buscar_clientes(clientes,i+1,max,send,res)
            }
        })
    }
    else{
        //si se acabo por llegar al maximo, pero aun hay clientes
        if(clientes.length<i)res.json({
            'err':1,
            'last':i,
            'max':clientes.length,
            'datos':send
        })
        else res.json({
            'err':0,
            'last':i,            
            'max':clientes.length,
            'datos':send
        })
    }
}

function productoscod(productos,i,max,send,res) {
    di.objetos.findOne({codigo:productos[i]},function(err,obj){
        if(err) console.log(err);
        else{
            console.log(productos[i])
            send[productos[i]]=obj;
            //condicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            //si verificar si el siguiente no es null
            if(i+2<max && i+1<productos.length) productosArray(productos,i+1,max,send,res)
            else{
                //si se completo la busqueda
                if(i+1==productos.length){
                    res.json({
                        'err':2,
                        'data':send,
                        'last':i
                    })
                }
                //si se alcanzo el maximo
                //si la entrada de datos es un array indicar en el last el ultimo i, si es id indicar el siguiente
                else{
                    res.json({
                    'err':0,
                    'data':send,
                    'last':i
                    })
                }
            }
        }
    })
}

function productosArray(productos,i,max,send,res) {
    di.objetos.findById(ObjectId(productos[i]),function(err,obj){
        if(err) console.log(err);
        else{
            console.log(productos[i])
            send[productos[i]]=obj;
            //condicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            //si verificar si el siguiente no es null
            if(i+2<max && i+1<productos.length) productosArray(productos,i+1,max,send,res)
            else{
                //si se completo la busqueda
                if(i+1==productos.length){
                    res.json({
                        'err':2,
                        'data':send,
                        'last':i
                    })
                }
                //si se alcanzo el maximo
                //si la entrada de datos es un array indicar en el last el ultimo i, si es id indicar el siguiente
                else{
                    res.json({
                    'err':0,
                    'data':send,
                    'last':i
                    })
                }
            }
        }
    })
}

function Clientes_Array(clientes,i,max,send,res) {
    dc.clientes.findById(ObjectId(clientes[i]),function(err,cln){
        if(err) console.log(err);
        else{
            send[clientes[i]]=cln;
            //considicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            //si verificar si el siguiente no es null
            if(i+2<max && i+1<clientes.length) Clientes_Array(clientes,i+1,max,send,res)
            else{
                //si se completo la busqueda
                if(i+1==clientes.length){
                    res.json({
                        'err':2,
                        'data':send,
                        'last':i
                    })
                }
                //si se alcanzo el maximo
                //si la entrada de datos es un array indicar en el last el ultimo i, si es id indicar el siguiente
                else{
                    res.json({
                        'err':0,
                        'data':send,
                        'last':i
                    })
                }
            }
        }
    })
}

function buscar_ArchSetts(Arch,i,send,res){
    darc.Archive.findById(ObjectId(Arch[i]),function(err,ArchStt){
        if(err) res.json({
            err:1,
            data:send
        })
        else{
            send.push(ArchStt);
            if(i<Arch.length-1) buscar_ArchSetts(Arch,i+1,send,res);
            else res.json({
                err:0,
                data:send
            })
        }
    })
}

//busquedas de inventarios
//colleciones
//busca entre el inventario las colecciones, dentro de cada coleccion los objectos cuando recorrio todos los objetos de una
//coleccion se coloca col en null y la siguiente iteracion se asignara la siguiente collecion o se enviara el resultado
function findInventario(invt,col,obj,i,send,res) {
    //console.log(invt,col,obj,i,send)
    //si se requiere cambiar de coleccion
    if(col==null){
        //si aun no se recorren todas las colecciones
        if(invt.categoria.length>i){
            if(send['keys']==undefined) send['keys']=[];
            send['keys'].push(invt.categoria[i])
            send[invt.categoria[i].nombre]=new Array(0);
            findInventario(invt,invt.categoria[i].nombre,invt.categoria[i].id,i+1,send,res)
        }
        //si se recorrio toda la coleccion se envia 
        else{
            res.json({
                'err':0,
                'data':send,
                'keys':Object.keys(send)
            })
        }
    }
    //si sigue la coleccion
    else{
        //si acaba la coleccion
        if(obj==null){
            findInventario(invt,null,obj,i,send,res)
        }
        //si la collecion aun tiene objetos
        else{
            di.objetos.findById(ObjectId(obj),function(err,objt){
                if(err) console.log(err);
                else{
                    if(objt){
                        send[col].push(objt);
                        findInventario(invt,col,objt.next,i,send,res)
                    }
                    else findInventario(invt,null,obj,i,send,res)
                }
            })
        }
    }
}

function findRevInvt(obj,send,res){
    console.log(obj)
    di.objetos.findById(ObjectId(obj),function(err,objt){
        if(err) console.log(err);
        else{
            if(objt){
                send.push(objt);
                if(objt.before)findRevInvt(objt.before,send,res)
                else res.json(send)
            }
            else res.json(send)
        }
    })
}

//busca el catalogo de una empresa
function findcatalogo(invt,col,obj,i,send,res) {
    //console.log(invt,col,obj,i,send)
    //si se requiere cambiar de coleccion
    if(col==null){
        //si aun no se recorren todas las colecciones
        if(invt.categoria.length>i){
            //si la categoria es de un producto terminado
            if(invt.categoria[i].tipo=='PT'){
                send[invt.categoria[i].nombre]=new Array(0);
                findcatalogo(invt,invt.categoria[i].nombre,invt.categoria[i].id,i+1,send,res)
            }
            else{
                findcatalogo(invt,null,obj,i+1,send,res)
            }
        }
        //si se recorrio toda la coleccion se envia 
        else{
            res.json({
                'err':0,
                'data':send,
                'keys':Object.keys(send)
            })
        }
    }
    //si sigue la coleccion
    else{
        //si acaba la coleccion
        if(obj==null){
            findcatalogo(invt,null,obj,i,send,res)
        }
        //si la collecion aun tiene objetos
        else{
            di.objetos.findById(ObjectId(obj),function(err,objt){
                if(err) console.log(err);
                else{
                    if(objt){
                        if(objt.Incatalogo){
                            if(objt.codigo==null) objt.codigo='0000'
                                send[col].push({
                                        nombre:objt.nombre.replace('\t\t',''),
                                        precio:objt.precio,
                                        descripcion:objt.descripcion,
                                        //imagen:null,
                                        //objeto:objt._id,
                                        trigger:objt.trigger,
                                        exentos:objt.exentos,
                                        Dolares:objt.Dolares,
                                        codigo:objt.codigo.replace('\t\t',''),
                                        cantidad:objt.cantidad,
                                        contenido_neto:objt.contenidoNeto,
                                        unidad:objt.unidad,
                                        id:objt._id,
                                        receta:objt.receta,
                                        convertFactor:objt.convertFactor,
                                        distribuidores:objt.distribuidores

                                });
                            
                                findcatalogo(invt,col,objt.next,i,send,res)
                        }
                        else findcatalogo(invt,col,objt.next,i,send,res)
                    }
                    else findcatalogo(invt,null,obj,i,send,res)
                }
            })
        }
    }
}

function findMateria(invt,col,obj,i,send,res) {
    //console.log(invt,col,obj,i,send)
    //si se requiere cambiar de coleccion
    if(col==null){
        //si aun no se recorren todas las colecciones
        if(invt.categoria.length>i){
            //si la categoria es de un producto terminado
            if(invt.categoria[i].tipo!='PT' ){
                send[invt.categoria[i].nombre]=new Array(0);
                findMateria(invt,invt.categoria[i].nombre,invt.categoria[i].id,i+1,send,res)
            }
            else{
                findMateria(invt,null,obj,i+1,send,res)
            }
        }
        //si se recorrio toda la coleccion se envia 
        else{
            res.json({
                'err':0,
                'data':send,
                'keys':Object.keys(send)
            })
        }
    }
    //si sigue la coleccion
    else{
        //si acaba la coleccion
        if(obj==null){
            findMateria(invt,null,obj,i,send,res)
        }
        //si la collecion aun tiene objetos
        else{
            di.objetos.findById(ObjectId(obj),function(err,objt){
                if(err) console.log(err);
                else{
                    if(objt){
                        send[col].push({
                                nombre:objt.nombre,
                                precio:objt.precio,
                                descripcion:objt.descripcion,
                                //imagen:null,
                                //objeto:objt._id,
                                trigger:objt.trigger,
                                exentos:objt.exentos,
                                Dolares:objt.Dolares,
                                codigo:objt.codigo,
                                cantidad:objt.cantidad,
                                contenido_neto:objt.contenidoNeto,
                                unidad:objt.unidad,
                                id:objt._id,
                                receta:objt.receta,
                                convertFactor:objt.convertFactor,
                                distribuidores:objt.distribuidores
                        });
                        findMateria(invt,col,objt.next,i,send,res)
                    }
                    else findMateria(invt,null,obj,i,send,res)
                }
            })
        }
    }
}

function busquedaReservas(reservas,i,max,send,res) {
    if(reservas.length>0){
        di.reservas.findById(ObjectId(reservas[i]),function(err,resv){
            if(err) console.log(err);
            else{
                send.push(resv);
                //considicion de finalizacion
                //si la entrada de datos es un array comparar el tamano de la entrada con i+1, 
                //si verificar si el siguiente no es null
                if(i+2<max && i+1<reservas.length) busquedaReservas(reservas,i+1,max,send,res)
                else{
                    //si se busco en todo
                    if(i+1==reservas.length){
                        res.json({
                            'err':0,
                            'data':send,
                            'last':i
                        })
                    }
                    //si se alcanzo el maximo
                    //si la entrada de datos es un array indicar en el last el ultimo i, si es id indicar el siguiente
                    else{
                        res.json({
                            'err':2,
                            'data':send,
                            'last':i
                        })
                    }
                }
            }
        })
    }
    else{
        res.json({
            'err':0,
            'data':send,
            'last':i
        })
    }
}

function buscarPedidos(id,i,max,send,res) {
    //console.log(id,i,max,send)
    di.pedido.findById(ObjectId(id),function(err,pedido){
        if(err) console.log(err);
        else{
            //console.log(pedido)
            send.push(pedido);
            //considicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            //si verificar si el siguiente no es null
            if(i+2<max && pedido.next!=null) buscarPedidos(pedido.next,i+1,max,send,res)
            else{
                //si se completo la busqueda
                if(pedido.next==null){
                    res.json({
                        'err':0,
                        'data':send,
                        'last':pedido.next
                    })
                }
                //si se alcanzo el maximo
                //si la entrada de datos es un array indicar en el last el ultimo i, si es id indicar el siguiente
                else{
                    res.json({
                    'err':2,
                    'data':send,
                    'last':pedido.next
                    })
                }
            }
        }
    })
}


//busaqueda de horarios
function buscar_horarios(id,send,res) {
    dcal.Horarios.findById(ObjectId(id),function(err,hrr){
        if(err) console.log(err);
        else{
            send.push(hrr);
            if(hrr.next != null ) buscar_horarios(hrr.next,send,res)
            else res.json({
                'err':0,
                'data':send
            })

        }
    })
}

function busacar_tareas(tareas,i,max,send,res) {
    dcal.tareas.findById(ObjectId(tareas[i]),function(err,trr){
        if(err) console.log(err);
        else{
            send.push(trr);
            //considicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            //si verificar si el siguiente no es null
            if(i+2<max && i+1<tareas.length) busacar_tareas(tareas,i+1,max,send,res)
            else{
                //si se completo la busqueda
                if(i+1==tareas.length){
                    res.json({
                        'err':2,
                        'data':send,
                        'last':i
                    })
                }
                //si se alcanzo el maximo
                //si la entrada de datos es un array indicar en el last el ultimo i, si es id indicar el siguiente
                else{
                    res.json({
                    'err':0,
                    'data':send,
                    'last':i
                    })
                }
            }
        }
    })
}

function Mensajes(mensaje,i,max,send,res){
    dm.Mensajes.findById(ObjectId(mensaje),function(err,mns){
        if(err) console.log(err);
        else{
            send.push(mns);
            if(i>=max || mns.next==null) res.json({
                err:0,
                data:send
            })
            else{
                if(mns.next!=null){
                    res.json({
                        err:1,
                        data:send
                    })
                }
                else{
                    res.json({
                        err:0,
                        data:send
                    })
                }
            }
        }
    })
}

function recetas(id,i,max,send,res) {
    console.log(id,i,max)
    di.recetas.findById(ObjectId(id),function(err,rct){
        if(err) console.log(err);
        else{
            send.push(rct);
            if(rct.next!=null && i+1<max) recetas(rct.next,i+1,max,send,res)
            else{
                if(rct.next==null)res.json({
                    err:0,
                    data:send
                })
                else res.json({
                    err:1,
                    data:send
                })
            }
        }
    })
}

function orednesSalida(id,i,max,send,callback){
    if(id !=null && i<max){
        dor.Salida.findById(ObjectId(id),function(err,ord){
            if(err) console.log(err);
            else{
                send.push(ord)
                orednesSalida(ord.before,i+1,max,send,callback)
            }
        })
    }
    else callback(send)
}

function orednesProduccion(id,i,max,send,callback){
    if(id!=null && i<max){
        dor.Produccion.findById(ObjectId(id),function(err,ord){
            if(err) console.log(err);
            else{
                send.push(ord)
                orednesProduccion(ord.before,i+1,max,send,callback)
            }
        })
    }
    else callback(send)
}

function orednesFrozen(id,i,max,send,callback){
    if(id!=null && i<max){
        dor.Frozen.findById(ObjectId(id),function(err,ord){
            if(err) console.log(err);
            else{
                send.push(ord)
                orednesFrozen(ord.before,i+1,max,send,callback)
            }
        })
    }
    else callback(send)
}

function findOrdenes(ordenes,max,res){
    orednesSalida(ordenes.Salida,0,max,[],function(salida){
        orednesProduccion(ordenes.Produccion,0,max,[],function(Produccion){
            orednesFrozen(ordenes.Frozen,0,max,[],function(Frozen){
                res.json({
                    salida:salida,
                    Produccion:Produccion,
                    Frozen:Frozen,
                })
            })
        })
    })
}

function proveedoresFunction(proveedores,i,send,res) {
    di.distribuidores.findById(ObjectId(proveedores[i]),function(err,prv){
        if(err) console.log(err);
        else{
            send.keys.push(prv.nombre);
            send.data[prv.nombre]=prv;
            //condicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            //si verificar si el siguiente no es null
            if(i+1<proveedores.length) proveedoresFunction(proveedores,i+1,send,res)
            else{
                //si se completo la busqueda
                if(i+1==proveedores.length){
                    res.json({
                        'err':2,
                        'data':send,
                        'last':i
                    })
                }
                //si se alcanzo el maximo
                //si la entrada de datos es un array indicar en el last el ultimo i, si es id indicar el siguiente
                else{
                    res.json({
                    'err':0,
                    'data':send,
                    'last':i
                    })
                }
            }
        }
    })
}

exports.findOrdenes=findOrdenes;
exports.find_allhist=find_allhist;
exports.find_historial=find_historial;
exports.LastPos=LastPos;
exports.buscar_alertas=buscar_alertas;
exports.buscar_alertas_array=buscar_alertas_array;
exports.buscar_divices=buscar_divices;
exports.buscar_keys=buscar_keys;
exports.buscar_Datekeys=buscar_Datekeys;
exports.Busq_user=find_users;
exports.clientes=buscar_clientes;
exports.Clientes_Array=Clientes_Array;
exports.Arch=buscar_ArchSetts;
exports.findInventario=findInventario;
exports.findRevInvt=findRevInvt;
exports.findcatalogo=findcatalogo;
exports.findMateria=findMateria;
exports.recetas=recetas;
exports.busquedaReservas=busquedaReservas;
exports.buscarPedidos=buscarPedidos
exports.buscar_horarios=buscar_horarios;
exports.busacar_tareas=busacar_tareas;
exports.Mensajes=Mensajes;
exports.productosArray=productosArray;
exports.proveedores=proveedoresFunction;
exports.findlogs=findlogs
exports.productoscod=productoscod