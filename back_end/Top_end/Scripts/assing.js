//notas: revisar el reassing por si no detecta el arreglo vacio

var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;

 //models
//El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
var di =require('./../../Top_end/models/Imeis_models.js');
var dv = require('./../models/Vehiculos_models.js');
var da = require('./../models/Archive.js');
var dh = require('./../models/history.js');
var dp = require('./../models/Posiciones_models.js');

function asignar(id,freeid,header){
    console.log(freeid,header)
    //revisa si no hay un dispositivo libre
    if(freeid==null){
        console.log('no se puede asignar id con el header', header);
        //busca la base de datos el esquema de sispositivo no asignados si no existe con este header se crea la lista
        dv.unassigned.findOne(header, function(err,unsig){
            if(err) console.log(err);
            else{
                console.log('point 1',unsig,id);
                if(unsig==null){
                    header['ids']=new Array(1).fill(id)
                    console.log(header)
                    var uns = new dv.unassigned(header)
                    uns.save(function(err){
                        if(err) console.log(err);
                    })
                }
                else{
                    unsig.ids.push(id);
                    unsig.save(function(err){
                        if(err) console.log(err);
                    })
                }
                dv.Vehiculos.findById(ObjectId(id),function(err,divice){
                    if (err) console.log(err);
                    else{
                        divice.imei=new Array(0);
                        divice.save();
                    }
                })
            }
        })
    }
    //si hay dispositivos de espera se asigna este dispositivo
    else{
        dv.Vehiculos.findById(ObjectId(id),function(err,divice){
            if (err) console.log(err);
            else{
                divice.imei.push(freeid.imei);
                freeid.Asign=true;
                freeid.asign_to=divice._id;
                divice.save();
                freeid.save();
            }
        })
    }
}

//asigna el id al los dispositivos
function assing_id(id,header){
    //busca el header de los datos y un dispositivo no asignado
    //si el header es solo uno se realiza solo una iteracion
    if(header.length==undefined){
        header['Asign']=false;
        di.imeis.findOne(header,function(err,freeid){
            if(err) console.log(err);
            else{
                delete  header.Asign
                asignar(id,freeid,header);
            }
        });
    }
    //si es un arreglo de headers lo quue se ocupa se pregunata por cada uno
    else{
        for (var i = 0; i < header.length; i++) {
            header[i]['Asign']=false
            di.imeis.findOne(header[i],function(err,freeid){
                if(err) console.log(err);
                else{
                    delete  header[i].Asign
                    asignar(id,freeid,header[i]);
                }
            });
        }
    }
}


//se le asigna un id a un vehiculo en lista si el dispositivo esta en lista de espera
function reassing_id(id,header){
    console.log('reassignar',header);
    //busaca el header en la lista de esperaw
    dv.unassigned.findOne(header, function(err,unsig){
        if(err) console.log(err);
        else{
            //revisa si no hay dispositivo en lista de espera
            if(unsig == null) console.log('no hay dispositivos esperando el header', header)
            else{
                if(unsig.ids.length==0) console.log('no hay dispositivos esperando el header', header)
                else{
                    //busca el dispoitivo
                    di.imeis.findById(ObjectId(id),function(err,freeid){
                        if(err) console.log(err);
                        else{
                            //busca un dispositivo en la lista de espera
                            dv.Vehiculos.findById(ObjectId(unsig.ids[0]),function(err,divice){
                                if(err) console.log(err);
                                else{
                                    //se asigna el id
                                    divice.imei.push(freeid.imei);
                                    freeid.Asign=true;
                                    freeid.asign_to=divice._id;
                                    divice.save();
                                    freeid.save();
                                    //se elimina el dispositivo asignado
                                    unsig.ids.splice(0,1);
                                    unsig.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            console.log(unsig);
                                        }
                                    });
                                }
                            })
                        }
                        
                    })
                }
            }
        }
    })
}

function achieve(id,settings,res){
    da.Archive.findOne(settings, function(err, Arch){
        if(err) console.log(err)
        else{
            if(Arch==null){
                var NewArch= new da.Archive(settings);
                console.log('nuevo criterio', NewArch);
                NewArch.save(function(err){
                    if(err) console.log(err);
                    else{
                        dv.Vehiculos.findById(ObjectId(id),function(err,veh){
                            if(err) console.log(err);
                            else{
                                veh.Archiving.push(NewArch._id);
                                veh.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        res.json({
                                            'Arch':veh.Archiving
                                        })
                                    }
                                })
                            }
                        })
                    }
                })

            } 
            else{
                console.log('criterio ',Arch);
                dv.Vehiculos.findById(ObjectId(id),function(err,veh){
                    if(err) console.log(err);
                    else{
                        veh.Archiving.push(Arch._id);
                        veh.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json(veh)
                            }
                        })
                    }
                })
            }
        }
    })
}

function SaveMake(id,fecha,since,to,callback){
    dv.Vehiculos.findById(ObjectId(id),function(err,device){
        if(err) console.log(err);
        else{   
            var history= new dh.history({
                fechas: fecha ,
                inicio: since,
                final: to,
                before: device.historycs,
            })
            history.save(function(err){
                if(err) console.log(err);
                else{
                   device.historycs=history._id;
                   device.save(function(err){
                       if(err) console.log(err);
                       else{
                            callback()
                       }
                   })
                }
            })
        }
    })
}

//realiza el proceso de histories de un vehiculo
function histories(id,dispositivo,i,j,last,date,res){
    dp.Position.findById(ObjectId(id),function(err,pos){
        if(err) console.log(err);
        else{
            if(date==null) histories(pos.before,dispositivo,1,1,last,pos.date,res);
            else{
                //si las fechas son diferentes se crea el nuevo historial
                if(date!=pos.date){
                    console.log('historial '+String(i)+' size '+String(j));
                    SaveMake(dispositivo,date,pos._id,last,function(){
                        if(pos.before==null)res.json('historiales creados: '+String(i))
                        else histories(pos.before,dispositivo,i+1,0,pos.before,pos.date,res)
                    })
                }
                else{
                    //si el anterior es null se crea el ultimo historial y se devuelve
                    if(pos.before==null){
                        SaveMake(dispositivo,date,pos._id,last,function(){
                            res.json('historiales creados: '+String(i))
                        })
                    }
                    //sino busca mas atras
                    else histories(pos.before,dispositivo,i,j+1,last,date,res)
                }
            }
        }
    })
}

exports.histories=histories;
exports.assig=assing_id;
exports.reassig=reassing_id;
exports.achieve=achieve;