/**

 */

//bibliotecas necesarias
var mongoose     = require('mongoose');
var path    = require("path");
var http = require('http');
var bodyParser = require('body-parser');
var ObjectId = require('mongodb').ObjectID;

//models
var da = require('./../models/Alertas_model.js');
var de = require('./../models/Empresas_models.js');
var du = require('./../models/Usuarios.js');

//scripts
var alertas = require('./Scripts/alertas.js')

module.exports=function(app){
    console.log('Alertas ready');
    //creacion de alertas
    //empresa
    app.post('/crear/alerta/',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                var alt=emp.Alertas[info.which]
                if(alt==undefined) res.json({
                    'err':1,
                    'data':null
                })
                else{
                    alertas.crear(info.data,alt.inicial,function(id){
                        emp.Alertas[info.which].inicial=id;
                        emp.Alertas[info.which].activa=true;
                        emp.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json({
                                    'err':0,
                                    'data':emp.Alertas[info.which]
                                })
                            }
                        })
                    })
                }
            }
        })
    })

    app.post('/eliminar/alerta',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                alertas.eliminar(info.alerta,info.empresa,info.which,function(response){
                    res.json(response)
                })
            }
        })
    })
}