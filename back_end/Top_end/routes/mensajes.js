module.exports=function(app){
    console.log('mensajes ready');
    //bibliotecas necesarias
    var mongoose     = require('mongoose');
	var path    = require("path");
	var http = require('http');
	var bodyParser = require('body-parser');
    var ObjectId = require('mongodb').ObjectID;

    //models
    //El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
    var du=require('./../models/Usuarios.js');
    var dm=require('./../models/Mensajes.js')

    //scripts
    var mensaje = require('./../Scripts/mensajes.js');
    var busquedas = require('./../Scripts/Busquedas.js');
    
    //routes
    /**
     * generalidades:
     * -la variable info guarda los datos que se envian al route ya sea el body o el params
     */
    app.post('/nuevo/mensaje/',function(req,res){
        var info=req.body;
        var mensajes = new dm.Mensajes({
            envia:info.sender,
            para:info.reciver,
            asunto:info.asunto,
            mensaje:info.mensj,
            next:null,
            before:null,
        })
        var mensajesr = new dm.Mensajes({
            envia:info.sender,
            para:info.reciver,
            asunto:info.asunto,
            mensaje:info.mensj,
            next:null,
            before:null,
        })
        mensajes.save(function(err){
            if(err) console.log(err);
            else{
                mensajesr.save(function(err){
                    if(err) console.log(err);
                    else{
                        //si se guarda se envia el script que guardara el mensaje tanto en el que envia como en el que recibe 
                        mensaje.nuevo(info.sender,info.reciver,mensajes,mensajesr,res)   
                    }
                })
            }
        })
    })

    //busca los mensajes, dependiendo del tipo busca los mensajes nuevo los leidos o los enviados
    app.get('/get/mensaje/:user/:max/:tipo',function(req,res){
        var info=req.params;
        du.Usuarios.findById(ObjectId(info.user),function(err,usr){
            if(err) console.log(err);
            else{
                switch (Number(info.tipo)) {
                    //nuevos
                    case 0:
                        if(usr.Mensajes_nuevos!=null) busquedas.Mensajes(usr.Mensajes_nuevos,0,info.max,new Array(0),res);
                        else res.json(new Array(0))
                        break;
                    //leidos
                    case 1:
                        if(usr.Mensajes_leidos!=null) busquedas.Mensajes(usr.Mensajes_leidos,0,info.max,new Array(0),res);
                        else res.json(new Array(0))
                        break;
                    //enviados
                    case 2:
                        if(usr.Mensajes_enviados!=null) busquedas.Mensajes(usr.Mensajes_enviados,0,info.max,new Array(0),res);
                        else res.json(new Array(0))
                        break;
                    default:
                        break;
                }
            }
        })
    })

    app.post('/Mensaje/leido/',function(req,res){
        var info=req.params;
        mensaje.leido(info.mensaje,info.user,res)
    })
}