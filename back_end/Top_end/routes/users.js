module.exports=function(app){
    console.log('users ready');
    //bibliotecas necesarias
    var mongoose     = require('mongoose');
	var path    = require("path");
	var http = require('http');
	var bodyParser = require('body-parser');
    var ObjectId = require('mongodb').ObjectID;

    //models
     //models
    //El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
    var de=require('./../models/Empresas_models.js');
    var du=require('./../models/Usuarios.js');
    var dl=require('./../models/Localizaciones.js');
    var dv=require('./../models/Vehiculos_models.js')

    //scripts
    //funciones usadas para buscar usarios
    var search=require('./../Scripts/Busquedas.js')

    //routes
    /**
     * generalidades:
     * -la variable info guarda los datos que se envian al route ya sea el body o el params
     */

/****
--index
crear
    --crear empresa /crear/empresa
    --crear usaurio /crear/usuario/
modificar
    --editar un usuario /edit/usuario/
    --actualizacion de la informacion de los clientes /user/act/clientes/
    -- ediicion de la informacion de la empresa/edit/empresa/
    --edidicion de la informacion de facturacion de la empresa /editar/facturacion/empresa/
    --asignar un usuario a una empresa /assignar/user
busqueda
    --busqueda de los datos de un usaurio /find/user/:usr
    --busqueda de una empresa y sus usuarios de nivel 2 /find/empresa/:empresa
    --busqueda de una empresa y sus usuarios de un nivel establecido/usuarios/empresa/:empresa/:level
    --busqueda unicamente de los datos de un empresa /find/company/:empresa
    --busqueda de todas las empresas /find/empresas/all
eliminar
    --eliminancion de un usuario /user/eliminar/
indefinidos
transporte
    --busqueda de transporte de una empresa /empresa/transportes/:emp
    --creacion de transporte de una empresa /empresa/nuevoTransporte
    --eliminacion de transporte de una empresa /empresa/eliminar/Transporte
costos
    --actualizacion de costos de una empresa /actualizar/costos
login
    --/login/
obsoletos
    --asignar dispositivos a un vehiculo (obsoleto) /assignar/divice
    --desasignar dispositivos a un vehiculo (obsoleto) /undivices/users
    
*/
//-------crear
     //crea una empresa
     /**
      * se ocupa enviar en el JSON:
      * Name :nombre de la empresa
        Ced_J: cedula juridiaca de la empresa
        phone: telefono de la empresa
        email: correo de la empresa
        Cont_Name: nombre del contacto directo
        Cont_ID: cedula del contacto directo
        Cont_phone:telefono del contacto directo
        Cont_email: correo del correo directo
      */
    app.post('/crear/empresa',function(req,res){
        var info=req.body;
        //creacion de esquemas anexados a la empresa
        var loc=new dl.Localizacion({
            'oficina':new Array(0),
            'planteles':new Array(0),
            'talleres':new Array(0),
        });
        var veh=new dv.Vehiculos();
        loc.save();
        veh.save();
        //creacion de la empresa
        var empresa = new de.Empresas({
            'Company'	  : info.Name,
			'cedula'      : info.Ced_J,
			'phone'       : info.phone,
			'email'       : info.email,
			'fechai'      : new Date(),
			'Cont_imm'	  : { 
                'nombre': 	info.Cont_Name,
                'cedula': 	info.Cont_ID,
                'telefono':	info.Cont_phone,
                'correo':		info.Cont_email,
            },
            'localizaciones': loc._id,
            'vehiculos': veh._id,
            'usuarios': null,
            'Last_users':null,
        });
        //guarda la empresa
        empresa.save(function(err){
            if(err) console.log(err)
            else res.json({
                'empresa':empresa._id,
                'localizaciones':empresa.localizaciones,
                'vehiculos':empresa.vehiculos,
                'usuarios':empresa.usuarios,
            })
        });
    });

    //crea un usario
    /**
     * se ocupa enviar en el json
     * nombre:nombre del usuario
        email:correo del usuario
        cedula:cedula del usuario
        numero_tel: telefono del usuario
        tipo: tipo del usuario
        user: nombre de usuario del usuario
        password: password del usuario
        empresa: id de la empresa a la cual se asocia el usuario
     */
    app.post('/crear/usuario/',function(req,res){
        var info=req.body;
        console.log(info);
        //creacion del usuario
        du.Usuarios.findOne({'user'	: info.user}, function(err, user){
            if(err) console.log(err)
            else{
                //si el usuario no existe se crea
                if(user==null){
                    var usuario =  new du.Usuarios({
                        'Nombre':		info.nombre,
                        'Mail':		info.email,
                        'cedula':		info.cedula,
                        'numero_tel': info.numero_tel,
                        'root':		info.tipo,
                        'user'		: info.user,
                        'password'	: info.password,
                        'empresa'   : info.empresa,
                    });
                    //se guarda el 
                    if(info.empresa==null) 
                        usuario.save(function(err){
                            if(err) res.json({'err':1})
                            else{
                                res.json({
                                    'err':0,
                                    'usuario':usuario._id
                                })
                            }
                        })
                    else{
                         de.Empresas.findById(ObjectId(info.empresa), function(err, empresa){
                            if(err) res.json({'err':2})
                            else{
                                usuario.save(function(err){
                                    if(err) res.json({'err':1})
                                    else{
                                        //si no hay usuarios en siguiente se pone el usuario creado como el primero y de ultimo
                                        if(empresa.usuarios== null){
                                            empresa.usuarios=usuario._id;
                                            empresa.Last_users=usuario._id;
                                            empresa.save(function(err){
                                                if(err) res.json({'err':1})
                                                else res.json({
                                                    'err':0,
                                                    'usuario':usuario._id
                                                })
                                            })
                                        }
                                        //sino se actualiza el siguiente del ultimo usuario y el ultimo usuario de la empresa
                                        else{
                                            //se busca el ultimo usuario
                                            du.Usuarios.findById(ObjectId(empresa.Last_users),function(err,usr){
                                                if(err) res.json({'err':2})
                                                else{
                                                    //actualizan los datos
                                                    empresa.Last_users=usuario._id;
                                                    usr.next=usuario._id;
                                                    empresa.save();
                                                    usr.save();
                                                    res.json({
                                                        'err':0,
                                                        'usuario':usuario._id
                                                    })
                                                }
                                            })
                                        }
                                    }
                                })
                            }
                         })
                    }
                }
                //si el usuario ya existe se devuelve el codigo error 3
                else{
                    res.json({'err':3})
                }
            }
        })
        
    });

//-------modificar
    //modicar usuario, ocupa el id del usuario
    /**
     * se ocupa enviar en el json
     * user: id del usuario a editar
     * nombre:nombre del usuario
        email:correo del usuario
        cedula:cedula del usuario
        numero_tel: telefono del usuario
        tipo: tipo del usuario
        user: nombre de usuario del usuario
        password: password del usuario
        empresa: id de la empresa a la cual se asocia el usuario
     */
    app.post('/edit/usuario/',function(req,res){
        var info=req.body;
        //se busca el usuario
        du.Usuarios.findById(ObjectId(info.id),function(err,user){
            if(err)console.log(err)
            else{
                //actualiza los datos
                user.Nombre=		info.nombre;
			    user.Mail=		info.email;
			    user.cedula=		info.cedula;
			    user.numero_tel= info.numero_tel;
			    user.root=		info.tipo;
                user.cod=       info.cod;
                if(info.user!= null) user.user= info.user;
                if(info.password!= null) user.password= info.password;
                console.log(user)
                //se guardan los datos cambiados
                user.save(function(err){
                    if(err) {
                        console.log(err)
                        res.json({
                            'err':1,
                            'data':info
                        })
                    }
                    else res.json({
                        'err':0,
                        'data':info
                    })
                })
            }
        })
    });

    app.post('/user/act/clientes/',function(req,res){
        var info=req.body;
        du.Usuarios.findById(ObjectId(info.id),function(err,usr){
            if(err) console.log(err);
            else{
                usr.clientes=info.clientes
                usr.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json()
                    }
                })
            }
        })
    })

    /**
      * se ocupa enviar en el JSON:
      * empresa: id de la empresa a modificar
      * Name :nombre de la empresa
        Ced_J: cedula juridiaca de la empresa
        phone: telefono de la empresa
        email: correo de la empresa
        Cont_Name: nombre del contacto directo
        Cont_ID: cedula del contacto directo
        Cont_phone:telefono del contacto directo
        Cont_email: correo del correo directo
      */
    app.post('/edit/empresa/',function(req,res){
        var info=req.body;
        //se buscar la empresa que se desea modificar
        de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
            if(err) console.log(err);
            else{
                //se cambian los datos
                emp.Company=	   info.Name;
			    emp.cedula  =     info.Ced_J;
			    emp.phone   =     info.phone;
			    emp.email   =     info.email;
                emp.fax     =     info.fax;
                emp.direccion =   info.direccion;
                emp.extemail = info.extemail;
			    emp.Cont_imm=	   { 
                    nombre :	info.Cont_Name,
                    cedula :	info.Cont_ID,
                    telefono	:info.Cont_phone,
                    correo		:info.Cont_email,   
                };
                //se guardan los cambios
                emp.save(function(err){
                    if(err) {
                        console.log(err)
                        res,json({
                            'err':1,
                            'data':info
                        })
                    }    
                    else res.json({
                        'err':0,
                        'data':info
                    })
                });
            }
        })
    });

    app.post('/editar/facturacion/empresa/',function(req,res){
        var info=req.body;
        console.log(info)
        //se buscar la empresa que se desea modificar
        de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
            if(err) console.log(err);
            else{
                emp.Facturacion={
                    codigo:info.codigo,
                    informacion:info.informacion,
                    legal:info.legal,
                }
                emp.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json({
                            'err':0,
                            'data':info
                        })
                    }
                })
            }
        })
    })

    //asigna un usuario a una empresa
    /**
     * se ocupa enviar en el JSON
     * empresa: id de la empresa a acutualizar
     * user: id del usuario a asignar
     */
    app.post('/assignar/user',function(req,res){
        var info=req.body;
        //se busca la empresa que se desea asignar
        de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
            if(err) console.log(err);
            else{
                //si no hay usarios en la empresa se asigna el usuario como el primero y ultimo
                if(emp.usuarios==null){
                    //se actualizan los datos
                    emp.usuarios=info.user;
                    emp.Last_users=info.user;
                    //se guardan los cambios
                    emp.save(function(err){
                        if(err) {
                            console.log(err);
                            res.json({
                                'err':1,
                                'user':info.user
                            });
                        }
                        else res.json({
                            'err':0,
                            'user':info.user
                        });
                    })
                }
                //si ya hay usuarios asignados se actualiza el ultimo y el siguiente del ultimo usuario
                else{
                    du.Usuarios.findById(ObjectId(emp.Last_users),function(err,usr){
                        if(err) {
                            console.log(err);
                            res.json({
                                'err':1,
                                'user':info.user
                            });
                        }
                        else{
                            //se actualizan los datos
                            emp.Last_users=info.user;
                            usr.next=info.user;
                            //se guardan los cambios
                            usr.save();
                            emp.save();
                            res.json({
                                'err':0,
                                'user':info.user
                            });
                        }
                    })
                }
            }
        })
    })
//-------busqueda
    app.get('/find/user/:usr',function(req,res){
        var info=req.params;
        du.Usuarios.findById(ObjectId(info.usr),function(err,usr){
            if(err) console.log(err);
            else{
                if(usr){
                    res.json({
                        nombre:usr.Nombre,
                        correo:usr.Mail,
                        cedula:usr.cedula,
                        telefono:usr.numero_tel,
                        horario:usr.horario,
                        usuario:usr.user,
                        estado:usr.estado,
                        clientes:usr.clientes,
                        dispositivos:usr.dispositivos,
                        nivel:usr.root,
                        id:usr._id,
                        cod:usr.cod
                    })
                }
                else res.json(null)
            }
        })
    })

    app.get('/find/empresa/:empresa',function(req,res){
        var info= req.params;
        console.log(info);
        //se busqueda la empresa
        de.Empresas.findById(ObjectId(info.empresa),function(err,empresa){
            if(err)  console.log(err);
            else{
                console.log(info,empresa);
                //usando el script se busca los usuarios de nivel 2 y se envia como extra el nombre de la empresa y su id
                search.Busq_user(new Array(0),2,empresa.usuarios,{'nombre':empresa.Company,'id':empresa._id},res)
            }
        })
    });

    app.get('/usuarios/empresa/:empresa/:level',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.empresa),function(err,empresa){
            if(err)  console.log(err);
            else{
                console.log(info,empresa);
                //usando el script se busca los usuarios de nivel 2 y se envia como extra el nombre de la empresa y su id
                search.Busq_user(new Array(0),Number(info.level),empresa.usuarios,{'nombre':empresa.Company,'id':empresa._id},res)
            }
        })
    })

    app.get('/find/company/:empresa',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.empresa),function(err,empresa){
            if(err)  console.log(err);
            else{
                console.log(info,empresa);
                //usando el script se busca los usuarios de nivel 2 y se envia como extra el nombre de la empresa y su id
                res.json({'nombre':empresa.Company,'id':empresa._id,'cedula':empresa.cedula,'phone':empresa.phone,'email':empresa.email, 'ExtEmail':empresa.extemail,'Cont_imm':empresa.Cont_imm,'costos':empresa.costos,'SolCln':empresa.Solicitudes, 'fax':empresa.fax, 'direccion':empresa.direccion, 'Facturacion':empresa.Facturacion})
            }
        })
    })

    //busqueda de todas las empresas
    app.get('/find/empresas/all',function(req,res){
        //busqueda de todo el esquema de empresas
        de.Empresas.find({},function(err,empresa){
            if(err)  console.log(err);
            else{
                //arreglo de empresas para guardar las empresas
                var array=new Array(0);
                //funcion for para recorrer todas las empresas, se usa la variable i como index para recorrer la respuesta
                for(var i=0;i<empresa.length;i++){
                    //se guarda el nombre y el id de la empresa 
                    array.push({
                        'id':empresa[i]._id,
                        'nombre':empresa[i].Company
                    })
                }
                //se convierte el arreglo en un JSON
                var envio= JSON.stringify(array);
                envio= JSON.parse(envio);
                //se envian los datos
                res.json(envio);
            }
        })
    });

//-------eliminar
    app.post('/user/eliminar/',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.empresa),function(err,empresa){
            if(err)  console.log(err);
            else{
                du.Usuarios.findOne({next:info.usuario}, function(errB, usrB){
                    du.Usuarios.findById(ObjectId(info.usuario),function(err,usr){
                        if(err) console.log(err);
                        else{
                            if(errB) console.log(errB)
                            else{
                                console.log(usrB,usr)
                                if(usrB==null){
                                    //si es el primer elemento de la empresa
                                    if(empresa.usuarios==info.user){
                                        empresa.usuarios=usr.next
                                        empresa.save(function(err){
                                            if(err) console.log(err);
                                            else{
                                                usr.remove(function(err){
                                                    if(err) console.log(err)
                                                    else res.json(empresa)
                                                })
                                            }
                                        })
                                    }
                                }
                                else{
                                    usrB.next=usr.next
                                    usrB.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            usr.remove(function(err){
                                                if(err) console.log(err)
                                                else res.json(empresa)
                                            })
                                        }
                                    })
                                }
                            }
                        }
                    })
                })
            }
        })
    })


//-------indefinidos
//-------transporte
    app.get('/empresa/transportes/:emp',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                res.json(emp.transportes)   
            }
        })
    })

    app.post('/empresa/nuevoTransporte',function(req,res){
        var info=req.body;
        console.log(info)
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                emp.transportes.push(info.transportes);
                emp.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(emp.transportes)
                    }
                })
            }
        })
    })

    app.post('/empresa/eliminar/Transporte',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                //emp.transportes.push(info.transportes);
                emp.transportes.splice(emp.transportes.indexOf(info.transportes),1);
                emp.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(emp.transportes)   
                    }
                })
            }
        })
    })
//-------costos
    app.post('/actualizar/costos',function(req,res){
        var info=req.body;
        console.log(info)
        de.Empresas.findById(ObjectId(info.empresa),function(err,empresa){
            if(err)  console.log(err);
            else{
                empresa.costos=info.costos
                empresa.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(empresa.costos)
                    }
                })
            }
        })
    })
//-------login
    //login

    //compara los datos del usuario y password para comprobar si se tiene el derecho al acceso
    app.post('/login/',function(req,res){
        var info=req.body;
        // se busca el usuario
        du.Usuarios.findOne({'user':info.user},function(err,user){
            if(err) console.log(err);
            else{
                //si no hay usuario se considera un error de usuario y se devuelve con el codigo error 1
                if(user==null) res.json({
                    'err':1,
                    'data':null
                })
                else{
                    console.log(user);
                    //si las contrasenas son iguales se devuelve error 0 y el nombre y id del usuario
                    if(user.password==info.password){
                    res.json({
                        'err':0,
                        'data':{
                            'id':user._id,
                            'user':user.user,
                            'empresa':user.empresa,
                            'nivel':user.root
                        }
                    })}
                    //si las passwords no son iguales se considera un error de contrasena y se devuelve con codigo error 2
                    else res.json({
                        'err':2,
                        'data':null
                    })
                }
            }
        })
    })

//-------obsoletos
    //asignar
    //asignar dispositivo a usuario
    /**
     * Se ocupa enviar en el JSON
     * user:id del usuario a asignar el dispositivo
     * divice: id del dispositivo a asignar
     */
    app.post('/assignar/divice',function(req,res){ 
        var info=req.body;
        //se busca el usuario
        du.Usuarios.findById(ObjectId(info.user),function(err,user){
            if(err) console.log(err);
            else{
                //si no hay dispositivo asignados al usuario se toma este como el primero y ultimo
                if(user.dispositivos==null){
                    //se actualizanlos datos
                    user.dispositivos=info.divice;
                    user.dispositivos_last=info.divice;
                    user.save(function(err){
                        if(err) {
                            console.log(err);
                            res.json({
                                'err':1,
                                'divice':info.divice
                            });
                        }
                        else res.json({
                            'err':0,
                            'divice':info.divice
                        });
                    })
                }
                //si ya hay dispositivos asignados se actuliza el ultimo del usuario y el siguiente en el ultimo dispositivo
                else{
                    //se busca el ultimo disposito
                    dv.Vehiculos.findById(ObjectId(user.dispositivos_last),function(err,dis_serc){
                        if(err){
                            console.log(err);
                            res.json({
                                'err':1,
                                'divice':info.divice
                            });
                        }
                        else{
                            //se actualizanlos datos
                            user.dispositivos_last=info.divice;
                            dis_serc.next=info.divice;
                            user.save();
                            dis_serc.save();
                            res.json({
                                'err':0,
                                'divice':info.divice
                            });
                        }
                    })
                }
            }
        })
    })

    app.get('/undivices/users',function(req,res){
        var info=req.params;
        du.Usuarios.find({"dispositivos" : null , "root":2},function(err,users){
            if(err) console.log(err)
            else{
                res.json(users)
            }
        })
    })


}