//notas: falta la asignacion de usuarios
//nota: probar si se guardan los valores de las caracteristicas
//bibliotecas necesarias
    var mongoose     = require('mongoose');
	var path    = require("path");
	var http = require('http');
	var bodyParser = require('body-parser');
    var ObjectId = require('mongodb').ObjectID;

    //models
    //El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
    var da = require('./../models/Alertas_model.js')
    var du = require('./../models/Usuarios.js');
    var dv = require('./../models/Vehiculos_models.js');
    var dp = require('./../models/Posiciones_models.js');
    var dh = require('./../models/history.js');
    var di = require('./../models/Imeis_models.js');
    var dobd = require('./../models/OBD.js');
    //script
    var assig = require('./../Scripts/assing.js');
    var buscar = require('./../Scripts/Busquedas.js');
module.exports=function(app){
    console.log('imeis ready');
    

    //routes
    /**
     * generalidades:
     * -la variable info guarda los datos que se envian al route ya sea el body o el params
     */
    //crear nuevo id
    /**
     * ocupa enviar el json:
     * {
     *  unid:nombre del dispositivo
     *  name:   valor booleano de si el dispositivo pudemedir la caracteristica
        gps:    valor booleano de si el dispositivo pudemedir la caracteristica
        odo:    valor booleano de si el dispositivo pudemedir la caracteristica
        gas:    valor booleano de si el dispositivo pudemedir la caracteristica
        km: valor booleano de si el dispositivo pudemedir la caracteristica
        temp:   valor booleano de si el dispositivo pudemedir la caracteristica
        ibt:    valor booleano de si el dispositivo pudemedir la caracteristica
        panic:  valor booleano de si el dispositivo pudemedir la caracteristica
        vs: valor booleano de si el dispositivo pudemedir la caracteristica
        dall:   valor booleano de si el dispositivo pudemedir la caracteristica
        ROnf:   valor booleano de si el dispositivo pudemedir la caracteristica
        Acc:    valor booleano de si el dispositivo pudemedir la caracteristica
        Canb:   valor booleano de si el dispositivo pudemedir la caracteristica
        voz,:   valor booleano de si el dispositivo pudemedir la caracteristica
        Others: valor booleano de si el dispositivo pudemedir la caracteristica
        header: header del id que ocupa el dispositivo
     * }
     */
    app.post('/new/device',function(req,res){
        var info=req.body;
        //creacion del nuevo esquema de posiciones
        //creacion del dispositivo
        var device = new dv.Vehiculos({
            unidad          : info.unid,
            nombre		    : info.name,
            
        })
        device.save(function(err){
            if(err) console.log(err);
            else{
                //este script se le asigna el id del dispositivo
                //assig.assig(device._id,info.header);
                res.json(device); 
            }
        })
    });

    app.get('/make/history/:div',function(req,res){
        var info=req.params;
        console.log(info.div);
        dv.Vehiculos.findById(ObjectId(info.div),function(err,vhc){
            if(err) console.log(err);
            else{
                assig.histories(vhc.posiciones_last,vhc._id,0,0,vhc.posiciones_last,null,res)
            }
        })
    })

    app.get('/divice/keys/:divice/:keys/:cantidad/:since',function(req,res){
        var info=req.params;
        ArrKeys=info.keys.split(',');
        //console.log(ArrKeys);
        dv.Vehiculos.findById(ObjectId(info.divice),function(err,vh){
            if(err) console.log(err);
            else{
                console.log(info.since);
                if(info.since == 'null' || info.since == null)buscar.buscar_keys(ArrKeys,vh.posiciones_last,0,info.cantidad,null,new Array(0),res)
                else  buscar.buscar_keys(ArrKeys,info.since,0,info.cantidad,null,new Array(0),res)
            }
        })
    });

    app.get('/search/keys/from/:keys/:from/:until',function(req,res){
        var info=req.params;
        console.log(info)
        ArrKeys=info.keys.split(',');
        //console.log(ArrKeys);
        buscar.buscar_keys(ArrKeys,info.from,0,'max',info.until,new Array(0),res)
    });

    app.get('/divice/Datekeys/:divice/:keys/:cantidad/:date',function(req,res){
        var info=req.params;
        ArrKeys=info.keys.split(',');
        //console.log(ArrKeys);
        dv.Vehiculos.findById(ObjectId(info.divice),function(err,vh){
            if(err) console.log(err);
            else{
                //console.log(vh);
                if(vh.historycs==null){
                    res.json({
                        'err':1,
                        'itr':0,
                        'data':salida,
                    })
                }
                else{
                    dh.history.findById(ObjectId(vh.historycs),function(err,hst){
                        if(err) console.log(err);
                        else{
                            console.log(hst.fechas[info.date])
                            if(hst.fechas[info.date]==null){
                                res.json({
                                    'err':1,
                                    'itr':0,
                                    'data':salida,
                                })
                            }
                            else buscar.buscar_Datekeys(ArrKeys,hst.fechas[info.date],0,info.cantidad,null,info.date,new Array(0),res)  
                        }
                    })
                }
            }
        })
    });

    app.get('/device/date/:device/',function(req,res){
        var info=req.params;
        dv.Vehiculos.findById(ObjectId(info.device),function(err,dvc){
            if(err) console.log(err);
            else{
                if(dvc.historycs != null){
                    dh.history.findById(ObjectId(dvc.historycs),function(err,hst){
                        if(err) console.log(err);
                        else{
                            console.log(hst);
                            res.json({
                                data:Object.keys(hst.fechas)
                            })
                        }
                    })
                }
            }
        })
    })

    app.get('/find/div/:device',function(req,res){
        var info=req.params;
        dv.Vehiculos.findById(ObjectId(info.device),function(err,dvc){
            if(err) console.log(err);
            else{
                res.json(dvc)
            }
        })
    })

    //permite asignar un imei a un dispositivo despues de crearlo
    //se requiere en el body el id del dispositivo con el key deviceId, y el header que se le desea asignar, ya sea que
    //solo sea uno y se envia como un string o un arreglo de los que desea asignar
    app.post('/divice/assing/imei',function(req,res){
        var info=req.body;
        console.log(info);
        assig.assig(info.deviceId,info.header);
    })

    //assigna los settings del key settings al vehivulo del id deviceId
    app.post('/divice/set/achieve',function(req,res){
        var info=req.body;
        console.log(info);
        assig.achieve(info.deviceId,info.settings,res);
    })

    app.post('/edit/divice/',function(req,res){
        var info=req.body;
        dv.Vehiculos.findById(ObjectId(info.id),function(err,vh){
            vh.unidad=info.unid
            vh.nombre=info.name
            vh.save(function(err){
                if(err) console.log(err);
                else{
                    res.json(vh)
                }
            })
        })
    })

    app.post('/get/alertas/array/',function(req,res){
        var info=req.body;
        buscar.buscar_alertas_array(info.alertas,0,[],res)
    })

    /**
        key1:nivel 1
        key2:subnivel
        id:identidficador de la alerta
        div:id del dispositivo
     */
    app.post('/eliminar/alerta/',function(req,res){
        var info=req.body;
        //se busca el dispositivo
        Splice_alerta(info.div,info.id,info.key1,function(err){
            if(err==0){
                da.Alertas.findById(ObjectId(info.id),function(err,alt){
                    if(err) console.log(err);
                    else{
                        alt.remove(function(err){
                            if(err) res.json(4)
                            else res.json(0)
                        })
                    }
                })
            }
            else res.json(err)
        })
    })

    //busca los dispositivos asignados a un usuario
    app.get('/divice/of/:user',function(req,res){
        var info=req.params;
        console.log(info)
        du.Usuarios.findById(ObjectId(info.user),function(err,usr){
            if(err) console.log(err);
            else{
                if(usr !=null){
                    console.log(usr);
                    if(usr.dispositivos!=null) buscar.buscar_divices(usr.dispositivos,new Array(0),res);
                    else res.json(null)
                }
                else{
                    res.json(null)
                }
            }
        })
    })

    //busca todos los historiales
    app.get('/all/historial/:div',function(req,res){
        var info=req.params;
        dv.Vehiculos.findById(ObjectId(info.div),function(err,dsp){
            if(err) console.log(err);
            else{
                console.log(dsp)
                if(dsp.historycs != null) buscar.find_allhist(dsp.historycs,[],res);
                else res.json(null)
            }
        })
    })

    //busca los datos de un historial
    app.get('/get/historial/:hist/:keys',function(req,res){
        var info=req.params;
        ArrKeys=info.keys.split(',');
        buscar.find_historial(info.hist,ArrKeys,[],res)
    })

    //buscar alertas
    app.get('/divice/alerts/:inicio/:max',function(req,res){
        var info=req.params;
        console.log(info);
        if(info.inicio==null || info.inicio=='null') res.json({
            'err':0,
            'final':true,
            'data':null
        })
        else buscar.buscar_alertas(info.inicio,0,info.max,new Array(0),res);
    })

    app.post('/device/pos/last',function(req,res){
        var info=req.body;
        console.log(info);
        if(info.users.length>0)buscar.LastPos(info.users,0,new Array(0),res)
        else res.json(null);
    })

    //encontrar criterios de arching
    app.post('/find/arch',function(req,res){
        var info=req.body;
        if(info.ArchSetts.length>0){
            buscar.Arch(info.ArchSetts,0,new Array(0),res);
        }
        else{
            res.json({
                err:0,
                data:[]
            })
        }
    })

    //desinstala un imei de un dispositivo
    app.get('/imei/desinstall/:imei/:dispositivo',function(req,res){
        var info=req.params;
        dv.Vehiculos.findById(ObjectId(info.dispositivo),function(err,dsp){
            if(err) console.log(err);
            else{
                console.log(dsp.imei,dsp.imei.indexOf(info.imei))
                dsp.imei.splice(dsp.imei.indexOf(info.imei),1);
                dsp.save(function(err){
                    if(err) console.log(err);
                    else{
                        di.imeis.findOne({imei:info.imei}, function(err, ims){
                            if(err) console.log(err)
                            else{
                                if(ims==null) console.log("no existe el imei")
                                else{
                                    ims.asign_to=null
                                    ims.Asign=false;
                                    ims.install =false;
                                    ims.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            res.json(dsp);
                                        }
                                    })
                                }
                            }
                        })
                    }
                })
            }
        })
    })

    app.get('/get/obd/:obd',function(req,res){
        var info=req.params;
        dobd.OBD.findById(ObjectId(info.obd),function(err,obd){
            if(err) console.log(err);
            else{
                res.json(obd)
            }
        })
    })
}

function Splice_alerta(disp,id,type,callback){
    dv.Vehiculos.findById(ObjectId(disp),function(err,vh){
        if(err) callback(1);
        else{
            switch (type) {
                case 'Sobre velocidad':
                    if(vh.Alt.overspeed.indexOf(id)>-1){
                        vh.Alt.overspeed.splice(vh.Alt.overspeed.indexOf(id),1);
                        vh.save(function(err){
                            if(err) console.log(err);
                            else{
                                callback(0)
                            }
                        })
                    }
                    else{
                        if(vh.Alt.overspeedFinish.indexOf(id)>-1){
                            vh.Alt.overspeedFinish.splice(vh.Alt.overspeed.indexOf(id),1);
                            vh.save(function(err){
                                if(err) console.log(err);
                                else{
                                    callback(0)
                                }
                            })
                        }
                        else callback(1)
                    }
                    break;
                case 'bateria':
                    if(vh.Alt.batteryDown.indexOf(id)>-1){
                        vh.Alt.batteryDown.splice(vh.Alt.batteryDown.indexOf(id),1);
                        vh.save(function(err){
                            if(err) console.log(err);
                            else{
                                callback(0)
                            }
                        })
                    }
                    else{
                        if(vh.Alt.batteryError.indexOf(id)>-1){
                            vh.Alt.batteryError.splice(vh.Alt.batteryError.indexOf(id),1);
                            vh.save(function(err){
                                if(err) console.log(err);
                                else{
                                    callback(0)
                                }
                            })
                        }
                        else callback(1)
                    }
                    break;
                case 'DPA':
                    var alerta=vh.Alt.DPA
                    if(alerta.fast_acelation.indexOf(id)>-1){
                        vh.Alt.DPA.fast_acelation.splice(vh.Alt.DPA.fast_acelation.indexOf(id),1);
                        vh.save(function(err){
                            if(err) console.log(err);
                            else{
                                callback(0)
                            }
                        })
                    }
                    else{
                        if(alerta.fast_acelation2.indexOf(id)>-1){
                            vh.Alt.DPA.fast_acelation2.splice(vh.Alt.DPA.fast_acelation2.indexOf(id),1);
                            vh.save(function(err){
                                if(err) console.log(err);
                                else{
                                    callback(0)
                                }
                            })
                        }
                        else{
                            if(alerta.overspeed.indexOf(id)>-1){
                                vh.Alt.DPA.overspeed.splice(vh.Alt.DPA.overspeed.indexOf(id),1);
                                vh.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        callback(0)
                                    }
                                })
                            }
                            else{
                                if(alerta.sharp_turn.indexOf(id)>-1){
                                    vh.Alt.DPA.sharp_turn.splice(vh.Alt.DPA.sharp_turn.indexOf(id),1);
                                    vh.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            callback(0)
                                        }
                                    })
                                }
                                else callback(1)
                            }
                        }
                    }
                    break
                default:
                    callback(2)
                    break;
            }
        }
    })
    
}