var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;
var XLSX = require('xlsx');
//scripts extras
//var FileManage=require('./../scripts/File_handler.js')

//modelos
var di = require('./../models/inventario.js');

module.exports = {
    load: load,
    main: main
 };

 var app, multer, fs;
 // Multer variables
 var upload, Storage;

 function load(rapp, rmulter, rfs, rdirname)
 {
    // Load parameters
    app = rapp;
    multer = rmulter;
    fs = rfs;
    dirname= rdirname;
    // Execute main routine
    main();
    // Load multer
    loadMulter();
 }

 function main(){
     app.post('/empresa/clientes/excel',function(req,res){
        // Get all the parameters
        var dataGot = req.query; 
        console.log(req.body);
        var dir = __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/";
        if(!fs.existsSync(dir))
            {
            console.log(dir);
            fs.mkdir(dir, 0777,function(err){
                if(err)
                    console.log("# Simulation API 1: " + err);
                else
                {
                    upload(req, res, function (err) {
                        if (err) {
                            // An error occurred when uploading 
                            res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                            return;
                        }
                        else{
                            var workbook= XLSX.readFile( __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/"+ dataGot.name+'.xlsx')
                            Sheet_name= workbook.SheetNames;
                            console.log(XLSX.utils.sheet_to_json(workbook.Sheets[Sheet_name[0]]))
                            send=new Array(0)
                            //console.log(XLSX.utils.sheet_to_json(workbook.Sheets[Sheet_name[0]]))
                            for (let i = 0; i < Sheet_name.length; i++) {
                                name = Sheet_name[i];
                                datos=XLSX.utils.sheet_to_json(workbook.Sheets[name])
                                console.log(datos)
                                send=send.concat(datos)
                            }
                            console.log(send)
                            res.json(send)
                        }
                                
                    // Everything went fine 
                    });
                }
            });
        }
        else{
            console.log(dir);
            upload(req, res, function (err) {
                if (err) {
                    // An error occurred when uploading 
                    console.log("# Simulation API: " + err);
                    if(err.code == 'LIMIT_FILE_SIZE')
                        res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                    else
                        res.json({result:null, errors:-1}); // Unknown error
                    return;

                }
                else{
                    var workbook= XLSX.readFile( __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/"+ dataGot.name+'.xlsx')
                    Sheet_name= workbook.SheetNames;
                    console.log(Sheet_name)
                    send=new Array(0)
                    //console.log(XLSX.utils.sheet_to_json(workbook.Sheets[Sheet_name[0]]))
                    for (let i = 0; i < Sheet_name.length; i++) {
                        name = Sheet_name[i];
                        datos=XLSX.utils.sheet_to_json(workbook.Sheets[name])
                        console.log(datos)
                        send=send.concat(datos)
                    }
                    console.log(send)
                    res.json(send)
                }
            });
        }
    });

    app.post('/empresa/cliente/archivo',function(req,res){
        var info=req.body;
        var dataGot = req.query;
        var dir = __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/";
        if(!fs.existsSync(dir)){
            if(err)
                console.log("# Simulation API 1: " + err);
            else{
                upload(req, res, function (err) {
                    if (err) {
                        // An error occurred when uploading 
                        res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                        return;
                    }
                    else{
                        res.json({err:0})
                    }
                })
            }
        }
        else{
            upload(req, res, function (err) {
                if (err) {
                    // An error occurred when uploading 
                    console.log("# Simulation API: " + err);
                    if(err.code == 'LIMIT_FILE_SIZE')
                        res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                    else
                        res.json({result:null, errors:-1}); // Unknown error
                    return;

                }
                else{
                    res.json({err:0})
                }
            })
        }
    })

    app.post('/upload/parser/page',function(req,res){
        // Get all the parameters
        var dataGot = req.query; 
        console.log(req.body);
        var dir = __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/";
        if(!fs.existsSync(dir))
            {
            console.log(dir);
            fs.mkdir(dir, 0777,function(err){
                if(err)
                    console.log("# Simulation API 1: " + err);
                else
                {
                    upload(req, res, function (err) {
                        if (err) {
                            // An error occurred when uploading 
                            res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                            return;
                        }
                        else{
                            var workbook= XLSX.readFile( __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/"+ dataGot.name+'.xlsx')
                            Sheet_name= workbook.SheetNames;
                            console.log(XLSX.utils.sheet_to_json(workbook.Sheets[Sheet_name[0]]))
                            send={}
                            //console.log(XLSX.utils.sheet_to_json(workbook.Sheets[Sheet_name[0]]))
                            for (let i = 0; i < Sheet_name.length; i++) {
                                name = Sheet_name[i];
                                console.log(name)
                                datos=XLSX.utils.sheet_to_json(workbook.Sheets[name])
                                //console.log(datos)
                                send[name]=datos
                            }
                            //console.log(send)
                            res.json(send)
                        }
                                
                    // Everything went fine 
                    });
                }
            });
        }
        else{
            console.log(dir);
            upload(req, res, function (err) {
                if (err) {
                    // An error occurred when uploading 
                    console.log("# Simulation API: " + err);
                    if(err.code == 'LIMIT_FILE_SIZE')
                        res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                    else
                        res.json({result:null, errors:-1}); // Unknown error
                    return;

                }
                else{
                    var workbook= XLSX.readFile( __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/"+ dataGot.name+'.xlsx')
                    Sheet_name= workbook.SheetNames;
                    console.log(Sheet_name)
                    send={}
                    //console.log(XLSX.utils.sheet_to_json(workbook.Sheets[Sheet_name[0]]))
                    for (let i = 0; i < Sheet_name.length; i++) {
                        name = Sheet_name[i];
                        datos=XLSX.utils.sheet_to_json(workbook.Sheets[name])
                        console.log(datos)
                        send[name]=datos
                    }
                    console.log(send)
                    res.json(send)
                }
            });
        }
    })

    app.post('/upload2/parser/page',function(req,res){
        // Get all the parameters
        var dataGot = req.query; 
        console.log(req.body);
        var dir = __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/";
        if(!fs.existsSync(dir))
            {
            console.log(dir);
            fs.mkdir(dir, 0777,function(err){
                if(err)
                    console.log("# Simulation API 1: " + err);
                else
                {
                    upload(req, res, function (err) {
                        if (err) {
                            // An error occurred when uploading 
                            res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                            return;
                        }
                        else{
                            var workbook= XLSX.readFile( __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/"+ dataGot.name+'.xlsx')
                            Sheet_name= workbook.SheetNames;
                            console.log(XLSX.utils.sheet_to_json(workbook.Sheets[Sheet_name[0]]))
                            send={}
                            //console.log(XLSX.utils.sheet_to_json(workbook.Sheets[Sheet_name[0]]))
                            for (let i = 0; i < Sheet_name.length; i++) {
                                name = Sheet_name[i];
                                //console.log(name)
                                datos=XLSX.utils.sheet_to_json(workbook.Sheets[name])
                                console.log(datos)
                                send[name]=datos
                            }
                            //console.log(send)
                            res.json(send)
                        }
                                
                    // Everything went fine 
                    });
                }
            });
        }
        else{
            console.log(dir);
            upload(req, res, function (err) {
                if (err) {
                    // An error occurred when uploading 
                    console.log("# Simulation API: " + err);
                    if(err.code == 'LIMIT_FILE_SIZE')
                        res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                    else
                        res.json({result:null, errors:-1}); // Unknown error
                    return;

                }
                else{
                    var workbook= XLSX.readFile( __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/"+ dataGot.name+'.xlsx')
                    Sheet_name= workbook.SheetNames;
                    console.log(Sheet_name)
                    send={}
                    //console.log(XLSX.utils.sheet_to_json(workbook.Sheets[Sheet_name[0]]))
                    for (let i = 0; i < Sheet_name.length; i++) {
                        name = Sheet_name[i];
                        datos=XLSX.utils.sheet_to_json(workbook.Sheets[name])
                        console.log(datos)
                        send[name]=datos
                    }
                    console.log(send)
                    res.json(send)
                }
            });
        }
    })

    app.post('/pdf/upload',function(req,res){
        // Get all the parameters
        var dataGot = req.query; 
        console.log(req.body);
        var dir = __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/";
        if(!fs.existsSync(dir))
            {
            console.log(dir);
            fs.mkdir(dir, 0777,function(err){
                if(err)
                    console.log("# Simulation API 1: " + err);
                else
                {
                    upload(req, res, function (err) {
                        if (err) {
                            // An error occurred when uploading 
                            res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                            return;
                        }
                        else{
                            res.json('upload')
                        }
                                
                    // Everything went fine 
                    });
                }
            });
        }
        else{
            console.log(dir);
            upload(req, res, function (err) {
                if (err) {
                    // An error occurred when uploading 
                    console.log("# Simulation API: " + err);
                    if(err.code == 'LIMIT_FILE_SIZE')
                        res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                    else
                        res.json({result:null, errors:-1}); // Unknown error
                    return;

                }
                else{
                    
                    res.json('upload')
                }
            });
        }
    })

    app.get('/look/file/:username/:name/:filename',function(req,res){
        var info=req.params;
        var dir = __dirname + "/../../../back_end/Top_end/uploads/" + info.username + "/" + info.name;
        /*fs.readFile(dir , function (err,data){
            res.contentType("application/pdf");
            res.send(data);
        });*/
        res.download(dir, info.filename)
    })
 }

 // ------------------------------ AUXILIAR FUNCIONS ---------------------------
/**
 * Get multer ready for uploading files
 */
function loadMulter()
{
    // Storage properties
    Storage = multer.diskStorage({
        destination: function(req, file, callback) {
            var dataGot = req.query;
            var dir = __dirname + "/../../../back_end/Top_end/uploads/" + dataGot.username + "/";
            console.log(dir)
            callback(null, dir);
        },
        filename: function(req, file, callback) {
            var dataGot = req.query;
            console.log(dataGot)
            if(dataGot.header) callback(null, dataGot.name+'.'+dataGot.header);
            else callback(null, dataGot.name+'.xlsx');
        }
    });
    upload = multer({
        storage: Storage,
    }).single("file"); //Field name and max count
}