//bibliotecas necesarias
    var mongoose     = require('mongoose');
	var path    = require("path");
	var http = require('http');
	var bodyParser = require('body-parser');
    var ObjectId = require('mongodb').ObjectID;

    //models
    var dc = require('./../models/Calendario.js');
    var de = require('./../models/Empresas_models.js');
    var du = require('./../models/Usuarios.js');

module.exports=function(app){
    console.log('clientes ready');
    //scripts
    var calendario = require('./../Scripts/calendario.js');
    var buscar = require('./../Scripts/Busquedas.js');

    //routes
    app.post('/crear/tarea/',function(req,res){
        var info=req.body;
        du.Usuarios.findById(ObjectId(info.usr),function(err,usr){
            if(err) console.log(err);
            else{
                if(usr.horario == null){
                    var calen = new dc.Meses({
                        'year':info.year
                    });
                    calen.save(function(err){
                        if(err) console.log(err);
                        else{
                            usr.horario = calen._id
                            usr.save(function(err){
                                if(err) console.log(err);
                                else{
                                    calendario.crear_actividad(usr.horario,info.mes,info.dia,info.tipo,info.descripcion,info.inicio,info.final,res)
                                }
                            })
                        }
                    })
                }
                else search_calendar(usr.horario,info.year,function(meses){
                    calendario.crear_actividad(meses._id,info.mes,info.dia,info.tipo,info.descripcion,info.inicio,info.final,res)
                })
            }
        })
    })

    app.post('/remover/tarea/',function(req,res){
        var info=req.body;
        dc.Horarios.findById(ObjectId(info.horario),function(err,hrr){
            if(err) console.log(err);
            else{
                if(info.tipo==1)hrr.horario.splice(hrr.horario.indexOf(info.tarea),1);
                else hrr.tareas.splice(hrr.horario.indexOf(info.tarea),1);
                hrr.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(hrr)
                    }
                })
            }
        })
    })

    app.get('/get/calendario/:user/:year',function(req,res){
        var info=req.params;
        du.Usuarios.findById(ObjectId(info.user),function(err,usr){
            if(err) console.log(err);
            else{
                console.log('horario', String(usr.horario).length)
                if(usr.horario == null || String(usr.horario).length==0){
                    var calen = new dc.Meses({
                        'year':info.year
                    });
                    calen.save(function(err){
                        if(err) console.log(err);
                        else{
                            usr.horario = calen._id
                            usr.save(function(err){
                                if(err) console.log(err);
                                else{
                                    search_calendar(usr.horario,info.year,function(meses){
                                        console.log(meses)
                                        res.json(meses)
                                    });    
                                }                           
                            })
                        }
                    })
                }
                else{
                    search_calendar(usr.horario,info.year,function(meses){
                        console.log(meses)
                        res.json(meses)
                    });
                }    
            }
        })
    })

    app.get('/get/horarios/:horario',function(req,res){
        var info=req.params;
        buscar.buscar_horarios(info.horario,new Array(0),res)
    })

    app.post('/get/tareas/',function(req,res){
        var info=req.body;
        buscar.busacar_tareas(info.tareas,0,100,new Array(0),res)
    })
}

function search_calendar(id,year,callback){
    dc.Meses.findById(ObjectId(id),function(err,mss){
        if(err) console.log(err);
        else{
            console.log(mss)
            if(mss.year==year){
                callback(mss);
            }
            else {
                if(mss.next!=null) search_calendar(mss.next,year,callback)
                else{
                    var meses= new dc.Meses({
                        'year':year
                    })
                    meses.save(function(err){
                        if(err) console.log(err);
                        else{
                            mss.next=meses._id;
                            mss.save(function(err){
                                if(err) console.log(err);
                                else{
                                    callback(meses)        
                                    
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}