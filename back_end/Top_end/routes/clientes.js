
    
    //bibliotecas necesarias
    var mongoose     = require('mongoose');
	var path    = require("path");
	var http = require('http');
	var bodyParser = require('body-parser');
    var ObjectId = require('mongodb').ObjectID;

    //models
    var dc = require('./../models/Clientes_models.js');
    var de = require('./../models/Empresas_models.js');
    var du = require('./../models/Usuarios.js');
    //scripts
    var clientes = require('./../Scripts/clientes.js');
    var buscar = require('./../Scripts/Busquedas.js');

/**
    ****Index****
    **rutas**
--creacion
    -Creacion de clientes: /clientes/set
    -creacion de solicitud de clientes: /make/solicitud/
    -aceptacion de la solictud de cliente: /sumit/solicitud/
    -creacion de la nueva zona: /new/zona/
--edicion
    -Edicion de clientes: /editar/cliente/
    -edicion de los datso de facturacion: /edit/cln/
    -edicion de la direccion de los arichivos del cliente: /editar/archivos/cliente/
    -actualiza la region de los clientes de la region: /clientes/editar
    -actualiza los productos de un cliente: /actualizar/objetos/cliente/
    -asignacion de un agente a un cliente: /agentes/asignar/
    -cambiar la zona de un cliente:/change/cliente/zona/
    -cambiar el id de una zona:/change/id/region/
    -cambiar el nombre de una region: /change/zona/region/
--eliminacion
    -eliminar un cliente:/cliente/delete

--busquedas
    --busqueda de una cantidad n de clientes en orden: /change/zona/region/
    --busqueda de clientes toda la informacion dentro de un array :/clientes/get/array
    --busqueda de clientes mediante el script: /clientes/array/ /*analizar si es necesaria
    --busqueda de clientes de un agente: /clientes/agentes/get/:usr/:emp/
    -- busqueda de zonas de la empresa: /empresa/zonas/:emp

    ** funciones
    findClients:busqueda rapida de clientes en un array
    ZonaClientes:actualizacion del id de zona de un clientes

 */


module.exports=function(app){
    console.log('clientes ready');
    //routes

//creacion
    app.post('/clientes/set',function(req,res){
        var info=req.body;
        console.log(info)
        //busca la empresa a la que se asigna el cliente
        de.Empresas.findById(ObjectId(info.id),function(err,emp){
            if(err) console.log(err);
            else{
                //si no tiene index de usuario lo crea
                if(emp.clientes == null){
                    var myclt= new dc.myclient({
                        empresa : emp._id,
                        id: new Array(0)
                    });
                    myclt.save(function(err){
                        if(err) console.log(err);
                        else{
                            //actualiza el indexes de clientes de la empresa
                            emp.clientes=myclt._id;
                            emp.save(function(err){
                                if(err) console.log(err);
                                else{
                                    clientes.addClient(myclt._id,info.datos,res)   
                                }
                            })
                        }
                    })
                }
                //si existe se agrega a este
                else clientes.addClient(emp.clientes,info.datos,res);
            }
        })
    })

    app.post('/make/solicitud/',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.id),function(err,emp){
            if(err) console.log(err);
            else{
                var cln = new dc.clientes(info.data)
                cln.save(function(err){
                    if(err) console.log(err);
                    else{
                        emp.Solicitudes.push(cln._id)
                        emp.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json(cln)
                            }
                        })     
                    }
                })
            }
        })
    })

    app.post('/sumit/solicitud/',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.id),function(err,emp){
            if(err) console.log(err);
            else{
                if(info.crear){
                    if(emp.clientes == null){
                        var myclt= new dc.myclient({
                            empresa : emp._id,
                            id: new Array(0)
                        });
                        myclt.save(function(err){
                            if(err) console.log(err);
                            else{
                                //actualiza el indexes de clientes de la empresa
                                emp.clientes=myclt._id;
                                emp.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        clientes.addClient(info.solicitud, myclt._id,function(err){
                                            console.log('error de acetacion:',err)
                                        })   
                                    }
                                })
                            }
                        })
                    }
                    //si existe se agrega a este
                    else clientes.addClient(info.solicitud, emp.clientes,function(err){
                        console.log('error de acetacion:',err)
                    });
                }
                else{
                    dc.clientes.findById(ObjectId(info.solicitud),function(err,cln){
                        if(err) console.log(err);
                        else{
                            cln.remove(function(err){
                                if(err)console.log(err)
                            })                            
                        }
                    })
                }
            }
            emp.Solicitudes.splice(emp.Solicitudes.indexOf(info.solicitud),1);
            emp.save(function(err){
                if(err) console.log(err);
                else{
                    res.json(emp.Solicitudes)
                }
            })
        })
    })

    app.post('/new/zona/',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                dc.myclient.findById(ObjectId(emp.clientes),function(err,mcln){
                    if(err) console.log(err);
                    else{
                        mcln.zonas.push({
                            region:info.region,
                            codigo:info.codigo,
                        })
                        mcln.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json(mcln)
                            }
                        })
                    }
                })
            }
        })
    })

//edicion
    app.post('/editar/cliente/',function(req,res){
        var info=req.body;
        dc.clientes.findById(ObjectId(info.id),function(err,cln){
            if(err) console.log(err);
            else{
                cln.Tdoc=info.data.Tdoc
                cln.codigo=info.data.codigo
                cln.nombre=info.data.nombre
                cln.cedula=info.data.cedula
                cln.Actividad=info.data.Actividad
                cln.size=info.data.size
                cln.tipo=info.data.tipo
                cln.Representante=info.data.Representante
                cln.contactos=info.data.contactos
                cln.pago=info.data.pago
                cln.credito=info.data.credito
                cln.Razon=info.data.Razon
                cln.correoFact=info.data.correoFact
                cln.referencias=info.data.referencias
                cln.lat=info.data.lat
                cln.lon=info.data.lon
                cln.CodigoRuta=info.data.CodigoRuta
                cln.Provincia=info.data.Provincia
                cln.Canton=info.data.Canton
                cln.Distrito=info.data.Distrito
                cln.direccion=info.data.direccion
                cln.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(cln)
                    }
                })
            }
        })
    })

    app.post('/edit/cln/',function(req,res){
        var info=req.body;
        dc.clientes.findById(ObjectId(info.data._id),function(err,cln){
            if(cln){
                if(err){
                    res.json({
                        'err':2,
                    })
                }
                else{
                    cln.Tdoc=info.data['Tipo de documento']
                    cln.codigo=info.data.codigo
                    cln.nombre=info.data.nombre
                    cln.cedula=info.data.Cedula
                    cln.Actividad=info.data.Actividad
                    cln.size=info.data['Tamaño']
                    cln.tipo=info.data.tipo
                    cln.pago.contado=info.data.Contado
                    cln.pago.transporte=info.data.Transporte
                    cln.pago.dias=info.data["Dias plazo"]
                    cln.credito.limite=info.data['Limite de credito']
                    cln.credito.deuda=info.data['Deuda Actual']
                    cln.Razon=info.data["Razon social"]
                    cln.correoFact=info.data["Correo para factura"]
                    cln.correoCortesia=info.data["Correo de cortesia"]
                    cln.lat=info.data.Latitud
                    cln.lon=info.data.Longitud
                    cln.Provincia=info.data.Provincia
                    cln.Canton=info.data.Canton
                    cln.Distrito=info.data.Distrito
                    cln.direccion=info.data.direccion
                    cln.save(function(err){
                        if(err) {
                            res.json({
                                'err':3,
                            })
                        }
                        else{
                            res.json({
                                'err':0,
                            })
                        }
                    })
                }
            } 
            else{
                res.json({
                    'err':1,
                })
            }   
        })
    })

    app.post('/editar/archivos/cliente/',function(req,res){
        var info=req.body;
        dc.clientes.findById(ObjectId(info.id),function(err,cln){
            if(err) console.log(err);
            else{
                cln.archivos=info.archivos
                cln.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(cln)
                    }
                })
            }
        })
    })
    //sobre escribe un cliente en particular
    app.post('/clientes/editar',function(req,res){
        var info=req.body;
        dc.clientes.findById(ObjectId(info.id),function(err,cln){
            if(err) console.log(err);
            else{
                console.log(cln,info);
                //edicion del index de objetos
                if(cln.region != info.datos.region){
                    //si cambia de region elimina el dato de la region anterior primero
                    de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
                        if(err) console.log(err);
                        else{
                            dc.myclient.findById(ObjectId(emp.clientes),function(err,indexC){
                                if(err) console.log(err);
                                else{
                                    //borrar el dato del index
                                    for (let i = 0; i < indexC.id.length; i++) {
                                        if(indexC.id[i].region == cln.region) {
                                            indexC.id[i].clientes.splice(indexC.id[i].clientes.indexOf(info.id),1);
                                            break;
                                        }
                                    }
                                    //mete el dato en el index correcto
                                    for (let i = 0; i < indexC.id.length; i++) {
                                        if(indexC.id[i].region == info.datos.region) {
                                            indexC.id[i].clientes.push(info.id);
                                            break;
                                        }
                                        else if(i == indexC.id.length-1){
                                            indexC.id.push({
                                                'region': info.datos.region,
                                                'id': new Array(1).fill(info.id)
                                            })
                                        }
                                    }
                                    indexC.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            //actualizan los datos del cliente
                                            cln.nombre=info.datos.nombre;
                                            cln.lat=info.datos.lat;
                                            cln.lon=info.datos.lon;
                                            cln.region=info.datos.region;
                                            cln.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    res.json(cln);
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
                //si la region es la misa en ambos casos solo actualiza los datos
                else{
                    cln.nombre=info.datos.nombre;
                    cln.lat=info.datos.lat;
                    cln.lon=info.datos.lon;
                    cln.region=info.datos.region;
                    cln.save(function(err){
                        if(err) console.log(err);
                        else{
                            res.json(cln);
                        }
                    })
                }
            }
        })
    })

    app.post('/actualizar/objetos/cliente',function(req,res){
        var info=req.body;
        dc.clientes.findById(ObjectId(info.id),function(err,cln){
            if(err) console.log(err);
            else{
                cln.productos=info.productos,
                cln.visita=new Date()
                cln.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(cln)   
                    }
                })
            }
        })
    })

    app.post('/agentes/asignar/',function(req,res){
        var info=req.body;
        console.log('asignar', info);
        du.Usuarios.findById(ObjectId(info.usuario),function(err,usr){
            if(err) console.log(err);
            else{
                if(usr.clientes!=null){
                    if(info.asignar)usr.clientes.push(info.Zona);
                    else usr.clientes.splice(usr.clientes.indexOf(info.Zona),1);
                }
                else usr.clientes=[info.Zona];
                usr.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(usr)
                    }
                })
            }
        })
    })

    app.post('/change/cliente/zona/',function(req,res){
        var info=req.body;
        dc.clientes.findById(ObjectId(info.id),function(err,cln){
            if(err) console.log(err);
            else{
                cln.region=info.region
                cln.regionId=info.regionId
                dc.myclient.findById(ObjectId(info.mycln),function(err,mycln){
                    if(err) console.log(err);
                    else{
                        mycln.id=info.myclnid
                        cln.save(function(err){
                            if(err) console.log(err);
                            else{
                                mycln.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        res.json(mycln)
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    })

    app.post('/change/id/region/',function(req,res){
        var info=req.body;
        console.log(info)
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                console.log(emp)
                dc.myclient.findById(ObjectId(emp.clientes),function(err,myclt){
                    for (let i = 0; i < myclt.id.length; i++) {
                        const region = myclt.id[i];
                        if(region.region.toUpperCase() ==info.region.toUpperCase() ){
                            myclt.id[i].codigo=info.codigo;
                            myclt.save(function(err){
                                if(err) console.log(err);
                                else{
                                    
                                }
                            })
                            ZonaClientes(region.clientes,0,info.codigo,function(){
                                res.json(region)
                            })
                            break
                        }
                        else{
                            if(i == myclt.id.length-1) res.json(region)
                        }
                    }
                })
            }
        })
    })

    app.post('/change/zona/region/',function(req,res){
        var info=req.body;
        console.log(info)
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                console.log(emp)
                dc.myclient.findById(ObjectId(emp.clientes),function(err,myclt){
                    for (let i = 0; i < myclt.id.length; i++) {
                        const region = myclt.id[i];
                        if(region.region.toUpperCase() ==info.region.toUpperCase() ){
                            myclt.id[i].zona=info.zona;
                            break
                        }
                    }
                    myclt.save(function(err){
                        if(err) console.log(err);
                        else{
                            res.json(myclt)
                        }
                    })
                })
            }
        })
    })

//eliminacion
    app.post('/cliente/delete',function(req,res){
        var info=req.body;
        //console.log(info)
        //busca la empresa a la que se asigna el cliente
        de.Empresas.findById(ObjectId(info.id),function(err,emp){
            if(err) console.log(err);
            else{
                if(emp.clientes == null){
                }
                else{
                    dc.myclient.findById(ObjectId(emp.clientes),function(err,myclt){
                        if(err) console.log(err);
                        else{
                            //console.log(myclt.id);
                            for (let i = 0; i < myclt.id.length; i++) {
                                if(myclt.id[i].region==info.region){
                                    if(myclt.id[i].clientes.indexOf(info.cliente)>-1){
                                        console.log(myclt.id[i].clientes.indexOf(info.cliente))
                                        myclt.id[i].clientes.splice(myclt.id[i].clientes.indexOf(info.cliente),1);
                                    }
                                }
                            }
                            myclt.save(function(err){
                                if(err) console.log(err);
                                else{
                                    res.json(myclt);
                                    
                                }
                            })
                        }
                    })
                }
            }
        })
    })

    
//busquedas
    app.get('/clientes/get/:emp/:where/:since/:max',function(req,res){
        var info=req.params;
        //console.log(info);
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                if(emp.clientes !=null){
                    dc.myclient.findById(ObjectId(emp.clientes),function(err,mycl){
                        if(err) console.log(err);
                        else{
                            //console.log(mycl)
                            if(info.where==null || info.where == "null"){
                                busqueda=[]
                                for (let i = 0; i < mycl.id.length; i++){
                                    busqueda=busqueda.concat(mycl.id[i].clientes)
                                    //console.log('arreglo',busqueda,mycl.id[i].clientes)
                                }
                                buscar.clientes(busqueda,Number(info.since),Number(info.since)+Number(info.max),new Array(0),res);
                            }
                            else for (let i = 0; i < mycl.id.length; i++) {
                                if(mycl.id[i].region=info.where) buscar.clientes(mycl.id[i].clientes,Number(info.since),Number(info.since)+Number(info.max),new Array(0),res);
                                else if(i == mycl.id.length-1) res.json({
                                    'err':0,
                                    'last':0,
                                    'datos':null,
                                })
                            }
                              
                        }
                    })
                }
                else res.json({
                    'err':0,
                    'last':0,
                    'datos':null,
                })
            }
        })
    })

    app.get('/clientes/agentes/get/:usr/:emp/',function(req,res){
        var info=req.params;
        //console.log(info);
        du.Usuarios.findById(ObjectId(info.usr),function(err,usr){
            if(err) console.log(err);
            else{
                de.Empresas.findById(ObjectId(info.emp),function(err,emp){
                    if(err) console.log(err);
                    else{
                        dc.myclient.findById(ObjectId(emp.clientes),function(err,mcln){
                            if(err) console.log(err);
                            else{
                                if(usr.clientes==null) {
                                    usr.clientes=[]
                                    usr.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            res.json([])
                                        }
                                    })
                                }
                                else{
                                    var clientes=[]
                                    for (let i = 0; i < mcln.id.length; i++) {
                                        const zona = mcln.id[i];
                                        //console.log(usr.clientes.indexOf(zona.region))
                                        if(usr.clientes.indexOf(zona.region)!=-1)clientes=clientes.concat(zona.clientes)
                                    }
                                    console.log(clientes)
                                    findClients(clientes,0,[],res)
                                }
                            }
                        })
                    }
                })
            }
        })
    })

    app.post('/clientes/get/array',function(req,res){
        var info=req.body;
        findClients(info.clientes,0,[],res)
    })

    app.post('/clientes/array/',function(req,res){
        var info=req.body;
        console.log(info)
        buscar.Clientes_Array(info.clientes,0,info.clientes.length,{},res)
    })

    app.get('/change/zona/region/',function(req,res){
        var info=req.params;
        //console.log(info);
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                if(emp.clientes !=null){
                    dc.myclient.findById(ObjectId(emp.clientes),function(err,mycl){
                        if(err) console.log(err);
                        else{
                            //console.log(mycl)
                            if(info.where==null || info.where == "null"){
                                busqueda=[]
                                for (let i = 0; i < mycl.id.length; i++){
                                    busqueda=busqueda.concat(mycl.id[i].clientes)
                                    //console.log('arreglo',busqueda,mycl.id[i].clientes)
                                }
                                buscar.clientes(busqueda,Number(info.since),Number(info.since)+Number(info.max),new Array(0),res);
                            }
                            else for (let i = 0; i < mycl.id.length; i++) {
                                if(mycl.id[i].region=info.where) buscar.clientes(mycl.id[i].clientes,Number(info.since),Number(info.since)+Number(info.max),new Array(0),res);
                                else if(i == mycl.id.length-1) res.json({
                                    'err':0,
                                    'last':0,
                                    'datos':null,
                                })
                            }
                              
                        }
                    })
                }
                else res.json({
                    'err':0,
                    'last':0,
                    'datos':null,
                })
            }
        })
    })

    app.get('/empresa/zonas/:emp',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                dc.myclient.findById(ObjectId(emp.clientes),function(err,mcln){
                    if(err) console.log(err);
                    else{
                        res.json(mcln)    
                    }
                })
            }
        })
    })

}

function findClients(clientes,i,send,res) {
    dc.clientes.findById(ObjectId(clientes[i]),function(err,cln){
        if(err) console.log(err);
        else{
            send.push(cln);
            //condicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            if(i+1<clientes.length) findClients(clientes,i+1,send,res)
            else{
                res.json({
                    'err':0,
                    'data':send,
                    'last':i
                })
            }
        }
    })
}

function ZonaClientes(clientes,i,id,callback) {
    dc.clientes.findById(ObjectId(clientes[i]),function(err,cln){
        if(err) console.log(err);
        else{
            cln.regionId=id
            cln.save(function(err){
                //condicion de finalizacion
                //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
                if(i+1<clientes.length) ZonaClientes(clientes,i+1,id,callback)
                else callback()
            })
            
        }
    })
}