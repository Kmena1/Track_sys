var mongoose     = require('mongoose');
var ObjectId = require('mongodb').ObjectID;
var XLSX = require('xlsx');
//scripts extras
//var FileManage=require('./../scripts/File_handler.js')

//modelos
var di = require('./../models/inventario.js');

module.exports = {
    load: load,
    main: main
 };

 var app, multer, fs;
 // Multer variables
 var upload, Storage;

 function load(rapp, rmulter, rfs, rdirname)
 {
    // Load parameters
    app = rapp;
    multer = rmulter;
    fs = rfs;
    dirname= rdirname;
    // Execute main routine
    main();
    // Load multer
    loadMulter();
 }

 function main(){
    app.post('/empresa/imagenes/catalogo',function(req,res){
        // Get all the parameters
        var dataGot = req.query; 
        console.log(req.body);
        di.objetos.findById(ObjectId(dataGot.name),function(err,obj){
            if(err) console.log(err);
            else{
                var dir = __dirname + "/../../../front_end/assets/img/" + dataGot.username + "/";
                if(!fs.existsSync(dir))
                    {
                    console.log(dir);
                    fs.mkdir(dir, 0777,function(err){
                        if(err)
                            console.log("# Simulation API 1: " + err);
                        else
                        {
                            upload(req, res, function (err) {
                                if (err) {
                                    // An error occurred when uploading 
                                    res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                                    return;
                                }
                                else
                                {
                                    //FileManage.start(dataGot.simuid,dataGot.username,dataGot.simuid+'.txt',res);
                                    var catalogo = new di.catalogo({
                                        objeto: obj._id,
                                        descripcion : dataGot.descripcion,
                                        imagen : "/assets/img/" + dataGot.username + "/" + dataGot.name+'.jpeg',
                                        exentos:Boolean(dataGot.excento)
                                    });
                                    catalogo.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            obj.catalogo=catalogo._id;
                                            obj.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    res.json(catalogo)
                                                }
                                            })                                            
                                        }
                                    })
                                }
                                        
                            // Everything went fine 
                            });
                        }
                    });
                }
                else{
                    console.log(dir);
                    upload(req, res, function (err) {
                        if (err) {
                            // An error occurred when uploading 
                            console.log("# Simulation API: " + err);
                            if(err.code == 'LIMIT_FILE_SIZE')
                                res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                            else
                                res.json({result:null, errors:-1}); // Unknown error
                            return;

                        }
                        else
                        {
                            //FileManage.start(dataGot.simuid,dataGot.username,dataGot.simuid+'.txt',res);
                            var catalogo = new di.catalogo({
                                objeto: obj._id,
                                descripcion : dataGot.descripcion,
                                imagen : "/assets/img/" + dataGot.username + "/" + dataGot.name+'.jpeg',
                                exentos:Boolean(dataGot.excento)
                            });
                            catalogo.save(function(err){
                                if(err) console.log(err);
                                else{
                                    obj.catalogo=catalogo._id;
                                    obj.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            res.json(catalogo)
                                        }
                                    })                                    
                                }
                            })
                        }
                    });
                }
            }
        })
    });


    app.post('/empresa/imagenes/inventario',function(req,res){
        // Get all the parameters
        var dataGot = req.query; 
        console.log(req.body);
        di.objetos.findById(ObjectId(dataGot.name),function(err,obj){
            if(err) console.log(err);
            else{
                var dir = __dirname + "/../../../front_end/assets/img/" + dataGot.username + "/";
                if(!fs.existsSync(dir))
                    {
                    console.log(dir);
                    fs.mkdir(dir, 0777,function(err){
                        if(err)
                            console.log("# Simulation API 1: " + err);
                        else
                        {
                            upload(req, res, function (err) {
                                if (err) {
                                    // An error occurred when uploading 
                                    res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                                    return;
                                }
                                else
                                {
                                    res.json('listo')
                                }
                                        
                            // Everything went fine 
                            });
                        }
                    });
                }
                else{
                    console.log(dir);
                    upload(req, res, function (err) {
                        if (err) {
                            // An error occurred when uploading 
                            console.log("# Simulation API: " + err);
                            if(err.code == 'LIMIT_FILE_SIZE')
                                res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                            else
                                res.json({result:null, errors:-1}); // Unknown error
                            return;

                        }
                        else
                        {
                            res.json('listo')
                        }
                    });
                }
            }
        })
    });

     app.post('/empresa/editar/catalogo',function(req,res){
        // Get all the parameters
        var dataGot = req.query; 
        console.log(dataGot);
        di.objetos.findById(ObjectId(dataGot.name),function(err,obj){
            if(err) console.log(err);
            else{
                var dir = __dirname + "/../../../front_end/assets/img/" + dataGot.username + "/";
                if(!fs.existsSync(dir))
                    {
                    console.log(dir);
                    fs.mkdir(dir, 0777,function(err){
                        if(err)
                            console.log("# Simulation API 1: " + err);
                        else
                        {
                            upload(req, res, function (err) {
                                if (err) {
                                    // An error occurred when uploading 
                                    res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                                    return;
                                }
                                else
                                {
                                    //FileManage.start(dataGot.simuid,dataGot.username,dataGot.simuid+'.txt',res);
                                    di.catalogo.findById(ObjectId(dataGot.catalogo),function(err,catl){
                                        if(err) console.log(err);
                                        else{
                                            catl.objeto= obj._id;
                                            catl.descripcion = dataGot.descripcion;
                                            catl.imagen = "/assets/img/" + dataGot.username + "/" + dataGot.name+'.jpeg';
                                            if(dataGot.exento=='true'||dataGot.exento==true)catl.exentos=true;
                                            else catl.exentos=false
                                            catl.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    obj.catalogo=catl._id;
                                                    obj.save(function(err){
                                                        if(err) console.log(err);
                                                        else{
                                                            res.json(catl)
                                                        }
                                                    })                                            
                                                }
                                            })
                                        }
                                    })
                                }
                                        
                            // Everything went fine 
                            });
                        }
                    });
                }
                else{
                    console.log(dir);
                    upload(req, res, function (err) {
                        if (err) {
                            // An error occurred when uploading 
                            console.log("# Simulation API: " + err);
                            if(err.code == 'LIMIT_FILE_SIZE')
                                res.json({result:null, errors:defs.errors.DATAFILE_EXCEEDED});
                            else
                                res.json({result:null, errors:-1}); // Unknown error
                            return;

                        }
                        else
                        {
                            di.catalogo.findById(ObjectId(dataGot.catalogo),function(err,catl){
                                if(err) console.log(err);
                                else{
                                    catl.objeto= obj._id;
                                    catl.descripcion = dataGot.descripcion;
                                    catl.imagen = "/assets/img/" + dataGot.username + "/" + dataGot.name+'.jpeg';
                                    if(dataGot.exento=='true'||dataGot.exento==true)catl.exentos=true;
                                    else catl.exentos=false
                                    catl.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            obj.catalogo=catl._id;
                                            obj.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    res.json(catl)
                                                }
                                            })                                            
                                        }
                                    })
                                }
                            })
                        }
                    });
                }
            }
        })
    });

    /*app.get('/read/:user/:file_name',function(req,res){
        var info=req.params;
        var dir = dirname + "/uploads/" + info.user + "/" + info.file_name;
        if(fs.existsSync(dir)){
            fs.readFile(dir, 'utf-8', (err, data) => {
                if(err) {
                  console.log('error: ', err);
                } else {
                  console.log(data.split('\n'));
                }
              });
        }
        else res.json('no existe el archivo'+dir )
    })*/

    app.get('/download/:user/:file(*)', function(req, res, next){ 
        var info = req.params;
        var file = req.params.file;
        var path = dirname + "/front_end/assets/img/" + dataGot.username + "/" +file;
        res.download(path, file, function(err){
          if (err){
            console.log(err);
          } else {
            console.log('downloading successful');
          }
        });
      });

 }

 // ------------------------------ AUXILIAR FUNCIONS ---------------------------
/**
 * Get multer ready for uploading files
 */
function loadMulter()
{
    // Storage properties
    Storage = multer.diskStorage({
        destination: function(req, file, callback) {
            var dataGot = req.query;
            var dir = __dirname + "/../../../front_end/assets/img/" + dataGot.username + "/";
            callback(null, dir);
        },
        filename: function(req, file, callback) {
            var dataGot = req.query;
            callback(null, dataGot.name+'.jpeg');
        }
    });
    upload = multer({
        storage: Storage,
    }).single("file"); //Field name and max count
}