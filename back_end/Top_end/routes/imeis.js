module.exports=function(app){
    console.log('imeis ready');
    //bibliotecas necesarias
    var mongoose     = require('mongoose');
	var path    = require("path");
	var http = require('http');
	var bodyParser = require('body-parser');
    var ObjectId = require('mongodb').ObjectID;

    //models
    //El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
    var di=require('./../models/Imeis_models.js');

    //scripts
    var assig = require('./../Scripts/assing.js');
    
    //routes
    /**
     * generalidades:
     * -la variable info guarda los datos que se envian al route ya sea el body o el params
     */

    //crear
    //crea un nuevo id
    /**
     * se ocupa enviar el json:
     * {
     *  tipo:header del imei
        imei:id del dipositivo
     * }
     */

    app.post('/imei/crear/',function(req,res){
        var info=req.body;
        if(typeof(info.tipo)=='string') info.tipo = JSON.parse(info.tipo);     
        delete info.tipo._id   
        console.log(info);
        //busca en las estucturas si existe una con este header
        di.Structure.findOne(info.tipo,function(err, strc){
            if(err) console.log(err)
            else{
                //comprueba si existe esta estructura
                if(strc==null) console.log('no hay estructura con el header', info.tipo);
                else{
                    info.tipo['structure']=strc.structure;
                    info.tipo['imei']=info.imei
                    //crea el nuevo id asignandole el header y la estructura en la base de datos
                    var imei= new di.imeis(info.tipo);
                    console.log("creando: ", imei)
                    imei.save(function(err){
                        if(err) {
                            console.log(err);
                        }
                        else {
                            //asigna a la lista de espera de los dispositivos sin id
                            assig.reassig(imei._id, {'tipo':info.tipo.tipo, 'model_ID':info.tipo.model_ID});
                            res.json(imei);
                        }
                    })
                }
            }
        })
    })

    //crear nueva estructura
    /**
     * se ocupa enviar
     * header: Header de la estructura
        prot: protocolo de la esturctura
        model: modelo de la estructura
        strc: estructura
     */
    app.post('/model/crear/',function(req,res){
        var info = req.body;
        console.log(info);
        //se crea una nueva estructura
        var structure = new di.Structure({
            tipo: info.header,
            protocol : info.prot,
            model_ID : info.model,
            structure:info.strc,
            header:info.header,
            nombre:info.nombre,
        });
        structure.save(function(err){
            if(err) console.log(err);
            else res.json(structure);
        })
    })

    //asigned
    app.post('/imei/asign/',function(req,res){
        var info=req.body;
        di.imeis.findById(ObjectId(info.imei),function(err,imei){
            if(err) console.log(err);
            else{
                imei.asign_to=info.asign;
                imei.Asign=true;
            };
            imei.save(function(err){
                if(err) console.log(err);
                else res.json(imei);
            })
        })
    });
    //modificar
        //instalado-desinstal
        //cambia el estado del dispositivo a instalado se ocupa enviar el id del dispositivo a cambiar en el JSON con el key imei
        app.post('/imei/installed/',function(req,res){
            var info=req.body;
            di.imeis.findById(ObjectId(info.imei),function(err,imei){
                if(err) console.log(err);
                else{
                    imei.install=info.inst;
                };
                imei.save(function(err){
                    if(err) console.log(err);
                    else res.json(imei);
                })
            })
        });

    //devuelve todas las estructuras en el sistema
    app.get('/struc/all',function(req,res){
        var info=req.params;
        di.Structure.find({},function(err,strc){
            if(err) console.log(err)
            else{
                res.json(strc);
            }
        })
    })

    //devuelve una estructura especificada en el parametro str
    app.get('/struc/one/:str',function(req,res){
        var info=req.params;
        di.Structure.findById(ObjectId(info.str),function(err,str){
            if(err) console.log(err);
            else{
                res.json(str);
            }
        })
    })

    //cambia los valores de una structura, requiere
    /**
    id: id de la estructura a cambiar si entra nulo crea una nueva estructura
    str: estructura a guardar debe cumplir con el json
        nombre:
        tipo:
        protocol : 
        model_ID : 
        structure:
     */
    app.post('/struc/change',function(req,res){
        var info=req.body;
        //si no hay id de la structura se asume que es una nueva a crear
        if(info.id==null || info.id == undefined){
            var StrNew = new di.Structure(info.StrChg);
            StrNew.save(function(err){
                if(err) console.log(err);
                else{
                    res.json(StrNew);
                }
            })
        }
        //sino se procede a editar
        else{
            //se busaca la estuctura
            di.Structure.findById(ObjectId(info.id),function(err,str){
                if(err) console.log(err);
                else{
                    //se realizan los cambios
                    str.nombre= info.StrChg.nombre;
                    str.tipo= info.StrChg.tipo;
                    str.header= info.StrChg.header
                    str.protocol = info.StrChg.protocol;
                    str.model_ID = info.StrChg.model_ID;
                    str.structure= info.StrChg.structure;
                    //se guardan los cambios
                    str.save(function(err){
                        if(err) console.log(err);
                        else{
                            res.json(str);
                        }
                    })
                }
            })
        }    
    })


    //devulave los imeis con las caracteristicas establecidas en filter que pueden ser
    /**
    imei	 	 numero de imei
    tipo        el header
    model_ID    el numero de modelo
    Asign        si fue asignado
    install     si fue instalado
     */
    app.post('/get/trackes',function(req,res){
        var info=req.body;
        console.log(info);
        di.imeis.find(info.filter,function(err,imeis){
            if(err) console.log(err);
            else{
                res.json(imeis);
            }
        })
    })

    app.get('/get/imei/:imei',function(req,res){
        var info=req.params;
        di.imeis.findOne({'imei':info.imei},function(err,imeis){
            if(err) res.json({'err':1,data:null})
            else{
                if(imeis==null)res.json({'err':2,data:null})
                else res.json({'err':0,data:imeis});
            }
        })
    })

}