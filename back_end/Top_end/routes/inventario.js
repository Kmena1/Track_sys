
//bibliotecas necesarias
var mongoose     = require('mongoose');
var path    = require("path");
var http = require('http');
var bodyParser = require('body-parser');
var ObjectId = require('mongodb').ObjectID;

//models
//models
//El nombre de la variable de los modelos se identifican comenzando con 'd' seguido de una letra que identifica el nombre del modelo
var de=require('./../models/Empresas_models.js');
var dc=require('./../models/Clientes_models.js');
var du=require('./../models/Usuarios.js');
var di=require('./../models/inventario.js');
var dor=require('./../models/ordenes.js');

//scripts
var busqueda=require('./../Scripts/Busquedas.js')
var log=require('./../Scripts/log.js')


/***
    ******Index******
    ---rutas
***crear
    --creacion de nueva categoria:/nuevo/categoria
    --crear objeto:/nuevo/objetos
    --creacion de lotes /make/lote/
    --creacion de recetas:/add/recetas/
    --creacion de pedididos: /make/pedido
    --crearion de ordenes: /crear/orden/
    --autosave de ordenes: /save/pedido
***editar
    --cambiar de tipo la categoria:/cambiar/cat/:empresa/:cat/:NewType
    --datos de competencia de un objeto: /objeto/competencia/
    --actualizacion de la cantidad en inventario: /editar/cantidad/
    --edidicon de precio cantidad min /max y contenido: /simple/edit
    --edicion de un objeto: /editar/objeto
    --actualizacion de minimo y maximo: /trigger/:objeto/:min/:max
    --cambiar objeto de coleccion /chng/objt/colect/
    --asignar lote a producto terminado /assig/lote/PT/
    --actualizacion de la informacion de los lotes /actualizar/lotes/
    --edicion de las tandas de las formulas: /formula/tanda/
    --agregar envases a la receta/receta/add/envace/
    --asignar receta a producto /asignar/receta/
    --actualizacion de la fuerza de trabajo /actulizar/workforce/
    --actualizacion de gastos /actualizar/gastos/
    --desasignar un formula /deasignar/formulas/
    --descongelar ordenes /descongelar/orden/
    --crear reserva de productos: /reservar/objeto
    --acepatar pedido /aceptar/pedido/'
    --cambiar el estado de un pedido a entregado/resevas/entregada
    --cancelar una reserva /reserva/cancelar
    --actualizar la prioridades de las ordenes de produccion /actualizar/prioridades/
    --aprovar orden de produccion /ordenes/aprovar/
    --actualiza la prioridad de ordenes de produccion /actualizar/prioridad/ordenes/
    --actualiza la prioridad de las ordenes de salida /actualizar/orden/salida/
    --actualizar la informacion de la orden de salida /orden/actualizar/
    --finalizar produccion /finalizar/production/
    --cancelar la produccion /cancelar/production/
    --actualizar el encargado en cada orden /orden/encargado/
    --cambio del estado de una orden de produccion /orden/estado/:orden/:tipo/:estado
***buscar 
    --busca directamente todos los objetos /all/object/
    --busqueda de productos dentro de un array /productos/array/
    --busqueda del catalogo de una empresa /get/catalogo/:emp
    --busqueda de la materia prima de una empresa /get/materia/:emp
    --busqueda de invetario en orden inverso /get/inventario/revers/:id
    --busqueda de una formula en especifico /find/formula/:rct
    --buqueda de las formulas de una empresa /find/recetas/:emp/:since/:max
    --busqueda de ordenes por su consecutivo /get/concecutivo/ordenes/
    --busqueda de una cantidad de reservas /get/reservas/:obj/:max
    --buqueda de todas las reservas /get/reserv/
    --buqueda de pedidos /get/pedidos/:emp/:since/:max
    --busqueda de un pedido en especifico /get/pedido/:pdd
    --busqueda de pedidos realizado por un usuario /get/pedidos/user/:user
    --busqueda de las ordenes de produccion/get/ordenes/:emp
    --busqueda de la ultima orden de una empresa /last/orden/:emp
    --Busqueda de la orden guarda: /get/autosave/:id
***eliminar
    -- elimina un objeto del inventario /eliminar/objeto
    --eliminacion de una categoria /eliminar/cat
    --elimina el envase de una formula /formula/envase/eliminar/
    --elimiancion de una formula /delete/receta/
***indefinidos
--log
    -- obtiene los registros de la empresa /get/logs/emp/:emp
--provedores
    --edicion de un provedor /editar/proveedor
    --agregar proveerdor /add/proveedor
    --obtener los provedores de la empresa/get/provedores/:empres
--lotes
    --busqueda de un lote /get/lote/:id
    --busqueda de los lotes de una empresa /find/lotes/
--dependientes
    --agrega un dependiente a un cliente /agregar/dependiente/
    --suma puntos a un dependiente /agregar/puntos/dependiente/
--facturacion
    -- aumenta la deuda de un cliente /aumentar/deuda/
    -- busqueda de facturas /find/facturas/:cln'
    -- pagar facturas /pagar/factura/
    --creacion de una nota de credito /nota/credito/
--parches No usar salvo una emergencia
    --cambia el inicio y el final de una coleccion/chng/beforeNext/:col/:name/:last/:id
    --elimina categorias repetidas de una empresa /comentada/ /eliminar/cat/repetidas/:id
--obsoletas no se sabe si el sistema sigue usandolas no eliminar
    --agrega producto al stock /add/stock/
    --cambia el stock de un producto /change/standBy
 */
module.exports=function(app){
    console.log('inventario ready');
//crear
//------------inventario
    //crea una nueva categoria en el inventario de la empresa
    app.post('/nuevo/categoria',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
            if(err) console.log(err);
            else{
                //si la empresa de no tiene un inventario le crea uno
                if(emp.inventario==null){
                    var inv = new di.inventario({
                        empresa : emp._id,
                        categoria:[{
                            nombre: info.cat,
                            tipo:info.tipo,
                            id: null
                        }]
                    })
                    inv.save(function(err){
                        if(err) console.log(err);
                        else{
                            //actualiza el inventario de la empresa
                            emp.inventario = inv._id;
                            emp.save(function(err){
                                if(err) console.log(err);
                                else{
                                    res.json(inv);
                                }
                            })
                        }
                    })
                }
                //sino solo agraga la categoria
                else{
                    di.inventario.findById(ObjectId(emp.inventario),function(err,inv){
                        if(err) console.log(err);
                        else{
                            //verifica si existe una categoria con el nombre deseado
                            var existe=false
                            for (let i = 0; i < inv.categoria.length; i++) {
                                if(inv.categoria[i].nombre==info.cat){
                                    existe=true
                                    break
                                }
                                
                            }
                            if(!existe){
                                inv.categoria.push({
                                    nombre: info.cat,
                                    tipo:info.tipo,
                                    id: null
                                });
                                inv.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        res.json(inv);
                                    }
                                })    
                            }
                            else res.json(inv)
                        }
                    })
                }
            }
        })
    })

    //crea un objeto en una categoria
    app.post('/nuevo/objetos',function(req,res){
        var info=req.body;
        console.log(info)
        de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
            if(err) console.log(err);
            else{
                if(emp.inventario!=null){
                    di.inventario.findById(ObjectId(emp.inventario),function(err,inv){
                        //console.log(inv)
                        if(err) console.log(err);
                        else{
                            let invt = inv.categoria;
                            for (let i = 0; i < invt.length; i++) {
                                if(invt[i].nombre==info.cat){
                                    console.log(invt[i])
                                    //si es el primer objeto del catalogo
                                    if(invt[i].last == null){
                                        var obj = new di.objetos({
                                            nombre:info.Obj,
                                            precio:info.precio,
                                            cantidad:info.cantidad,
                                            codigo:info.codigo,
                                            exentos: info.exentos,
                                            descripcion : info.descripcion,
                                            Incatalogo: info.Incatalogo,
                                            unidad:info.unidad,
                                            Dolares:info.Dolares,
                                            contenidoNeto:info.contenidoNeto,
                                            convertFactor:info.convertFactor,
                                            trigger:{
                                                max:info.max,
                                                min:info.min
                                            },
                                            convertFactor:info.convertFactor,
                                            contenidoNeto:info.contenidoNeto
                                        })
                                        //se actualiza la ultima posicion en el esquema
                                        inv.categoria[i].id = obj._id;
                                        inv.categoria[i].last = obj._id;
                                        console.log(inv.categoria[i].id)
                                        obj.save(function(err){
                                            if(err) console.log(err);
                                            else{
                                                //se guarda el ultimo cambio
                                                inv.save(function(err){
                                                    if(err) console.log(err);
                                                    else{
                                                        log.make(info.empresa,
                                                        'Creacion de producto',
                                                        'Se creo el producto '+obj.codigo+'-'+obj.nombre+' de la base de datos',
                                                        {
                                                            'err':0,
                                                            'data':inv
                                                        },res)    
                                                    }
                                                })
                                            }
                                        })
                                    }
                                    //si no se busca el ultimo y se actualiza el siguiente en el ultimo 
                                    else{
                                        var obj = new di.objetos({
                                            nombre:info.Obj,
                                            precio:info.precio,
                                            cantidad:info.cantidad,
                                            codigo:info.codigo,
                                            exentos: info.exentos,
                                            descripcion : info.descripcion,
                                            Incatalogo: info.Incatalogo,
                                            before: invt[i].last,
                                            unidad:info.unidad,
                                            Dolares:info.Dolares,
                                            contenidoNeto:info.contenidoNeto,
                                            convertFactor:info.convertFactor,
                                            trigger:{
                                                max:info.max,
                                                min:info.min
                                            },
                                            convertFactor:info.convertFactor,
                                            contenidoNeto:info.contenidoNeto
                                        })
                                        let last =inv.categoria[i].last;
                                        //se actualiza la ultima posicion en el esquema y el next del ultimo objeto
                                        inv.categoria[i].last = obj._id;
                                        di.objetos.findById(ObjectId(last),function(err,objL){
                                            if(err) console.log(err);
                                            else{
                                                objL.next=obj.id
                                                obj.save(function(err){
                                                    if(err) console.log(err);
                                                    else{
                                                       //se guarda el ultimo cambio
                                                       objL.save(function(err){
                                                           if(err) console.log(err);
                                                           else{
                                                               inv.save(function(err){
                                                                   if(err) console.log(err);
                                                                   else{
                                                                        log.make(info.empresa,
                                                                        'Creacion de producto',
                                                                        'Se creo el producto '+obj.codigo+'-'+obj.nombre+' de la base de datos',
                                                                        {
                                                                            'err':0,
                                                                            'data':inv
                                                                        },res)       
                                                                   }
                                                               })
                                                           }
                                                       })
                                                   }
                                               }) 
                                            }
                                        })
                                    }
                                    break;
                                }
                                else{
                                    //si no exite la categoria se devuelve el error 2
                                    if(i==invt.length-1){
                                        res.json({
                                            'err':2,
                                            'data':null
                                        })
                                    }
                                }
                            }
                        }
                    })
                }
                else{
                    res.json({
                        'err':1,
                        'data':null
                    })
                }
            }
        })
    })

    app.post('/make/lote/',function(req,res){
        var info=req.body;
        di.objetos.findById(ObjectId(info.obj),function(err,objt){
            if(err) console.log(err);
            else{
                var lote = new di.lotes({
                    factura:info.factura,
                    Numero:info.numero,
                    Proveedor:info.proveedor,
                    FProduccion:info.FProduccion,
                    FVencimiento:info.FVencimiento,
                    FIngreso:new Date(),
                    Cas:info.Cas, 
                    cantidad:info.cantidad
                })
                lote.save(function(err){
                    if(err) console.log(err);
                    else{
                        objt.lotes.push(lote._id)
                        objt.cantidad=objt.cantidad+info.cantidad
                        objt.save(function(err){
                            if(err) console.log(err);
                            else{
                                log.make(info.empresa,
                                    'Ingreso de producto',
                                    'Se ingreso '+info.cantidad+' del producto '+objt.codigo+'-'+objt.nombre+' Con el lote: '+info.numero,
                                    lote,res)
                            }
                        })
                    }
                })
            }
        })
    })

//------------recetas
    //crear recetas y edita recetas
    app.post('/add/recetas/',function(req,res){
        var info=req.body;
        console.log(info)
        //se busca la empresa que crea las recetas
        de.Empresas.findById(ObjectId(info.id),function(err,emp){
            if(err) console.log(err);
            else{
                //si se agrega una nueva receta
                if(info.receta==null){
                    //se crea la receta
                    var recetas=new di.recetas({
                        nombre:info.nombre,
                        codigo:info.codigo,
                        tipo:info.tipo,
                        ingredientes:info.ingredientes,
                        before:emp.recetas_last
                    })
                    recetas.save(function(err){
                        if(err) console.log(err);
                        else{                            
                            if(info.objeto){
                                //se indexa en el objeto
                                di.objetos.findById(ObjectId(info.objeto),function(err,objt){
                                    if(err) console.log(err);
                                    else{
                                        //asignacion de la recta la objeto
                                        objt.receta=recetas._id;
                                        objt.save(function(err){
                                            if(err) console.log(err);
                                            else{
                                                //si es la primera receta creada
                                                if(emp.recetas_last==null){
                                                    emp.recetas=recetas._id;
                                                    emp.recetas_last=recetas._id;
                                                    emp.save(function(err){
                                                        if(err) console.log(err);
                                                        else{
                                                            res.json(recetas)
                                                        }
                                                    })
                                                }
                                                //si existen recetas previamente creadas
                                                else{
                                                    di.recetas.findById(ObjectId(emp.recetas_last),function(err,lastRecet){
                                                        if(err) console.log(err);
                                                        else{
                                                            console.log(lastRecet)
                                                            lastRecet.next=recetas._id;
                                                            emp.recetas_last=recetas._id;
                                                            emp.save(function(err){
                                                                if(err) console.log(err);
                                                                else{
                                                                    lastRecet.save(function(err){
                                                                        if(err) console.log(err);
                                                                        else{
                                                                            res.json(recetas)
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            }
                                        })
                                    }
                                })
                            }
                            else{
                                //si es la primera receta creada
                                if(emp.recetas_last==null){
                                    emp.recetas=recetas._id;
                                    emp.recetas_last=recetas._id;
                                    emp.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            res.json(recetas)
                                        }
                                    })
                                }
                                //si existen recetas previamente creadas
                                else{
                                    di.recetas.findById(ObjectId(emp.recetas_last),function(err,lastRecet){
                                        if(err) console.log(err);
                                        else{
                                            console.log(lastRecet)
                                            lastRecet.next=recetas._id;
                                            emp.recetas_last=recetas._id;
                                            emp.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    lastRecet.save(function(err){
                                                        if(err) console.log(err);
                                                        else{
                                                            res.json(recetas)
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            }
                        }
                    })
                }
                //si se desea editar una receta
                else{
                    di.recetas.findById(ObjectId(info.receta),function(err,rct){
                        if(err) console.log(err);
                        else{
                            rct.nombre=info.nombre;
                            rct.codigo=info.codigo;
                            rct.tipo=info.tipo;
                            rct.ingredientes=info.ingredientes;
                            rct.save(function(err){
                                if(err) console.log(err);
                                else{
                                    res.json(rct)
                                }
                            })
                        }
                    })
                }
            }
        })
    })

//------------pedidos
    app.post('/make/pedido',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
            if(err) console.log(err);
            else{
                du.Usuarios.findById(ObjectId(info.agente),function(err,usr){
                    if(err) console.log(err);
                    else{
                        usr.pedido_guardado={
                            reservas:[],
                            notas:null,
                            resposable:null,
                            contado:false,
                            Abono:0,
                            total:0,
                            mensaje:null,
                            transporte:null,
                        }
                        usr.save(function(err){
                            if(err) console.log(err);
                            else{
                                
                            }
                        })
                    }
                })   
                if(emp.pedidos==null){
                    var pedido = new di.pedido({
                        before:null,
                        next:null,
                        cliente:info.cliente,
                        resposable:info.resposable,
                        agente:info.agente,
                        contado:info.contado,
                        Abono:info.abono,
                        total:info.total,
                        transporte:info.transporte,
                        fecha: new Date(),
                        reservas:info.reservas,
                        consecutivo:0
                    })
                    pedido.save(function(err){
                        if(err) console.log(err);
                        else{
                            emp.pedidos= pedido._id;
                            emp.pedidoslast=pedido._id;
                            emp.save(function(err){
                                if(err) console.log(err);
                                else{
                                    res.json(pedido)
                                }
                            })
                        }
                    })
                }
                else{
                    di.pedido.findById(ObjectId(emp.pedidoslast),function(err,pddl){
                        if(err) console.log(err);
                        else{
                            var consecutivo=pddl.consecutivo+1
                            console.log(consecutivo)
                             var pedido = new di.pedido({
                                before:pddl._id,
                                next:null,
                                cliente:info.cliente,
                                agente:info.agente,
                                resposable:info.resposable,
                                contado:info.contado,
                                Abono:info.abono,
                                total:info.total,
                                transporte:info.transporte,
                                fecha: new Date(),
                                reservas:info.reservas,
                                consecutivo:consecutivo
                            })
                            pedido.save(function(err){
                                if(err) console.log(err);
                                else{
                                    pddl.next = pedido._id;
                                    pddl.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            emp.pedidoslast=pedido._id;
                                            emp.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    res.json(pedido)
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            }
        })
    })

//------------ordenes
    app.post('/crear/orden/',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                console.log(info,emp.ordenes)
                switch (Number(info.tipo)) {
                    //salida
                    case 0:
                        var Osalida=new dor.Salida({
                            cliente: info.cliente,
                            Transporte: info.Transporte,
                            prductos:JSON.parse(info.productos),
                            before:emp.ordenes.Salida,
                            empresa:info.emp
                        })
                        console.log(Osalida)
                        Osalida.save(function(err){
                            if(err) console.log(err);
                            else{
                                emp.ordenes.Salida=Osalida._id;
                                emp.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        res.json(Osalida)
                                    }
                                })
                            }
                        })
                        break;
                    //produccion
                    case 1:
                        var OProduccion=new dor.Produccion({
                            numero:info.numero,
                            Fecha_creacion:new Date(),
                            Aprovado: Boolean(info.Aprv),
                            Apr_By:info.Apr_By,
                            prductos:JSON.parse(info.productos),
                            before:emp.ordenes.Produccion,
                            formula:info.formula,
                            empresa:info.emp,
                            comentario:info.comentario,
                        })
                        OProduccion.save(function(err){
                            if(err) console.log(err);
                            else{
                                emp.ordenes.Produccion=OProduccion._id;
                                emp.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        restatMP(info.MP,0,function(data){
                                            texto=''
                                            for (let i = 0; i < data.length; i++) {
                                                texto =texto+ data[i].nombre+'\tCantidad:'+data[i].cantidad+'\n';
                                                
                                            }
                                            log.make(info.empresa,
                                                'Creacion de orden de produccion',
                                                'Se solicito la crecion de la orden de produccion '+info.numero+'-'+info.formula+' con la amteria prima productos \n'+texto,
                                                OProduccion,res)
                                        })
                                    }
                                })
                            }
                        })
                        break;
                    //produccion
                    case 2:
                        var OFrozen=new dor.Frozen({
                            cliente: info.cliente,
                            Transporte: info.Transporte,
                            prductos:JSON.parse(info.productos),
                            before:emp.ordenes.Frozen,
                            empresa:info.emp
                        })
                        OFrozen.save(function(err){
                            if(err) console.log(err);
                            else{
                                emp.ordenes.Frozen=OFrozen._id;
                                emp.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        res.json(OFrozen)
                                    }
                                })
                            }
                        })
                        break;
                    default:
                        break;
                } 
                       
            }
        })
    })

    app.post('/save/pedido',function(req,res){
        var info=req.body;
        du.Usuarios.findById(ObjectId(info.id),function(err,usr){
            if(err) console.log(err);
            else{
                console.log(info,usr.pedido_guardado)
                usr.pedido_guardado=info.pedido;
                usr.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(usr)
                    }
                })
            }
        })
    })
//editar
//------------inventario
    app.get('/cambiar/cat/:empresa/:cat/:NewType',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
            if(err) console.log(err);
            else{
                if(emp.inventario!=null){
                    di.inventario.findById(ObjectId(emp.inventario),function(err,inv){
                        //console.log(inv)
                        if(err) console.log(err);
                        else{
                            let invt = inv.categoria;
                            for (let i = 0; i < invt.length; i++) {
                                if(invt[i].nombre==info.cat){
                                    inv.categoria[i].tipo=info.NewType;
                                    inv.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            res.json(inv)
                                        }
                                    })
                                }
                            }
                        }
                    })
                }
            }
        })
    })

    app.post('/objeto/competencia/',function(req,res){
        var info=req.body;
        di.objetos.findById(ObjectId(info.obj),function(err,obj){
            //console.log(inv)
            if(err) console.log(err);
            else{
                obj.competencia=info.competencia
                obj.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(obj)
                    }
                })
            }
        })
    })

    app.post('/editar/cantidad/',function(req,res){
        var info=req.body;
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err) console.log(err);
            else{
                //actualizan los datos
                obj.cantidad = obj.cantidad+info.cantidad;
                //se guardan los cambios
                obj.save(function(err){
                    if(err){
                        console.log(err);
                        res.json(err);
                    }
                    else{
                        
                        log.make(info.empresa,
                        'Edicion de objeto',
                        'El usuario '+info.user+' agrego la cantidad:'+info.cantidad+' al objeto '+obj.codigo+'-'+obj.nombre,
                        obj,res)
                    }
                })
            }
        })
    })

    app.post('/simple/edit',function(req,res){
        var info=req.body;
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err){
                res.json({'err':1})
            }
            else{
                if(obj){
                    obj.precio=Number(info.precio);
                    obj.cantidad=Number(info.cantidad);
                    obj.trigger.min=Number(info.min);
                    obj.trigger.max=Number(info.max);
                    obj.contenidoNeto=Number(info.contenido);
                    obj.save(function(err){
                        if(err) res.json({'err':3})
                        else{
                            log.make(info.empresa,
                            'Edicion de objeto',
                            'El usuario '+info.user+' edito:\nCantidad->'+info.cantidad+' \nPrecio->'+info.precio+'\nMinimo->'+info.min+'\nMaximo->'+info.max+'\nContenido neto->'+info.contenido+'\nAl objeto '+obj.codigo+'-'+obj.nombre,
                            obj,res)
                        }
                    })
                }
                else{
                    res.json({'err':2})
                }
            }
        })
    })

    app.post('/editar/objeto',function(req,res){
        var info=req.body;
        //se busca el id
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err) console.log(err);
            else{
                //actualizan los datos
                obj.nombre = info.nombre;
                obj.precio = info.precio;
                obj.cantidad = info.cantidad;
                obj.codigo = info.codigo;
                obj.exentos= info.exentos;
                obj.descripcion = info.descripcion;
                obj.Incatalogo= info.Incatalogo;
                obj.unidad=info.unidad;
                obj.Dolares=info.Dolares;
                obj.contenidoNeto=info.contenidoNeto;
                obj.convertFactor=info.convertFactor;
                obj.puntos=info.puntos
                //se guardan los cambios
                obj.save(function(err){
                    if(err){
                        console.log(err);
                        res.json(err);
                    }
                    else{
                        log.make(info.empresa,
                            'Edicion de objeto',
                            'El usuario: '+info.user+'Se edito:\nCantidad->'+info.cantidad+' \nPrecio->'+info.precio+'\nDescripcion->'+info.descripcion+'\nContenido neto->'+info.contenidoNeto+'\nAl objeto '+obj.codigo+'-'+obj.nombre,
                            obj,res)
                    }
                })
            }
        })
    })

    app.get('/trigger/:objeto/:min/:max',function(req,res){
        var info=req.params;
        console.log(info)
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err) console.log(err);
            else{
                var max=info.max
                var min=info.min
                console.log(max,min)
                if(Number(min)>Number(max)){
                    obj.trigger.max=min
                    obj.trigger.min=max

                }
                else{
                    obj.trigger.min=min
                    obj.trigger.max=max
                }
                console.log(obj.trigger)
                obj.save(function(err){
                    if(err) console.log(err);
                    else{
                       log.make(info.empresa,
                            'Edicion de objeto',
                            'Se edito:\nMinimo->'+info.min+'\nMaximo->'+info.max+'\nContenido neto->'+info.contenido+'\nAl objeto '+obj.codigo+'-'+obj.nombre,
                            obj,res)
                    }
                })
            }
        })
    })

    app.post('/chng/objt/colect/',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                if(emp.inventario){
                    di.inventario.findById(ObjectId(emp.inventario),function(err,invt){
                        if(err) console.log(err);
                        else{
                            //busqueda del inventario destinatario y receptor
                            for (let i = 0; i < invt.categoria.length; i++) {
                                const cat = invt.categoria[i];
                                if(cat.nombre==info.last){
                                    //busca el objeto a mover
                                    di.objetos.findById(ObjectId(info.objeto),function(err,obj){
                                        if(err) console.log(err);
                                        else{
                                            //busqueda de la nueva categoria
                                            for (let j = 0; j < invt.categoria.length; j++) {
                                                const cat2 = invt.categoria[j];
                                                if(cat2.nombre==info.new){
                                                    //busqueda del ultimo producto en el nuevo inventario
                                                    di.objetos.findById(ObjectId(cat2.last),function(err,objcl){
                                                        //verifica si el producto es el primero o el ultimo de la categoria
                                                        if(cat.id==info.objeto || cat.last==info.objeto){
                                                            if(cat.id==info.objeto){
                                                                invt.categoria[i].id=obj.next
                                                            }
                                                            else {
                                                                invt.categoria[i].last=obj.before
                                                            }
                                                        }
                                                        else{

                                                        }
                                                    })
                                                }
                                            }
                                        }
                                    })
                                }
                            }
                        }
                    })
                }
                else res.json(null)
            }
        })

    })

    app.post('/assig/lote/PT/',function(req,res){
        var info=req.body;
        makeLotePT(info.data,0,[],function(response){
            texto=''
            for (let i = 0; i < response.length; i++) {
                texto =texto+ response[i].nombre+'\tCantidad actual:'+data[i].obj+'\n';               
            }
            log.make(info.empresa,
                'Creacion de lotes',
                'Se creo el lote para los productos \n'+texto,
                response,res)
        })
    })

    app.post('/actualizar/lotes/',function(req,res){
        var info=req.body;
        actualizarLotes(info.data,0,function(response){
            res.json(response)
        })
    })
//------------formulas
    app.post('/formula/tanda/',function(req,res){
        var info=req.body;
        di.recetas.findById(ObjectId(info.rct),function(err,rct){
            if(err) console.log(err);
            else{
                if(rct.Tanda.length>0){
                    for (let i = 0; i < rct.Tanda.length; i++) {
                        if(rct.Tanda[i].cantidad==info.size){
                            rct.Tanda[i].workforce=info.workforce
                            rct.Tanda[i].expenses=info.expenses
                        }
                        else{
                            if(i+1 == rct.Tanda.length){
                                rct.Tanda.push({
                                    cantidad:info.size,
                                    workforce:info.workforce,
                                    expenses:info.expenses,
                                })
                            }
                        }
                        
                    }
                }
                else{
                     rct.Tanda.push({
                        cantidad:info.size,
                        workforce:info.workforce,
                        expenses:info.expenses,
                    })
                }
                rct.save(function(err){
                    if(err) console.log(err);
                    else{
                        log.make(info.empresa,
                        'Edicion de formula',
                        'Se agrego la tanda:\nCantidad:'+info.size+'\nFuerza laboral:'+info.workforce+'\nGastos:'+info.expenses+'\nA la formula'+rct.codigo+'-'+rct.nombre,
                        rct,res)
                    }
                })
            }
        })
    })

    app.post('/receta/add/envace/',function(req,res){
        var info=req.body;
        di.recetas.findById(ObjectId(info.receta),function(err,rct){
            var encontrada=false;
            for (let i = 0; i < rct.envase.length; i++) {
                if(rct.envase[i].asignado==info.data.asignado){
                    //se edita
                    encontrada=true;
                    rct.envase[i]=info.data
                    break;
                }
            }
            if(!encontrada)rct.envase.push(info.data);
            rct.save(function(err){
                if(err) console.log(err);
                else{
                    res.json(rct)
                }
            })
        })
    })

    app.post('/asignar/receta/',function(req,res){
        var info=req.body;
        console.log(info)
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err) console.log(err);
            else{
                obj.receta=info.receta
                obj.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(obj)
                    }
                })
            }
        })
    })

    app.post('/actulizar/workforce/',function(req,res){
        var info=req.body;
        di.recetas.findById(ObjectId(info.id),function(err,rct){
            if(err) console.log(err);
            else{
                rct.workforce=info.workforce,
                rct.save(function(err){
                    if(err) console.log(err);
                    else{
                        console.log(rct)
                        res.json(rct)
                    }
                })
            }
        })
    })

    app.post('/actualizar/gastos/',function(req,res){
        var info=req.body;
        var gastos={
            Electricidad:info.Electricidad/info.tanda,
            Agua:info.Agua/info.tanda,
            Alquiler:info.Alquiler/info.tanda,
        }
        di.recetas.findById(ObjectId(info.id),function(err,rct){
            if(err) console.log(err);
            else{
                rct.expenses=gastos,
                rct.save(function(err){
                    if(err) console.log(err);
                    else{
                        console.log(rct)
                        res.json(rct)
                    }
                })
            }
        })
    })

    app.post('/deasignar/formulas/',function(req,res){
        var info=req.body;
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err) console.log(err);
            else{
                di.recetas.findById(ObjectId(info.formula),function(err,rct){
                    if(err) console.log(err);
                    else{
                        for (let i = 0; i < rct.envase.length; i++) {
                            if(obj._id == rct.envase[i].asignado){
                                rct.envase.splice(i,1);
                                break
                            }
                        }
                        rct.save(function(err){
                            if(err) console.log(err);
                            else{
                                obj.receta=null
                                obj.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        res.json(obj)
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    })

//------------pedidos
    app.post('/descongelar/orden/',function(req,res){
        var info=req.body;
        dor.Frozen.findOne({consecutivo:info.concecutivo}, function(err, congelado){
            if(err) console.log(err)
            else{
                if(congelado!=null){
                    dor.Salida.findOne({consecutivo:info.concecutivo}, function(err, Salida){
                        if(Salida){
                            Salida.prductos.push(congelado.prductos[info.i])
                            Salida.Fmodificacion=new Date();
                            Salida.estado=0;
                            Salida.save(function(err){
                                if(err) console.log(err);
                                else{
                                    di.objetos.findById(ObjectId(congelado.prductos[info.i].producto),function(err,objt){
                                        if(err) console.log(err);
                                        else{
                                            objt.cantidad=objt.cantidad-congelado.prductos[info.i].cantidad
                                            objt.save(function(err){
                                                if(err) console.log(err);
                                                else{
                                                    congelado.prductos.splice(info.i,1);
                                                    congelado.save(function(err){
                                                        if(err) console.log(err);
                                                        else{
                                                            res.json(congelado)
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                        else{
                            var salidas=new dor.Salida({
                                Transporte:congelado.Transporte,
                                Fmodificacion:new Date(),
                                cliente:congelado.cliente,
                                consecutivo:congelado.consecutivo,
                                prductos:[
                                    congelado.prductos[info.i]
                                ]
                            })
                            salidas.save(function(err){
                                if(err) console.log(err);
                                else{
                                    congelado.prductos.splice(inof.i,1);
                                    congelado.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            res.json(congelado)
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            }
        })
    })

    app.post('/reservar/objeto',function(req,res){
        var info=req.body;
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err) console.log(err);
            else{
                //si no hay suficiente productos se genera una alerta
                //if(obj.cantidad < info.cantidad){}
                //se actualiza la cantidad de elementos que quedan disponibles
                //obj.cantidad=obj.cantidad-info.cantidad;
                //se crea reserva se guardan los cambios
                var resv= new di.reservas({
                    objeto:obj._id,
                    cantidad:info.cantidad,
                    cliente:info.cliente,
                    agente:info.agente,
                    descuento:info.descuento,
                    entregado:false,
                });
                resv.save(function(err){
                    if(err) console.log(err);
                    else{
                        //se asigna la reserva del objeto
                        obj.reservados.push(resv._id);
                        obj.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json(resv)
                            }
                        })
                    }
                })
            }
        })
    })

    app.post('/aceptar/pedido/',function(req,res){
        var info=req.body;
        di.pedido.findById(ObjectId(info.pedido),function(err,reserv){
            if(err) console.log(err);
            else{
                if(reserv.aprovado==false){
                    reserv.factura=0;
                    reserv.aprvby=info.id;
                    reserv.aprovado=true;
                    reserv.total=info.finalPrice+info.otros*1.13;
                    reserv.transporte=info.transporte;
                    reserv.reservas=info.reservas;
                    reserv.mensaje=info.mensaje;
                    reserv.otros=info.otros
                    //reserv.resposable=info.resposable
                    reserv.Faprovado=new Date()
                    reserv.save(function(err){
                        if(err) console.log(err);
                        else{
                            console.log(info)
                            Makefactura(reserv,function(){
                                if(info.reservas.length>0){
                                    restarInventario(info.reservas,0,[],[],info.empresa,function(cliente,salida,congelado){
                                        console.log(salida,congelado)
                                        //esquema de salida
                                        if(salida.length>0){
                                            de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
                                                if(err) console.log(err);
                                                else{
                                                    var sald =dor.Salida({
                                                        cliente: info.cliente,
                                                        Transporte: reserv.transporte,
                                                        prductos:salida,
                                                        before:emp.ordenes.Salida,
                                                        consecutivo:reserv.consecutivo,
                                                        Fecha_creacion:new Date(),
                                                        Apr_By:info.id,
                                                        empresa:info.empresa
                                                    })
                                                    sald.save(function(err){
                                                        if(err) console.log(err);
                                                        else{
                                                            emp.ordenes.Salida=sald._id
                                                            emp.save(function(err){
                                                                if(err) console.log(err);
                                                            })
                                                        }
                                                    })
                                                    
                                                }
                                            })
                                        }
                                        //esquema de congelado
                                        if(congelado.length>0){
                                            de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
                                                if(err) console.log(err);
                                                else{
                                                    var cong =dor.Frozen({
                                                        Fecha_solicitud:new Date(),
                                                        cliente: info.cliente,
                                                        Transporte: reserv.transporte,
                                                        prductos:congelado,
                                                        before:emp.ordenes.Frozen,
                                                        consecutivo:reserv.consecutivo,
                                                        empresa:info.empresa
                                                    })
                                                    cong.save(function(err){
                                                        if(err) console.log(err);
                                                        else{
                                                            emp.ordenes.Frozen=cong._id
                                                            emp.save(function(err){
                                                                if(err) console.log(err);
                                                                else{
                                                                    
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })    
                                        }
                                        texto=''
                                        for (let i = 0; i < salida.length; i++) {
                                            texto = texto+ salida[i].nombre+'\tCantidad:'+salida[i].cantidad+'\n';
                                            
                                        }
                                        log.make(info.empresa,
                                            'Reserva se productos',
                                            'Se solicitaron los productos \n'+texto,
                                            {
                                                salida:salida,
                                                congelado:congelado
                                            },res)
                                    })
                                }
                                else{
                                    res.json(reserv)
                                }
                            })
                        }
                    })
                }
                else{
                    res.json(null)
                }
            }
        })
    })

    //acciones sobre reservas
    app.post('/resevas/entregada',function(req,res){
        var info=req.body;
        di.reservas.findById(ObjectId(info.reserva),function(err,resv){
            if(err) console.log(err);
            else{
                resv.entregado = info.entregado;
                resv.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(obj)
                    }
                })
            }
        })
    })

    //cancelar reserva
    app.post('/reserva/cancelar',function(req,res){
        var info=req.body;
        //busqueda del objeto y la reserva
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err) console.log(err);
            else{
                di.reservas.findById(ObjectId(info.reserva),function(err,resv){
                    if(err) console.log(err);
                    else{
                        di.pedido.findById(ObjectId(info.pedido),function(err,pdd){
                            if(err) console.log(err);
                            else{
                                //se devuelve el objeto a la normalidad
                                //obj.cantidad=obj.cantidad+resv.cantidad;
                                obj.reservados.splice(obj.reservados.indexOf(resv._id),1);
                                pdd.reservas.splice(pdd.reservas.indexOf(resv._id),1);
                                obj.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        if(pdd.reservas.length==0){
                                            //falta continuidad
                                            de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
                                                if(err) console.log(err);
                                                else{
                                                    //reasignacion del anterior
                                                    if(pdd.before == null) {
                                                        emp.pedidos = pdd.next
                                                        //reasignacion del sguiente
                                                        if(pdd.next==null){
                                                            emp.pedidoslast=pdd.before;
                                                            emp.save(function(err){
                                                                if(err) console.log(err);
                                                                else{
                                                                    pdd.remove(function(err){
                                                                        if(err)console.log(err)
                                                                        else {
                                                                            resv.remove(function(err){
                                                                                if(err)console.log(err)
                                                                                else res.json(obj)
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                        //
                                                        else{
                                                            di.pedido.findById(ObjectId(pdd.next),function(err,ppdn){
                                                                if(err) console.log(err);
                                                                else{
                                                                    ppdn.before=pdd.before;
                                                                    ppdn.save(function(err){
                                                                        if(err) console.log(err);
                                                                        else{
                                                                           emp.save(function(err){
                                                                                if(err) console.log(err);
                                                                                else{
                                                                                    pdd.remove(function(err){
                                                                                        if(err)console.log(err)
                                                                                        else {
                                                                                            resv.remove(function(err){
                                                                                                if(err)console.log(err)
                                                                                                else res.json(obj)
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            }) 
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    }  
                                                    else{
                                                        di.pedido.findById(ObjectId(pdd.before),function(err,ppdb){
                                                            if(err) console.log(err);
                                                            else{
                                                                ppdb.next=pdd.next;
                                                                ppdb.save(function(err){
                                                                    if(err) console.log(err);
                                                                    else{
                                                                        if(pdd.next==null){
                                                                            emp.pedidoslast=pdd.before;
                                                                            emp.save(function(err){
                                                                                if(err) console.log(err);
                                                                                else{
                                                                                    pdd.remove(function(err){
                                                                                        if(err)console.log(err)
                                                                                        else {
                                                                                            resv.remove(function(err){
                                                                                                if(err)console.log(err)
                                                                                                else res.json(obj)
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                        //
                                                                        else{
                                                                            di.pedido.findById(ObjectId(pdd.next),function(err,ppdn){
                                                                                if(err) console.log(err);
                                                                                else{
                                                                                    ppdn.before=pdd.before;
                                                                                    ppdn.save(function(err){
                                                                                        if(err) console.log(err);
                                                                                        else{
                                                                                        emp.save(function(err){
                                                                                                if(err) console.log(err);
                                                                                                else{
                                                                                                    pdd.remove(function(err){
                                                                                                        if(err)console.log(err)
                                                                                                        else {
                                                                                                            resv.remove(function(err){
                                                                                                                if(err)console.log(err)
                                                                                                                else res.json(obj)
                                                                                                            })
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            }) 
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }  
                                                }
                                            })
                                        }
                                        else pdd.save(function(err){
                                            if(err)console.log(err)
                                            else {
                                                resv.remove(function(err){
                                                    if(err)console.log(err)
                                                    else res.json(obj)
                                                })
                                            }
                                        })
                                    }
                                })        
                            }
                        })
                    }
                })
            }
        })
    })

//------------odenes
    app.post('/actualizar/prioridades/',function(req,res){
        var info=req.body;
        ActPrioridad(info.ordenes,0,res)
    })

    app.post('/ordenes/aprovar/',function(req,res){
        var info=req.body;
        console.log(info)
        switch (Number(info.tipo)) {
            //salida de producto
            case 0:
                dor.Salida.findById(ObjectId(info.id),function(err,ord){
                    if(err) console.log(err);
                    else{
                        ord.Fecha_Salida=new Date();
                        ord.Aprovado=true;
                        ord.Apr_By=info.usr;
                        ord.prductos=info.productos;
                        ord.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json(ord)
                            }
                        })
                    }
                })
                break;
            //aprovar orden de produccion
            case 1:
                dor.Produccion.findById(ObjectId(info.id),function(err,ord){
                    if(err) console.log(err);
                    else{
                        ord.Aprovado=true;
                        ord.Apr_By=info.usr;
                        ord.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json(ord)
                            }
                        })
                    }
                })
            //salida de produccion
            case 2:
                dor.Produccion.findById(ObjectId(info.id),function(err,ord){
                    if(err) console.log(err);
                    else{
                        ord.Fecha_creacion=new Date();
                        ord.estado=4;
                        ord.save(function(err){
                            if(err) console.log(err);
                            else{
                                res.json(ord)
                            }
                        })
                    }
                })
            default:
                break;
        }
    })

    app.post('/actualizar/prioridad/ordenes/',function(req,res){
        var info=req.body;
        actualizarPrioridad(info.productos,0,function(){
            res.json(info.productos)
        })
    })

    app.post('/actualizar/orden/salida/',function(req,res){
        var info=req.body;
        //console.log(info)
        dor.Salida.findById(ObjectId(info.orden),function(err,ord){
            if(err) console.log(err);
            else{
                ord.estado=Number(info.estado)
                if(info.estado==3){
                    ord.Apr_By=info.user
                    ord.Aprovado=true;
                    ord.Fecha_Salida=new Date()
                }
                else ord.Fmodificacion=new Date()
                ord.prductos=info.productos;
                ord.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(ord)
                    }
                })
            }
        })
    })

    app.post('/orden/actualizar/',function(req,res){
        var info=req.body;
        dor.Produccion.findById(ObjectId(info.orden),function(err,ord){
            if(err) console.log(err);
            else{
                ord.prductos=info.productos
                ord.Act_By=info.user
                ord.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(ord)
                    }
                })
            }
        })
    })

    app.post('/finalizar/production/',function(req,res){
        var info=req.body;
        dor.Produccion.findById(ObjectId(info.orden),function(err,ord){
            if(err) console.log(err);
            else{
                ord.estado=3
                ord.encargado=info.encargado
                ord.Fmodificacion=new Date()
                ord.fecha=info.fechas
                ord.save(function(err){
                    actulizarLotes(info.data,0,function(data){
                        if(err) {
                            log.make(info.empresa,
                                'Error en la Finalizacion de Orden de produccion',
                                'Se produjo un error en la finalizo de la orden de produccion '+ord.numero+'-'+ord.formula+'\nCon el error: '+err,
                                ord,res)
                        }
                        else{
                            texto=''
                            for (let i = 0; i < data.length; i++) {
                                texto =texto+ data[i].nombre+'\tSobrante:'+data[i].diferencia+'\n';               
                            }
                            log.make(info.empresa,
                                'Finalizacion de Orden de produccion',
                                'Se finalizo la orden de produccion '+ord.numero+'-'+ord.formula+' con la diferencia materia prima productos \n'+texto,
                                ord,res)
                        }
                    })
                })
            }
        })
    })

    app.post('/cancelar/production/',function(req,res){
        var info=req.body;
        dor.Produccion.findById(ObjectId(info.orden),function(err,ord){
            if(err) console.log(err);
            else{
                ord.estado=6
                ord.Fmodificacion=new Date()
                ord.save(function(err){
                    actulizarLotes(info.data,0,function(data){
                        if(err) {
                            log.make(info.empresa,
                                'Error en la Finalizacion de Orden de produccion',
                                'Se produjo un error en la finalizo de la orden de produccion '+ord.numero+'-'+ord.formula+'\nCon el error: '+err,
                                ord,res)
                        }
                        else{
                            texto=''
                            for (let i = 0; i < data.length; i++) {
                                texto =texto+ data[i].nombre+'\tSobrante:'+data[i].diferencia+'\n';               
                            }
                            log.make(info.empresa,
                                'Finalizacion de Orden de produccion',
                                'Se finalizo la orden de produccion '+ord.numero+'-'+ord.formula+' con la diferencia materia prima productos \n'+texto,
                                ord,res)
                        }
                    })
                })
            }
        })
    })

    app.post('/orden/encargado/',function(req,res){
        var info=req.body;
        dor.Produccion.findById(ObjectId(info.orden),function(err,ord){
            if(err) console.log(err);
            else{
                ord.encargado=info.encargado
                ord.fecha=info.fechas
                ord.Fmodificacion=new Date()
                ord.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(ord)
                    }
                })
            }
        })
    })

    app.get('/orden/estado/:orden/:tipo/:estado',function(req,res){
        var info=req.params;
        if(info.tipo=='salida'){
            dor.Salida.findById(ObjectId(info.orden),function(err,ord){
                if(err) console.log(err);
                else{
                    ord.estado=Number(info.estado)
                    ord.Fmodificacion=new Date()
                    ord.save(function(err){
                        if(err) console.log(err);
                        else{
                            res.json(ord)
                        }
                    })
                }
            })
        }
        else{
            dor.Produccion.findById(ObjectId(info.orden),function(err,ord){
                if(err) console.log(err);
                else{
                    ord.estado=Number(info.estado)
                    ord.Fmodificacion=new Date()
                    ord.save(function(err){
                        if(err) console.log(err);
                        else{
                            res.json(ord)
                        }
                    })
                }
            })
        }
    })
//buscar
//------------inventario
    app.get('/all/object/',function(req,res){
        var info=req.params;
        di.objetos.find({},function(err,obj){
            if(err)console.log(err)
            else res.json(obj)
        })
    })

    //buscar el inventario de una empresa
    app.get('/get/inventario/:emp',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.emp),function(err,empr){
            if(err) console.log(err);
            else{
                if(empr.inventario==null) res.json({
                    'err':1,
                    'data':null,
                    'keys':null,
                })
                else{
                    di.inventario.findById(ObjectId(empr.inventario),function(err,inv){
                        if(err) console.log(err);
                        else{
                            busqueda.findInventario(inv,null,null,0,{},res)
                        }
                    })
                }
            }
        })
    })

    app.post('/productos/array/',function(req,res){
        var info=req.body;
        console.log(info.productos.length)
        busqueda.productosArray(info.productos,0,info.productos.length+1,{},res)
    })

    app.get('/get/catalogo/:emp',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.emp),function(err,empr){
            if(err) console.log(err);
            else{
                if(empr.inventario==null) res.json({
                    'err':1,
                    'data':null,
                    'keys':null,
                })
                else{
                    di.inventario.findById(ObjectId(empr.inventario),function(err,inv){
                        if(err) console.log(err);
                        else{ 
                            busqueda.findcatalogo(inv,null,null,0,{},res)
                        }
                    })
                }
            }
        })
    })

    app.get('/get/materia/:emp',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.emp),function(err,empr){
            if(err) console.log(err);
            else{
                if(empr.inventario==null) res.json({
                    'err':1,
                    'data':null,
                    'keys':null,
                })
                else{
                    di.inventario.findById(ObjectId(empr.inventario),function(err,inv){
                        if(err) console.log(err);
                        else{ 
                            busqueda.findMateria(inv,null,null,0,{},res)
                        }
                    })
                }
            }
        })
    })

    app.get('/get/inventario/revers/:id',function(req,res){
        var info=req.params;
        busqueda.findRevInvt(info.id,[],res)
    })
    
//------------formulas
    app.get('/find/formula/:rct',function(req,res){
        var info=req.params;
        di.recetas.findById(ObjectId(info.rct),function(err,rct){
            if(err) console.log(err);
            else{
                res.json(rct)
            }
        })
    })

    app.get('/find/recetas/:emp/:since/:max',function(req,res){
        var info=req.params;
        if(info.since=='null' || info.since ==null){
            de.Empresas.findById(ObjectId(info.emp),function(err,emp){
                if(err) console.log(err);
                else{
                    if(emp.recetas!=null) busqueda.recetas(emp.recetas,0,info.max,[],res)
                    else res.json({
                        'err':0,
                        'data':[]
                    })
                }
            })
        }
        else{
            busqueda.recetas(since,0,info.max,[],res)
        }
    })

//------------ordenes
    app.post('/get/concecutivo/ordenes/',function(req,res){
        var info=req.body;
        var response={}
        console.log(info)
        dor.Salida.findOne({consecutivo:info.concecutivo, empresa:info.emp}, function(err, Salida){
            if(err) console.log(err)
            else{
                response['salida']=Salida;
                dor.Frozen.findOne({consecutivo:info.concecutivo, empresa:info.emp}, function(err, congelado){
                    if(err) console.log(err)
                    else{
                        response['congelado']=congelado;
                        res.json(response)
                        
                    }
                })
            }
        })
    })

    app.get('/get/reservas/:obj/:max',function(req,res){
        var info=req.params;
        console.log(info)
        di.objetos.findById(ObjectId(info.obj),function(err,objt){
            if(err) console.log(err);
            else{
                console.log(objt)
                busqueda.busquedaReservas(objt.reservados,0,Number(info.max),new Array(0),res)
            }
        })
    })

    app.post('/get/reserv/',function(req,res){
        var info=req.body;
        busqueda.busquedaReservas(info.array,0,info.array.length+1,new Array(0),res)
    })

    app.get('/actualizar/pedidos/:since',function(req,res){
        var info=req.params;
        di.pedido.findById(ObjectId(info.since),function(err,pdd){
            if(err) console.log(err);
            else{
                if(pdd.next){
                    pedidosAct(info.since,[],res)
                }
                else{
                    res.json({
                        err:1,
                        data:null
                    })
                }
            }
        })
    })

    app.get('/get/pedidos/:emp/:since/:max',function(req,res){
        var info=req.params;
        console.log('pedidos',info)
        if(info.since != "null") busqueda.buscarPedidos(info.since,0,info.max,new Array(0),res);
        else{
            de.Empresas.findById(ObjectId(info.emp),function(err,empr){
                if(err) console.log(err);
                else{
                    //console.log('empresa',empr)
                    if(empr.pedidos==null) res.json({
                        'err':0,
                        'data':[],
                        'last':null
                    })
                    else busqueda.buscarPedidos(empr.pedidos,0,info.max,new Array(0),res)
                }
            })
        }
    })

    app.get('/get/pedido/:pdd',function(req,res){
        var info=req.params;
        //console.log('pedidos',info)
        di.pedido.findById(ObjectId(info.pdd), function(err, pedidos){
            if(err) console.log(err)
            else{
                res.json(pedidos)
            }
        })
    })

    app.get('/get/pedidos/user/:user',function(req,res){
        var info=req.params;
        //console.log('pedidos',info)
        di.pedido.find({agente:info.user}, function(err, pedidos){
            if(err) console.log(err)
            else{
                res.json(pedidos)
            }
        })
    })
//------------produccion
    app.get('/get/ordenes/:emp/',function(req,res){
        var info = req.params;
        de.Empresas.findById(ObjectId(info.emp),function(err,empr){
            if(err) console.log(err);
            else{
                console.log(empr.ordenes)
                busqueda.findOrdenes(empr.ordenes,100,res);
            }
        })
    })

    app.get('/last/orden/:emp',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                dor.Produccion.findById(ObjectId(emp.ordenes.Produccion),function(err,ord){
                    if(err) console.log(err);
                    else{
                        res.json(ord)
                    }
                })
            }
        })
    })

    app.get('/get/autosave/:id',function(req,res){
        var info=req.params;
        du.Usuarios.findById(ObjectId(info.id),function(err,usr){
            if(err) console.log(err);
            else{
                res.json(usr.pedido_guardado)
            }
        })
    })
//eliminar
    /**
        codigo de errores
        1:producto no existe
        2:busqueda
        3:guardado
        4:independiente
     */
//------------inventario
    /**error 4:error de eliminacion*/
    app.post('/eliminar/objeto',function(req,res){
        var info=req.body;
        //busqueda de la empresa
        de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
            if(err) console.log(err);
            else{
                //si la empresa posee inventario
                if(emp.inventario!=null){
                    di.inventario.findById(ObjectId(emp.inventario),function(err,inv){
                        //console.log(inv)
                        if(err) res.json({'err':2});
                        else{
                            //todas las categorias de la empresa
                            let invt = inv.categoria;
                            for (let i = 0; i < invt.length; i++) {
                                //si es la categoria que se desea
                                if(invt[i].nombre==info.cat){
                                    //si es el ultimo producto
                                    if(invt[i].last==info.objeto){
                                        eliminarProducto(info.objeto,function(err,before,next){
                                            console.log(err,before,next)
                                            if(err==0){
                                                if(before==null && next==null){
                                                    inv.categoria[i].last=null
                                                    inv.categoria[i].id=null
                                                }
                                                else inv.categoria[i].last=before;
                                                inv.save(function(err){
                                                    if(err) res.json({'err':3})
                                                    else{
                                                        log.make(info.empresa,
                                                        'Eliminacion de producto',
                                                        'Se elimino el producto '+info.objeto.codigo+'-'+info.objeto.nombre+' de la base de datos',
                                                        {'err':0},res)
                                                    }
                                                })
                                            }
                                            else res.json({'err':err})
                                        })
                                    }
                                    else{
                                        if(invt[i].id==info.objeto){
                                            eliminarProducto(info.objeto,function(err,before,next){
                                                console.log(err,before,next)
                                                if(err==0){
                                                    inv.categoria[i].id=next;
                                                    inv.save(function(err){
                                                        if(err) res.json({'err':3})
                                                        else{
                                                            log.make(info.empresa,
                                                            'Eliminacion de producto',
                                                            'Se elimino el producto '+info.objeto.codigo+'-'+info.objeto.nombre+' de la base de datos',
                                                            {'err':0},res)
                                                        }
                                                    })
                                                }
                                                else res.json({'err':err})
                                            })
                                        }
                                        else{
                                            eliminarProducto(info.objeto,function(err,before,next){
                                                console.log(err,before,next)
                                                res.json({'err':err})
                                            })
                                        }
                                    }
                                    break
                                }
                                else{
                                    //si la categoria que se ingreso no es correcta
                                    if(i == invt.length-1) res.json({'err':1})
                                }
                            }
                        }
                    })
                }
                else res.json({'err':1})
            }
        })
    })

    /** error 4:la categoria objetos*/
    app.post('/eliminar/cat',function(req,res){
        var info=req.body;
        //busqueda de la empresa
        de.Empresas.findById(ObjectId(info.empresa),function(err,emp){
            if(err) console.log(err);
            else{
                //si la empresa posee inventario
                if(emp.inventario!=null){
                    di.inventario.findById(ObjectId(emp.inventario),function(err,inv){
                        //console.log(inv)
                        if(err) res.json({'err':2});
                        else{
                            //todas las categorias de la empresa
                            let invt = inv.categoria;
                            for (let i = 0; i < invt.length; i++) {
                                //si es la categoria que se desea
                                if(invt[i].nombre==info.cat){
                                    //if(invt[i].last==null){
                                        inv.categoria.splice(i,1);
                                        inv.save(function(err){
                                            if(err) res.json({'err':3})
                                            else{
                                                log.make(info.empresa,
                                                'Eliminacion de categoria',
                                                'Se elimino la categoria '+info.cat+' de la base de datos',
                                                {'err':0},res)
                                            }
                                        })
                                    //}
                                    //else res.json({'err':4})
                                    break
                                }
                                else{
                                    //si la categoria que se ingreso no es correcta
                                    if(i == invt.length-1) res.json({'err':1})
                                }
                            }
                        }
                    })
                }
                else res.json({'err':1})
            }
        })
    })
//------------formulas
    app.post('/formula/envase/eliminar/',function(req,res){
        var info=req.body;
        di.recetas.findById(ObjectId(info.formula),function(err,rct){
            if(err) console.log(err);
            else{
                rct.envase=info.envase;
                rct.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(rct)
                    }
                })
            }
        })
    })

    app.post('/delete/receta/',function(req,res){
        var info=req.body;
        de.Empresas.findById(ObjectId(info.id),function(err,emp){
            if(err) console.log(err);
            else{
                eliminarReceta(emp.recetas,info.receta,function(err,before,next){
                    if(emp.recetas==info.receta)emp.recetas=before
                    if(emp.recetas_last==info.receta)emp.recetas_last=next
                    emp.save(function(err){
                        if(err) console.log(err);
                        else{
                            res.json({err:err})
                        }
                    })
                })
            }
        })
    })
//---indefinidos
//------------log
    app.get('/get/logs/emp/:emp',function(req,res){
        var info=req.params;
        de.Empresas.findById(ObjectId(info.emp),function(err,emp){
            if(err) console.log(err);
            else{
                if(emp.Log)busqueda.findlogs(emp.Log,[],res)
                else{
                    res.json({
                        'l':0,
                        'data':[]
                    })
                }
            }
        })
    })
//------------provedores
    app.post('/editar/proveedor',function(req,res){
        var info=req.body;
        di.distribuidores.findById(ObjectId(info.id),function(err,prv){
            if(err) console.log(err);
            else{
                prv.nombre=info.nombre;
                prv.correo=info.correo;
                prv.telefono=info.telefono;
                prv.pais=info.pais;
                prv.nota=info.nota;
                prv.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(prv)
                    }
                })
            }
        })
    })

    app.post('/add/proveedor',function(req,res){
        var info=req.body;
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err) console.log(err);
            else{
                //actualizan los datos
                obj.distribuidores=info.proveedor
                //se guardan los cambios
                obj.save(function(err){
                    if(err){
                        console.log(err);
                        res.json(err);
                    }
                    else{
                        res.json(obj);
                    }
                })
            }
        })
    })

    app.get('/get/provedores/:empresa',function(req,res){
        var info=req.params;
        var respuesta={
            keys:[],
            data:{}
        }
        di.Provedores.findOne({empresa:info.empresa}, function(err, provedor){
            if(err) console.log(err)
            else{
                if(provedor==null) {
                    res.json(respuesta)
                }
                else{
                    busqueda.proveedores(provedor.distribuidores,0,respuesta,res)
                }
            }
        })
    })
//----------lotes
    app.get('/get/lote/:id',function(req,res){
        var info=req.params;
        di.lotes.findById(ObjectId(info.id),function(err,lt){
            if(err) console.log(err);
            else{
                res.json(lt)
            }
        })
    })

    app.post('/find/lotes/',function(req,res){
        var info=req.body;
        findLotes(info.lotes,0,[],res)
    })

//dependientes
    app.post('/agregar/dependiente/',function(req,res){
        var info=req.body;
        dc.clientes.findById(ObjectId(info.cln),function(err,cln){
            if(err) console.log(err);
            else{
                cln.puntos.push({
                    correo:info.correo,
                    dependiente:info.dependiente,
                })
                cln.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(cln)
                    }
                })
            }
        })
    })

    app.post('/agregar/puntos/dependiente/',function(req,res){
        var info=req.body;
        dc.clientes.findById(ObjectId(info.cln),function(err,cln){
            if(err) console.log(err);
            else{
                cln.puntos[info.i].cantidad=cln.puntos[info.i].cantidad+info.puntos
                di.lotes.findById(ObjectId(info.id),function(err,lt){
                    if(err) console.log(err);
                    else{
                        lt.qr.push(info.num)
                        cln.save(function(err){
                            if(err) console.log(err);
                            else{
                                lt.save(function(err){
                                    if(err) console.log(err);
                                    else{
                                        res.json()
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    })

//facturacion
    app.post('/aumentar/deuda/',function(req,res){
        var info=req.body;
        dc.clientes.findById(ObjectId(info.cliente),function(err,cln){
            if(err) console.log(err);
            else{
                cln.credito.deuda=cln.credito.deuda+info.Monto
                cln.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(cln)
                    }
                })
            }
        })
    })

    app.get('/find/facturas/:cln',function(req,res){
        var info=req.params;
        dc.clientes.findById(ObjectId(info.cln),function(err,cln){
            if(err) console.log(err);
            else{
                if(cln.facturas)findFct(cln.facturas,100,[],res)
                else res.json({
                    'err':0,
                    'data':[],
                    'last':0
                })
            }
        })
    })

    app.post('/pagar/factura/',function(req,res){
        var info=req.body;
         dc.facturas.findById(ObjectId(info.factura),function(err,fct){
            if(err) console.log(err);
            else{
                fct.monto=info.monto;
                if(info.monto==0)fct.cancelada=true;
                fct.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(fct)
                    }
                })
            }
         })
    })

    app.post('/nota/credito/',function(req,res){
        var info=req.body;
        dc.facturas.findById(ObjectId(info.factura),function(err,fct){
            if(err) console.log(err);
            else{
                fct.monto=info.NewMonto
                if(info.NewMonto==0)fct.cancelada=true;
                fct.save(function(err){
                    if(err) console.log(err);
                    else{
                        if(info.tipo){
                            di.pedido.findById(ObjectId(fct.pedido),function(err,pdd){
                                if(err) console.log(err);
                                else{
                                    pdd.reservas=info.reserva
                                    pdd.total=info.NewMonto
                                    pdd.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            res.json(fct)
                                        }
                                    })
                                }
                            })
                        }
                        else res.json(fct)
                    }
                })
            }
        })
    })


//parches
    app.get('/chng/beforeNext/:col/:name/:last/:id',function(req,res){
        var info=req.params;
        di.inventario.findById(ObjectId(info.col),function(err,invt){
            if(err) console.log(err);
            else{
                var send=null
                for (let i = 0; i < invt.categoria.length; i++) {
                    const cat = invt.categoria[i];
                    if(cat.nombre==info.name){
                        var send=cat
                        //no desomentar, este es el parche
                        invt.categoria[i].id=info.id
                        invt.categoria[i].last=info.last
                        break
                    }
                }
                invt.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(send)
                    }
                })
            }
        })
    })

    /*app.get('/eliminar/cat/repetidas/:id',function(req,res){
        var info=req.params;
        var cat=[]
        di.inventario.findById(ObjectId(info.id),function(err,inv){
            //console.log(inv)
            if(err) res.json(null);
            else{
                for (let i = 0; i < inv.categoria.length; i++) {
                    const categoria = inv.categoria[i];
                    if(cat.indexOf(categoria.nombre)==-1) {
                        cat.push(categoria.nombre)
                        console.log(cat)
                    }
                    else{
                        console.log(inv.categoria[i])
                        inv.categoria.splice(i,1);
                        i=i-1
                    }
                }
                inv.save(function(err){
                    if(err) console.log(err);
                    else{
                        res.json(inv)
                    }
                })
            }
            
        })
    })*/

//obsoletas
    app.post('/add/stock/',function(req,res){
        var info=req.body;
        var items = Object.keys(info.items)
        if(items.length>0){
            EditStock(0,items,info.items,function(err){
                res.json(err)
            })
        }
        else res.json(0)
    })

    app.post('/change/standBy',function(req,res){
        var info=req.body;
        //se busca el id
        di.objetos.findById(ObjectId(info.objeto),function(err,obj){
            if(err) console.log(err);
            else{
                //actualizan los datos
                if(info.add)var standBy = info.value+objt.StandBy;
                else var standBy = objt.StandBy-info.value;
                objt.StandBy=standBy
                //se guardan los cambios
                obj.save(function(err){
                    if(err){
                        console.log(err);
                        res.json(err);
                    }
                    else{
                        res.json(obj);
                    }
                })
                
            }
        })
    })
//
 
}

function makeLotePT(Lt,i,send,callback){
    di.objetos.findById(ObjectId(Lt[i].object),function(err,objt){
        var lote=new di.lotes({
            Numero:Lt[i].number,
            Proveedor:Lt[i].prove,
            FProduccion:new Date(),
            FIngreso:new Date(),
            cantidad:Lt[i].cantidad,
            FVencimiento:Lt[i].vencimiento,
        })
        lote.save(function(err){
            if(err) console.log(err);
            else{
                objt.lotes.push(lote._id)
                objt.cantidad=objt.cantidad+Lt[i].cantidad,
                send.push({
                    nombre:objt.codigo+'-'+objt.nombre,
                    id:lote._id,
                    cantidad:Lt[i].cantidad,
                    puntos:objt.puntos,
                    obj:objt.cantidad
                })
                objt.save(function(err){
                    if(err) console.log(err);
                    else{
                        if(i+1>Lt.length)makeLotePT(Lt,i+1,send,callback)
                        else callback(send)
                    }
                })
            }
        })
    })
}

function restatMP(MpP,i,callback) {
    di.objetos.findById(ObjectId(MpP[i].object),function(err,objt){
        if(err) console.log(err);
        else{
            objt.cantidad=objt.cantidad-MpP[i].cantidad
            MpP[i]['nombre']=objt.codigo+'-'+objt.nombre
            objt.save(function(err){
                if(err) console.log(err);
                else{
                    if(i+1<MpP.length) restatMP(MpP,i+1,callback)
                    else callback(MpP)
                }
            })
        }
    })
}

function findLotes(lotes,i,send,res) {
    di.lotes.findById(ObjectId(lotes[i]),function(err,lts){
        if(err) console.log(err);
        else{
            send.push(lts);
            //condicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            //si verificar si el siguiente no es null
            if(i+1<lotes.length) findLotes(lotes,i+1,send,res)
            else{
                //si se completo la busqueda
                if(i+1==lotes.length){
                    res.json({
                        'err':2,
                        'data':send,
                        'last':i
                    })
                }
                //si se alcanzo el maximo
                //si la entrada de datos es un array indicar en el last el ultimo i, si es id indicar el siguiente
                else{
                    res.json({
                    'err':0,
                    'data':send,
                    'last':i
                    })
                }
            }
        }
    })
}

function actulizarLotes(lotes,i,callback){
    console.log(i+1,lotes.length)
    di.objetos.findById(ObjectId(lotes[i].ingediente),function(err,objt){
        if(err) console.log(err);
        else{
            if(objt.lote){
                di.lotes.findById(ObjectId(objt.lote[0]),function(err,lot){
                    if(err) console.log(err);
                    else{
                        lotes[i]['nombre']=objt.codigo+'-'+objt.nombre
                        objt.cantidad=objt.cantidad+lotes[i].diferencia
                        lot.cantidad=lot.cantidad-lotes[i].usado
                        if(lot.cantidad>0){
                            lot.save(function(err){
                                if(err) console.log(err);
                                else{
                                    objt.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            if(i+1<lotes.length)actulizarLotes(lotes,i+1,callback)
                                            else callback(lotes)                                            
                                        }
                                    })
                                }
                            })
                        }
                        else{
                            objt.splice(objt.indexOf(lot._id),1);
                            lot.remove(function(err){
                                if(err) console.log(err);
                                else{
                                    objt.save(function(err){
                                        if(err) console.log(err);
                                        else{
                                            if(i+1<lotes.length)actulizarLotes(lotes,i+1,callback)
                                            else callback(lotes)                                            
                                        }
                                    })
                                }
                            })
                        }
                    }
                })
            }
            else{
                lotes[i]['nombre']=objt.codigo+'-'+objt.nombre
                objt.cantidad=objt.cantidad+lotes[i].diferencia
                objt.save(function(err){
                    if(err) console.log(err);
                    else{
                        if(i+1<lotes.length)actulizarLotes(lotes,i+1,callback)
                        else callback(lotes)
                    }
                })
            }
        }
    })
}

function restarInventario(reservas,i,salida,congelado,empresa,callback){
    //console.log(reservas)
    reserv=reservas[i]
    console.log(reserv)
    if(reserv.Aprovado){
        di.objetos.findById(ObjectId(reserv.objeto),function(err,objt){
            if(err) console.log(err);
            else{
                objt.cantidad=objt.cantidad-reserv.cantidad;
                    //console.log(objt)
                    //si se definieron los limites
                    if(objt.trigger.min != null && objt.trigger.min !=undefined) {
                        //si el objeto bajo el umbral
                        if(objt.cantidad<objt.trigger.min){
                            //si no hay producto se cancela la reserva y se agrega a congelado
                            if(objt.cantidad<0){
                                objt.cantidad=objt.cantidad+reserv.cantidad;
                                congelado.push({
                                    producto:reserv.objeto,
                                    cantidad:reserv.cantidad
                                })
                            }
                            //si aun hay porducto se saca pero se alerta
                            else{
                                salida.push({
                                    nombre:objt.codigo+'-'+objt.nombre,
                                    producto:reserv.objeto,
                                    cantidad:reserv.cantidad
                                });
                                reserv.aprovado=true;
                                //alerta
                                de.Empresas.findById(ObjectId(empresa),function(err,emp){
                                    if(err) console.log(err);
                                    else{
                                        emp.Alertas.inventario=true;
                                    }
                                })
                            }
                        }
                        //si hay suficiente producto, sale el producto
                        else{
                            salida.push({
                                nombre:objt.codigo+'-'+objt.nombre,
                                producto:reserv.objeto,
                                cantidad:reserv.cantidad
                            });
                        }
                    }
                    else{
                        //si no hay suficiente producto
                        if(objt.cantidad<0){
                            objt.cantidad=objt.cantidad+reserv.cantidad;
                            congelado.push({
                                producto:reserv.objeto,
                                cantidad:reserv.cantidad
                            })
                        }
                        else{
                            salida.push({
                                nombre:objt.codigo+'-'+objt.nombre,
                                producto:reserv.objeto,
                                cantidad:reserv.cantidad
                            })
                        }
                    }
                    //console.log(salida)
                    objt.save(function(err){
                        if(err) console.log(err);
                        else{
                            if(i+1<reservas.length)restarInventario(reservas,i+1,salida,congelado,empresa,callback)
                            else{
                                callback(reserv.cliente,salida,congelado)
                            }
                        }
                    })
            }
        })
    }
    else{
        if(i+1<reservas.length)restarInventario(reservas,i+1,salida,congelado,empresa,callback)
        else{
            callback(reserv.cliente,salida,congelado)
        }
    }
}

function eliminarProducto(id,callback){
    //se busca el producto y el antorior y el siguiente
    di.objetos.findById(ObjectId(id),function(err,obj){
        if(err) callback(2,null,null);
        else{
            //si el objeto es el primero
            if(obj.before==null){
                //si el objeto es unico; solo se elimina el objeto
                if(obj.next==null){
                    obj.remove(function(err){
                        if(err)callback(4,null,null);
                        else callback(0,null,null);
                    })
                }
                //sino se cambia el tag 'before' a null y se devuelve el id del siguiente
                else{
                    di.objetos.findById(ObjectId(obj.next),function(err,objN){
                        if(err) callback(2,null,null);
                        else{
                            objN.before=null
                            objN.save(function(err){
                                if(err) callback(3,null,null);
                                else{
                                    obj.remove(function(err){
                                        if(err)callback(4,null,objN._id);
                                        else callback(0,null,objN._id);
                                    })
                                }
                            })
                        }
                    })
                }
            }
            else{
                //se busca el objeto anterior
                di.objetos.findById(ObjectId(obj.before),function(err,objB){
                    if(err) callback(2,null,null);
                    else{
                        //si el objeto es el ultimo se cambia el tag 'next' del anterior a null
                        if(obj.next==null){
                            objB.next=null
                            objB.save(function(err){
                                if(err) callback(3,null,null);
                                else{
                                    obj.remove(function(err){
                                        if(err)callback(4,objB._id,null);
                                        else callback(0,objB._id,null);
                                    })
                                }
                            })
                        }
                        //sino se cambian los id del anterior y el siguiente
                        else{
                            di.objetos.findById(ObjectId(obj.next),function(err,objN){
                                if(err) callback(2,null,null);
                                else{
                                    objN.before=objB._id;
                                    objB.next=objN._id;
                                    objB.save(function(err){
                                        if(err) callback(3,null,null);
                                        else{
                                            objN.save(function(err){
                                                if(err) callback(3,objB._id,null);
                                                else{
                                                    obj.remove(function(err){
                                                        if(err)callback(4,objB._id,null);
                                                        else callback(0,objB._id,objN._id);
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    }
                })
            }
        }
    })
}

 function pedidosAct(ped,send,res) {
        di.pedido.findById(ObjectId(ped),function(err,pdd){
            if(err) console.log(err);
            else{
                send.push(pdd);
                //condicion de finalizacion
                //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
                //si verificar si el siguiente no es null
                if(pdd.next !=null) pedidosAct(pdd.next,send,res)
                else{
                    
                    res.json({
                    'err':0,
                    'data':send,
                    })
                }
            }
        })
    }

    function ActPrioridad(ordenes,i,res){
        console.log(ordenes[i].id)
        dor.Produccion.findById(ObjectId(ordenes[i].id),function(err,ord){
            if(err) console.log(err);
            else{
                for (let j = 0; j < ord.prductos.length; j++) {
                    if(ord.prductos[j]._id==ordenes[i]._id){
                        ord.prductos[j].prioridad=ordenes[i].prioridad;
                        break
                    }
                }
                ord.save(function(err){
                    if(err) console.log(err);
                    else{
                        if(i+1<ordenes.length) ActPrioridad(ordenes,i+1,res)
                        else res.json()
                    }
                })
            }
        })
    }

    function actualizarPrioridad(productos,i,callback){
        dor.Produccion.findById(ObjectId(productos[i].OrdenId),function(err,ord){
            if(err) console.log(err);
            else{
                for (let j = 0; j < ord.prductos.length; j++) {
                    if(ord.prductos[j].producto==productos[i].Producto){
                        ord.prductos[j].prioridad=productos[i].Prioridad
                        break
                    }
                }
                ord.save(function(err){
                    if(err) console.log(err);
                    else{
                        if(productos.length>i+1) actualizarPrioridad(productos,i+1,callback)
                        else callback()
                    }
                })
            }
        })
    }

//funciones para eliminar recetas
function RemoveRCT(id,receta,callback){
    di.recetas.findById(ObjectId(id),function(err,rct){
        if(err) callback(2,null,null)
        else{
            //si la siguiente receta es la que se desea eliminar
            if(rct.next==receta){
                di.recetas.findById(ObjectId(receta),function(err,rctN){
                    if(err) callback(2,rct._id,null)
                    else{
                        rct.next=rctN.next
                        rct.save(function(err){
                            if(err) callback(3,rct._id,rctN._id)
                            else{
                                rctN.remove(function(err){
                                    if(err) callback(4,rct._id,rct.next)
                                    else callback(0,rct._id,rct.next)
                                })
                            }
                        })
                    }
                })
            }
            else{
                if(rct.next!=null)RemoveRCT(rct.next,receta,callback)
                else callback(2,null,null)
            }
        }
    })
}

function eliminarReceta(inicio,receta,callback){
    di.objetos.find({receta:receta},function(err,objt){
        if(err) callback(1,null,null)
        else{
            console.log(objt)
            for (let i = 0; i < objt.length; i++) {
                objt[i].receta=null;
                objt[i].save(function(err){
                    if(err) console.log(err)
                })
            }
            //si es la primera receta se elimina
            if(inicio==receta){
                di.recetas.findById(ObjectId(receta),function(err,rct){
                    var next=rct.next
                    if(err) console.log(err);
                    else{
                        rct.remove(function(err){
                            if(err) callback(4,rct._id,rct._id)
                            else callback(0,next,next)
                        })
                    }
                })
            }
            else RemoveRCT(inicio,receta,callback)
        }
    })
}

function EditStock(i,items,data,callback){
    di.objetos.findById(ObjectId(items[i]),function(err,obj){
        if(err) callback(1);
        else{
            obj.cantidad=obj.cantidad+data[items[i]]
            obj.save(function(err){
                if(err) callback(2)
                else{
                    if(i+1<items.length) EditStock(i+1,items,data,callback)
                    else callback(0)
                }
            })
        }
    })
}

function Makefactura(pedido,callback){
    dc.clientes.findById(ObjectId(pedido.cliente),function(err,cln){
        if(err) console.log(err);
        else{
            var factura = new dc.facturas({
                numero:pedido.factura,
                monto:pedido.total,
                pedido:pedido._id,
                cancelada:false
            })
            //console.log(factura)
            if(cln.facturas)factura.before=cln.facturas
            factura.save(function(err){
                if(err) console.log(err);
                else{
                    cln.facturas=factura._id
                    cln.save(function(err){
                        if(err) console.log(err);
                        else{
                            console.log(cln)
                            callback(factura)
                        }
                    })
                }
            })
        }
    })
}

function findFct(fct,max,send,res) {
    dc.facturas.findById(ObjectId(fct),function(err,factura){
        if(err) console.log(err);
        else{
            //facturaconsole.log(factura)
            send.push(factura);
            //condicion de finalizacion
            //si la entrada de datos es un array comparar el tamano de la entrada con i+1,
            //si verificar si el siguiente no es null
            if(factura.before != null) findFct(factura.before,max,send,res)
            else{
                res.json({
                    'err':2,
                    'data':send,
                })
            }
        }
    })
}