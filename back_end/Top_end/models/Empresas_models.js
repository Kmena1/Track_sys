/*modelo para crear las empresas, este posee datos de la empresa tales como el nombre la cedula juridica el telefono el 
 correo la fecha de inicio y el contrado
 Ademas del contacto inmediato
 las localizaciones, vehiculos, y usuraios son referencias del id de los esquemas donde se guardan estos datos
*/
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var EmpresasSchema = new Schema({
	foto		: { type:String ,required:false},
    Company	  : { type: String, required: false, unique: false },
    cedula          : { type: Number, required: false, unique: false },
    phone           : { type: Number, required: false, unique: false },
    fax             : { type: Number, required: false, unique: false },
    email           : { type: String, required: false, unique: false },
    extemail        : [{ type: String, required: false, unique: false } ],
    fechai          : { type: Date, required: false, unique: false },
    contrato	  : { type: String, required: false, unique: false },
    direccion  : { type: String, required: false, unique: false },
    Cont_imm	  : { 
        nombre: 	{type: String},
        cedula: 	{type: String},
        telefono:	{type: Number},
        correo: 	{type: String},
    },
    transportes:[],
    //por id
    pedidos:{ type: String, required : false, default:null },
    pedidoslast:{ type: String, required : false, default:null },
    ordenes:{
        Salida: {type:String,required:false,default:null},
        Produccion: {type:String,required:false,default:null},
        Frozen: {type:String,required:false,default:null}
    },
    inventario:{ type: String, required : false, default: null  },
    recetas:{ type: String, required : false, default: null  },
    recetas_last:{ type: String, required : false, default: null  },
    clientes:{ type: String, required : false, default: null  },
    localizaciones: { type: String, required: false, unique: false },
    vehiculos: { type: String, required: false, unique: false },
    usuarios: { type: String, required: false, unique: false, default:null},
    Last_users:{type:String,default:null},
    Log:{type:String,default:null},
    Solicitudes:[],
    Facturacion:{
        codigo:{ type: Number, required: false, unique: false, default:0},
        informacion:{type: String, required: false, unique: false, default:null},
        legal:[{type: String, required: false, unique: false, default:null}]
    },
    codigo_facturacion:{type: String, required: false, unique: false, default:null},
    costos:{
        electricidad:{
            KwH:{ type: Number, required : false, default:180.69},
            consumo:{ type: Number, required : false, default:10},
        },
        agua:{
            m3:{ type: Number, required : false, default:1.669},
            consumo:{ type: Number, required : false, default:100}
        },
        alquiler:{
            mensual:{ type: Number, required : false, default:300000},
            dias:{ type: Number, required : false, default:20},
            horas:{ type: Number, required : false, default:10},            
        },
        empleados:[
            {
                nombre:{ type: String, required : true},
                costo:{ type: Number, required : false, default:1000}
            },
        ],
    },
    Alertas:{
        inventario:{
            activa:{type:Boolean, required:false, default:false},
            inicial:{type:String, required:false, default:null}
        },
        falta_Producto:{
            activa:{type:Boolean, required:false, default:false},
            inicial:{type:String, required:false, default:null}
        },
    }

});

var Empresas = mongoose.model('Empresas', EmpresasSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	Empresas : mongoose.model('Empresas', EmpresasSchema),
	mongoose : mongoose

}