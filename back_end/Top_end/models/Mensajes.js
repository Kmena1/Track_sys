/**
 * este guarda los datos de cada trama
 * puede optimizarse 
 */
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var MensajesSchema = new Schema({
    envia:{type:String,required:true},
    para:{type:String,required:true},
    asunto:{type:String,required:true},
    mensaje:{type:String,required:true},
    next:{type:String,required:false,default:null},
    before:{type:String,required:false,default:null},
})

var Mensajes = mongoose.model('Mensajes', MensajesSchema);

// create the model for myAgents and expose it to our app
module.exports = {
    Mensajes : mongoose.model('Mensajes', MensajesSchema),
	mongoose : mongoose

}