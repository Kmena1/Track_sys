/*
	
*/

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var SaveSchema = new Schema({
	i:{type:Number,required:true},
    data:[],
    ips:[],
});

var Save = mongoose.model('Save', SaveSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	Save : mongoose.model('Save', SaveSchema),
	mongoose : mongoose

}