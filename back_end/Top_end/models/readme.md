Se decidio usar una metodologia de esquemas en indexacion
Por lo que se creo un rango de niveles de permisos que evita que los modulos mas pesados usen busquedas mas complejas

-**nivel 0**: usado por modulos que se ocupan obtener toda la informacion de su esquema, estos pueden usar los metodos de busquedas 
 -find /n
 -findOne /n
 -findById /n
-**nivel 1**: usados por los modelos que se ocupan buscar sin su id, estos no pueden contener muchos datos, puede usar:
 -findOne
 -findById
-**nivel 2**: usados por los modelos que tienen muchos datos o tienden a crecer muy rapido, estos pueden usar:
 -findById
