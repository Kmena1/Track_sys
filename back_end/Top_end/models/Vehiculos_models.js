/**
 * el esquema guarda un registro de los dispositivo registrados y el id de los dispositovos que le envian datos a este
 * ademas de los id's de los datos que envia este en las etiquetas de los esquemas de posiciones
 * tambien el id del criterio de archiving
 * y el siguiente dispositivo en caso de que el usuario tenga varios dispositivos
 */

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var VehiculosSchema = new Schema({
	imei		: [{ type: String, required : false  }],//imei
	unidad          : { type: Number, required : false  ,default:0},
	nombre		: { type: String, required : false , default:"false"},
	//desconectado 3-4
	gps             : { type: Boolean, default: true},
	//fecha de la calibracion del odometro 66-67
    odometro        : { type: Date, default: Date.now()},//to
	//fecha de la calibracion del RPM 65
    RPM        : { type: Date, default: null},//to
	//fecha de la calibracion del DPA 85
    DPACal        : { type: Date, default: null},//to
	//sendor combustible 1 81-82
	Sgas1			:  {type: Boolean, default: false},
	//sendor combustible 2 83-84
	Sgas2			:  {type: Boolean, default: false},
	//0 bajo, 1 normal, 2 alto 
  	temperatura     : { type: Number, default: 1},//t1-t2-t3
	//conectado-desconestado 59/60
  	ibutton         : { type: Boolean, default: false},//DID-DIDR
	//apagado automatico
  	RemoteOnOff		: { type: Boolean, default: false},
	//deap sleep
	sleep				: { type: Boolean, default: false},
	//ignite
	ignite			: { type: Boolean, default: false},
	//back-up battery connect
	back_up_Battery	: { type: Boolean, default: false},
	//ignite
	battery			: { type: Boolean, default: false},
	//OBD conectado 80-81
	OBD				: { type: Boolean, default: false},
	OBD_Pos			: { type: String, required: false, default: null },
	//DTCOBD
	DTC				: { type: Boolean, default: false},
  	panico          : { type: Boolean, default: false},//Emg-id=2
  	velocidad       : { type: Boolean, default: true},//speed/vs
  	Dallas			: { type: Boolean, default: false},
  	aceleracion		: { type: Boolean, default: false},
  	canbus			: { type: Boolean, default: false},
	  voz				: { type: Boolean, default: false},
	others			:{ type: Boolean, default: false},
    //por id
	historycs : { type: String, required: false, default: null },
    posiciones: { type: String, required: false, default: null },
	posiciones_last: { type: String, required: false, default: null },
	Alertas:{ type: String, required: false, default: null },
	Alertas_last:{ type: String, required: false, default: null },
	//fecha de desconeccion del la sim
	sim:{ type: String, default: null},
	Alt:{
		overspeed:[{ type: String, required: false, default: null }],
		overspeedFinish:[{ type: String, required: false, default: null }],
		geofenceout :[{ type: String, required: false, default: null }],
		geofencein :[{ type: String, required: false, default: null }],
		batteryError:[{ type: String, required: false, default: null }],
		batteryDown:[{ type: String, required: false, default: null }],
		schocked:[{ type: String, required: false, default: null }],
		//reduccion del combustible 73
  		gas             : [{ type: String, default: null}],//tf-ADC
		rutaDefinidain:[{ type: String, required: false, default: null }],
		rutaDefinidaout:[{ type: String, required: false, default: null }],
		Engine:{
			Excced_Speed:[{ type: String, required: false, default: null }],
			Violation_Speed:[{ type: String, required: false, default: null }],
			Coolant:[{ type: String, required: false, default: null }],
			oil:[{ type: String, required: false, default: null }],
			RPM:[{ type: String, required: false, default: null }],
			Break:[{ type: String, required: false, default: null }],
			DTC:[{ type: String, required: false, default: null }],
			Service:[{ type: String, required: false, default: null }],
		},
		DPA:{
			fast_acelation:[{ type: String, required: false, default: null }],
			fast_acelation2:[{ type: String, required: false, default: null }],
			sharp_turn:[{ type: String, required: false, default: null }],
			overspeed:[{ type: String, required: false, default: null }],
		},
		stop:{
			on_Ndrive:[{ type: String, required: false, default: null }],
			Stoped:[{ type: String, required: false, default: null }],
			stop_ignitOn:[{ type: String, required: false, default: null }],
			moving:[{ type: String, required: false, default: null }],
		},
		Emergency:{
			panic:			[{ type: String, required: false, default: null }],
			parking_lock:	[{ type: String, required: false, default: null }],
			remove_power:	[{ type: String, required: false, default: null }],
			anti_theft:		[{ type: String, required: false, default: null }],
			antitheft_door:	[{ type: String, required: false, default: null }],
			motion:			[{ type: String, required: false, default: null }],
			antitheft_shock:	[{ type: String, required: false, default: null }],
		},
		RPM:{
			SDHR:			[{ type: String, required: false, default: null }],
			FDHR:	[{ type: String, required: false, default: null }],
			HSLR:	[{ type: String, required: false, default: null }],
			LSHR:		[{ type: String, required: false, default: null }],
		}
	},
    Archiving: [{ type: String, required: false, unique: false }],
    next:{ type: String, required: false, default: null },
});

var unassignedSchema = new Schema({
	tipo : { type: String, required : false  },
	model_ID : {type:String},
	ids :[]
})

var Vehiculos = mongoose.model('Vehiculos', VehiculosSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	Vehiculos : mongoose.model('Vehiculos', VehiculosSchema),
	unassigned : mongoose.model('unassigned', unassignedSchema),
	mongoose : mongoose

}