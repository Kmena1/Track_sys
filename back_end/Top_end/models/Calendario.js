/*
	este esquema guarda los horarios de los empleados y vehiculos
*/

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var MesesSchema = new Schema({
    'year':{ type: Number, required : true},
    '0':{ type: String, required : false, default: null  },
    '1':{ type: String, required : false, default: null  },
    '2':{ type: String, required : false, default: null  },
    '3':{ type: String, required : false, default: null  },
    '4':{ type: String, required : false, default: null  },
    '5':{ type: String, required : false, default: null  },
    '6':{ type: String, required : false, default: null  },
    '7':{ type: String, required : false, default: null  },
    '8':{ type: String, required : false, default: null  },
    '9':{ type: String, required : false, default: null  },
    '10':{ type: String, required : false, default: null  },
    '11':{ type: String, required : false, default: null  },
    'next':{ type: String, required : false, default: null  },
});

var HorariosSchema = new Schema({
    'dia':{ type: Number, required : true},
    'horario':[],
    'tareas':[],
    'next':{ type: String, required : false, default: null  },
})

var tareasSchema = new Schema({
    'descripcion':{ type: String, required : false, default: null  },
    'inicio':{type: String, required : true},
    'final':{ type: String, required :true}
})

var Meses = mongoose.model('Meses', MesesSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	Meses : mongoose.model('Meses', MesesSchema),
    Horarios: mongoose.model('Horarios', HorariosSchema),
    tareas: mongoose.model('tareas', tareasSchema),
	mongoose : mongoose

}