/*
    Este esquema guarda los datos de los dispositivos de la empresa, el imei representa el id caracteristico de cada dispositivo
    por ejemplo en los trackers de sunteck serian los imeis el tipo seria el header de cada dispositivo
    ademas se guardan banderas que indican si se asigno y a que dispositivo de guardo y la badera que indica si ya se instalo
    Tambien tiene el modelos de las estructuras que tienen el tipo que igual es el header, el protocolo y el modelo de cada dispositivo
    y una copia de su esturctura.
*/
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var imeisSchema = new Schema({
    imei	 	 : { type: Number, required : true  },
    tipo        :{ type: String,required:true },
    model_ID : { type: String, default:null },
    asign_to    : { type: String, default:null },
    Asign        : {type:Boolean, default:false},
    install     : {type:Boolean, default:false},
    ip: [],
    structure:[]

});

var StructureSchema = new Schema({
    header:{ type: String, default:'ST910' },
    nombre:{ type: String, default:null },
    tipo:{ type: String,required:true },
    protocol : { type: String, default:null },
    model_ID : { type: String, default:null },
    structure:[]
})

var imeis = mongoose.model('imeis', imeisSchema);
var Structure = mongoose.model('Structure', StructureSchema);

// create the model for myAgents and expose it to our app
module.exports = {
    imeis : mongoose.model('imeis', imeisSchema),
    Structure : mongoose.model('Structure', StructureSchema),
	mongoose : mongoose

}