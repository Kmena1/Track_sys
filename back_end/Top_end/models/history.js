/**
 * el esquema guarda un registro de los dispositivo registrados y el id de los dispositovos que le envian datos a este
 * ademas de los id's de los datos que envia este en las etiquetas de los esquemas de posiciones
 * tambien el id del criterio de archiving
 * y el siguiente dispositivo en caso de que el usuario tenga varios dispositivos
 */

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var historySchema = new Schema({
    fechas: {type:String},
	inicio: {type:String, require: true},
	final: {type:String,require:true},
	before: {type:String, default:null}
});

var history = mongoose.model('history', historySchema);

// create the model for myAgents and expose it to our app
module.exports = {
	history : mongoose.model('history', historySchema),
	mongoose : mongoose

}