/*
	este esquema guarda los datos de los usuarios
	ademas de la referencias de los dispositivos usados, esquema de vehiculos, y una etiqueta del siguiente usuario en caso
	de estar inscrito en una empresa.
*/

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var UsuariosSchema = new Schema({
	//id		 : { type: Number, required : false },
	Nombre	 	 : { type: String, required : false, default: null  },
	cod			 : { type: String, default : false, default: null },
	Mail		 : { type: String, required : false, default: null },
	cedula	 	 : { type: String, required : false, default: null  },
	numero_tel	 : { type: Number, required : false, default: null },
	root	 	 : { type: Number, default : false, default: null },
	horario		 :[{
		tipo:{type: Number, required : false}
	}],
	pedido_guardado:{
		reservas:[{
			Aprovado:{type: Boolean, default: false},
			objeto:{ type: String, required : false, default:null },
			codigo:{ type: String, required : false, default:null },
			Maximo:{ type: Number, required : false, default:null },
			nombre:{ type: String, required : false, default:null },
			cantidad:{ type: Number, required : false, default:0 },
			descuento:{ type: Number, required : false, default:0 },
			Adescuento:{ type: Number, required : false, default:null },
			Adescuento2:{ type: Number, required : false, default:null },
			Tdescuento:{ type: Number, required : false, default:0 },
			total:{ type: Number, required : false, default:null },
			precio:{ type: Number, required : false, default:null },
			excento:{ type: Boolean, required : false, default:false },
			impuesto:{ type: Number, required : false, default:null },
			agente:{ type: String, required : false, default:null },
		}],
		notas:{ type: String, required : false, default:null },
    	resposable:{ type: String, required : false, default:null },
		contado:{type: Boolean,required:false, default:false},
		Abono:{type: Number,default:0},
		total:{type: Number,default:0},
		mensaje:{ type: String, required : false, default:null },
    	transporte:{ type: String, required : false, default:null },
		impuesto:{type: Number, required : false, default:0 },
	},
	dinero_dia  : {type:Number, default :0},
	user		: {type:String, default: null},
	password	: {type:String, default: null},
	dallas		: {type:String, default: null},
	fechaN		: {type:String, default: null},
	hoja_delin	: {type:String, default: null},
	foto		: {type:String, default: null},
	estado: {type:Number, default:0},
	//por id
	clientes:[{type:String,required: false, default:null}],
	horario:{type:String,required: false, default:null},
	empresa:{type:String,required: false, default:null},
	dispositivos:{ type: String, required: false, unique: false , default: null},
	dispositivos_last:{ type: String, required: false, unique: false , default: null},
	Mensajes_enviados:{ type: String, required: false, unique: false , default: null},
	Mensajes_nuevos:{ type: String, required: false, unique: false , default: null},
	Mensajes_leidos:{ type: String, required: false, unique: false , default: null},
    next:{ type: String, required: false, unique: false , default: null},
});

var Usuarios = mongoose.model('Usuarios', UsuariosSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	Usuarios : mongoose.model('Usuarios', UsuariosSchema),
	mongoose : mongoose

}