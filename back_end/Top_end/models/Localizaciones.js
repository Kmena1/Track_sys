/* 
	Este esquema guarda las posiciones de la empresa desde las oficinas y planteles y sus talleres
*/

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var LocalizacionSchema = new Schema({
	oficina:[{
		geocerca: {type:Number},
  		nombre : { type: String},
		lat		  : { type: String, required : false  },
		lon		  : { type: String, required : false  }
		}],
	planteles:[{
		geocerca: {type:Number},
		nombre	  :	{ type: String},
		lat		  : { type: String, required : false  },
		lon		  : { type: String, required : false  },
		}],
	talleres:[{
		geocerca: {type:Number},
		nombre : { type: String},
		lat		  : { type: String, required : false  },
		lon		  : { type: String, required : false  }
	}]
});

var Localizacion = mongoose.model('Localizacion', LocalizacionSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	Localizacion : mongoose.model('Localizacion', LocalizacionSchema),
	mongoose : mongoose

}