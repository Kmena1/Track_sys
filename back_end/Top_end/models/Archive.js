//modelo para crear los criterios de Archaving

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var ArchiveSchema = new Schema({
    rule		 :{type:String,required: true},
    property	 :{type:String,required: true},
    tolerance	 :{type:Number,required: true},
    module		 :{type:Number,default:0},
    reference: {
        ref1:{type: Number, default: null},
        ref2:{type: Number,default:null}
    },
    operator     :{type:String, default:'ge'},
    master       :{type:Boolean, default:false}
});

var Archive = mongoose.model('Archive', ArchiveSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	Archive : mongoose.model('Archive', ArchiveSchema),
	mongoose : mongoose

}