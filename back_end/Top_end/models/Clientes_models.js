/*
	este esquema guarda los datos de los clientes de una empresa
    incluyendo el nombre y la posicion
*/

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var clientesSchema = new Schema({
    codigo: {type: Number, required: false, default:null},
	nombre: { type: String, required : false, default: null  },
    cedula: {type:Number, required: false, default:null},
    Tdoc: {type:Number, required: false, default:0},
    CodigoRuta:{type:Number, required:false, default: null  },
    lat:{ type: Number, required : false, default: null  },
    lon:{ type: Number, required : false, default: null  },
    Provincia:{type:Number, required:false, default: null  },
    Canton:{type:Number, required:false, default: null  },
    Distrito:{type:Number, required:false, default: null  },
    direccion:{ type: String, required : false, default: null  },
    Actividad:{type: String, required:false, default:null},
    Razon:{type: String, required:false, default:null},
    //0: small, 1: med, 2: grande, 3 indefinidad
    size:{type:Number, required:false, default:0},
    //0: indefinido, 1: distribuidor 2. supermercado, 3. farmacia, 4. Macrobiotica, 5. personal, 6. veterinaria
    tipo:{ type: Number, required : false, default: 0  },
    correoFact:{ type: String, required : false, default: null  },
    correoCortesia:{ type: String, required : false, default: null  },
    puntos:[{
        correo:{type:String,required:false,default:null},
        dependiente:{type:String,required:false,default:null},
        cantidad:{type:Number, required:false, default:0}
    }],
    Representante:{
        nombre:{ type: String, required : false, default: null  },
        ced:{ type: Number, required : false, default: null  },
        cargo:{ type: String, required : false, default: null  },
        email:{ type: String, required : false, default: null  },
        tel:{ type: Number, required : false, default: null  },
        celular:{ type: Number, required : false, default: null  },
        direccion:{ type: String, required : false, default: null  },
    },
    contactos:[
        {
            nombre:{ type: String, required : false, default: null  },
            cargo:{ type: String, required : false, default: null  },
            email:{ type: String, required : false, default: null  },
            tel:{ type: Number, required : false, default: null  },
            celular:{ type: Number, required : false, default: null  },
        }
    ],
    pago:{
        contado:{type:Boolean, require: false, default:true},
        dias:{type: Number, required : false, default: null  },
        transporte:{type: String, required : false, default: null  },
    },
    referencias:[{
        nombre:{type:String},
        contacto:{type:String},
        telfono1:{type:Number},
        telfono2:{type:Number},
        plazo:{type:Number},
        Monto:{type:Number},
        aprovado:{type:Boolean},
        aprovadoBy:{type:String}
    }],
    credito:{
        limite:{type:Number, require:false, default:0},
        deuda:{type:Number, require:false, default:0}
    },
    region:{ type: String, required : false, default: null  },
    regionId:{ type: String, required : false, default: null  },
    //lista de productos
    productos:[{
        producto:{ type: String, required : false, default: null  },
        cantidad:{ type: Number, required : false, default: null  },
        optimo:{ type: Number, required : false, default: null  },
    }],
    facturas:{ type: String, required : false, default: null  },
    visita:{type:Date, required:false, default:null},
    archving:{ type: String, required : false, default: null  },
    agentes:[],
    archivos:{
        Arc1:{type:String,require:false,default:null},
        Arc2:{type:String,require:false,default:null},
        Arc3:{type:String,require:false,default:null},
        Arc1ref:{type:String,require:false,default:null},
        Arc2ref:{type:String,require:false,default:null},
        Arc3ref:{type:String,require:false,default:null},
    }
});

var myclientSchema = new Schema({
    empresa: { type: String, required : false, default: null  },
    id: [{
        zona:{ type: String, required : false, default: null  },
        region:{ type: String, required : false, default: null  },
        codigo:{ type: Number, required :false, default: null},
        clientes:[Schema.Types.Mixed]
    }],
    zonas:[{
        region:{ type: String, required : false, default: null  },
        codigo:{ type: Number, required :false, default: null},
    }]
})

var facturasSchema=new Schema({
    numero:{type:String,required:true},
    monto:{type:Number,required:false,default:0},
    cancelada:{type:Boolean,required:false,default:false},
    pedido:{type:String,required:true},
    before:{type:String,default:null},
    fecha:{type:Date, default:Date.now}
})

var clientes = mongoose.model('clientes', clientesSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	clientes : mongoose.model('clientes', clientesSchema),
    myclient: mongoose.model('mycliente', myclientSchema),
    facturas: mongoose.model('facturas', facturasSchema),
	mongoose : mongoose

}