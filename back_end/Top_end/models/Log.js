/*
	este esquema guarda los datos de los clientes de una empresa
    incluyendo el nombre y la posicion
*/

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

var logSchema=new Schema({
    numero:{type:String,required:true},
    tipo:{type:String,required:false,default:null},
    info:{type:String,required:false,default:null},
    fecha:{type:Date,require:false,default:Date.now()},
    before:{type:String,required:false,default:null}
})

//var clientes = mongoose.model('clientes', clientesSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	//clientes : mongoose.model('clientes', clientesSchema),
    //myclient: mongoose.model('mycliente', myclientSchema),
    log: mongoose.model('log', logSchema),
	mongoose : mongoose

}