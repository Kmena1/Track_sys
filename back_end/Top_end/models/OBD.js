/**
 * este guarda los datos de cada trama
 * puede optimizarse 
 */
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var OBDSchema = new Schema({
    lat		  : { type: String, default: null  },
	lon		  : { type: String, default: null  },
    date :  { type: String, default: null  },
    time :  { type: String, default: null  },
	before	:{ type: String, default: null  },
	next	:{type: String, default: null },
    //general
    OBD:{type: String, default: null },
    RPM:{type: String, default: null },
    PosAcel:{type: String, default: null },
    Speed:{type: String, default: null },
    Data_4f:{
        'aire':{type: String, default: null },
        'voltaje':{type: String, default: null },
        'corriente':{type: String, default: null },
        'presión':{type: String, default: null }
    },
    //aceleracion
    PosAceB:{type: String, default: null },
    PosAceC:{type: String, default: null },
    PedalD:{type: String, default: null },
    PedalE:{type: String, default: null },
    PedalF:{type: String, default: null },
    pedalPos:{type: String, default: null },
    comadAcc:{type: String, default: null },
    TurboRPM:{type: String, default: null },
    TurboT:{type: String, default: null },
    //Gas
    FuelLevel:{type: String, default: null },
    OilT:{type: String, default: null },
    FuelSpeed:{type: String, default: null },
    gas_type:{type: String, default: null },
    Ethanol:{type: String, default: null },
    syncIny:{type: String, default: null },
    ALT1:{type: String, default: null },
    ALS1:{type: String, default: null },
    ALT2:{type: String, default: null },
    ALS2:{type: String, default: null },
    PresionFuel:{type: String, default: null },
    PresionAdmision:{type: String, default: null },
    fuelPresion:{type: String, default: null },
    fuelMesPresion:{type: String, default: null },
    fuelAir:{type: String, default: null },
    DiselIn:{type: String, default: null },
    DiselControl:{type: String, default: null },
    fuelPresion:{type: String, default: null },
    InyPresion:{type: String, default: null },
    TurboPresion:{type: String, default: null },
    risePresion:{type: String, default: null },
        //voltaje
    BattLife:{type: String, default: null },
    ControlVolt:{type: String, default: null },
    Charg:{type: String, default: null },
    
    //engine
    enginetime:{type: String, default: null },
    torqueSol:{type: String, default: null },
    torqueAct:{type: String, default: null },
    torqueRef:{type: String, default: null },
    EnginePorc:{type: String, default: null },
    EngCoolT:{type: String, default: null },
    motor_stt:{type: String, default: null },
    //sensors
    IO:{type: String, default: null },
    IOAux:{type: String, default: null },
    Presion:{type: String, default: null },
    EvapPresio:{type: String, default: null },
    EvapPresioABS:{type: String, default: null },
    ColectorPresion:{type: String, default: null },
    GateWaste:{type: String, default: null },
    EspPresion:{type: String, default: null },
        //catalizadores
    CatB1S1T:{type: String, default: null },
    CatB1S2T:{type: String, default: null },
    CatB2S1T:{type: String, default: null },
    CatB2S2T:{type: String, default: null },
    //temperatura
    breakliquidT:{type: String, default: null },
    colectorT:{type: String, default: null },
    AirInT:{type: String, default: null },
    EscGasT:{type: String, default: null },
    //air
    EmisionReq:{type: String, default: null },
    AirSecSTT:{type: String, default: null },
    AirFlux:{type: String, default: null },
    SairFlux:{type: String, default: null },
    SenOxi:{type: String, default: null },
    SenOxi4:{type: String, default: null },
    AjsSenOxiSB1B3:{type: String, default: null },
    AjsSenOxiLB1B3:{type: String, default: null },
    AjsSenOxiSB2B4:{type: String, default: null },
    AjsSenOxiLB2B4:{type: String, default: null },
    Sox1:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox2:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox3:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox4:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox5:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox6:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox7:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox8:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox1CA:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox2CA:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox3CA:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox4CA:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox5CA:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox6CA:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox7CA:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox8CA:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
    },
    Sox1Act:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox2Act:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox3Act:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox4Act:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox5Act:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox6Act:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox7Act:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    Sox8Act:{
            'voltaje':{type: String, default: null },
            'ajuste':{type: String, default: null }
        },
    //alertas
    fallasDTC:{type: String, default: null },
    engineFail:{type: String, default: null },
    timeAvc:{type: String, default: null },
    MAF:{type: String, default: null },
    DistMIL:{type: String, default: null },
    ERG:{type: String, default: null },
    ERG_ComFail:{type: String, default: null },
    ERGfail:{type: String, default: null },
    Purga:{type: String, default: null },
    heatErase:{type: String, default: null },
    DistErase:{type: String, default: null },
    monitors:{type: String, default: null },
    MilT:{type: String, default: null },
    TimeErase:{type: String, default: null },
    //???
    CACT:{type: String, default: null },
    EGT1:{type: String, default: null },
    EGT2:{type: String, default: null },
    DPF:{type: String, default: null },
    DPFT:{type: String, default: null },
    ContNox:{type: String, default: null },
    ContPM:{type: String, default: null },
    AECD:{type: String, default: null },
    SNox:{type: String, default: null },
    SistNox:{type: String, default: null },
    PM:{type: String, default: null },
    MIL:{type: String, default: null },
})

var OBD = mongoose.model('OBD', OBDSchema);

// create the model for myAgents and expose it to our app
module.exports = {
    OBD : mongoose.model('OBD', OBDSchema),
	mongoose : mongoose

}