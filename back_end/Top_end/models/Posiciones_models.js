/**
 * este guarda los datos de cada trama
 * puede optimizarse 
 */
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var PositionSchema = new Schema({
    lat		  : { type: String, default: null  },
	lon		  : { type: String, default: null  },
	to		  : { type: String, default: null  },
	tf		  : { type: String, default: null  },
	ADC		  : { type: String, default: null  },
	drive	          : { type: String, default: null  },
	t1		  : { type: String, default: null  },
	t2		  : { type: String, default: null  },
	t3		  : { type: String, default: null  },
	DID		  : { type: String, default: null  },
	DIDR		  : { type: String, default: null  },
	EmgerId	  	  : { type: String, required : false  },
	speed		  : { type: String, default: null  },
	speed2		  : { type: String, default: null  },
	course		  : { type: String, default: null  },
	vs		  : { type: String, default: null  },
	before	:{ type: String, default: null  },
	next	:{type: String, default: null },
	date	:{type: String, default: null},
	time	:{type: String, default: null},
	dist	:{type: String, default: null},
	power	:{type: String, default:null},
	battery :{type: String, default:null},
	mode :{type: String, default:null},
	io :{type: String, default:null},
	volt:{type: String, default:null},
	rpm:{type: String, default:null},
	OSR:{type: String, default:null},
	OST:{type: String, default:null},
})

var Position = mongoose.model('Position', PositionSchema);

// create the model for myAgents and expose it to our app
module.exports = {
    Position : mongoose.model('Position', PositionSchema),
	mongoose : mongoose

}