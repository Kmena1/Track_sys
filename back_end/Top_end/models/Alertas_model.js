//modelo para crear los criterios de Archaving

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var AlertSchema = new Schema({
    tipo: {type: String, require: true},
    lat:  {type: String, require: true},
    lon:  {type: String, require: true},
    time:  {type: String, require: true},
    next:  {type: String, default:null},
    before:  {type: String, default:null},
});

var alertasSchema = new Schema({
    fecha:{type: String, require: false, default: Date.now()},
    value:{type: String, require: false, default: null},
    Descripcion:{type:String, require:false},
    lat:  {type: String, require: false, default: null},
    lon:  {type: String, require: false, default: null},
    Page: {type:String, require:false},
    user: {type:String, require:false},
    next: {type:String, require:false},
    before:{type:String, require:false},
})

var Alert = mongoose.model('Alert', AlertSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	Alert : mongoose.model('Alert', AlertSchema),
    Alertas: mongoose.model('Alertas', alertasSchema),
	mongoose : mongoose

}