/*
	Hay 3 esquemas, si la orden puede salir sin problemas se anida en el esquema de salida
    Si se tiene que hacer mas producto se anida en la de produccion
    Si se tiene que congelar parte de un pedido se anida en la de Frozen
*/

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var SalidaSchema = new Schema({
	Fecha_Salida: {type: Date, require:false, default: null},
    Aprovado: {type: Boolean, default: false},
    Apr_By:{type:String,default:null},
    cliente: {type: String, require:true },
    Transporte: {type: String, require:true},
    prductos:[{
        producto:{type:String,require:true},
        cantidad:{type:Number,require:true},
        estado:{type:Number,require:false, default:0},
        Peso:{type:Number,require:false, default:0},
    }],

    consecutivo:{type: Number,default:0},
    before:{type:String,require:false,default:null},
    estado:{type:Number,default:0},
    empresa:{type:String,default:"5b7e1a4f4923f55d8fc426f2"},
    Fmodificacion:{type:Date,default:null},
    Fecha_creacion: {type: Date, require:false, default: null},
    Detalles:{type:String},
});

var ProduccionSchema = new Schema({
	Fecha_creacion: {type: Date, require:false, default: null},
    Aprovado: {type: Boolean, default: false},
    Apr_By:{type:String,default:null},
    empresa:{type:String,default:"5b7e1a4f4923f55d8fc426f2"},
    prductos:[{
        producto:{type:String,require:true},
        cantidad:{type:Number,require:true},
        prioridad:{type:Number,require:false, default:3},
        estado:{type:Number,default:0},
    }],
    encargado:{
        realizado:{type:String,default:null},
        JefeProduc:{type:String,default:null},
        JefeOpr:{type:String,default:null},
        Dips:{type:String,default:null},
        VDisp:{type:String,default:null},
        Envasa:{type:String,default:null},
        Tapado:{type:String,default:null},
        Etiquetado:{type:String,default:null},
        Loteado:{type:String,default:null},
    },
    fecha:{
        realizado:{type:Date},
        JefeProduc:{type:Date},
        JefeOpr:{type:Date},
        Dips:{type:Date},
        VDisp:{ype:Date},
        Envasa:{type:Date},
        Tapado:{type:Date},
        Etiquetado:{type:Date},
        Loteado:{type:Date},
    },
    Act_By:{type:String,default:null},
    formula:{type:String,default:null},
    numero:{type:String,require:true},
    comentario:{type:String,default:null},
    before:{type:String,require:false,default:null},
    estado:{type:Number,default:0},
    Fmodificacion:{type:Date,default:null}
});

var FrozenSchema = new Schema({
	Fecha_solicitud: {type: Date, require:true},
    Aprovado: {type: Boolean, default: false},
    cliente: {type: String, require:true },
    Transporte: {type: String, require:true},
    consecutivo:{type: Number,default:0},
    empresa:{type:String,default:"5b7e1a4f4923f55d8fc426f2"},
    prductos:[{
        producto:{type:String,require:true},
        cantidad:{type:Number,require:true}
    }],
    before:{type:String,require:false,default:null}
}); 

var Salida = mongoose.model('Salida', SalidaSchema);

// create the model for myAgents and expose it to our app
module.exports = {
	Salida : mongoose.model('Salida', SalidaSchema),
    Produccion:mongoose.model('Produccion', ProduccionSchema),
	Frozen : mongoose.model('Frozen', FrozenSchema),
	mongoose : mongoose

}