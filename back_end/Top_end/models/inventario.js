var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Tracks');
var Schema = mongoose.Schema;

//Define the schema for our myAgent model
var inventarioSchema = new Schema({
    empresa : { type: String, required : true  },
    categoria:[{
        nombre:{ type: String, required : true  },
        tipo: { type: String, required : false, default : "PT"  },
        id: { type: String, required : false, default : null  },
        last : { type: String, required : false, default : null  },
    }]
});

var objetosSchema = new Schema({
    codigo :  { type: String, required : false, default : '0000'  }, 
    nombre : { type: String, required : true  },
    precio : { type: Number, required : false, default : null  },
    cantidad : { type: Number, required : false, default : null  },
    reservados :[],
    distribuidores:[],
    lotes:[],
    receta: { type: String, required : false, default:null  },
    objeto: { type: String, required : false, default:null  },
    exentos: {type:Boolean,default:false},
    descripcion : { type: String, required : false, default : null  },
    Incatalogo: { type: Boolean, required : false, default : false  },
    catalogo: { type: String, required : false, default : null  },
    before: { type: String, required : false, default : null  },
    unidad: { type:String,required:false, default:"KG"},
    convertFactor:{type:Number,require:false,default:1},
    Dolares: { type:Boolean,required:false, default:false},
    next: { type: String, required : false, default : null  },
    contenidoNeto: { type: Number, required : false, default : 0  }, 
    puntos:{type:Number,require:false,default:1},
    trigger:{
        max: {type:Number, require:false, default:null},
        min: {type:Number, require:false, default:null}
    },
    StandBy:{ type: Number, required : false, default : 0  },
    competencia:{
        uno:{
            nombre:{ type: String, required : false, default : null  },
            contenidoNeto: { type: Number, required : false, default : 0  }, 
            precio: { type: Number, required : false, default : 0  }, 
        },
        dos:{
            nombre:{ type: String, required : false, default : null  },
            contenidoNeto: { type: Number, required : false, default : 0  }, 
            precio: { type: Number, required : false, default : 0  }, 
        },
        tres:{
            nombre:{ type: String, required : false, default : null  },
            contenidoNeto: { type: Number, required : false, default : 0  }, 
            precio: { type: Number, required : false, default : 0  }, 
        },
    }
})

var PedidoSchema = new Schema({
    reservas:[{
        objeto:{ type: String, required : false, default:null },
        cantidad:{ type: Number, required : false, default:0 },
        descuento:{ type: Number, required : false, default:0 },
        Tdescuento:{ type: Number, required : false, default:0 },
        Aprovado:{type: Boolean, default: false}
    }],
    NRecibo:{},
    notas:{ type: String, required : false, default:null },
    resposable:{ type: String, required : false, default:null },
    factura:{type: Number,default:0},
    cliente:{ type: String, required : false, default:null },
    agente:{ type: String, required : false, default:null },
    fecha:{ type: Date, required : false, default:Date.now },
    Faprovado:{ type: Date, required : false, default:null},
    aprovado:{type: Boolean,required:false, default:false},
    aprvby:{ type: String, required : false, default:null },
    contado:{type: Boolean,required:false, default:false},
    Abono:{type: Number,default:0},
    total:{type: Number,default:0},
    consecutivo:{type: Number,default:0},
    otros:{type: Number,default:0},
    mensaje:{ type: String, required : false, default:null },
    transporte:{ type: String, required : false, default:null },
    before:{ type: String, required : false, default:null },
    next:{ type: String, required : false, default:null },

})

var catalogoSchema = new Schema({
    objeto: { type: String, required : true  },
    exentos: {type:Boolean,default:false},
    descripcion : { type: String, required : false, default : null  },
    imagen : { type: String, required : false, default : null  },
})

var ProvedoresSchema = new Schema({
    empresa:{type:String, require:true},
    distribuidores:[]
})

var distribuidoresSchema = new Schema({
    nombre:{type:String, required:true},
    correo:{type:String, required:false},
    telefono:{type:String, required:false, default:null},
    pais:{type:String, required:false, default:null},
    nota:{type:String, required:false, default:null},
})

var recetasSchema = new Schema({
    nombre:{type:String, required:true},
    codigo:{type:String},
    tipo:{type:Boolean, default:true},
    ingredientes:[{
        objeto:{type:String},
        cantidad:{type:Number},
        orden:{type:Number,require:false,default:0},
    }],
    envase:[{
        asignado:{type:String, required:true},
        tapa:{
            objeto:{type:String},
            cantidad:{type:Number},
        },
        envase:{
            objeto:{type:String},
            cantidad:{type:Number},
        },
        etiqueta:[{ 
            objeto:{type:String},
            cantidad:{type:Number},
        }],
    }],
    before:{type:String,require:false,default:null},
    next:{type:String,require:false,default:null},
    Tanda:[{
        cantidad:{type:Number,require:false,default:null},
        workforce: [{
            nombre:{type:String,require:false,default:null},
            cantidad:{type:Number,require:false,default:null},
            costo:{type:Number,require:false,default:null},
        }],
        expenses: {
            Electricidad:{type:Number,require:false,default:2},
            Agua:{type:Number,require:false,default:2},
            Alquiler:{type:Number,require:false,default:2},
        },
    }],
    workforce: [{
        nombre:{type:String,require:false,default:null},
        cantidad:{type:Number,require:false,default:null},
        costo:{type:Number,require:false,default:null},
    }],
    expenses: {
        Electricidad:{type:Number,require:false,default:2},
        Agua:{type:Number,require:false,default:2},
        Alquiler:{type:Number,require:false,default:2},
    }, 
})

var lotesSchema = new Schema({
    factura:{type:String, required: false, default:null},
    Numero:{type:String, required: false, default:null},
    Proveedor:{type:String, required: false, default:null},
    FProduccion:{type:Date, required: false, default: null},
    FVencimiento:{type:Date, required: false, default: null},
    FIngreso:{type:Date, required: false, default: null},
    Cas:{type:String, required: false, default:null}, 
    cantidad:{type:Number, require: true},
    qr:[]
})

var inventario = mongoose.model('inventario', inventarioSchema);
var objetos = mongoose.model('objetos', objetosSchema);
//var reservas = mongoose.model('reservas', reservasSchema);
var catalogo = mongoose.model('catalogo', catalogoSchema);
var pedido = mongoose.model('pedido', PedidoSchema)

// create the model for myAgents and expose it to our app
module.exports = {
    inventario : mongoose.model('inventario', inventarioSchema),
    objetos : mongoose.model('objetos', objetosSchema),
    lotes: mongoose.model('lotes', lotesSchema),
    //reservas : mongoose.model('reservas', reservasSchema),
    catalogo : mongoose.model('catalogo', catalogoSchema),
    pedido : mongoose.model('pedido', PedidoSchema),
    Provedores : mongoose.model('Provedores', ProvedoresSchema),
    distribuidores : mongoose.model('distribuidores', distribuidoresSchema),
    recetas : mongoose.model('recetas', recetasSchema),
	mongoose : mongoose

}