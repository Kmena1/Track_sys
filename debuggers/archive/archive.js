app.controller("archive",["$scope","$http","$cookies",function($scope,$http,$cookies){ 
    $scope.rules=["moduled_tolerance","simple_tolerance","fixed_tolerance","time","radio"];
    $scope.propertys=["lat","lon","speed","dist","battery"];
    $scope.operators=["ge","le","l","g","e","ne"];
    $scope.rule="simple_tolerance";
    $scope.operator="ge";
    $scope.modulo=0;
    $scope.master=false;

    $scope.check =function(){
        if($scope.rule=="time"){
            $scope.propertys=["Y","M","D","H","m","s"];
        }
        else{
            $scope.propertys=["lat","lon","speed","dist","battery"];
        }
    }

    $scope.set=function(){
        console.log($scope.rule,$scope.property,$scope.operator,$scope.master);
        if($scope.diviceId != undefined && $scope.operator!=undefined && $scope.tolerancia!=undefined  && $scope.property!=undefined){
            if($scope.ref==undefined)var reference=null
            else reference=$scope.ref
            if($scope.rule=="radio"){
                var property="radius";
            }
            else{
                var property=$scope.property
            }
            var data={
                deviceId:$scope.diviceId,
                settings:{
                    rule		 :$scope.rule,
                    property	 :property,
                    tolerance	 :$scope.tolerancia,
                    module		 :$scope.modulo,
                    reference    :{
                        ref1:reference,
                    },
                    operator     :$scope.operator,
                    master       :$scope.master
                }
            };
            $http({
                method:'POST',
                url:'http://34.237.241.0:2500/divice/set/achieve',
                data:data
            }).
            then(function(response){
                console.log(response.data);
                })
        }
    }
}])
